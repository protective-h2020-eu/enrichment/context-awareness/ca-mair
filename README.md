PROTECTIVE  Context Awareness (CA) - Mission and Asset Information Repository (MAIR)
========== 

This version of `ca-mair` is installed with `ca-fusioninventory` on an individual machine that supports docker. The two products are distributed as docker images, each stored in different docker registry locations on GitLab. A MongoDB image, located in DockerHub, is also installed on the same machine.

Prerequisites
-------------

Prerequisites are that the machine (or a VM) has git and docker installed. The Docker Engine release must be 18.06.0 or better. Machines monitored by the MAIR must have a FusionInventory Agent installed (see below).

Install
-------

To install the products:
- create an empty folder, e.g. `ca-mair`, on the machine to host the products.
- copy the `docker-compose.yml` file to the newly created folder.
- open a terminal or command prompt
- `cd` into the newly created folder.
- run `docker-compose pull` to download images that make up the product to the local machine. (only run when images change)

Run
---

To run ca:

```
docker-compose up -d
```
To stop ca:

```
docker-compose down
```

The `ca-mair` REST API can be accessed through port `8080` of the machine.

Agents
------

A **FusionInventory Agent** must be installed on each machine monitored by `ca-mair`/`ca-fusioninventory`. 

These will funnel inventory data from their machines through `ca-fusioninventory` and onto `ca-mair`, which persists the data in MongoDB.

For Windows Machines:
- an installer can be downloaded from `http://fusioninventory.org/documentation/agent/installation/windows`

For Linux Machines:
- Linux installers developed by PROTECTIVE can be found at `https://gitlab.com/protective-h2020-eu/enrichment/context-awareness/fusioninventory-agent-installers.git`

The `agent.cfg` file in each of the installers' `zip` file must be modified to point to the docker host's DNS name and port (port `8089` for `http` or `8449` for `https`) of the `ca-fusioninventory` docker container. e.g. 

```
server = https://ca.protective.sri.net:8449/api/upload
```

In future robust security will be applied to all CA APIs with https, SSL, etc.

Building this image
--------

Note: The image build machine must have Java 8 installed. 

In order to build this image (if the source code has changed)
- clone this repository
- checkout the appropriate branch, only the master branch is being delivered!
- on Linux/macOS run `./gradlew buildDocker` in a terminal
- or on Windows run `gradlew.bat buildDocker` at the command prompt

In order to push this image to the GitLab registry (for use in installs)
- on Linux/macOS run `./gradlew pushDocker` in a terminal
- or on Windows run `gradlew.bat pushDocker` at the command prompt

API Reference
---

Read the API Reference [here](./ca-mair-api.html). If it shows raw html then you will need to display the html in a browser.

Read the JSON Model Reference [here](./ca-mair-model.html). If it shows raw html then you will need to display the html in a browser.

The reference can also be found by pointing your web browser at the address and port of a running instance of the `ca-mair` application and accessing `/docs/api` or `/docs/model`