grammar cluster;

@header{
package eu.h2020.protective.ca.mair.antlr.cluster.insight;
}

cluster
	:	'{' relations ',' resources '}'
	;
	
////////

annotations
	:	'"annotations"' ':' '{' annotation (',' annotation)* '}'
	;
	
metrics
	:	'"metrics"' ':' '{' metric (',' metric)* '}'
	;
	
////////

names
    :   '"names"' ':' '[' name (',' name)* ']'
    ;

relations
    :   '"relations"' ':' '[' relation (',' relation)* ']'
    ;

resources
//  :   '"resources"' ':' '[' resource (',' resource)* ']'
    :   '"resources"' ':' array
    ;
    
volumeMounts
    :   '"volumeMounts"' ':' '[' volumeMount (',' volumeMount)* ']'
    ;

array
	:	'[' (~']' | array)* ']'
	;
	
//object
//	:	'{' (~'}' | object)* '}'
//	;
	
////////

lastState
	:	'"lastState"' ':' '{' terminated | running '}'
	;
	
metadata
	:	'"metadata"' ':' '{' '"name"' ':' STRING '}'
	;
	
running
	:	'"running"' '{' startedAt '}'
	;

spec_resources
    :	'"resources"' '{' '}' // empty object
    ;

spec
    :	'"spec"' '{' image ',' imagePullPolicy ',' spec_resources ',' terminationMessagePath ',' 
    		terminationMessagePolicy ',' volumeMounts '}'
    ;

state
	:	'"state"' ':' '{' terminated | running '}'
	;
	
status
	:	'"status"' '{' containerID ',' image ',' imageID ',' lastState ',' ready ',' restartCount ',' state '}'
	;

terminated
	:	'"terminated"' '{' containerID ',' exitCode ',' finishedAt ',' reason ',' startedAt '}'
	;

////////

gcm
	:	'{' container_name ',' hostname ',' pod_id '}'
	;
	
relation
	:	'{' annotations ',' source ',' target ',' timestamp ',' type '}'
	;
	
resource
	:	'{' annotations ',' id ',' properties ',' timestamp ',' type '}'
	;
	
volumeMount
	:	'{' mountPath ',' mount_name ',' readOnly '}'
	;
	
////////

properties
	:	'{' (metadata)? (',' spec)? (',' status)? '}'
	;
	
////////

annotation
	:	label | metrics
	;
	
metric
	:	gcm | labels_prefix | names | project
	;
	
////////

container_name
	:	'"container_name"' ':' STRING
	;
	
containerID
	:	'"containerID"' ':' STRING
	;
	
hostname
	:	'"hostname"' ':' STRING
	;
	
exitCode
	:	'"exitCode"' ':' NUMBER
	;
	
finishedAt
	:	'"finishedAt"' ':' STRING
	;
	
id
	:	'"id"' ':' STRING
	;
	
image
	:	'"image"' ':' STRING
	;
	
imageID
	:	'"imageID"' ':' STRING
	;
	
imagePullPolicy
	:	'"imagePullPolicy"' ':' STRING
	;
	
label
	:	'"label"' ':' STRING
	;
	
labels_prefix
	:	'"labels_prefix"' ':' STRING
	;
	
mount_name
	:	'"name"' ':' STRING
	;
	
mountPath
	:	'"mountPath"' ':' STRING
	;
	
name
	:	STRING
	;
	
pod_id
	:	'"pod_id"' ':' STRING
	;
	
project
	:	'"project"' ':' STRING
	;
	
readOnly
	:	'"readOnly"' ':' BOOLEAN
	;
	
ready
	:	'"ready"' ':' BOOLEAN
	;
	
reason
	:	'"reason"' ':' STRING
	;
	
restartCount
	:	'"restartCount"' ':' NUMBER
	;
	
source
	:	'"source"' ':' STRING
	;
	
startedAt
	:	'"startedAt"' ':' STRING
	;
	
target
	:	'"target"' ':' STRING
	;
	
terminationMessagePath
	:	'"terminationMessagePath"' ':' STRING
	;
	
terminationMessagePolicy
	:	'"terminationMessagePolicy"' ':' STRING
	;
	
timestamp
	:	'"timestamp"' ':' STRING
	;
	
type
	:	'"type"' ':' STRING
	;
	
//////// Lexer ////////////////////

STRING 
	:	'"' (ESC | ~["\\])* '"'
		{ setText(getText().substring(1, getText().length() - 1)); }
	;
fragment ESC :   '\\' (["\\/bfnrt] | UNICODE) ;
fragment UNICODE : 'u' HEX HEX HEX HEX ;
fragment HEX : [0-9a-fA-F] ;

NUMBER
    :   '-'? INT '.' [0-9]+ EXP? // 1.35, 1.35E-9, 0.3, -4.5
    |   '-'? INT EXP             // 1e10 -3e4
    |   '-'? INT                 // -3, 45
    ;
fragment INT :   '0' | [1-9] [0-9]* ; // no leading zeros
fragment EXP :   [Ee] [+\-]? INT ; // \- since - means "range" inside [...]

BOOLEAN
	:	'true' | 'false'
	;
	
NULL
	:	'null'
	;
	
WS  
	:	[ \t\n\r]+ -> skip
	;

LINE_COMMENT
    : 	'//' ~[\r\n]* -> skip
    ;
