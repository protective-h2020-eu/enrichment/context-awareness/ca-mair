
grammar cdg;

@header{
package eu.h2020.protective.ca.mair.antlr.original;
}

mair_context: 'context' '=' ID deps | deps;
deps: (cdg_deps)*;
cdg_deps: 'cdg' '=' ID '{' layer+ '}' ;

layer: mission_layer | operational_layer | application_layer | infrastructure_layer;

mission_layer: (mission_dep)+  ;
operational_layer: (bp_dep)+  (ia_dep)* | (ia_dep)+ ;
application_layer: (its_dep)+  ;
infrastructure_layer: (sw_dep)+  (net_dep)* | (net_dep)+ ;

inter_layer_deps: 'ild' '=' '{' inter_layer_dep+ '}';
inter_layer_dep: '(' ID ',' ID ',' WEIGHT (',' ID)? ')';

//dependency: mission_dep | bp_dep | ia_dep | its_dep | sw_dep | net_dep ;
mission_dep: '<' 'm' '=' ID ',' (vweight)? (bdeps)?  (iadeps)? (itsdeps)? inter_layer_deps? agg_func? '>';
bp_dep: '<' 'bp' '=' ID ',' (vweight)? (bdeps)?  (iadeps)? (itsdeps)? (netdeps)? inter_layer_deps? agg_func?'>';
ia_dep: '<' 'ia' '=' ID ',' (vweight)? (iadeps)? (itsdeps)? inter_layer_deps? agg_func? '>';
its_dep:'<' 'its' '=' ID ',' (vweight)? (itsdeps)? (sw_deps)? inter_layer_deps? agg_func? '>';
sw_dep: '<' 'sw' '=' ID ',' (vweight)? 'node' '=' ID (',')? (sw_deps)? inter_layer_deps? agg_func? '>';
net_dep: '<' 'node' '=' ID ',' (vweight)? (netdeps)? inter_layer_deps? agg_func? '>';

mdeps: 'mdep' '=' dep_list;
bdeps: 'bpdep' '=' dep_list;
iadeps: 'iadep' '=' dep_list;
itsdeps: 'itsdep' '=' dep_list;
sw_deps: 'swdep' '=' dep_list;
netdeps: 'netdep' '=' dep_list;

dep_list: '{' dep_nodes (dep_set)* '}';
dep_nodes: dep_node* ;  //(dep_node)*;
dep_set: '[' dep_nodes  agg_func ']';
dep_node: '('ID ',' WEIGHT (',' ID)? ')';
agg_func:  'agfunc' '=' ID;
vweight: 'Vweight' '=' WEIGHT ',';

/////////Lexer /////////////////////

WEIGHT: 'none' | 'low' | 'medium' | 'high' | 'critical';

RELOP: '==' | '<=' | '>=' | '!=' | '<' | '>' ;
EQ: '=';
OPSELECT:'->' ;

LITERAL: (STRINGLITERAL) | (INTLITERAL) | (REALITERAL) ;

STRINGLITERAL: '"'ID'"';
INTLITERAL: DIGIT+;
REALITERAL: DIGIT+'.'DIGIT+;

ID: IDENT | COMPOSED_ID ;
IDENT: (LETTER) (LETTER|DIGIT|'_'| '-')* ;
fragment COMPOSED_ID: IDENT ('.' IDENT)* ;
STRINGS: (LETTER|DIGIT|OTHERCHAR)+ ;
OTHERCHAR:  '%' |'*'|'$'|'&'|'@'|'?'|'"'|'.' ;
//ID : [a-zA-Z] [a-zA-Z0-9]* ;

LETTER:[a-zA-Z];
DIGIT:[0-9];

FCL: (LETTER | DIGIT | ';' | ':=' | ':' |','|'('|')'| '//');

WS:[ \t\r\n]+ -> skip; // skip spaces, tabs, newlines ;

SL_COMMENT:   '//' .*? '\n' -> skip ;
