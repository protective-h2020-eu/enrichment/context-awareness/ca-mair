
grammar cdgs;

@header{
package eu.h2020.protective.ca.mair.antlr.json;
}

mair_context
	:	'{' '"context"' ':' ID ',' '"cdgs"' ':' deps '}'
	|	deps
	;
	
deps
    :   '[' cdg_deps (',' cdg_deps)* ']'
    |   '[' ']' // empty array
    ;

cdg_deps
	: 	'{' '"cdg"' ':' '{' '"name"' ':' ID ',' (layer) '}' '}' // TODO make as array of cdgs?
	;

layer
	:	mission_layer | operational_layer | application_layer | infrastructure_layer
	;

mission_layer
	:	m_list (',' inter_layer_list)?
	;
	
operational_layer
	:	bp_list (',' ia_list)? (',' inter_layer_list)?
	|	ia_list (',' bp_list)? (',' inter_layer_list)?
	;
	
application_layer
	:	its_list (',' inter_layer_list)?
	;
	
infrastructure_layer
	:	net_list (',' sw_list)? (',' inter_layer_list)?
	|	sw_list (',' net_list)? (',' inter_layer_list)?
	;

// TODO What about BusinessUnit and SecurityObjective?

m_list
	:	'"m"' ':' '[' m_dep (',' m_dep)* ']'
	;

bp_list
	:	'"bp"' ':' '[' bp_dep (',' bp_dep)* ']'
	;

ia_list
	:	'"ia"' ':' '[' ia_dep (',' ia_dep)* ']'
	;

its_list
	:	'"its"' ':' '[' its_dep (',' its_dep)* ']'
	;

sw_list
	:	'"sw"' ':' '[' sw_dep (',' sw_dep)* ']'
	;

net_list
	:	'"node"' ':' '[' net_dep (',' net_dep)* ']'
	;

inter_layer_list
	:	'"ild"' ':' '[' inter_layer_dep (',' inter_layer_dep)* ']'
	;

inter_layer_dep
	:	'{' '"fromVertexId"' ':' ID ',' '"toVertexId"' ':' ID ',' '"toCdg"' ':' ID (',' '"weight"' ':' ID)? '}'
	;

m_dep
	: 	'{' name (',' mdeps)? '}'
	;
	
bp_dep
	: 	'{' name (',' bpdeps)? (',' iadeps)? '}'
	| 	'{' name (',' iadeps)? (',' bpdeps)? '}'
	;

ia_dep
	: 	'{' name (',' iadeps)? '}'
	;

its_dep
	: 	'{' name (',' itsdeps)? '}'
	;

sw_dep
	: 	'{' name (',' swdeps)? (',' netdeps)? '}'
	| 	'{' name (',' netdeps)? (',' swdeps)? '}'
	;

net_dep
	: 	'{' name (',' netdeps)? '}'
	;


mdeps: '"mdep"' ':' dep_list ;
bpdeps: '"bpdep"' ':' dep_list ;
iadeps: '"iadep"' ':' dep_list ;
itsdeps: '"itsdep"' ':' dep_list ;
swdeps: '"swdep"' ':' dep_list ;
netdeps: '"netdep"' ':' dep_list ;

dep_list
    :   '[' name_and_grade (',' name_and_grade)* ']'
    ;
    
name_and_grade
	:	'{' '"id"' ':' ID ',' '"weight"' ':' ID  '}'
	;

name
	:	'"id"' ':' ID (',' weight)? (',' expression)?
	|	'"id"' ':' ID (',' expression)? (',' weight)?
	;

weight
	:	'"weight"' ':' ID
	;
	
expression
	:	'"expression"' ':' (ID | STRING)
	;
	

/////////Lexer /////////////////////


//////// CDGs part of Lexer ////

//CLASS
//   : '"' (LETTER) (LETTER | DIGIT | '_')* '"'
//     { setText(getText().substring(1, getText().length() - 1)); }
//   ;   

ID
   : '"' (LETTER | DIGIT) (LETTER | DIGIT | '_' | '-' | '.' | ':' | ' ')* '"'
     { setText(getText().substring(1, getText().length() - 1)); }
   ;
   
LETTER
   : [a-zA-Z]
   ;
DIGIT
   : [0-9]
   ;


//////// JSON extension to CDGs part of Lexer ////

STRING
   : '"' (ESC | SAFECODEPOINT)* '"'
     { setText(getText().substring(1, getText().length() - 1)); }
   ;


fragment ESC
   : '\\' (["\\/bfnrt] | UNICODE)
   ;
fragment UNICODE
   : 'u' HEX HEX HEX HEX
   ;
fragment HEX
   : [0-9a-fA-F]
   ;
fragment SAFECODEPOINT
   : ~ ["\\\u0000-\u001F]
   ;


NUMBER
   : '-'? INT ('.' [0-9] +)? EXP?
   ;


fragment INT
   : '0' | [1-9] [0-9]*
   ;

// no leading zeros

fragment EXP
   : [Ee] [+\-]? INT
   ;

// \- since - means "range" inside [...]

WS
   : [ \t\n\r] + -> skip
   ;


//////// Line Comment extension to JSONs part of Lexer ////

LINE_COMMENT
   : '//' ~[\r\n]* -> skip
   ;
