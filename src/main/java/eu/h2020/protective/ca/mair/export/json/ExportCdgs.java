package eu.h2020.protective.ca.mair.export.json;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

import eu.h2020.protective.ca.mair.MongoDB;
import eu.h2020.protective.ca.mair.api.model.core.Edge;
import eu.h2020.protective.ca.mair.api.model.core.Vertex;
import eu.h2020.protective.ca.mair.api.model2.assets.Information;
import eu.h2020.protective.ca.mair.api.model2.assets.Software;
import eu.h2020.protective.ca.mair.api.model2.nodes.Node;
import eu.h2020.protective.ca.mair.api.model2.other.MissionObjective;
import eu.h2020.protective.ca.mair.api.model2.services.BusinessProcess;
import eu.h2020.protective.ca.mair.api.model2.services.ITService;

// FIXME What about BusinessUnit, SecurityObjective

public class ExportCdgs {

	private JsonNodeFactory factory;

	private Map<String, ObjectNode> verticesInJson;
	private Map<String, ObjectNode> cdgsInJson;
	
	public static String asJson(String context, MongoDB mongoDB) throws IOException {
		return new ExportCdgs().exportAsJson(context, mongoDB);
	}

	private String exportAsJson(String context, MongoDB mongoDB) throws IOException {
		StringWriter writer = new StringWriter();

		// Create the node factory that gives us nodes.
		factory = new JsonNodeFactory(false);

		// create a json factory to write the treenode as json. for the example
		// we just write to console
		JsonFactory jsonFactory = new JsonFactory();
		JsonGenerator generator = jsonFactory.createGenerator(writer);
		ObjectMapper mapper = new ObjectMapper();

		ArrayNode cdgArrayNode = factory.arrayNode();
		cdgsInJson = new HashMap<>();
		verticesInJson = new HashMap<>();

		for (String cdgName : mongoDB.getAllCdgNames(context)) {
			if (cdgName.equals(Edge.ALL_EDGES_CDG)) {
				continue;
			}
			ObjectNode cdgWrapperObjectNode = factory.objectNode();
			ObjectNode cdgObjectNode = cdgWrapperObjectNode.putObject("cdg");
			cdgObjectNode.put("name", cdgName);
			cdgsInJson.put(cdgName, cdgObjectNode);
			cdgArrayNode.add(cdgWrapperObjectNode);
		}

		for (String cdgName : cdgsInJson.keySet()) {
			if (cdgName.equals(Edge.ALL_EDGES_CDG)) {
				continue;
			}
			for (Edge edge : mongoDB.findAllEdges(context, cdgName)) {
				Vertex from = mongoDB.findVertexByKey(context, edge.getFrom());
				Vertex to = mongoDB.findVertexByKey(context, edge.getTo());
				processFrom(edge, from, to);
				processTo(edge, to);
			}
			for (Edge edge : mongoDB.findAllEdges(context, cdgName)) {
				addOrUpdateIldArray(cdgName, edge);
			}
		}

		mapper.writeTree(generator.useDefaultPrettyPrinter(), cdgArrayNode);

		return writer.toString();
	}

	private void processFrom(Edge edge, Vertex from, Vertex to) {
		String cdgName = edge.getFromCdg();
		if (from instanceof MissionObjective) {
			ObjectNode objectNode = addOrUpdateDepObject(cdgName, "m", from);
			if (to instanceof MissionObjective && !edge.isInterLayerDependency()) {
				addOrUpdateDepArray(objectNode, "mdep", to, edge.getGrade());
			}
		} else if (from instanceof BusinessProcess) {
			ObjectNode objectNode = addOrUpdateDepObject(cdgName, "bp", from);
			if (to instanceof BusinessProcess && !edge.isInterLayerDependency()) {
				addOrUpdateDepArray(objectNode, "bpdep", to, edge.getGrade());
			} else if (to instanceof Information && !edge.isInterLayerDependency()) {
				addOrUpdateDepArray(objectNode, "iadep", to, edge.getGrade());
			}
		} else if (from instanceof Information) {
			ObjectNode objectNode = addOrUpdateDepObject(cdgName, "ia", from);
			if (to instanceof Information && !edge.isInterLayerDependency()) {
				addOrUpdateDepArray(objectNode, "iadep", to, edge.getGrade());
			}
		} else if (from instanceof ITService) {
			ObjectNode objectNode = addOrUpdateDepObject(cdgName, "its", from);
			if (to instanceof ITService && !edge.isInterLayerDependency()) {
				addOrUpdateDepArray(objectNode, "itsdep", to, edge.getGrade());
			}
		} else if (from instanceof Software) {
			ObjectNode objectNode = addOrUpdateDepObject(cdgName, "sw", from);
			if (to instanceof Software && !edge.isInterLayerDependency()) {
				addOrUpdateDepArray(objectNode, "swdep", to, edge.getGrade());
			} else if (to instanceof Node && !edge.isInterLayerDependency()) {
				addOrUpdateDepArray(objectNode, "netdep", to, edge.getGrade());
			}
		} else if (from instanceof Node) {
			ObjectNode objectNode = addOrUpdateDepObject(cdgName, "node", from);
			if (to instanceof Node && !edge.isInterLayerDependency()) {
				addOrUpdateDepArray(objectNode, "netdep", to, edge.getGrade());
			}
		}
	}

	private void processTo(Edge edge, Vertex to) {
		String cdgName = edge.getToCdg();
		if (to instanceof MissionObjective) {
			addOrUpdateDepObject(cdgName, "m", to);
		} else if (to instanceof BusinessProcess) {
			addOrUpdateDepObject(cdgName, "bp", to);
		} else if (to instanceof Information) {
			addOrUpdateDepObject(cdgName, "ia", to);
		} else if (to instanceof ITService) {
			addOrUpdateDepObject(cdgName, "its", to);
		} else if (to instanceof Node) {
			addOrUpdateDepObject(cdgName, "node", to);
		} else if (to instanceof Software) {
			addOrUpdateDepObject(cdgName, "sw", to);
		}
	}

	private ObjectNode addOrUpdateDepObject(String cdgName, String arrayNodeName, Vertex vertex) {
		ObjectNode cdgObjectNode = cdgsInJson.get(cdgName);
		ArrayNode arrayNode = (ArrayNode) cdgObjectNode.get(arrayNodeName);
		if (arrayNode == null) {
			arrayNode = cdgObjectNode.putArray(arrayNodeName);
		}
		ObjectNode objectNode = verticesInJson.get(vertex.getKey());
		if (objectNode == null) {
			objectNode = factory.objectNode();
			objectNode.put("id", vertex.getKey());
			if (vertex.getWeight() != 0.0d) {
				objectNode.put("weight", getGradeFromWeight(vertex.getWeight()));
			}
			if (!vertex.getExpression().equals(Vertex.OR)) {
				objectNode.put("expression", vertex.getExpression());
			}
			arrayNode.add(objectNode);
			verticesInJson.put(vertex.getKey(), objectNode);
		}
		return objectNode; // Never null.
	}

	private String getGradeFromWeight(Double weight) {
		switch (weight.intValue()) {
		case 0:
			return "none";
		case 1:
			return "low";
		case 2:
			return "medium";
		case 3:
			return "high";
		case 4:
			return "critical";
		default:
			return null;
		}
	}
	private void addOrUpdateDepArray(ObjectNode depObjectNode, String depArrayName,
			Vertex toVertex, String grade) {
		ArrayNode depArray = (ArrayNode) depObjectNode.get(depArrayName);
		if (depArray == null) {
			depArray = depObjectNode.putArray(depArrayName);
		}
		ObjectNode nameAndGradeObjectNode = factory.objectNode();
		nameAndGradeObjectNode.put("id", toVertex.getKey());
		nameAndGradeObjectNode.put("weight", grade);
		depArray.add(nameAndGradeObjectNode);
	}

	private void addOrUpdateIldArray(String cdgName, Edge edge) {
		if (edge.isInterLayerDependency()) {
			final String ILD = "ild";
			ObjectNode cdgObjectNode = cdgsInJson.get(cdgName);
			ArrayNode ildArrayNode = (ArrayNode) cdgObjectNode.get(ILD);
			if (ildArrayNode == null) {
				ildArrayNode = cdgObjectNode.putArray(ILD);
			}
			ObjectNode ildObjectNode = factory.objectNode();
			ildObjectNode.put("fromVertexId", edge.getFrom());
			ildObjectNode.put("toVertexId", edge.getTo());
			ildObjectNode.put("toCdg", edge.getToCdg());
			ildObjectNode.put("weight", edge.getGrade());
			ildArrayNode.add(ildObjectNode);
		}
	}

}
