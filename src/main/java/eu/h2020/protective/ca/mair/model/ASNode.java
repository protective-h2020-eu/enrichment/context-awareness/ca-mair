package eu.h2020.protective.ca.mair.model;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

import eu.h2020.protective.ca.mair.api.constraints.ApiDescription;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = false)
@ApiModel(value = "ASNode")
public class ASNode {

	@NotBlank
	@ApiDescription("A globally unique ID independent of the DB")
	@ApiModelProperty(required = true, value = "A globally unique ID independent of the DB")
	@JsonProperty(value = "id")
	private String key;

	@ApiModelProperty(required = true, value = "List of Operating Systems")
	private List<ASSoftware> operatingSystems = new ArrayList<>();

	@ApiModelProperty(required = true, value = "List of Software")
	private List<ASSoftware> software = new ArrayList<>();

	public ASNode(String key) {
		this.key = key;
	}

}
