package eu.h2020.protective.ca.mair;

import org.springframework.beans.factory.annotation.Autowired;

import eu.h2020.protective.ca.mair.api.model.assets.Asset;
import eu.h2020.protective.ca.mair.api.model.cam.Attack;
import eu.h2020.protective.ca.mair.api.model.cam.HardwarePlatform;
import eu.h2020.protective.ca.mair.api.model.cam.ImpactFactor;
import eu.h2020.protective.ca.mair.api.model.cam.OperationalCapacity;
import eu.h2020.protective.ca.mair.api.model.core.Edge;
import eu.h2020.protective.ca.mair.api.model.core.Vertex;
import eu.h2020.protective.ca.mair.api.model.idg.And;
import eu.h2020.protective.ca.mair.api.model.idg.Or;
import eu.h2020.protective.ca.mair.api.model.idg.Task;
import eu.h2020.protective.ca.mair.api.model.other.Mission;
import eu.h2020.protective.ca.mair.api.model.other.Vulnerability;
import eu.h2020.protective.ca.mair.api.model.services.Service;
import eu.h2020.protective.ca.mair.model.Metadata;

@org.springframework.stereotype.Service
public class MongoDBInit {

	@Autowired
	private MongoDB mongoDB;
/*	
	private Edge addReversedEdge(Edge edge) {
		return mongoDB.insertOrUpdateOne(edge.changeDirection());
	}
*/
	public void initImpactDependentGraph() throws Exception {
		initImpactDependentGraph(null);
	}

	public void initImpactDependentGraph(int min, int max) throws Exception {
		for (int i = min; i <= max; i++) {
			initImpactDependentGraph(i);
		}
	}
	
	private Edge insertOrUpdateOne(Vertex from, Vertex to) {
		Edge e = new Edge(from, to);
		return mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, e.getSource(), e.getTarget(), e);
	}

	private Edge insertOrUpdateOne(String key, Vertex from, Vertex to) {
		Edge e = new Edge(key, from, to);
		return mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, e.getSource(), e.getTarget(), e);
	}

	private void initImpactDependentGraph(Integer i) throws Exception {
		String suffix = (i == null ? "" : "-" + i);

		Vertex mission1 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Mission("Mission1" + suffix));
		Vertex mission2 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Mission("Mission2" + suffix));

		Vertex t1 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Task("T1" + suffix));
		Vertex t2 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Task("T2" + suffix));
		Vertex t3 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Task("T3" + suffix));
		Vertex t4 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Task("T4" + suffix));
		Vertex t5 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Task("T5" + suffix));
		Vertex t6 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Task("T6" + suffix));
		Vertex t7 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Task("T7" + suffix));

		Vertex s1 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Service("S1" + suffix));
		Vertex s2 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Service("S2" + suffix));
		Vertex s3 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Service("S3" + suffix));
		Vertex s4 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Service("S4" + suffix));
		Vertex s5 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Service("S5" + suffix));
		Vertex s6 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Service("S6" + suffix));
		Vertex s7 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Service("S7" + suffix));

		Vertex a1 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Asset("A1" + suffix));
		Vertex a2 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Asset("A2" + suffix));
		Vertex a3 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Asset("A3" + suffix));
		Vertex a4 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Asset("A4" + suffix));
		Vertex a5 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Asset("A5" + suffix));

		Vertex orA2A4 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Or("orA2A4" + suffix));
		Vertex andA3A5 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new And("andA3A5" + suffix));
		Vertex orS2S4 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Or("orS2S4" + suffix));
		Vertex andT1T2 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new And("andT1T2" + suffix));
		Vertex andT3T4 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new And("andT3T4" + suffix));
		Vertex orT5T6T7 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Or("orT5T6T7" + suffix));

		insertOrUpdateOne(a2, a1);
		insertOrUpdateOne(a2, s2);
		insertOrUpdateOne(a2, orA2A4);

		insertOrUpdateOne(a4, orA2A4);
		insertOrUpdateOne(a4, a3);
		insertOrUpdateOne(a4, a5);

		insertOrUpdateOne(a1, s1);

		insertOrUpdateOne(orA2A4, s4);
		insertOrUpdateOne(orA2A4, s5);

		insertOrUpdateOne(a3, s6);
		insertOrUpdateOne(a3, andA3A5);

		insertOrUpdateOne(a5, andA3A5);

		insertOrUpdateOne(andA3A5, s7);

		insertOrUpdateOne(s1, t1);
		insertOrUpdateOne(s2, orS2S4);
		insertOrUpdateOne(s4, orS2S4);
		insertOrUpdateOne(orS2S4, t2);
		insertOrUpdateOne(s5, s3);
		insertOrUpdateOne(s3, t3);
		insertOrUpdateOne(s3, t4);
		insertOrUpdateOne(s6, t6);
		insertOrUpdateOne(s7, t7);

		insertOrUpdateOne(t1, andT1T2);
		insertOrUpdateOne(t2, andT1T2);
		insertOrUpdateOne(andT1T2, mission1);

		insertOrUpdateOne(t3, andT3T4);
		insertOrUpdateOne(t4, andT3T4);
		insertOrUpdateOne(andT3T4, t5);

		insertOrUpdateOne(t5, orT5T6T7);
		insertOrUpdateOne(t6, orT5T6T7);
		insertOrUpdateOne(t7, orT5T6T7);
		insertOrUpdateOne(orT5T6T7, mission2);
	}

	public void initImpactDependentGraphWithAttack() throws Exception {
		initImpactDependentGraph();
		Vertex asset = mongoDB.findVertexByKey(Metadata.DEFAULT_CONTEXT, "A2");
		if (asset != null) {
			initCyberAttackModel(asset);
		}
	}

	public void initCyberAttackModel(Vertex asset) throws Exception {
		Vertex attack1 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Attack("Attack1"));
		Vertex hp1 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new HardwarePlatform("HardwarePlatform1"));
		Vertex asset1 = asset;
		Vertex vuln1 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Vulnerability("Vulnerability1"));
		Vertex impactFactor1 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new ImpactFactor("ImpactFactor1", 0.7));
		Vertex oc1 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new OperationalCapacity("OperationalCapacity1", 0.3));

		insertOrUpdateOne("Targets", attack1, hp1);
		insertOrUpdateOne("Houses", hp1, asset1);
		insertOrUpdateOne("Impacts", attack1, asset1);
		insertOrUpdateOne("Exploits", attack1, vuln1);
		insertOrUpdateOne("HasVulnerability", asset1, vuln1);
		insertOrUpdateOne("FromAttack", attack1, impactFactor1);
		insertOrUpdateOne("FromIF", impactFactor1, oc1);
		insertOrUpdateOne("FromOC", oc1, asset1);
	}
/*
	public void initTopDownGraph() throws Exception {
		Vertex mission1 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Mission("Mission1"));
		Vertex mission2 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Mission("Mission2"));

		Vertex t1 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Task("T1"));
		Vertex t2 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Task("T2"));
		Vertex t3 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Task("T3"));
		Vertex t4 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Task("T4"));
		Vertex t5 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Task("T5"));
		Vertex t6 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Task("T6"));
		Vertex t7 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Task("T7"));

		Vertex s1 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Service("S1"));
		Vertex s2 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Service("S2"));
		Vertex s3 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Service("S3"));
		Vertex s4 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Service("S4"));
		Vertex s5 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Service("S5"));
		Vertex s6 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Service("S6"));
		Vertex s7 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Service("S7"));

		Vertex a1 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Asset("A1"));
		Vertex a2 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Asset("A2"));
		Vertex a3 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Asset("A3"));
		Vertex a4 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Asset("A4"));
		Vertex a5 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Asset("A5"));

		Vertex orA2A4 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Or());
		Vertex andA3A5 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new And());
		Vertex orS2S4 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Or());
		Vertex andT1T2 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new And());
		Vertex andT3T4 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new And());
		Vertex orT5T6T7 = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, new Or());

		addReversedEdge(new Edge(a2, a1));
		addReversedEdge(new Edge(a2, s2));
		addReversedEdge(new Edge(a2, orA2A4));

		addReversedEdge(new Edge(a4, orA2A4));
		addReversedEdge(new Edge(a4, a3));
		addReversedEdge(new Edge(a4, a5));

		addReversedEdge(new Edge(a1, s1));

		addReversedEdge(new Edge(orA2A4, s4));
		addReversedEdge(new Edge(orA2A4, s5));

		addReversedEdge(new Edge(a3, s6));
		addReversedEdge(new Edge(a3, andA3A5));

		addReversedEdge(new Edge(a5, andA3A5));

		addReversedEdge(new Edge(andA3A5, s7));

		addReversedEdge(new Edge(s1, t1));
		addReversedEdge(new Edge(s2, orS2S4));
		addReversedEdge(new Edge(s4, orS2S4));
		addReversedEdge(new Edge(orS2S4, t2));
		addReversedEdge(new Edge(s5, s3));
		addReversedEdge(new Edge(s3, t3));
		addReversedEdge(new Edge(s3, t4));
		addReversedEdge(new Edge(s6, t6));
		addReversedEdge(new Edge(s7, t7));

		addReversedEdge(new Edge(t1, andT1T2));
		addReversedEdge(new Edge(t2, andT1T2));
		addReversedEdge(new Edge(andT1T2, mission1));

		addReversedEdge(new Edge(t3, andT3T4));
		addReversedEdge(new Edge(t4, andT3T4));
		addReversedEdge(new Edge(andT3T4, t5));

		addReversedEdge(new Edge(t5, orT5T6T7));
		addReversedEdge(new Edge(t6, orT5T6T7));
		addReversedEdge(new Edge(t7, orT5T6T7));
		addReversedEdge(new Edge(orT5T6T7, mission2));
	}
*/
}
