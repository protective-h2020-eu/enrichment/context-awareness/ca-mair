package eu.h2020.protective.ca.mair.antlr.cluster.insight;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.misc.Pair;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeProperty;

import eu.h2020.protective.ca.mair.MongoDB;
import eu.h2020.protective.ca.mair.antlr.errors.CdgErrorListener;
import eu.h2020.protective.ca.mair.antlr.errors.VisitorErrorInterface;
import eu.h2020.protective.ca.mair.antlr.json.CdgSupport;
import eu.h2020.protective.ca.mair.api.model.core.Edge;
import eu.h2020.protective.ca.mair.api.model.core.Vertex;
import eu.h2020.protective.ca.mair.api.model2.assets.Software;
import eu.h2020.protective.ca.mair.api.model2.groups.Cluster;
import eu.h2020.protective.ca.mair.api.model2.groups.Network;
import eu.h2020.protective.ca.mair.api.model2.nodes.Container;
import eu.h2020.protective.ca.mair.api.model2.nodes.Node;
import eu.h2020.protective.ca.mair.api.model2.services.Service;
import lombok.Getter;

public class ClusterPass1Visitor extends clusterBaseVisitor<Object> implements VisitorErrorInterface {

	@Getter
	private CdgErrorListener errorListener;

	@Getter
	private CdgSupport cdgSupport;

	private ParseTreeProperty<Vertex> fromVertexProps = new ParseTreeProperty<>();
	private ParseTreeProperty<Vertex> toVertexProps = new ParseTreeProperty<>();
	private ParseTreeProperty<Boolean> inRelationsProps = new ParseTreeProperty<>();
	private ParseTreeProperty<Boolean> inResourcesProps = new ParseTreeProperty<>();

	private void setFromVertex(ParseTree node, Vertex value) {
		fromVertexProps.put(node, value);
	}

	private Vertex getFromVertex(ParseTree node) {
		return fromVertexProps.get(node);
	}

	private void setToVertex(ParseTree node, Vertex value) {
		toVertexProps.put(node, value);
	}

	private Vertex getToVertex(ParseTree node) {
		return toVertexProps.get(node);
	}

	private void setInRelations(ParseTree node, Boolean value) {
		inRelationsProps.put(node, value);
	}

	private Boolean isInRelations(ParseTree node) {
		return inRelationsProps.get(node);
	}

	private void setInResources(ParseTree node, Boolean value) {
		inResourcesProps.put(node, value);
	}

	private Boolean isPreviousInRelations(RuleContext ctx) {
		Boolean value = isInRelations(ctx);
		while (value == null) {
			ctx = ctx.getParent();
			if (ctx == null) {
				return null;
			}
			value = isInRelations(ctx);
		}
		return value;
	}

	private Vertex getChildFromVertex(RuleContext ctx) {
		for (int i = 0; i < ctx.getChildCount(); i++) {
			ParseTree childCtx = ctx.getChild(i);
			if (childCtx != null) {
				Vertex value = getFromVertex(childCtx);
				if (value != null) {
					return value;
				}
			}
		}
		return null;
	}

	private Vertex getChildToVertex(RuleContext ctx) {
		for (int i = 0; i < ctx.getChildCount(); i++) {
			ParseTree childCtx = ctx.getChild(i);
			if (childCtx != null) {
				Vertex value = getToVertex(childCtx);
				if (value != null) {
					return value;
				}
			}
		}
		return null;
	}

	public ClusterPass1Visitor(Map<String, Vertex> vertices, Map<String, Map<Pair<String, String>, Edge>> edges,
			String originalFilename, File file, boolean guessEdgeType, Map<String, Double> missionWeights,
			Map<String, Double> vertexWeights, Map<String, Double> edgeWeights) throws IOException {
		setup(vertices, edges, null, originalFilename, file, true, missionWeights, vertexWeights, edgeWeights);
	}

	public ClusterPass1Visitor(MongoDB mongoDB, String originalFilename, File file, boolean guessEdgeType,
			Map<String, Double> missionWeights, Map<String, Double> vertexWeights, Map<String, Double> edgeWeights)
			throws IOException {
		setup(null, null, mongoDB, originalFilename, file, true, missionWeights, vertexWeights, edgeWeights);
	}

	private void setup(Map<String, Vertex> vertices, Map<String, Map<Pair<String, String>, Edge>> edges,
			MongoDB mongoDB, String originalFilename, File file, boolean guessEdgeType,
			Map<String, Double> missionWeights, Map<String, Double> vertexWeights, Map<String, Double> edgeWeights)
			throws IOException {
		CharStream input = CharStreams.fromFileName(file.getAbsolutePath());
		clusterLexer lexer = new clusterLexer(input); // specific to this class
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		clusterParser parser = new clusterParser(tokens); // specific to this class

		errorListener = new CdgErrorListener();

		cdgSupport = new CdgSupport(vertices, edges, mongoDB, originalFilename, file, true, missionWeights,
				vertexWeights, edgeWeights, errorListener, tokens, parser);

		cdgSupport.setParseTree(parser.cluster()); // specific to this class

		visit(cdgSupport.getParseTree());
	}

	private String getVertexTypeFromKey(String key) {
		return key.substring(0, key.indexOf(":"));
	}

	private Class<? extends Vertex> getVertexClassFromKey(String key) {
		return getVertexClassFromType(getVertexTypeFromKey(key));
	}

	private Class<? extends Vertex> getVertexClassFromType(String type) {
		switch (type) {
		case "Cluster":
			return Cluster.class;
		case "Container":
			return Container.class;
		case "Image":
			return Software.class; // FIXME new type?
		case "Node":
			return Node.class; // FIXME NetworkNode?
		case "Pod":
			return Network.class; // FIXME new type?
		case "ReplicationController":
			return Node.class; // FIXME new type?
		case "Service":
			return Service.class;
		default:
			return Vertex.class;
		}
	}

//	private Class<? extends Edge> getEdgeClassFromType(String type) {
//		switch (type) {
//		case "contains":
//			return Contains.class;
//		case "createdFrom":
//			return Edge.class; // FIXME new type?
//		case "runs":
//			return Edge.class; // FIXME new type?
//		case "monitors":
//			return Edge.class; // FIXME new type?
//		case "loadBalances":
//			return Edge.class; // FIXME new type?
//		default:
//			return Edge.class; // FIXME Set name to type!
//		}
//	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitCluster(clusterParser.ClusterContext ctx) {
		if (!getErrors().isEmpty()) {
			return null;
		}
		cdgSupport.initEdgeCollection("missionLayer");
		cdgSupport.initEdgeCollection("operationalLayer");
		cdgSupport.initEdgeCollection("applicationLayer");
		cdgSupport.initEdgeCollection("infrastructureLayer");
		return visitChildren(ctx);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitRelations(clusterParser.RelationsContext ctx) {
		setInRelations(ctx, true);
		setInResources(ctx, false);
		return visitChildren(ctx);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitResources(clusterParser.ResourcesContext ctx) {
		setInRelations(ctx, false);
		setInResources(ctx, true);
		return visitChildren(ctx);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitRelation(clusterParser.RelationContext ctx) {
		Object result = visitChildren(ctx);
		Vertex from = getChildFromVertex(ctx);
		Vertex to = getChildToVertex(ctx);
		// String type = getChildEdgeType(ctx);
		String fromCdg = cdgSupport.calculateCdg(from);
		String toCdg = cdgSupport.calculateCdg(to);
		String grade = "none";
		cdgSupport.setCdgName(ctx, fromCdg);
		cdgSupport.addOrUpdateEdge(toCdg, ctx, from, to, grade);
		return result;
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitSource(clusterParser.SourceContext ctx) {
		if (isPreviousInRelations(ctx)) {
			Token keyToken = ctx.STRING().getSymbol();
			String key = keyToken.getText();
			Class<? extends Vertex> clazz = getVertexClassFromKey(key);
			Vertex vertex = cdgSupport.addOrUpdateFromVertex(ctx, clazz, key, null, null);
			if (vertex == null) {
				errorListener.error(ctx.STRING().getSymbol(), CdgSupport.VERTEX_MISSING_PATTERN, key,
						cdgSupport.getOriginalFilename());
			} else {
				setFromVertex(ctx, vertex);
			}
		}
		return visitChildren(ctx);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitTarget(clusterParser.TargetContext ctx) {
		if (isPreviousInRelations(ctx)) {
			Token keyToken = ctx.STRING().getSymbol();
			String key = keyToken.getText();
			Class<? extends Vertex> clazz = getVertexClassFromKey(key);
			Vertex vertex = cdgSupport.addOrUpdateToVertex(clazz, keyToken);
			if (vertex == null) {
				errorListener.error(ctx.STRING().getSymbol(), CdgSupport.VERTEX_MISSING_PATTERN, key,
						cdgSupport.getOriginalFilename());
			} else {
				setToVertex(ctx, vertex);
			}
		}
		return visitChildren(ctx);
	}

}
