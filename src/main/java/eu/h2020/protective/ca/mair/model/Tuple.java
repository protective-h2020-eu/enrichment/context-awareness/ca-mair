package eu.h2020.protective.ca.mair.model;

import lombok.Data;

@Data
public class Tuple<A, B> {

    public final A a;
    public final B b;

}