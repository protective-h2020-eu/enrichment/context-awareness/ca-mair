package eu.h2020.protective.ca.mair.matrices;

import java.io.File;
import java.lang.reflect.Constructor;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import eu.h2020.protective.ca.mair.MongoDB;
import eu.h2020.protective.ca.mair.api.model.core.Edge;
import eu.h2020.protective.ca.mair.api.model.core.Vertex;
import eu.h2020.protective.ca.mair.model.MatrixElement;
import lombok.extern.slf4j.XSlf4j;

@Service
@XSlf4j
public class Matrices {

	@Autowired
	private MongoDB mongoDB;

	public void addWeightingMatrices(String context, File file) throws Exception {
		initVerticesForMatrices(context, file);
		initEdgesAndMatrices(context, file);
		if (!file.delete()) {
			log.warn("Could not delete file {}", file.getAbsoluteFile());
		}
	}

	private void initVerticesForMatrices(String context, File csvFile) throws Exception {
		CsvMapper mapper = new CsvMapper();
		CsvSchema schema = CsvSchema.emptySchema().withHeader(); // use first row as header; otherwise defaults are fine
		MappingIterator<Map<String, String>> it = mapper.readerFor(Map.class).with(schema).readValues(csvFile);
		while (it.hasNext()) {
			Map<String, String> rowAsMap = it.next();
			// access by column name, as defined in the header row...
			String type = rowAsMap.get("type");
			String keyX = rowAsMap.get("key");
			createVertex(context, keyX, type);
		}
	}

	private void initEdgesAndMatrices(String context, File csvFile) throws Exception {
		CsvMapper mapper = new CsvMapper();
		CsvSchema schema = CsvSchema.emptySchema().withHeader(); // use first row as header; otherwise defaults are fine
		MappingIterator<Map<String, String>> it = mapper.readerFor(Map.class).with(schema).readValues(csvFile);
		while (it.hasNext()) {
			Map<String, String> rowAsMap = it.next();
			// access by column name, as defined in the header row...
			String keyX = rowAsMap.get("key");
			for (Map.Entry<String, String> entry : rowAsMap.entrySet()) {
				Double value = null;
				try {
					value = Double.parseDouble(entry.getValue());
				} catch (Exception e) {
				}
				if (value != null) {
					String keyY = entry.getKey();
					createEdge(context, keyX, keyY, value);
				}
			}
		}
	}

	private void createVertex(String context, String key, String type) throws Exception {
		Class<?> clazz = Class.forName(type);
		Constructor<?> constructor = clazz.getConstructor(String.class);
		Vertex vertex = (Vertex) constructor.newInstance(key);
		mongoDB.insertOrUpdateOne(context, vertex);
	}

	private void createEdge(String context, String keyX, String keyY, Double value) throws Exception {
		Vertex vertexX = mongoDB.findVertexByKey(context, keyX);
		Vertex vertexY = mongoDB.findVertexByKey(context, keyY);
		if (vertexX != null && vertexY != null) {
			Edge edge = new Edge(vertexY, vertexX, value); // XXX from bottom Y to top X
			mongoDB.insertOrUpdateOne(context, vertexY, vertexX, edge);
			mongoDB.insertOrUpdateOne(new MatrixElement(keyX, keyY, value));
		}
	}

}
