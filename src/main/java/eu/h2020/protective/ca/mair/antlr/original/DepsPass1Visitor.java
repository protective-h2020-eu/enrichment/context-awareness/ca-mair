package eu.h2020.protective.ca.mair.antlr.original;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.misc.Pair;

import eu.h2020.protective.ca.mair.MongoDB;
import eu.h2020.protective.ca.mair.antlr.errors.CdgErrorListener;
import eu.h2020.protective.ca.mair.antlr.errors.VisitorErrorInterface;
import eu.h2020.protective.ca.mair.antlr.json.CdgSupport;
import eu.h2020.protective.ca.mair.api.model.core.Edge;
import eu.h2020.protective.ca.mair.api.model.core.Vertex;
import eu.h2020.protective.ca.mair.api.model2.assets.Information;
import eu.h2020.protective.ca.mair.api.model2.assets.Software;
import eu.h2020.protective.ca.mair.api.model2.nodes.Node;
import eu.h2020.protective.ca.mair.api.model2.other.MissionObjective;
import eu.h2020.protective.ca.mair.api.model2.services.BusinessProcess;
import eu.h2020.protective.ca.mair.api.model2.services.ITService;
import lombok.Getter;

public class DepsPass1Visitor extends cdgBaseVisitor<Object> implements VisitorErrorInterface {

	@Getter
	private final CdgErrorListener errorListener;

	@Getter
	private final CdgSupport cdgSupport;

	@Getter
	private final DepsPass2Visitor depsPass2Visitor;

	public DepsPass1Visitor(Map<String, Vertex> vertices, Map<String, Map<Pair<String, String>, Edge>> edges,
			String originalFilename, File file, boolean guessEdgeType, Map<String, Double> missionWeights,
			Map<String, Double> vertexWeights, Map<String, Double> edgeWeights) throws IOException {
		this(vertices, edges, null, originalFilename, file, true, missionWeights, vertexWeights, edgeWeights);
	}

	public DepsPass1Visitor(MongoDB mongoDB, String originalFilename, File file, boolean guessEdgeType,
			Map<String, Double> missionWeights, Map<String, Double> vertexWeights, Map<String, Double> edgeWeights)
			throws IOException {
		this(null, null, mongoDB, originalFilename, file, true, missionWeights, vertexWeights, edgeWeights);
	}

	private DepsPass1Visitor(Map<String, Vertex> vertices, Map<String, Map<Pair<String, String>, Edge>> edges,
			MongoDB mongoDB, String originalFilename, File file, boolean guessEdgeType,
			Map<String, Double> missionWeights, Map<String, Double> vertexWeights, Map<String, Double> edgeWeights)
			throws IOException {
		CharStream input = CharStreams.fromFileName(file.getAbsolutePath());
		cdgLexer lexer = new cdgLexer(input); // specific to this class
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		cdgParser parser = new cdgParser(tokens); // specific to this class

		errorListener = new CdgErrorListener();

		cdgSupport = new CdgSupport(vertices, edges, mongoDB, originalFilename, file, true, missionWeights,
				vertexWeights, edgeWeights, errorListener, tokens, parser);

		cdgSupport.setParseTree(parser.mair_context()); // specific to this class

		visit(cdgSupport.getParseTree());

		depsPass2Visitor = new DepsPass2Visitor(this);
	}

	@Override
	public List<String> getErrors() {
		List<String> allErrors = new ArrayList<>();
		allErrors.addAll(this.getErrorListener().getErrors());
		allErrors.addAll(depsPass2Visitor.getErrorListener().getErrors());
		return allErrors;
	}
	
	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitMair_context(cdgParser.Mair_contextContext ctx) {
		if (getErrorListener().hasSyntaxError()) {
			return null;
		}
		if (ctx.ID() != null) {
			Token contextToken = ctx.ID().getSymbol();
			if (contextToken != null) {
				String context = contextToken.getText();
				cdgSupport.setContext(context);
			}
		}
		return visitChildren(ctx);
	}


	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitCdg_deps(cdgParser.Cdg_depsContext ctx) {
		String cdgName = ctx.ID().getSymbol().getText();
		cdgSupport.initEdgeCollection(cdgName); // prepare for second pass
		cdgSupport.setCdgName(ctx, cdgName);
		return visitChildren(ctx);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitMission_dep(cdgParser.Mission_depContext ctx) {
		Token key = ctx.ID().getSymbol();
		cdgSupport.addOrUpdateFromVertex(ctx, MissionObjective.class, key.getText(), null, null);
		return visitChildren(ctx);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitBp_dep(cdgParser.Bp_depContext ctx) {
		Token key = ctx.ID().getSymbol();
		cdgSupport.addOrUpdateFromVertex(ctx, BusinessProcess.class, key.getText(), null, null);
		return visitChildren(ctx);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitIa_dep(cdgParser.Ia_depContext ctx) {
		Token key = ctx.ID().getSymbol();
		cdgSupport.addOrUpdateFromVertex(ctx, Information.class, key.getText(), null, null);
		return visitChildren(ctx);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitIts_dep(cdgParser.Its_depContext ctx) {
		Token key = ctx.ID().getSymbol();
		cdgSupport.addOrUpdateFromVertex(ctx, ITService.class, key.getText(), null, null);
		return visitChildren(ctx);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitSw_dep(cdgParser.Sw_depContext ctx) {
		Token key0 = ctx.ID(0).getSymbol();
		Token key1 = ctx.ID(1).getSymbol();
		Vertex from = cdgSupport.addOrUpdateFromVertex(ctx, Software.class, key0.getText(), null, null);
		Vertex to = cdgSupport.addOrUpdateToVertex(Node.class, key1);
		String toCdg = cdgSupport.calculateCdg(to); // cdgSupport.getPreviousCdgName(ctx);
		cdgSupport.addOrUpdateEdge(toCdg, ctx, from, to, "critical");
		return visitChildren(ctx);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitNet_dep(cdgParser.Net_depContext ctx) {
		Token key = ctx.ID().getSymbol();
		cdgSupport.addOrUpdateFromVertex(ctx, Node.class, key.getText(), null, null);
		return visitChildren(ctx);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitBdeps(cdgParser.BdepsContext ctx) {
		cdgSupport.setClass(ctx, BusinessProcess.class);
		return visitChildren(ctx);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitIadeps(cdgParser.IadepsContext ctx) {
		cdgSupport.setClass(ctx, Information.class);
		return visitChildren(ctx);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitItsdeps(cdgParser.ItsdepsContext ctx) {
		cdgSupport.setClass(ctx, ITService.class);
		return visitChildren(ctx);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitSw_deps(cdgParser.Sw_depsContext ctx) {
		cdgSupport.setClass(ctx, Software.class);
		return visitChildren(ctx);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitNetdeps(cdgParser.NetdepsContext ctx) {
		cdgSupport.setClass(ctx, Node.class);
		return visitChildren(ctx);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitDep_node(cdgParser.Dep_nodeContext ctx) {
		Token key = ctx.ID(0).getSymbol();
		Token grade = ctx.WEIGHT().getSymbol();
		String edgeClass = null;
		if (ctx.ID().size() > 1) {
			edgeClass = ctx.ID(1).getSymbol().getText();
		}
		Class<? extends Vertex> vertexClass = cdgSupport.getPreviousClass(ctx);
		Vertex from = cdgSupport.getPreviousVertex(ctx);
		Vertex to = cdgSupport.addOrUpdateToVertex(vertexClass, key);
		String toCdg = cdgSupport.calculateCdg(to); // cdgSupport.getPreviousCdgName(ctx);
		if (to != null) {
			cdgSupport.addOrUpdateEdge(toCdg, ctx, from, to, edgeClass, grade.getText());
		}
		return visitChildren(ctx);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitVweight(cdgParser.VweightContext ctx) {
		Token grade = ctx.WEIGHT().getSymbol();
		Vertex v = cdgSupport.getPreviousVertex(ctx);
		cdgSupport.addOrUpdateFromVertex(ctx, v.getClass(), v.getKey(), grade, null);
		return visitChildren(ctx);
	}

}
