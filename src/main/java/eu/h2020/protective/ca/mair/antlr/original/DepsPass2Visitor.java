package eu.h2020.protective.ca.mair.antlr.original;

import java.io.IOException;

import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeProperty;

import eu.h2020.protective.ca.mair.antlr.errors.CdgErrorListener;
import eu.h2020.protective.ca.mair.antlr.errors.VisitorErrorInterface;
import eu.h2020.protective.ca.mair.antlr.json.CdgSupport;
import eu.h2020.protective.ca.mair.api.model.core.Edge;
import eu.h2020.protective.ca.mair.api.model.core.Vertex;
import lombok.Getter;

public class DepsPass2Visitor extends cdgBaseVisitor<Object> implements VisitorErrorInterface {

	@Getter
	private final CdgErrorListener errorListener;

	@Getter
	private final CdgSupport cdgSupport;

	private ParseTreeProperty<String> fromVertexKeyProps = new ParseTreeProperty<>();

	private void setFromVertexKey(ParseTree node, String value) {
		fromVertexKeyProps.put(node, value);
	}

	private String getFromVertexKey(ParseTree node) {
		return fromVertexKeyProps.get(node);
	}

	private String getPreviousFromVertexKey(RuleContext ctx) {
		String value = getFromVertexKey(ctx);
		while (value == null) {
			ctx = ctx.getParent();
			if (ctx == null) {
				return null;
			}
			value = getFromVertexKey(ctx);
		}
		return value;
	}

	public DepsPass2Visitor(DepsPass1Visitor pass1) throws IOException {
		errorListener = pass1.getErrorListener();
		cdgSupport = pass1.getCdgSupport();
		visit(cdgSupport.getParseTree());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitCdg_deps(cdgParser.Cdg_depsContext ctx) {
		// TODO Drop this check? Will need to handle possible null Tokens.
		if (getErrorListener().hasSyntaxError()) {
			return null;
		}
		String cdgName = ctx.ID().getSymbol().getText();
		cdgSupport.initEdgeCollection(cdgName); // prepare for second pass
		cdgSupport.setCdgName(ctx, cdgName);
		return visitChildren(ctx);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitInter_layer_dep(cdgParser.Inter_layer_depContext ctx) {
		String toCdgName = ctx.ID(0).getSymbol().getText();
		String toKey = ctx.ID(1).getSymbol().getText();

		String fromKey = getPreviousFromVertexKey(ctx);
		Vertex fromVertex = cdgSupport.getVertex(fromKey);
		Vertex toVertex = cdgSupport.getVertex(toKey);

		if (fromVertex == null) {
			getErrorListener().error(ctx.ID(0).getSymbol(), CdgSupport.VERTEX_MISSING_PATTERN, fromKey,
					cdgSupport.getOriginalFilename());
		}
		if (toVertex == null) {
			getErrorListener().error(ctx.ID(1).getSymbol(), CdgSupport.VERTEX_MISSING_PATTERN, toKey,
					cdgSupport.getOriginalFilename());
		}
		
		Token gradeToken = ctx.WEIGHT().getSymbol();
		String grade = gradeToken.getText();
		if (!Edge.isValidGrade(grade)) {
			getErrorListener().error(gradeToken, "\"{}\" is not one of {}.", grade, Edge.getAllowedGrades());
		}

		String edgeClass = null;
		if (ctx.ID().size() > 2) {
			Token edgeClassToken = ctx.ID(2).getSymbol();
			edgeClass = edgeClassToken.getText();
		}

		if (fromVertex != null && toVertex != null) {
			cdgSupport.addOrUpdateEdge(toCdgName, ctx, fromVertex, toVertex, edgeClass, grade);
		}

		return visitChildren(ctx);
	}
	
	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitMission_dep(cdgParser.Mission_depContext ctx) {
		Token key = ctx.ID().getSymbol();
		setFromVertexKey(ctx, key.getText());
		return visitChildren(ctx);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitBp_dep(cdgParser.Bp_depContext ctx) {
		Token key = ctx.ID().getSymbol();
		setFromVertexKey(ctx, key.getText());
		return visitChildren(ctx);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitIa_dep(cdgParser.Ia_depContext ctx) {
		Token key = ctx.ID().getSymbol();
		setFromVertexKey(ctx, key.getText());
		return visitChildren(ctx);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitIts_dep(cdgParser.Its_depContext ctx) {
		Token key = ctx.ID().getSymbol();
		setFromVertexKey(ctx, key.getText());
		return visitChildren(ctx);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitSw_dep(cdgParser.Sw_depContext ctx) {
		Token key = ctx.ID(0).getSymbol();
		setFromVertexKey(ctx, key.getText());
		return visitChildren(ctx);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitNet_dep(cdgParser.Net_depContext ctx) {
		Token key = ctx.ID().getSymbol();
		setFromVertexKey(ctx, key.getText());
		return visitChildren(ctx);
	}

}
