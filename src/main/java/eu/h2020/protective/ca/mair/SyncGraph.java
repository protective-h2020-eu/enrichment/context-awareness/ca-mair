package eu.h2020.protective.ca.mair;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.h2020.protective.ca.mair.api.model.core.Edge;
import eu.h2020.protective.ca.mair.api.model.core.Graph;
import eu.h2020.protective.ca.mair.api.model.core.Vertex;
import eu.h2020.protective.ca.mair.api.model2.nodes.Node;
import eu.h2020.protective.ca.mair.model.Metadata;
import lombok.extern.slf4j.XSlf4j;

@Service
@XSlf4j
public class SyncGraph {

	@Autowired
	private MongoDB mongoDB;

	@Autowired
	private CaAsClient caAsClient;

	public List<String> uploadVertices(File file) {
		return uploadGraph(file, true);
	}

	public List<String> uploadGraph(File file) {
		return uploadGraph(file, false);
	}

	private List<String> uploadGraph(File file, boolean removeEdges) {
		List<String> errors = new ArrayList<>();
		try {
			ObjectMapper mapper = new ObjectMapper();
			// mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			Graph graph = mapper.readValue(file, Graph.class);
			if (removeEdges) {
				graph.clearEdges();
			}
			graph.setContext(Metadata.DEFAULT_CONTEXT);
			addOrUpdateGraph(graph);
		} catch (Throwable e) {
			log.warn("Error uploading Graph", e);
			errors.add(e.getLocalizedMessage());
		}
		return errors;
	}

	public void uploadGraph(InputStream inputStream) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		Graph graph = null;
		graph = mapper.readValue(inputStream, Graph.class);
		addOrUpdateGraph(graph);
	}

	public Graph getGraph(String context) {
		// Assumes all vertices belong to one or more edges, i.e. no isolated vertices.
		Graph graph = new Graph();
		graph.setContext(context);
		for (Edge edge : mongoDB.findAllEdges(context)) {
			graph.addVertex(edge.getSource());
			graph.addVertex(edge.getTarget());
			graph.addEdge(edge.getSource(), edge.getTarget(), edge);
		}
		return graph;
	}

	public void addOrUpdateGraph(Graph graph) {
		addOrUpdateGraph(graph, null, null);
	}

	public void addOrUpdateGraph(Graph graph, Boolean updateCvss) {
		addOrUpdateGraph(graph, null, null, updateCvss);
	}

	public void addOrUpdateGraph(Graph graph, String fromCdg, String toCdg) {
		String context = graph.getContext();
//		log.info("Last update time in graph in addOrUpdateGraph: " + Graph.lastLocalCpeUpdateTime);
		Map<ObjectId, ObjectId> idMap = new HashMap<>();
		for (Vertex vertex : graph.getVertices()) {
			saveVertex(context, idMap, vertex);
		}
		for (Edge edge : graph.getEdges()) {
			if (fromCdg != null) {
				edge.setFromCdg(fromCdg);
			}
			if (toCdg != null) {
				edge.setToCdg(toCdg);
			}
			// JSON passed by REST API does not contain source and target vertices, hence
			// they must be set here.
			Vertex from = mongoDB.findVertexByKey(context, edge.getFrom());
			Vertex to = mongoDB.findVertexByKey(context, edge.getTo());
			edge.setSource(from);
			edge.setTarget(to);
			saveEdge(context, idMap, edge);
		}
//		log.info("Graph being sent to CAAS is: " + graph);
//		caAsClient.getAssetCvssTryCatch(graph);
//		try {
//			caAsClient.getAssetCvss(graph);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}

	public void addOrUpdateGraph(Graph graph, String fromCdg, String toCdg, Boolean updateCvss) {
		String context = graph.getContext();
		log.info("Last update time in graph in addOrUpdateGraph: " + Graph.lastLocalCpeUpdateTime);
		Map<ObjectId, ObjectId> idMap = new HashMap<>();
		for (Vertex vertex : graph.getVertices()) {
			saveVertex(context, idMap, vertex);
		}
		for (Edge edge : graph.getEdges()) {
			if (fromCdg != null) {
				edge.setFromCdg(fromCdg);
			}
			if (toCdg != null) {
				edge.setToCdg(toCdg);
			}
			// JSON passed by REST API does not contain source and target vertices, hence
			// they must be set here.
			Vertex from = mongoDB.findVertexByKey(context, edge.getFrom());
			Vertex to = mongoDB.findVertexByKey(context, edge.getTo());
			edge.setSource(from);
			edge.setTarget(to);
			saveEdge(context, idMap, edge);
		}
//		caAsClient.getAssetCvssAsync(graph);
		try {
			caAsClient.getAssetCvss(graph);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private Vertex saveVertex(String context, Map<ObjectId, ObjectId> idMap, Vertex vertex) {
		ObjectId originalId = vertex.getId();
		vertex.setCdgCount(0);
		Vertex oldVertex = mongoDB.findVertexByKey(context, vertex.getKey());
		if (oldVertex != null) {
			vertex.setId(oldVertex.getId());
			vertex.setCdgCount(oldVertex.getCdgCount());
			if (oldVertex.getScore() != 0.0d && oldVertex.getScore() != 0.2506d && oldVertex.getScore() != 0.1245d
					&& vertex.getScore() == 0.0d) {
				vertex.setScore(oldVertex.getScore());

				if (oldVertex instanceof Node && vertex instanceof Node) {
					Node oldNode = (Node) oldVertex;
					Node vertexNew = (Node) vertex;
					if (vertexNew.getMaxCvss() == null && oldNode.getMaxCvss() != 0.0d) {
						vertexNew.setMaxCvss(oldNode.getMaxCvss());
						Vertex result = mongoDB.insertOrUpdateOne(context, vertexNew);
						ObjectId currentId = result.getId();
						if (!currentId.equals(originalId)) {
							idMap.put(originalId, currentId);
						}
						return result;

					} else if (vertexNew.getMaxCvss() != null && oldNode.getMaxCvss() != 0.0d) {
						if (vertexNew.getMaxCvss() == 0.0d) {
							vertexNew.setMaxCvss(oldNode.getMaxCvss());
							Vertex result = mongoDB.insertOrUpdateOne(context, vertexNew);
							ObjectId currentId = result.getId();
							if (!currentId.equals(originalId)) {
								idMap.put(originalId, currentId);
							}
							return result;
						}
					}
				}
				// FIXME element.setCreated(oldElement.getCreated());
			}
		}

		Vertex result = mongoDB.insertOrUpdateOne(context, vertex);
		ObjectId currentId = result.getId();
		if (!currentId.equals(originalId)) {
			idMap.put(originalId, currentId);
		}
		return result;
	}

	private Edge saveEdge(String context, Map<ObjectId, ObjectId> idMap, Edge edge) {
//		ObjectId originalId = edge.getId();
		Edge oldEdge = mongoDB.findEdgeByKeyAndCdg(context, edge.getKey(), edge.getFromCdg());
		if (oldEdge != null) {
			edge.setId(oldEdge.getId());
			// FIXME element.setCreated(oldElement.getCreated());
		}
		Edge result = mongoDB.insertOrUpdateOne(context, edge.getSource(), edge.getTarget(), edge);
//		ObjectId currentId = result.getId();
//		if (!currentId.equals(originalId)) {
//			// element.setId(currentId); // TODO drop this?
//		}
		return result;
	}

}
