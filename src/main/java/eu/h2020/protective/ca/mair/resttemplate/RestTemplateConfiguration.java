package eu.h2020.protective.ca.mair.resttemplate;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RestTemplateConfiguration {

	@Bean
	public CustomRestTemplateCustomizer customRestTemplateCustomizer() {
	    return new CustomRestTemplateCustomizer();
	}
	
}
