package eu.h2020.protective.ca.mair.antlr.errors;

import java.util.List;

public interface VisitorErrorInterface {

	CdgErrorListener getErrorListener();

	default List<String> getErrors() {
		return getErrorListener().getErrors();
	}

}
