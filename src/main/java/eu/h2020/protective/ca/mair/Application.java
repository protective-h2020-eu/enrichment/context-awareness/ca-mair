package eu.h2020.protective.ca.mair;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.info.BuildProperties;

import lombok.extern.slf4j.XSlf4j;

@SpringBootApplication
@XSlf4j
public class Application implements ApplicationRunner {

	@Autowired
	private Optional<BuildProperties> buildProperties;

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Override
	public void run(ApplicationArguments appArgs) throws Exception {
		if (buildProperties.isPresent()) {
			AboutProperties about = new AboutProperties(buildProperties);
			log.info("{} (v{}) {}", about.getDescription(), about.getVersion(), about.getTime());
		}
	}

}
