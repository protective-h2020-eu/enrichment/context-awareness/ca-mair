package eu.h2020.protective.ca.mair;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.h2020.protective.ca.mair.api.model.assets.Asset;
import eu.h2020.protective.ca.mair.api.model.assets.Computer;
import eu.h2020.protective.ca.mair.api.model.assets.Device;
import eu.h2020.protective.ca.mair.api.model.assets.Information;
import eu.h2020.protective.ca.mair.api.model.assets.NetworkNode;
import eu.h2020.protective.ca.mair.api.model.cam.OperationalCapacity;
import eu.h2020.protective.ca.mair.api.model.core.Edge;
import eu.h2020.protective.ca.mair.api.model.core.Graph;
import eu.h2020.protective.ca.mair.api.model.core.Vertex;
import eu.h2020.protective.ca.mair.api.model.idg.And;
import eu.h2020.protective.ca.mair.api.model.idg.Or;
import eu.h2020.protective.ca.mair.api.model.other.Mission;
import eu.h2020.protective.ca.mair.api.model.other.SecurityObjective;
import eu.h2020.protective.ca.mair.api.model.services.Service;
import eu.h2020.protective.ca.mair.api.model2.nodes.Node;
import eu.h2020.protective.ca.mair.model.Metadata;
import eu.h2020.protective.ca.mair.model.Tuple;
import lombok.extern.slf4j.XSlf4j;

import org.apache.commons.net.util.*;
//import org.javers.common.collections.Arrays;
@XSlf4j
@Component
public class MongoDBQueries {

	@Autowired
	private MongoDB mongoDB;

	@Autowired
	private MatrixInit matrixInit;

	private final Double DEFAULT_OC = 1.0;

	private Iterable<Vertex> getMissions(String context) {
		return mongoDB.findVerticesByType(context, Mission.class);
	}

	public Double getMissionOC(String context, Vertex mission) throws Exception {
		if (mission != null) {
			return getMissionOC(context, mission.getKey());
		}
		return DEFAULT_OC;
	}

	public Double getMissionOC(String context, String missionKey) throws Exception {
		if (missionKey != null) {
			Vertex mission = mongoDB.findVertexByKey(context, missionKey);
			if (mission != null && mission instanceof Mission) {
				return getVertexOC(context, mission);
			}
		}
		return DEFAULT_OC;
	}

	public Double getOverallOC(String context) throws Exception {
		Double minOC = 1.0;
		for (Vertex mission : getMissions(context)) {
			minOC = Double.min(getMissionOC(context, mission), minOC);
		}
		return minOC;
	}

	private Double getAndOC(String context, Vertex operator) throws Exception {
		if (operator != null) {
			String edgeTo = operator.getKey();
			Double minOC = 1.0;
			for (Edge edge : mongoDB.findEdgesByTo(context, edgeTo)) {
				minOC = Double.min(getEdgeOC(context, edge), minOC);
			}
			return mongoDB.updateVertexCollection(context, operator.getId(), minOC);
		}
		return DEFAULT_OC;
	}

	private Double getOrOC(String context, Vertex operator) throws Exception {
		if (operator != null) {
			String edgeTo = operator.getKey();
			Double maxOC = 0.0;
			for (Edge edge : mongoDB.findEdgesByTo(context, edgeTo)) {
				maxOC = Double.max(getEdgeOC(context, edge), maxOC);
			}
			return mongoDB.updateVertexCollection(context, operator.getId(), maxOC);
		}
		return DEFAULT_OC;
	}

	private Double getEdgeOC(String context, Edge edge) throws Exception {
		if (edge != null) {
			String edgeFrom = edge.getFrom();
			Vertex vertex = mongoDB.findVertexByKey(context, edgeFrom);
			if (vertex != null) {
				return mongoDB.updateEdgeCollection(context, edge.getId(), getVertexOC(context, vertex),
						edge.getFromCdg());
			}
		}
		return DEFAULT_OC;
	}

	private Double getVertexOC(String context, Vertex vertex) throws Exception {
		if (vertex != null) {
			if (vertex.getExpression().equals(And.class.getSimpleName())) {
				return getAndOC(context, vertex);
			}
			if (vertex.getExpression().equals(Or.class.getSimpleName())) {
				return getOrOC(context, vertex);
			}
			if (vertex.getExpression().equals(OperationalCapacity.class.getName())) {
				return getOCOC(vertex);
			}
			String edgeTo = vertex.getKey();
			// TODO name not generic, specific to graph type
			for (Edge edge : mongoDB.findEdgesByToAndName(context, edgeTo, "Edge")) {
				return mongoDB.updateVertexCollection(context, vertex.getId(), getEdgeOC(context, edge));
			}
			// TODO name not generic, specific to graph type
			for (Edge fromOC : mongoDB.findEdgesByToAndName(context, edgeTo, "FromOC")) {
				return mongoDB.updateVertexCollection(context, vertex.getId(), getEdgeOC(context, fromOC));
			}
		}
		return DEFAULT_OC;
	}

	private Double getOCOC(Vertex oc) {
		return oc.getWeight();
	}

	private Mission getHighestWeight(String context, Set<String> missions) {
		Double weight = 0.0;
		Mission mission = null;
		for (String key : missions) {
			Vertex vertex = mongoDB.findVertexByKey(context, key);
			if (vertex != null && vertex.getWeight() >= weight) {
				// XXX finds last of highest equal weights
				weight = vertex.getWeight();
				if (vertex instanceof Mission) {
					mission = (Mission) vertex;
				}
			}
		}
		return mission;
	}

	public Set<String> getMissionsImpactedByAsset(String context, String assetKey) {
		return mongoDB.getVerticesDependingOnVertex(context, Mission.class, assetKey);
	}

	// TODO If tied should we report more than one mission?
	public Mission getMostImportantMissionForVertexKey(String context, String vertexKey) {
		Set<String> missions = mongoDB.getVerticesDependingOnVertex(context, Mission.class, vertexKey);
		Mission mission = getHighestWeight(context, missions);
		return mission;
	}

	// TODO If tied should we report more than one mission?
	public Mission getMostImportantMissionForNodeIpAddress(String context, String ipAddress) {
		Set<String> missions = mongoDB.getVerticesDependingOnNodeByIpAddress(context, Mission.class, ipAddress);
		Mission mission = getHighestWeight(context, missions);
		return mission;
	}

	// TODO If tied should we report more than one mission?
	public Mission getMostImportantMissionForNodeDns(String context, String dns) {
		Set<String> missions = mongoDB.getVerticesDependingOnNodeByDns(context, Mission.class, dns);
		Mission mission = getHighestWeight(context, missions);
		return mission;
	}

	public Node getNodeWithIpAddress(String context, String ipAddress) {
		for (Vertex vertex : mongoDB.findVerticesByIpAddress(context, ipAddress)) {
			if (vertex != null && vertex instanceof Node) {
				return (Node) vertex;
			}
		}
		return null;
	}

	public Node getNodeWithDns(String context, String dns) {
		for (Vertex vertex : mongoDB.findVerticesByDns(context, dns)) {
			if (vertex != null && vertex instanceof Node) {
				return (Node) vertex;
			}
		}
		return null;
	}

	public Map<String, Node> getNodesWithIpAddresses(String context, List<String> ipAddresses) {
		Map<String, Node> nodes = new HashMap<>(ipAddresses.size());
		for (String ipAddress : ipAddresses) {
			for (Vertex vertex : mongoDB.findVerticesByIpAddress(context, ipAddress)) {
				if (vertex != null && vertex instanceof Node) {
					nodes.put(ipAddress, (Node) vertex);
					break;
				}
			}
		}
		return nodes;
	}

	public Map<String, Node> getNodesWithDnses(String context, List<String> dnses) {
		Map<String, Node> nodes = new HashMap<>(dnses.size());
		for (String dns : dnses) {
			for (Vertex vertex : mongoDB.findVerticesByDns(context, dns)) {
				if (vertex != null && vertex instanceof Node) {
					nodes.put(dns, (Node) vertex);
					break;
				}
			}
		}
		return nodes;
	}

	public Set<Vertex> getSupportingAssetsForMission(String context, String missionKey) {
		return mongoDB.getVerticesAndEdgesSupportingVertex(context, Asset.class, missionKey).getA();
	}

	public List<Asset> getMostCriticalAssets(String context, int maxSize) throws Exception {
		List<Tuple<String, Double>> weights = getNetworkNodeRelativeWeights();
		weights = weights.subList(0, maxSize);
		List<Asset> assets = new ArrayList<>(weights.size());
		for (Tuple<String, Double> weight : weights) {
			Vertex vertex = mongoDB.findVertexByKey(context, weight.a);
			if (vertex != null && vertex instanceof Asset) {
				assets.add((Asset) vertex);
			}
		}
		return assets;
	}

	public Set<String> getServicesDependingOnService(String context, String serviceKey) {
		return mongoDB.getVerticesDependingOnVertex(context, Service.class, serviceKey);
	}

//	public Set<String> getServicesDependingOnAsset(String context, String assetKey) {
//		// FIXME What about also asset depending on services?
//		return mongoDB.getVerticesDependingOnVertex(Service.class, assetKey);
//	}

	public Graph getSubGraph(String context, String rootVertexKey) {
		Tuple<Set<Vertex>, Set<Edge>> tuple = mongoDB.getVerticesAndEdgesSupportingVertex(context, Vertex.class,
				rootVertexKey);
		Graph graph = new Graph();
		for (Vertex vertex : tuple.getA()) {
			if (vertex != null) {
				graph.addVertex(vertex);
			}
		}
		for (Edge edge : tuple.getB()) {
			if (edge != null) {
				Vertex from = mongoDB.findVertexByKey(context, edge.getFrom());
				Vertex to = mongoDB.findVertexByKey(context, edge.getTo());
				graph.addEdge(from, to, edge);
			}
		}
		return graph;
	}

	public List<Tuple<String, Double>> getNetworkNodeRelativeWeights() throws Exception {
		Map<String, Double> values = new HashMap<>();
		values.put("Stay safe", 1.0);
		values.put("Stay profitable", 1.25);
		values.put("Stay in compliance", 1.75);
		values.put("Supply customers well", 2.0);
		List<Tuple<String, Double>> matrix1weights = matrixInit.getBusinessObjectiveRelativeWeights(values);
		List<Tuple<String, Double>> matrix2weights = matrixInit.getNextRelativeWeights(matrix1weights,
				SecurityObjective.class);
		List<Tuple<String, Double>> matrix3weights = matrixInit.getNextRelativeWeights(matrix2weights,
				Information.class);
		List<Tuple<String, Double>> matrix4weights = matrixInit.getNextRelativeWeights(matrix3weights,
				NetworkNode.class, Computer.class, Device.class);
		return matrix4weights;
	}

	public Map<String, Node> getNodesFromSubnet(String defaultContext, String subnet) {
		log.info("subnet in Mongo: " + subnet);
		SubnetUtils utils = new SubnetUtils(subnet);
		utils.setInclusiveHostCount(true);
		log.info("cidrSignature is: " + utils.getInfo().getCidrSignature());
		String[] allIps = utils.getInfo().getAllAddresses();
		log.info("allIps empty?: " + (allIps.length == 0));
		List<String> arrayList = new ArrayList<>(); 
		Collections.addAll(arrayList, allIps);
		log.info("arrayList empty?: " + arrayList.isEmpty());
		Map<String, Node> nodes = getNodesWithIpAddresses(Metadata.DEFAULT_CONTEXT, arrayList);
		
		// TODO Auto-generated method stub
		return nodes;
	}

}
