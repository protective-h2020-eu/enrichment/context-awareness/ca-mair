package eu.h2020.protective.ca.mair;

import static java.util.stream.Collectors.toSet;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.jgrapht.alg.interfaces.VertexScoringAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import eu.h2020.protective.ca.mair.antlr.cluster.insight.ClusterPass1Visitor;
import eu.h2020.protective.ca.mair.antlr.json.CdgPass1Visitor;
import eu.h2020.protective.ca.mair.antlr.original.DepsPass1Visitor;
import eu.h2020.protective.ca.mair.api.model.assets.Asset;
import eu.h2020.protective.ca.mair.api.model.core.GVertex;
import eu.h2020.protective.ca.mair.api.model.core.Graph;
import eu.h2020.protective.ca.mair.api.model.core.Vertex;
import eu.h2020.protective.ca.mair.api.model.other.Mission;
import eu.h2020.protective.ca.mair.api.model2.nodes.Node;
import eu.h2020.protective.ca.mair.assetrank.AssetRank;
import eu.h2020.protective.ca.mair.matrices.Matrices;
import eu.h2020.protective.ca.mair.model.Metadata;
import lombok.extern.slf4j.XSlf4j;

@XSlf4j
@RequestMapping(path = "/api", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@RestController
public class CaRestController {

	@Autowired
	private CaAsClient caAsClient;
	
	@Autowired
	private Optional<BuildProperties> buildProperties;

	@Autowired
	private SyncGraph syncGraph;

	@Autowired
	private MongoDB mongoDB;

	@Autowired
	private MongoDBQueries mongoDBQueries;

	@Autowired
	private Matrices matrices;

	private Map<String, Double> missionWeights = new HashMap<>();
	private Map<String, Double> vertexWeights = new HashMap<>();
	private Map<String, Double> edgeWeights = new HashMap<>();

	private static Double maxAssetScore = 0.5;
	private static Double minAssetScore = 0.001;
	private static Double assetSplitValue = 0.12475;
	private static final Map<String, Double> finalScore;
	static {
		Map<String, Double> map = new HashMap<>();
		map.put("none", 0.0d);
		map.put("low", 1.0d);
		map.put("medium", 2.0d);
		map.put("high", 3.0d);
		map.put("critical", 4.0d);
		finalScore = Collections.unmodifiableMap(map);
	}

	@PostConstruct
	private void init() {
		missionWeights.put("none", 0.0d);
		missionWeights.put("low", 1.0d);
		missionWeights.put("medium", 2.0d);
		missionWeights.put("high", 3.0d);
		missionWeights.put("critical", 4.0d);

		vertexWeights.put("none", 0.0d);
		vertexWeights.put("low", 1.0d);
		vertexWeights.put("medium", 2.0d);
		vertexWeights.put("high", 3.0d);
		vertexWeights.put("critical", 4.0d);

		edgeWeights.put("none", 0.0d);
		edgeWeights.put("low", 1.0d);
		edgeWeights.put("medium", 2.0d);
		edgeWeights.put("high", 3.0d);
		edgeWeights.put("critical", 4.0d);
	}

	////////////////////////////////////////////////////
	private static final String template = "Hello, %s!";
	private final AtomicLong counter = new AtomicLong();

	// @CrossOrigin(origins = "http://localhost:9000")
	@GetMapping("/greeting")
	public Greeting greeting(@RequestParam(required = false, defaultValue = "World") String name) {
		System.out.println("==== in greeting ====");
		return new Greeting(counter.incrementAndGet(), String.format(template, name));
	}
	////////////////////////////////////////////////////

	/**
	 * Returns information about this build of ca-mair.
	 */
	@GetMapping("/about")
	public AboutProperties about() {
		return new AboutProperties(buildProperties);
	}

	/**
	 * Start Asset Rank.
	 */
	@GetMapping("/start-asset-rank")
	public String startAssetRank() {
		AssetRank assetRank = new AssetRank(graphGet());
		syncGraph.addOrUpdateGraph(assetRank.getGraph());
		
		if (!this.getCdgNames().isEmpty()) {
			this.setMaxAndMinCriticality(assetRank);
		}
		return scoresString(assetRank);
	}
	
	/**
	 * Add or Update MaxCvss scores in nodes in the Graph
	 */
	@GetMapping("/add-or-update-cvss-scores")
	public String addOrUpdateCvssScores() {
		this.caAsClient.getAssetCvssTryCatch(this.graphGet());
		return "\nCVSS scores have have been updated in the graph";
	}
	

	private String scoresString(VertexScoringAlgorithm<GVertex, Double> algorithm) {
		Map<GVertex, Double> sortedScores = algorithm.getScores().entrySet().stream()
				.sorted(Map.Entry.comparingByValue(Comparator.reverseOrder())).collect(Collectors.toMap(
						Map.Entry::getKey, Map.Entry::getValue, (oldValue, newValue) -> oldValue, LinkedHashMap::new));
		double totalScore = 0.0d;
		StringBuilder builder = new StringBuilder();
		for (Entry<GVertex, Double> entry : sortedScores.entrySet()) {
			double score = entry.getValue().doubleValue() * 100;
			String name = entry.getKey().getVertex().getName();
			builder.append(String.format("%10.3f = %s %n", score, name));
			totalScore += score;
		}
		builder.append(String.format("%10.3f = %s %n", totalScore, "Total Score"));
		builder.append(((AssetRank) algorithm).getFinalMessage());
		return builder.toString();
	}
	private Double getFinalScore(Double rawScore) {
		
		if (rawScore > (CaRestController.assetSplitValue) * 3 + CaRestController.minAssetScore) 
		{
			return CaRestController.finalScore.get("critical");
		}
		else if (rawScore > (CaRestController.assetSplitValue * 2) + CaRestController.minAssetScore && rawScore <= ((CaRestController.assetSplitValue) * 3) + CaRestController.minAssetScore)
		{
			return CaRestController.finalScore.get("high");
		}
		else if (rawScore > (CaRestController.assetSplitValue + CaRestController.minAssetScore) && rawScore <= ((CaRestController.assetSplitValue) * 2) + CaRestController.minAssetScore)
		{
			return CaRestController.finalScore.get("medium");
		}
		else if (rawScore > CaRestController.minAssetScore && rawScore <= (CaRestController.assetSplitValue) + CaRestController.minAssetScore)
		{
			return CaRestController.finalScore.get("low");
		} else
		{
			return CaRestController.finalScore.get("none");
		}
	}

	private void setMaxAndMinCriticality(VertexScoringAlgorithm<GVertex, Double> algorithm) {
//		Map<GVertex, Double> sortedScores = algorithm.getScores().entrySet().stream()
//				.sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
//				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
//						(oldValue, newValue) -> oldValue, LinkedHashMap::new));

		Entry<GVertex,Double>maxEntry=Collections.max(algorithm.getScores()
				.entrySet(),(Entry<GVertex,Double>e1,Entry<GVertex,Double>e2)
				->e1.getValue()
				.compareTo(e2.getValue()));
		
		Entry<GVertex,Double>minEntry=Collections.min(algorithm.getScores()
				.entrySet(),(Entry<GVertex,Double>e1,Entry<GVertex,Double>e2)
				->e1.getValue()
				.compareTo(e2.getValue()));
		
		CaRestController.maxAssetScore=maxEntry.getValue();
		log.info("Current max crit: " + CaRestController.maxAssetScore);
		CaRestController.minAssetScore=minEntry.getValue();
		log.info("Current min crit: " + CaRestController.minAssetScore);
		CaRestController.assetSplitValue = (CaRestController.maxAssetScore - CaRestController.minAssetScore) / 4;
		
	}

	/**
	 * Get all of the graph data stored in this ca-mair instance.
	 */
	@GetMapping("/graph-get") // TODO optimize to limit or chunk data?
	public Graph graphGet() {
		return syncGraph.getGraph(Metadata.DEFAULT_CONTEXT);
	}

	/**
	 * Get a subset of the graph data stored in this ca-mair instance.
	 * 
	 * @param key A globally unique ID independent of the DB identifying the root of
	 *            the Graph.
	 */
	@GetMapping("/graph-get/{key}")
	public Graph graphGet(@PathVariable String key) {
		return mongoDBQueries.getSubGraph(Metadata.DEFAULT_CONTEXT, key);
	}

	/**
	 * Get a subset of the graph data stored in this ca-mair instance.
	 * 
	 * @param context e.g. PSNC or ROEDUNET or CESNET
	 * @param key     A globally unique ID independent of the DB identifying the
	 *                root of the Graph.
	 */
	@GetMapping("/graph-get/{context}/{key}")
	public Graph graphGet(@PathVariable String context, @PathVariable String key) {
		return mongoDBQueries.getSubGraph(context, key);
	}

	/**
	 * Add or update a subgraph of data to the data already stored in this ca-mair
	 * instance.
	 */
	@PostMapping("/graph-add")
	public void graphAdd(@RequestBody Graph graph) {
		graph.setContext(Metadata.DEFAULT_CONTEXT);
		syncGraph.addOrUpdateGraph(graph);
	}

	/**
	 * Add one or more elements to a CDG.
	 * 
	 * @param cdgName A globally unique CDG name independent of the DB.
	 * @throws UnsupportedEncodingException
	 */
	@PostMapping("/graph-add/{cdgName}") // FIXME forces edges into CDG
	public void graphAdd(@RequestBody Graph graph, @PathVariable String cdgName) throws UnsupportedEncodingException {
		cdgName = URLDecoder.decode(cdgName, StandardCharsets.UTF_8.name());
		graph.setContext(Metadata.DEFAULT_CONTEXT);
		syncGraph.addOrUpdateGraph(graph, cdgName, null); // FIXME Replace null toCdg?
	}

	/**
	 * Add or update a subgraph of data to the data already stored in this ca-mair
	 * instance.
	 */
	@PostMapping("/graph-update") // TODO only send deltas to save bandwidth.
	public void graphUpdate(@RequestBody Graph graph) {
		graph.setContext(Metadata.DEFAULT_CONTEXT);
		syncGraph.addOrUpdateGraph(graph);
	}

	/**
	 * Remove a CDG instance.
	 * 
	 * @param cdgName A globally unique CDG name independent of the DB.
	 * @throws UnsupportedEncodingException
	 */
	@PostMapping("/remove-cdg/{cdgName}")
	public void removeCdg(@PathVariable String cdgName) throws UnsupportedEncodingException {
		cdgName = URLDecoder.decode(cdgName, StandardCharsets.UTF_8.name());
		mongoDB.deleteCDG(Metadata.DEFAULT_CONTEXT, cdgName);
	}

	/**
	 * Remove a subgraph of data from the data already stored in this ca-mair
	 * instance.
	 * 
	 * @param cdgName A globally unique CDG name independent of the DB.
	 * @throws UnsupportedEncodingException
	 */
	@PostMapping("/graph-remove/{cdgName}") // TODO only send ids/keys to save bandwidth.
	public void graphRemove(@RequestBody Graph graph, @PathVariable String cdgName)
			throws UnsupportedEncodingException {
		cdgName = URLDecoder.decode(cdgName, StandardCharsets.UTF_8.name());
		graph.setContext(Metadata.DEFAULT_CONTEXT);
		mongoDB.deleteGraphFromCDG(graph, cdgName);
	}

	/**
	 * Remove all graph data from this ca-mair instance.
	 */
	@PostMapping("/graph-clear") // TODO DROP? Add role based security?
	public void graphClear() {
		mongoDB.clearAllMatricesAndGraphs(Metadata.DEFAULT_CONTEXT); // TODO ignore matrices
	}

	/**
	 * @param assetKey A globally unique ID independent of the DB.
	 */
	@GetMapping("/get-missions-impacted-by-asset/{assetKey}")
	public Set<String> getMissionsImpactedByAsset(@PathVariable String assetKey) {
		return mongoDBQueries.getMissionsImpactedByAsset(Metadata.DEFAULT_CONTEXT, assetKey);
	}

	/**
	 * For a given vertex get the most important mission. FIXME add 3 more
	 * 
	 * @param assetKey A globally unique ID independent of the DB.
	 */
	@GetMapping("/get-most-important-mission-for-vertex-key/{vertexKey}")
	public Mission getMostImportantMissionForVertexKey(@PathVariable String vertexKey) {
		return mongoDBQueries.getMostImportantMissionForVertexKey(Metadata.DEFAULT_CONTEXT, vertexKey);
	}

	/**
	 * Get the Node information for an IP Address.
	 * 
	 * @param ipAddress An IPv4 or IPv6 Address.
	 */
	@GetMapping("/get-node-with-ip/{ipAddress}")
	public Node getNodeWithIpAddress(@PathVariable String ipAddress) {
		return mongoDBQueries.getNodeWithIpAddress(Metadata.DEFAULT_CONTEXT, ipAddress);
	}

	/**
	 * Get the Node information for a DNS.
	 * 
	 * @param dns A DNS.
	 */
	@GetMapping("/get-node-with-dns/{dns}")
	public Node getNodeWithDns(@PathVariable String dns) {
		return mongoDBQueries.getNodeWithDns(Metadata.DEFAULT_CONTEXT, dns);
	}

	/**
	 * Get the Node criticality for an IP Address.
	 * 
	 * @param ipAddress An IPv4 or IPv6 Address.
	 */
	@GetMapping("/get-node-criticality-for-ip/{ipAddress}")
	public Double getNodeCriticalityForIpAddress(@PathVariable String ipAddress) {
		Node node = mongoDBQueries.getNodeWithIpAddress(Metadata.DEFAULT_CONTEXT, ipAddress);

		if (node != null) {
			double score = node.getScore();
			node.setScore(getFinalScore(score));
			return node.getScore();
//			return node.getWeight();
		}
		return null;
	}

	/**
	 * Get the Node criticality for a DNS.
	 * 
	 * @param dns A DNS.
	 */
	@GetMapping("/get-node-criticality-for-dns/{dns}")
	public Double getNodeCriticalityForDns(@PathVariable String dns) {
		Node node = mongoDBQueries.getNodeWithDns(Metadata.DEFAULT_CONTEXT, dns);

		if (node != null) {
//			return node.getWeight();
			double score = node.getScore();
			node.setScore(getFinalScore(score));
			return node.getScore();			
		}
		return null;
	}

	/**
	 * Get the Node criticalities for an IP Address list.
	 * 
	 * @param ipAddressList An IPv4 or IPv6 Address list.
	 */
	@PostMapping("/get-node-criticalities-for-ip-list")
	public Map<String, Double> getNodeCriticalitiesForIpAddressList(@RequestBody List<String> ipAddressList) {
		Map<String, Node> nodes = mongoDBQueries.getNodesWithIpAddresses(Metadata.DEFAULT_CONTEXT, ipAddressList);
		Map<String, Double> map = new HashMap<>(ipAddressList.size());

		for (Entry<String, Node> entry : nodes.entrySet()) {
			double score = entry.getValue().getScore();
			entry.getValue().setScore(getFinalScore(score));
//			map.put(entry.getKey(), entry.getValue().getWeight());
			map.put(entry.getKey(), entry.getValue().getScore());
		}
		return map;
	}
	/**
	 * Get the Node criticalities for an IP Address subnet.
	 * 
	 * @param A list where the first element is an ip and the second element is a cidr bit number. ()
	 */
	/**
	 * Get the Node criticalities for an IP Address subnet.
	 * 
	 * @param A map where the first element is an ip and the second element is a cidr bit number as in CIDR notation. 
	 * example: { "ip": "x.x.x.x", "mask": "x"}
	 */
	@PostMapping("/get-node-criticalities-for-subnet")
	public Map<String, Double> getNodeCriticalitiesForSubnet(@RequestBody Map<String, String> cidr) {		
		String subnet = cidr.get("ip") + "/" + cidr.get("mask");
		log.info("subnet: " + subnet);
		Map<String, Node> nodes = mongoDBQueries.getNodesFromSubnet(Metadata.DEFAULT_CONTEXT, subnet);
		log.info("Nodes returned from DB: ", nodes);
		Map<String, Double> map = new HashMap<>(nodes.size());

		for (Entry<String, Node> entry : nodes.entrySet()) {
			double score = entry.getValue().getScore();
			entry.getValue().setScore(getFinalScore(score));
			map.put(entry.getKey(), entry.getValue().getScore());
		}
		return map;
	}
	/**
	 * Get the Node with the max criticality for an IP Address subnet.
	 * 
	 * @param A map where the first element is an ip and the second element is a cidr bit number as in CIDR notation. 
	 * example: { "ip": "x.x.x.x", "mask": "x"}
	 */
	@PostMapping("/get-node-with-max-criticality-from-subnet")
	public Map.Entry<String, Double> getNodeWithMaxCriticalityFromSubnet(@RequestBody Map<String, String> cidr) {		
		Map<String, Double> map = this.getNodeCriticalitiesForSubnet(cidr);
		Map.Entry<String, Double> maxMap = maxUsingCollectionsMax(map);
		return maxMap;
	}
	
	/**
	 * Get the Node CVSS scores  for an IP Address subnet.
	 * 
	 * @param A map where the first element is an ip and the second element is a cidr bit number as in CIDR notation. 
	 * example: { "ip": "x.x.x.x", "mask": "x"}
	 */
	@PostMapping("/get-max-cvss-scores-for-nodes-from-subnet")
	public Map<String, Double> getMaxCvssScoresForNodesFromSubnet(@RequestBody Map<String, String> cidr) {		
		String subnet = cidr.get("ip") + "/" + cidr.get("mask");
		log.info("subnet: " + subnet);
		Map<String, Node> nodes = mongoDBQueries.getNodesFromSubnet(Metadata.DEFAULT_CONTEXT, subnet);
		log.info("Nodes returned from DB: ", nodes);
		Map<String, Double> map = new HashMap<>(nodes.size());

		for (Entry<String, Node> entry : nodes.entrySet()) {
			double maxCvss = entry.getValue().getMaxCvss();
			map.put(entry.getKey(), maxCvss);
		}
		return map;
	}
	
	/**
	 * Get the Node with the max CVSS score for an IP Address subnet.
	 * 
	 * @param A map where the first element is an ip and the second element is a cidr bit number as in CIDR notation. 
	 * example: { "ip": "x.x.x.x", "mask": "x"}
	 */
	@PostMapping("/get-node-with-max-cvss-score-from-subnet")
	public Map.Entry<String, Double> getNodeWithMaxCvssScoresFromSubnet(@RequestBody Map<String, String> cidr) {		
		Map<String, Double> map = this.getMaxCvssScoresForNodesFromSubnet(cidr);
		Map.Entry<String, Double> maxMap = maxUsingCollectionsMax(map);
		return maxMap;
	}
	/**
	 * Get the Node maxCvss scores and Criticality for a subnet.
	 * 
	 * @param  a cidr subnet e.g. 192.168.200.0/24
	 * @return map E.g.
	 *         {"192.168.101.11":{"criticality":3.0,"maxCvss":6.0},"192.168.101.2":{"criticality":1.0,"maxCvss":10.0}}...
	 */
	@PostMapping("/get-max-cvss-and-criticality-for-nodes-from-subnet")
	public Map<String, Map<String, Double>> getMaxCvssAndCriticalityFornodesFromSubnet(@RequestBody Map<String, String> cidr) {
		String subnet = cidr.get("ip") + "/" + cidr.get("mask");
		log.info("subnet: " + subnet);
		Map<String, Node> nodes = mongoDBQueries.getNodesFromSubnet(Metadata.DEFAULT_CONTEXT, subnet);
		log.info("Nodes returned from DB: ", nodes);
		
		Map<String, Map<String, Double>> map = new HashMap<>(nodes.size());

		for (Entry<String, Node> entry : nodes.entrySet()) {
			Map<String, Double> scores = new HashMap<>(2);
			double score = entry.getValue().getScore();
			entry.getValue().setScore(getFinalScore(score));
			
			scores.put("criticality", entry.getValue().getScore());
			scores.put("maxCvss", entry.getValue().getMaxCvss());
			map.put(entry.getKey(), scores);
		}
		return map;
	}
	

	/**
	 * Get the Node criticalities for a DNS list.
	 * 
	 * @param dnsList A DNS list.
	 */
	@PostMapping("/get-node-criticalities-for-dns-list")
	public Map<String, Double> getNodeCriticalitiesForDnsList(@RequestBody List<String> dnsList) {
		Map<String, Node> nodes = mongoDBQueries.getNodesWithDnses(Metadata.DEFAULT_CONTEXT, dnsList);
		Map<String, Double> map = new HashMap<>(dnsList.size());

		for (Entry<String, Node> entry : nodes.entrySet()) {
			double score = entry.getValue().getScore();
			entry.getValue().setScore(getFinalScore(score));
			map.put(entry.getKey(), entry.getValue().getScore());
		}
		return map;
	}

	/**
	 * Get the Max. Node criticality for an IP Address list.
	 * 
	 * @param ipAddressList An IPv4 or IPv6 Address list.
	 */
	@PostMapping("/get-node-with-max-criticality-from-ip-list")
	public Map<String, Double> getNodeWithMaxCriticalityFromIpAddressList(@RequestBody List<String> ipAddressList) {
		Map<String, Node> nodes = mongoDBQueries.getNodesWithIpAddresses(Metadata.DEFAULT_CONTEXT, ipAddressList);
		Map<String, Double> map = new HashMap<>(ipAddressList.size());

		Entry<String, Node> max = null;
		for (Entry<String, Node> entry : nodes.entrySet()) {
			if (max == null || max.getValue().getImportance() < entry.getValue().getImportance()) {
				max = entry;
			}
		}
		if (max != null) {
//			map.put(max.getKey(), max.getValue().getWeight());
			double score = max.getValue().getScore();
			double finalScore = getFinalScore(score);
			log.info("final score is: " + finalScore);
			max.getValue().setScore(finalScore);
//			map.put(max.getKey(), finalScore);
			map.put(max.getKey(), max.getValue().getScore());
		}
		return map;
	}

	/**
	 * Get the Max. Node criticality for a DNS list.
	 * 
	 * @param dnsList A DNS list.
	 */
	@PostMapping("/get-node-with-max-criticality-from-dns-list")
	public Map<String, Double> getNodeWithMaxCriticalityFromDnsList(@RequestBody List<String> dnsList) {
		Map<String, Node> nodes = mongoDBQueries.getNodesWithDnses(Metadata.DEFAULT_CONTEXT, dnsList);
		Map<String, Double> map = new HashMap<>(dnsList.size());

		Entry<String, Node> max = null;
		for (Entry<String, Node> entry : nodes.entrySet()) {
			if (max == null || max.getValue().getImportance() < entry.getValue().getImportance()) {
				max = entry;
			}
		}
		if (max != null) {
//			map.put(max.getKey(), max.getValue().getWeight());
			double score = max.getValue().getScore();
			max.getValue().setScore(getFinalScore(score));
			map.put(max.getKey(), max.getValue().getScore());
		}
		return map;
	}

	/**
	 * Get the Node vulnerability rating for an IP Address.
	 * 
	 * @param ip An IP address
	 */
	@GetMapping("/get-max-cvss-for-node-with-ip/{ipAddress}")
	public Double getMaxCvssForNodeWithIpAddress(@PathVariable String ipAddress) {
		Node node = mongoDBQueries.getNodeWithIpAddress(Metadata.DEFAULT_CONTEXT, ipAddress);

		if (node != null) {
			return node.getMaxCvss();
		}
		return null;
	}

	/**
	 * Get the Node vulnerability for a DNS.
	 * 
	 * @param dns A DNS.
	 */
	@GetMapping("/get-max-cvss-for-node-with-dns/{dns}")
	public Double getMaxCvssForNodeWithDns(@PathVariable String dns) {
		Node node = mongoDBQueries.getNodeWithDns(Metadata.DEFAULT_CONTEXT, dns);

		if (node != null) {
			return node.getMaxCvss();
		}
		return null;
	}

	/**
	 * Get the Node maxCvss scores for an IP Address list.
	 * 
	 * @param ipAddressList An IPv4 or IPv6 Address list.
	 * @return map E.g. {"IP_ADDR1":doubleValue,"IP_ADDR2":doubleValue...}
	 */
	@PostMapping("/get-max-cvss-scores-for-ip-list")
	public Map<String, Double> getMaxCvssScoresForIpList(@RequestBody List<String> ipAddressList) {
		Map<String, Node> nodes = mongoDBQueries.getNodesWithIpAddresses(Metadata.DEFAULT_CONTEXT, ipAddressList);
		Map<String, Double> map = new HashMap<>(ipAddressList.size());

		for (Entry<String, Node> entry : nodes.entrySet()) {
			map.put(entry.getKey(), entry.getValue().getMaxCvss());
		}
		return map;
	}

	/**
	 * Get the Node maxCvss scores for an DNS list.
	 * 
	 * @param dnsList A DNS list.
	 * @return map {"dns.example.com":doubleValue,"dns2.example.com":doubleValue...}
	 * 
	 */
	@PostMapping("/get-max-cvss-scores-for-dns-list")
	public Map<String, Double> getMaxCvssScoresForDnsList(@RequestBody List<String> dnsList) {
		Map<String, Node> nodes = mongoDBQueries.getNodesWithDnses(Metadata.DEFAULT_CONTEXT, dnsList);
		Map<String, Double> map = new HashMap<>(dnsList.size());

		for (Entry<String, Node> entry : nodes.entrySet()) {
			map.put(entry.getKey(), entry.getValue().getMaxCvss());
		}
		return map;
	}

	/**
	 * Get the maxCvss and Criticality for a DNS.
	 * 
	 * @param dns A DNS.
	 * 
	 * @return scores E.g. {"criticality":1.0,"maxCvss":10.0}
	 */
	@GetMapping("/get-max-cvss-and-criticality-for-dns/{dns}")
	public Map<String, Double> getMaxCvssAndCriticalityForDns(@PathVariable String dns) {
		Node node = mongoDBQueries.getNodeWithDns(Metadata.DEFAULT_CONTEXT, dns);

		if (node != null) {
			Map<String, Double> scores = new HashMap<>(2);
			double score = node.getScore();
			node.setScore(getFinalScore(score));
			
			scores.put("criticality", node.getScore());
			scores.put("maxCvss", node.getMaxCvss());
			return scores;
		}
		return null;
	}
	/**
	 * Get the maxCvss and Criticality for an Ip.
	 * 
	 * @param ip
	 * @return scores E.g. {"criticality":1.0,"maxCvss":10.0}
	 */
	@GetMapping("/get-max-cvss-and-criticality-for-ip/{ip}")
	public Map<String, Double> getMaxCvssAndCriticalityForIp(@PathVariable String ip) {
		Node node = mongoDBQueries.getNodeWithIpAddress(Metadata.DEFAULT_CONTEXT, ip);

		if (node != null) {
			Map<String, Double> scores = new HashMap<>(2);
//			ArrayList<Double> scores = new ArrayList<>();
			double score = node.getScore();
			node.setScore(getFinalScore(score));
			
			scores.put("criticality", node.getScore());
			scores.put("maxCvss", node.getMaxCvss());
			return scores;
		}
		return null;
	}

	/**
	 * Get the Node maxCvss scores and Criticality for a DNS list.
	 * 
	 * @param dnsList A DNS list.
	 * @return map E.g.
	 *         {"dns.11.com":{"criticality":3.0,"maxCvss":6.0},"dns.2.com":{"criticality":1.0,"maxCvss":10.0}}...
	 */
	@PostMapping("/get-max-cvss-and-criticality-for-dns-list")
	public Map<String, Map<String, Double>> getMaxCvssAndCriticalityForDnsList(@RequestBody List<String> dnsList) {
		Map<String, Node> nodes = mongoDBQueries.getNodesWithDnses(Metadata.DEFAULT_CONTEXT, dnsList);
		Map<String, Map<String, Double>> map = new HashMap<>(dnsList.size());

		for (Entry<String, Node> entry : nodes.entrySet()) {
			Map<String, Double> scores = new HashMap<>(2);
			double score = entry.getValue().getScore();
			entry.getValue().setScore(getFinalScore(score));
			
			scores.put("criticality", entry.getValue().getScore());
			scores.put("maxCvss", entry.getValue().getMaxCvss());
			map.put(entry.getKey(), scores);
		}
		return map;
	}

	/**
	 * Get the Node maxCvss scores and Criticality for an Ip list.
	 * 
	 * @param  ipList An Ip list.
	 * @return map E.g.
	 *         {"192.168.101.11":{"criticality":3.0,"maxCvss":6.0},"192.168.101.2":{"criticality":1.0,"maxCvss":10.0}}...
	 */
	@PostMapping("/get-max-cvss-and-criticality-for-ip-list")
	public Map<String, Map<String, Double>> getMaxCvssAndCriticalityForIpList(@RequestBody List<String> ipList) {
		Map<String, Node> nodes = mongoDBQueries.getNodesWithIpAddresses(Metadata.DEFAULT_CONTEXT, ipList);
		Map<String, Map<String, Double>> map = new HashMap<>(ipList.size());

		for (Entry<String, Node> entry : nodes.entrySet()) {
			Map<String, Double> scores = new HashMap<>(2);
			double score = entry.getValue().getScore();
			entry.getValue().setScore(getFinalScore(score));
			
			scores.put("criticality", entry.getValue().getScore());
			scores.put("maxCvss", entry.getValue().getMaxCvss());
			map.put(entry.getKey(), scores);
		}
		return map;
	}

	/**
	 * Get one or more vertices of a particular type from a CDG.
	 * 
	 * @param vertexType The full name of the class for this vertex type.
	 * 
	 * @param cdgName    A globally unique CDG name independent of the DB.
	 */
	@GetMapping("/get-vertices-by-type-and-cdg/{vertexType}/{cdgName}")
	public Set<Vertex> getVerticesByTypeAndCdg(@PathVariable String vertexType, @PathVariable String cdgName)
			throws UnsupportedEncodingException {
		try {
			Class<?> type = Class.forName(vertexType);
			cdgName = URLDecoder.decode(cdgName, StandardCharsets.UTF_8.name());
			return mongoDB.getVerticesByTypeAndCdg(Metadata.DEFAULT_CONTEXT, type, cdgName);
		} catch (ClassNotFoundException e) {
			return new HashSet<>(0);
		}
	}

	/**
	 * List all Contexts.
	 */
	@GetMapping("/get-context-names")
	public Set<String> getContextNames() {
		return mongoDB.findAllContextNames();
	}

	/**
	 * List all Context Dependency Graphs.
	 */
	@GetMapping("/get-cdg-names")
	public List<String> getCdgNames() {
		return mongoDB.getAllCdgNames(Metadata.DEFAULT_CONTEXT);
	}

	/**
	 * List all Context Dependency Graphs.
	 * 
	 * @param context e.g. PSNC or ROEDUNET or CESNET
	 */
	@GetMapping("/get-cdg-names/{context}")
	public List<String> getCdgNames(@PathVariable String context) {
		return mongoDB.getAllCdgNames(context);
	}

	/**
	 * Get a particular CDG (edges + vertices).
	 * 
	 * @param cdgName A globally unique CDG name independent of the DB.
	 * @throws UnsupportedEncodingException
	 */
	@GetMapping("/get-graph-by-cdg/{cdgName}")
	public Graph getGraphByCdg(@PathVariable String cdgName) throws UnsupportedEncodingException {
		cdgName = URLDecoder.decode(cdgName, StandardCharsets.UTF_8.name());
		return mongoDB.getGraphByCdg(Metadata.DEFAULT_CONTEXT, cdgName);
	}

	/**
	 * Get a particular CDG (edges + vertices).
	 * 
	 * @param context e.g. PSNC or ROEDUNET or CESNET
	 * @param cdgName A globally unique CDG name independent of the DB.
	 * @throws UnsupportedEncodingException
	 */
	@GetMapping("/get-graph-by-cdg/{context}/{cdgName}")
	public Graph getGraphByCdg(@PathVariable String context, @PathVariable String cdgName)
			throws UnsupportedEncodingException {
		cdgName = URLDecoder.decode(cdgName, StandardCharsets.UTF_8.name());
		return mongoDB.getGraphByCdg(context, cdgName);
	}

	/**
	 * Get a particular CDG (vertices only).
	 * 
	 * @param cdgName A globally unique CDG name independent of the DB.
	 * @throws UnsupportedEncodingException
	 */
	@GetMapping("/get-vertices-by-cdg/{cdgName}")
	public Set<Vertex> getVerticesByCdg(@PathVariable String cdgName) throws UnsupportedEncodingException {
		cdgName = URLDecoder.decode(cdgName, StandardCharsets.UTF_8.name());
		return mongoDB.getVerticesByCdg(Metadata.DEFAULT_CONTEXT, cdgName);
	}

	/**
	 * @param missionKey A globally unique ID independent of the DB.
	 */
	@GetMapping("/get-supporting-assets-for-mission/{missionKey}")
	public Set<String> getSupportingAssetsForMission(@PathVariable String missionKey) {
		Set<Vertex> assets = mongoDBQueries.getSupportingAssetsForMission(Metadata.DEFAULT_CONTEXT, missionKey);
		Set<String> assetKeys = assets.stream().map(e -> e.getKey()).collect(toSet());
		return assetKeys;
	}

	@GetMapping("/get-most-critical-assets")
	public List<Asset> getMostCriticalAssets() throws Exception {
		return mongoDBQueries.getMostCriticalAssets(Metadata.DEFAULT_CONTEXT, 10); // TODO pass size param to API?
	}

	/**
	 * @param serviceKey A globally unique ID independent of the DB.
	 */
	@GetMapping("/get-services-depending-on-service/{serviceKey}")
	public Set<String> getServicesDependingOnService(@PathVariable String serviceKey) {
		return mongoDBQueries.getServicesDependingOnService(Metadata.DEFAULT_CONTEXT, serviceKey);
	}

	@PostMapping(path = "/graph-upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<?> uploadFile(@RequestPart("json") Graph graph) {
		// TODO is check of vertices / edges present necessary?
		graph.setContext(Metadata.DEFAULT_CONTEXT);
		log.info("Graph being uploaded by /graph-upload endpoint");
		syncGraph.addOrUpdateGraph(graph);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	/**
	 * Upload a CDG file describing the relationships between Vertices and Edges
	 * 
	 * @param file A CDG file describing the relationships between Vertices and
	 *             Edges.
	 */
	@PostMapping(path = "/upload-cdg-file", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<List<String>> uploadCdgFile(@RequestPart("file") MultipartFile file) throws Exception {
		List<String> errors = new ArrayList<>();
		if (file == null) {
			String error = String.format("Invalid multi-part file");
			errors.add(error);
			return ResponseEntity.badRequest().body(errors);
		}
		String originalFilename = file.getOriginalFilename();
		if (originalFilename == null) {
			String error = String.format("Invalid original filename");
			errors.add(error);
			return ResponseEntity.badRequest().body(errors);
		}
		File tmpFile = File.createTempFile(originalFilename, null);
		file.transferTo(tmpFile);
		if (originalFilename.endsWith(".cdgs")) {
			CdgPass1Visitor pass1 = new CdgPass1Visitor(mongoDB, originalFilename, tmpFile, true, missionWeights,
					vertexWeights, edgeWeights);
			errors.addAll(pass1.getErrors());
		} else if (originalFilename.endsWith(".deps")) {
			DepsPass1Visitor pass1 = new DepsPass1Visitor(mongoDB, originalFilename, tmpFile, true, missionWeights,
					vertexWeights, edgeWeights);
			errors.addAll(pass1.getErrors());
		} else if (originalFilename.endsWith(".json")) {
			ClusterPass1Visitor pass1 = new ClusterPass1Visitor(mongoDB, originalFilename, tmpFile, true,
					missionWeights, vertexWeights, edgeWeights);
			errors.addAll(pass1.getErrors());
		} else if (originalFilename.endsWith(".cdgv")) { // XXX ignore edges in file, upload vertices only
			List<String> uploadErrors = syncGraph.uploadVertices(tmpFile);
			errors.addAll(uploadErrors);
		} else if (originalFilename.endsWith(".cdgg")) { // XXX upload vertices and edges
			List<String> uploadErrors = syncGraph.uploadGraph(tmpFile);
			errors.addAll(uploadErrors);
		} else {
			String error = String.format("Invalid file extension in %s", originalFilename);
			errors.add(error);
		}
		if (errors.isEmpty()) {
			return ResponseEntity.ok().body(errors);
			// } else { // TODO This does not fix statusText, is OK as statusText ok?
			// return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errors);
		} else {
			// TODO should sort be in visitor, should it sort numbers not strings?
			// errors.sort(null);
			return ResponseEntity.badRequest().body(errors);
		}
	}

	/**
	 * Upload a CSV file describing the matrices used to calculate weightings
	 * 
	 * @param file A CSV file describing the matrices used to calculate weightings.
	 */
	@PostMapping(path = "/weighting-matrix-upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<?> uploadMatrix(@RequestPart("file") MultipartFile file) throws Exception {
		if (file == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		String originalFilename = file.getOriginalFilename();
		if (originalFilename == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		File tmpFile = File.createTempFile(originalFilename, null);
		file.transferTo(tmpFile);
		matrices.addWeightingMatrices(Metadata.DEFAULT_CONTEXT, tmpFile);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	// @GetMapping("/get-asset-and-service-types")
	// public List<String> getAssetAndServiceTypes() throws Exception {
	// return SubClasses.getSubClasses(Asset.class, Service.class);
	// }
	//
	// @GetMapping("/get-relationship-types")
	// public List<String> getRelationshipTypes() throws Exception {
	// return SubClasses.getSubClasses(Edge.class);
	// }
	public <K, V extends Comparable<V>> Entry<K, V> maxUsingCollectionsMax(Map<K, V> map) {
	    Entry<K, V> maxEntry = Collections.max(map.entrySet(), new Comparator<Entry<K, V>>() {
	        public int compare(Entry<K, V> e1, Entry<K, V> e2) {
	            return e1.getValue()
	                .compareTo(e2.getValue());
	        }
	    });
//	    return maxEntry.getValue();
	    return maxEntry;
	}

}