package eu.h2020.protective.ca.mair.scheduled.tasks;

//import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@EnableScheduling
@Conditional(ConfigNameCondition.class)
public class SchedulingConfiguration {

}
