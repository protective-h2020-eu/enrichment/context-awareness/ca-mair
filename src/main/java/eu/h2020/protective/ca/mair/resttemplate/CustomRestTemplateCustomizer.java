package eu.h2020.protective.ca.mair.resttemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.boot.web.client.RestTemplateCustomizer;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

public class CustomRestTemplateCustomizer implements RestTemplateCustomizer {
	@Override
	public void customize(RestTemplate restTemplate) {
        restTemplate.getInterceptors().add(new LoggingRequestInterceptor());
        
//		 restTemplate = new RestTemplate();
//        ClientHttpRequestFactory factory = new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory());
//        
//        restTemplate = new RestTemplate(factory);
//        
//        restTemplate.setInterceptors(Collections.singletonList(new LoggingRequestInterceptor()));
//		restTemplate.setRequestFactory(new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory()));
//		List<ClientHttpRequestInterceptor> interceptors = restTemplate.getInterceptors();
//		if (CollectionUtils.isEmpty(interceptors)) {
//			interceptors = new ArrayList<>();
//		}
//		interceptors.add(new LoggingRequestInterceptor());
//		restTemplate.setInterceptors(interceptors);
	}
}
