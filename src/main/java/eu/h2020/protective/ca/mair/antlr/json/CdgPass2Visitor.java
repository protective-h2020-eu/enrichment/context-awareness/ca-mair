package eu.h2020.protective.ca.mair.antlr.json;

import java.io.IOException;

import org.antlr.v4.runtime.Token;

import eu.h2020.protective.ca.mair.antlr.errors.CdgErrorListener;
import eu.h2020.protective.ca.mair.antlr.errors.VisitorErrorInterface;
import eu.h2020.protective.ca.mair.api.model.core.Edge;
import eu.h2020.protective.ca.mair.api.model.core.Vertex;
import lombok.Getter;

public class CdgPass2Visitor extends cdgsBaseVisitor<Object> implements VisitorErrorInterface {

	@Getter
	private final CdgErrorListener errorListener;

	@Getter
	private final CdgSupport cdgSupport;

	public CdgPass2Visitor(CdgPass1Visitor pass1) throws IOException {
		errorListener = pass1.getErrorListener();
		cdgSupport = pass1.getCdgSupport();
		visit(cdgSupport.getParseTree());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitCdg_deps(cdgsParser.Cdg_depsContext ctx) {
		// TODO Drop this check? Will need to handle possible null Tokens.
		if (getErrorListener().hasSyntaxError()) {
			return null;
		}
		String cdgName = ctx.ID().getSymbol().getText();
		cdgSupport.initEdgeCollection(cdgName); // prepare for second pass
		cdgSupport.setCdgName(ctx, cdgName);
		return visitChildren(ctx);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitInter_layer_dep(cdgsParser.Inter_layer_depContext ctx) {
		String fromKey = ctx.ID(0).getSymbol().getText();
		String toKey = ctx.ID(1).getSymbol().getText();
		String toCdgName = ctx.ID(2).getSymbol().getText();

		Vertex fromVertex = cdgSupport.getVertex(fromKey);
		Vertex toVertex = cdgSupport.getVertex(toKey);

		if (fromVertex == null) {
			getErrorListener().error(ctx.ID(0).getSymbol(), CdgSupport.VERTEX_MISSING_PATTERN, fromKey,
					cdgSupport.getOriginalFilename());
		}
		if (toVertex == null) {
			getErrorListener().error(ctx.ID(1).getSymbol(), CdgSupport.VERTEX_MISSING_PATTERN, toKey,
					cdgSupport.getOriginalFilename());
		}
		
		String grade = null;
		if (ctx.ID().size() > 3) {
			Token gradeToken = ctx.ID(3).getSymbol();
			grade = gradeToken.getText();
			if (!Edge.isValidGrade(grade)) {
				getErrorListener().error(gradeToken, "\"{}\" is not one of {}.", grade, Edge.getAllowedGrades());
			}
		}

		if (fromVertex != null && toVertex != null) {
			cdgSupport.addOrUpdateEdge(toCdgName, ctx, fromVertex, toVertex, grade);
		}

		return visitChildren(ctx);
	}
	
}
