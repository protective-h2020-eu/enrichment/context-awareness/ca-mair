package eu.h2020.protective.ca.mair.assetrank;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.Vector;

import eu.h2020.protective.ca.mair.api.model.core.Vertex;
import lombok.extern.slf4j.XSlf4j;

@XSlf4j
public class AssetRanking {

	private final double adjacencyMatrixDvu[][];
	private final double dampingFactor = 0.85;
	private double arcweight;
	private String s1 = "";

	// Taken from paper describing java algorithm.
	public AssetRanking(String file_name, int vertices, int max_iteration) {

		adjacencyMatrixDvu = new double[vertices][vertices];

		String line = null;

		int i, k, x, y, z, iteration, converged;

		Vector<Double> IntrinsicVector = new Vector<Double>(20, 2);
		Vector<Double> ScaledIntrinsicVector = new Vector<Double>(20, 2);
		Vector<Double> TempAssetRank = new Vector<Double>(20, 2);
		Vector<Double> IntermediateAssetRank = new Vector<Double>(20, 2);
		Vector<Double> AssetRank = new Vector<Double>(20, 2);
		Vector<Double> NormalizedAssetRank = new Vector<Double>(20, 2);
		// Vector<Double> diff = new Vector<Double>(20, 2);

		iteration = 0;
		// max_iteration = Integer.parseInt(args[2]); // XXX paper java algorithm is 1000
		double tolerance = 0.000_001; // XXX JGraphT's PageRank has a default of 0.0001
		converged = 0;

		AssetRanking q = this;

		// Initializing Adjacency matrix to all 0s
		for (x = 0; x < vertices; x++) {
			for (y = 0; y < vertices; y++) {
				q.adjacencyMatrixDvu[x][y] = 0.0d;
			}
		}

		try (FileInputStream readStream = new FileInputStream(file_name)) {
			try (InputStreamReader readFile = new InputStreamReader(readStream, StandardCharsets.UTF_8)) {
				try (BufferedReader readBuffer = new BufferedReader(readFile)) {
					i = 0;
					while ((line = readBuffer.readLine()) != null) {
						s1 = line;
						String[] splitString = s1.split(",");
						for (k = 0; k < splitString.length; k++) {
							log.debug("{}", splitString[0]);
							if (k == 0) {
								if (splitString[0].equals(Vertex.AND)) {
									arcweight = 1d;
								} else if (splitString[0].equals(Vertex.OR)) {
									arcweight = 1d / (splitString.length - 1);
								} else if (splitString[0].equals(Vertex.SINK)) {
									// arcweight = 1d; // XXX 1d for PageRank, else not set
									break;
								} else {
									log.error("Vertex type {} is invalid. "
											+ "Please rerun the program with valid vertex " + "type AND,OR,SINK",
											splitString[0]);
									break;
								}
							} else {
								int adjacentvertex = Integer.parseInt(splitString[k]);
								q.adjacencyMatrixDvu[adjacentvertex][i] = arcweight;
							}
						}
						i++;
					}
				}
			}
		} catch (FileNotFoundException a) {
			log.error("Unable to open file '{}'", file_name);
		} catch (IOException a) {
			log.error("Error reading file '{}'", file_name);
		}

		// q.AdjacencyMatrixDvu = q.invertMatrix(q.AdjacencyMatrixDvu);

		System.out.println("\n-->Adjacency matrix for the inputted graph is: \n ");
		for (x = 0; x < vertices; x++) {
			for (y = 0; y < vertices; y++) {
				System.out.print("\t" + q.adjacencyMatrixDvu[x][y]);
			}
			System.out.print("\n\n");
		}

		// IntrinsicVector.add(1.0);
		double damp_val; // = (1-DampingFactor)*0.5 ;
		// ScaledIntrinsicVector.add(damp_val);
		for (z = 0; z < vertices; z++) {
			if (z == 45) {
				damp_val = (1 - dampingFactor) * 0.9;
				IntrinsicVector.add(0.9);
				ScaledIntrinsicVector.add(damp_val);
			} else if (z == 54) {
				damp_val = (1 - dampingFactor) * 0.1;
				IntrinsicVector.add(0.1);
				ScaledIntrinsicVector.add(damp_val);
			} else {
				IntrinsicVector.add(0.0);
				ScaledIntrinsicVector.add(0.0);
			}
			// damp_val = (1-DampingFactor)*0.0;
			// ScaledIntrinsicVector.add(damp_val);
		}
		// XXX Begin Eoin.
		IntrinsicVector.clear();
		ScaledIntrinsicVector.clear();
		IntrinsicVector.add(1d);
		ScaledIntrinsicVector.add((1d - dampingFactor) * IntrinsicVector.get(0));
		for (z = 1; z < vertices; z++) {
			IntrinsicVector.add(0d); // XXX 1d for PageRank, else 0d
			ScaledIntrinsicVector.add((1d - dampingFactor) * IntrinsicVector.get(z));
		}
		// XXX End Eoin.
		log.debug("Intrinsic Vector Initial Values are : {}", IntrinsicVector);
		log.debug("Scaled Intrinsic Vector Initial Values are : {}", ScaledIntrinsicVector);

		// Initializing TempAssetRank to IntrinsicVector only in iteration 0
		if (iteration == 0) {
			// Initializing TempAssetRank before copying IntrinsicVector
			for (z = 0; z < vertices; z++) {
				TempAssetRank.add(0.0);
			}
			log.debug("Intrinsic Vector test Initial Values are : {}", IntrinsicVector);
			// Copy to TempAssetRank from IntrinsicVector
			Collections.copy(TempAssetRank, IntrinsicVector);
			iteration++;
			log.debug("Iteration value: {}", iteration);
		}
		log.debug("TempAssetRank Vector Initial Values are : {}", TempAssetRank);

		while (iteration > 0 && iteration <= max_iteration) {
			log.debug("Computing AssetRank in iteration: {}", iteration);
			for (x = 0; x < vertices; x++) {
				double tempvar = 0.0;
				for (y = 0; y < vertices; y++) {
					double Xt = q.adjacencyMatrixDvu[x][y] * TempAssetRank.get(y) * dampingFactor;
					IntermediateAssetRank.add(Xt);
				}
				log.debug("IntermediateAssetRank Vector : {} ", IntermediateAssetRank);
				for (z = 0; z < vertices; z++) {
					tempvar = tempvar + IntermediateAssetRank.get(z);
					if (z == (vertices - 1)) {
						log.debug("Dvu*Xt*Damp: {}", tempvar);
						tempvar = tempvar + ScaledIntrinsicVector.get(x);
					}
				}
				AssetRank.add(tempvar);
				IntermediateAssetRank.clear();
			}

			double L1_norm = 0.0;
			for (i = 0; i < vertices; i++) {
				L1_norm = L1_norm + AssetRank.get(i);
			}
			double norm_val = 0.0;
			for (i = 0; i < vertices; i++) {
				norm_val = AssetRank.get(i) / L1_norm;
				NormalizedAssetRank.add(norm_val);
			}
			log.debug("TempAssetRank Vector : {}", TempAssetRank);
			log.debug("AssetRank Vector : {}", AssetRank);

			log.debug("Normalized AssetRank Vector : {}", NormalizedAssetRank);
			// XXX Begin Eoin
			double maxChange = 0d;
			for (i = 0; i < vertices; i++) {
				double vOldValue = TempAssetRank.get(i);
				double vNewValue = NormalizedAssetRank.get(i);
				maxChange = Math.max(maxChange, Math.abs(vNewValue - vOldValue));
			}
			if (maxChange < tolerance) {
				converged = 1;
			} else {
				converged = 0;
			}
			// XXX End Eoin
			// double diff_val = 0.0;
			// for (i = 0; i < vertices; i++) {
			// diff_val = Math.abs(NormalizedAssetRank.get(i) - TempAssetRank.get(i));
			// diff.add(diff_val);
			// }
			// converged = 1;
			// // System.out.println("Error : "+ error);
			// for (x = 0; x < vertices; x++) {
			// if (diff.get(x) >= tolerance) { // != 0.0) {
			// converged = 0;
			// }
			// }
			if (converged == 1) {
				log.debug("AssetRanking Algorithm has converged in iteration: {}", iteration);
//				log.info("Final AssetRanks for the Attack Graph: {}", NormalizedAssetRank);
				break;
			} else if (iteration == max_iteration) {
//				log.info("AssetRanking Algorithm has reached maximum number of iterations: {}", iteration);
//				log.info("Final AssetRanks for the Attack Graph: {}", NormalizedAssetRank);
				break;
			}
			// Copy to TempAssetRank from NormalizedAssetRank
			Collections.copy(TempAssetRank, NormalizedAssetRank);
			log.debug("TempAssetRank Vector after copying NormalizedAssetRank into it : {}", TempAssetRank);
			// diff.clear();
			AssetRank.clear();
			IntermediateAssetRank.clear();
			NormalizedAssetRank.clear();
			iteration++;
		}
		logScores(NormalizedAssetRank);
	}

	private static void logScores(Vector<Double> NormalizedAssetRank) {
		int i = 1;
		for (Double rank : NormalizedAssetRank) {
			double score = rank * 100;
			DecimalFormat df = new DecimalFormat("###.###");
			log.info("{} = {}", df.format(score), i++);
		}
	}

//	@SuppressWarnings("unused")
//	private double[][] invertMatrix(double matrix[][]) {
//		for (int x = 0; x < vertices; x++) {
//			for (int y = x; y < vertices; y++) {
//				double t = matrix[x][y];
//				matrix[x][y] = matrix[y][x];
//				matrix[y][x] = t;
//			}
//		}
//		return matrix;
//	}

}
