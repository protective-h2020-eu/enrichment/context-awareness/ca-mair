/*
 * (C) Copyright 2016-2018, by Dimitrios Michail and Contributors.
 *
 * JGraphT : a free Java graph-theory library
 *
 * This program and the accompanying materials are dual-licensed under
 * either
 *
 * (a) the terms of the GNU Lesser General Public License version 2.1
 * as published by the Free Software Foundation, or (at your option) any
 * later version.
 *
 * or (per the licensee's choosing)
 *
 * (b) the terms of the Eclipse Public License v1.0 as published by
 * the Eclipse Foundation.
 */
package eu.h2020.protective.ca.mair.assetrank;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jgrapht.alg.interfaces.VertexScoringAlgorithm;

import eu.h2020.protective.ca.mair.api.model.core.GEdge;
import eu.h2020.protective.ca.mair.api.model.core.GVertex;
import eu.h2020.protective.ca.mair.api.model.core.Graph;
import eu.h2020.protective.ca.mair.api.model.core.Vertex;
import lombok.Getter;
import lombok.extern.slf4j.XSlf4j;

/**
 * AssetRank implementation. Derived from PageRank implementation
 * 
 * <p>
 * The <a href="https://en.wikipedia.org/wiki/PageRank">wikipedia</a> article contains a nice description of PageRank.
 * The method can be found on the article: Sergey Brin and Larry Page: The Anatomy of a Large-Scale Hypertextual Web
 * Search Engine. Proceedings of the 7th World-Wide Web Conference, Brisbane, Australia, April 1998. See also the
 * following <a href="http://infolab.stanford.edu/~backrub/google.html">page</a>.
 * </p>
 * 
 * <p>
 * This is a simple iterative implementation of PageRank which stops after a given number of iterations or if the
 * PageRank values between two iterations do not change more than a predefined value. The implementation uses the
 * variant which divides by the number of nodes, thus forming a probability distribution over graph nodes.
 * </p>
 *
 * <p>
 * Each iteration of the algorithm runs in linear time $O(n+m)$ when $n$ is the number of nodes and $m$ the number of
 * edges of the graph. The maximum number of iterations can be adjusted by the caller. The default value is
 * {@link AssetRank#MAX_ITERATIONS_DEFAULT}.
 * </p>
 * 
 * <p>
 * If the graph is a weighted graph, a weighted variant is used where the probability of following an edge e out of node
 * $v$ is equal to the weight of $e$ over the sum of weights of all outgoing edges of $v$.
 * </p>
 * 
 * @param <V>
 *        the graph vertex type
 * @param <E>
 *        the graph edge type
 * 
 * @author Dimitrios Michail (PageRank)
 * @since August 2016
 * 
 * @author Eoin O'Meara, SRI, Athlone Institute of Technology (AssetRank)
 * @since June 2018
 */
@XSlf4j
public final class AssetRank implements VertexScoringAlgorithm<GVertex, Double> {
	/**
	 * Default number of maximum iterations.
	 */
	public static final int MAX_ITERATIONS_DEFAULT = 100;

	/**
	 * Default value for the tolerance. The calculation will stop if the difference of PageRank values between
	 * iterations change less than this value.
	 */
	public static final double TOLERANCE_DEFAULT = 0.000_001; // PageRank default = 0.0001

	/**
	 * Damping factor default value.
	 */
	public static final double DAMPING_FACTOR_DEFAULT = 0.85d;

	@Getter
	private String finalMessage;

	@Getter
	private final Graph graph;

	private Map<GVertex, Double> scores;

	/**
	 * Create and execute an instance of PageRank.
	 * 
	 * @param g
	 *          the input graph
	 */
	public AssetRank(Graph g) {
		this(g, DAMPING_FACTOR_DEFAULT, MAX_ITERATIONS_DEFAULT, TOLERANCE_DEFAULT);
	}

	/**
	 * Create and execute an instance of PageRank.
	 * 
	 * @param g
	 *                      the input graph
	 * @param dampingFactor
	 *                      the damping factor
	 */
	public AssetRank(Graph g, double dampingFactor) {
		this(g, dampingFactor, MAX_ITERATIONS_DEFAULT, TOLERANCE_DEFAULT);
	}

	/**
	 * Create and execute an instance of PageRank.
	 * 
	 * @param g
	 *                      the input graph
	 * @param dampingFactor
	 *                      the damping factor
	 * @param maxIterations
	 *                      the maximum number of iterations to perform
	 */
	public AssetRank(Graph g, double dampingFactor, int maxIterations) {
		this(g, dampingFactor, maxIterations, TOLERANCE_DEFAULT);
	}

	/**
	 * Create and execute an instance of PageRank.
	 * 
	 * @param g
	 *                      the input graph
	 * @param dampingFactor
	 *                      the damping factor
	 * @param maxIterations
	 *                      the maximum number of iterations to perform
	 * @param tolerance
	 *                      the calculation will stop if the difference of PageRank values between iterations change
	 *                      less than this value
	 * @throws Exception
	 */
	public AssetRank(Graph g, double dampingFactor, int maxIterations, double tolerance) {
		this.graph = g;
		this.scores = new HashMap<>();

		if (maxIterations <= 0) {
			throw new IllegalArgumentException("Maximum iterations must be positive");
		}

		if (dampingFactor < 0.0 || dampingFactor > 1.0) {
			throw new IllegalArgumentException("Damping factor not valid");
		}

		if (tolerance <= 0.0) {
			throw new IllegalArgumentException("Tolerance not valid, must be positive");
		}

		try {
			runAssetRank(dampingFactor, maxIterations, tolerance);
		} catch (Exception e) {
			throw new IllegalArgumentException("Graph not valid", e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<GVertex, Double> getScores() {
		return Collections.unmodifiableMap(scores);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Double getVertexScore(GVertex v) {
		if (!graph.containsVertex(v)) {
			throw new IllegalArgumentException("Cannot return score of unknown vertex");
		}
		return scores.get(v);
	}

	private double[][] initSquareMatrix(int width, double value) { // AssetRanking
		double[][] matrix = new double[width][width];
		// Initializing all entries in matrix to value
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < width; y++) {
				matrix[x][y] = value;
			}
		}
		return matrix;
	}

	private double[][] partFillSquareMatrix(Graph g, double[][] matrix) throws Exception {
		for (int x = 0; x < g.getVertices().size(); x++) { // AssetRanking
			Vertex vx = g.getVertices().get(x);
			if (vx != null) {
				GVertex gvx = new GVertex(vx.getKey(), vx);
				for (int y = 0; y < g.getVertices().size(); y++) {
					Vertex vy = g.getVertices().get(y);
					if (vy != null) {
						GVertex gvy = new GVertex(vy.getKey(), vy);
						Set<GEdge> edges = g.getAllEdges(gvx, gvy);
						if (edges != null) {
							for (GEdge e : edges) {
								if (e != null) {
									double weight = getInitArcWeight(gvx, e);
									g.setEdgeWeight(e, weight);
									matrix[y][x] = weight;
									break; // take first of multiple edges between vx and vy
								}
							}
						}
					}
				}
			}
		}
		return matrix;
	}

	private double[][] printSquareMatrix(double[][] matrix) { // AssetRanking
		int width = matrix[0].length;
//		System.out.println("\n--> Square Matrix is: \n ");
//		for (int x = 0; x < width; x++) {
//			for (int y = 0; y < width; y++) {
//				System.out.print("\t" + matrix[x][y]);
//			}
//			System.out.print("\n\n");
//		}
		return matrix;
	}

	private List<Double> initIntrinsicVector(Graph g) { // Asset Ranking
		List<Double> intrinsicVector = new ArrayList<Double>(g.getVertices().size());
		for (Vertex v : g.getVertices()) {
			intrinsicVector.add(v.getWeight());
		}
		return Collections.unmodifiableList(intrinsicVector);
	}

	private List<Double> initScaledIntrinsicVector(List<Double> intrinsicVector, double dampingFactor) { // AssetRanking
		List<Double> scaledIntrinsicVector = new ArrayList<Double>(intrinsicVector.size());
		for (int i = 0; i < intrinsicVector.size(); i++) {
			scaledIntrinsicVector.add((1.0d - dampingFactor) * intrinsicVector.get(i));
		}
		return Collections.unmodifiableList(scaledIntrinsicVector);
	}

	private List<Double> copyVector(List<Double> vector) { // Asset Ranking
		List<Double> copy = new ArrayList<>(vector);
		return copy;
	}

	private Map<GVertex, Double> copyVectorToMap(List<Double> vector) { // Asset Ranking
		Map<GVertex, Double> map = new HashMap<>();
		for (int i = 0; i < graph.getVertices().size(); i++) {
			Vertex v = graph.getVertices().get(i);
			GVertex gv = new GVertex(v.getKey(), v);
			double score = vector.get(i);
			v.setScore(score);
			map.put(gv, score);
		}
		return map;
	}

	private double getInitArcWeight(GVertex v, GEdge e) { // AssetRanking
		int totalWeights = 0; // g.outDegreeOf(v);
		for (GEdge oe : graph.outgoingEdgesOf(v)) {
			totalWeights += oe.getEdge().getWeight(); // 1.0d;
		}
		String expression = v.getVertex().getExpression();
		double edgeWeight = e.getEdge().getWeight(); // 1.0d;
		switch (expression) {
		case Vertex.AND:
			return edgeWeight;
		case Vertex.OR:
			return edgeWeight / totalWeights;
		default:
			return 0.0d;
		}
	}

	// private void run(double dampingFactor, int maxIterations, double tolerance) {
	// // initialization
	// int totalVertices = g.vertexSet().size();
	// boolean weighted = g.getType().isWeighted();
	// Map<Vertex, Double> weights;
	// if (weighted) {
	// weights = new HashMap<>(totalVertices);
	// } else {
	// weights = Collections.emptyMap();
	// }
	//
	// double initScore = 1.0d / totalVertices;
	// for (Vertex v : g.vertexSet()) {
	// scores.put(v, initScore);
	// if (weighted) {
	// double sum = 0;
	// for (Edge e : g.outgoingEdgesOf(v)) {
	// sum += g.getEdgeWeight(e);
	// }
	// weights.put(v, sum);
	// }
	// }
	//
	// // run PageRank
	// Map<Vertex, Double> nextScores = new HashMap<>();
	// double maxChange = tolerance;
	//
	// while (maxIterations > 0 && maxChange >= tolerance) {
	// // compute next iteration scores
	// double r = 0d;
	// for (Vertex v : g.vertexSet()) {
	// if (g.outgoingEdgesOf(v).size() > 0) {
	// r += (1d - dampingFactor) * scores.get(v);
	// } else {
	// r += scores.get(v);
	// }
	// }
	// r /= totalVertices;
	//
	// maxChange = 0d;
	// for (Vertex v : g.vertexSet()) {
	// double contribution = 0d;
	//
	// if (weighted) {
	// for (Edge e : g.incomingEdgesOf(v)) {
	// Vertex w = Graphs.getOppositeVertex(g, e, v);
	// contribution += dampingFactor * scores.get(w) * g.getEdgeWeight(e)
	// / weights.get(w);
	// }
	// } else {
	// for (Edge e : g.incomingEdgesOf(v)) {
	// Vertex w = Graphs.getOppositeVertex(g, e, v);
	// contribution += dampingFactor * scores.get(w) / g.outgoingEdgesOf(w).size();
	// }
	// }
	//
	// double vOldValue = scores.get(v);
	// double vNewValue = r + contribution;
	// maxChange = Math.max(maxChange, Math.abs(vNewValue - vOldValue));
	// nextScores.put(v, vNewValue);
	// }
	//
	// // swap scores
	// Map<Vertex, Double> tmp = scores;
	// scores = nextScores;
	// nextScores = tmp;
	//
	// // progress
	// maxIterations--;
	// }
	//
	// }

	private void runAssetRank(double dampingFactor, int maxIterations, double tolerance) throws Exception {
		// TODO Why is ordering necessary? Is there a bug in the algorithm?
//		graph.orderVertices(); 
//		graph.orderEdges();

		// initialization
		int totalVertices = graph.vertexSet().size();

		double adjacencyMatrixDvu[][] = initSquareMatrix(totalVertices, 0.0d);

		List<Double> intrinsicVector = initIntrinsicVector(graph);
		List<Double> scaledIntrinsicVector = initScaledIntrinsicVector(intrinsicVector, dampingFactor);
		List<Double> previousNormalizedAssetRank = copyVector(intrinsicVector);
		List<Double> intermediateAssetRank = new ArrayList<Double>(totalVertices);
		List<Double> unnormalizedAssetRank = new ArrayList<Double>(totalVertices);
		List<Double> normalizedAssetRank = new ArrayList<Double>(totalVertices);

		adjacencyMatrixDvu = partFillSquareMatrix(graph, adjacencyMatrixDvu);

//		printSquareMatrix(adjacencyMatrixDvu);

		// run AssetRank
		double maxChange = tolerance;
		int iterations = 0;

		while (iterations < maxIterations && maxChange >= tolerance) {
			// progress
			iterations++;

			for (int x = 0; x < totalVertices; x++) {
				double tempvar = 0.0d;
				for (int y = 0; y < totalVertices; y++) {
					double Xt = adjacencyMatrixDvu[x][y] * previousNormalizedAssetRank.get(y) * dampingFactor;
					intermediateAssetRank.add(Xt);
				}
				for (int z = 0; z < totalVertices; z++) {
					tempvar = tempvar + intermediateAssetRank.get(z);
					if (z == (totalVertices - 1)) {
						tempvar = tempvar + scaledIntrinsicVector.get(x);
					}
				}
				unnormalizedAssetRank.add(tempvar);
				intermediateAssetRank.clear(); // TODO drop to optimize?
			}

			double L1_norm = 0.0d;
			for (int i = 0; i < totalVertices; i++) {
				L1_norm = L1_norm + unnormalizedAssetRank.get(i);
			}
			double norm_val = 0.0d;
			for (int i = 0; i < totalVertices; i++) {
				norm_val = unnormalizedAssetRank.get(i) / L1_norm;
				normalizedAssetRank.add(norm_val);
			}

			maxChange = 0.0d;
			for (int i = 0; i < totalVertices; i++) {
				double vOldValue = previousNormalizedAssetRank.get(i);
				double vNewValue = normalizedAssetRank.get(i);
				maxChange = Math.max(maxChange, Math.abs(vNewValue - vOldValue));
			}

			boolean converged = maxChange < tolerance;
			if (converged) {
//				log.info("Final AssetRanks for the Attack Graph: {}", normalizedAssetRank);
				finalMessage = String.format("AssetRanking Algorithm has converged in iteration: %s %n", iterations);
				log.debug("AssetRanking Algorithm has converged in iteration: {}", iterations);
				scores = copyVectorToMap(normalizedAssetRank);
				break;
			} else if (iterations == maxIterations) {
//				log.info("Final AssetRanks for the Attack Graph: {}", normalizedAssetRank);
				finalMessage = String.format("AssetRanking Algorithm has reached maximum number of iterations: %s %n",
						iterations);
				log.info("AssetRanking Algorithm has reached maximum number of iterations: {}", iterations);
				scores = copyVectorToMap(normalizedAssetRank);
				break;
			}
			previousNormalizedAssetRank = copyVector(normalizedAssetRank);

			unnormalizedAssetRank.clear(); // TODO drop to optimize?
			intermediateAssetRank.clear(); // TODO drop to optimize?
			normalizedAssetRank.clear(); // TODO drop to optimize?
		}
	}
}