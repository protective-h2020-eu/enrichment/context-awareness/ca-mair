package eu.h2020.protective.ca.mair;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import eu.h2020.protective.ca.mair.api.model.core.Vertex;
import eu.h2020.protective.ca.mair.api.model.other.Mission;
import eu.h2020.protective.ca.mair.matrices.Matrices;
import eu.h2020.protective.ca.mair.model.MatrixElement;
import eu.h2020.protective.ca.mair.model.Metadata;
import eu.h2020.protective.ca.mair.model.Tuple;

@Service
public class MatrixInit {

	@Autowired
	private MongoDB mongoDB;

	@Autowired
	private Matrices matrices;

	@Autowired
	private ConfigurationService config;

	public void clearMatrices() {
		mongoDB.clearAllMatricesAndGraphs(Metadata.DEFAULT_CONTEXT);
	}

	public void initMatrices() throws Exception {
		String filename = config.getMatrixCsvFilename();
		File tmpFile = classPathResourceToTemporaryFile(filename);
		matrices.addWeightingMatrices(Metadata.DEFAULT_CONTEXT, tmpFile);
	}

	private File classPathResourceToTemporaryFile(String filename) throws Exception {
		Resource resource = new ClassPathResource(filename);
		File tmpFile = File.createTempFile(filename, null);
		Files.copy(resource.getInputStream(), tmpFile.toPath(),
				StandardCopyOption.REPLACE_EXISTING);
		return tmpFile;
	}

	public List<Tuple<String, Double>> getBusinessObjectiveRelativeWeights(
			Map<String, Double> initialWeights) throws Exception {

		List<String> keys = new ArrayList<>(initialWeights.size());
		List<Double> values = new ArrayList<>(initialWeights.size());

		initialWeights.entrySet().stream().sorted(Map.Entry.<String, Double>comparingByValue())
				.forEach(entry -> {
					keys.add(entry.getKey());
					values.add(entry.getValue());
				});

		Set<String> setOfKeys = getVertexCollectionKeysByType(Mission.class);

		if (keys.size() != setOfKeys.size() || !keys.containsAll(setOfKeys)) {
			throw new IllegalArgumentException("Keys in Map argument do not match keys in matrix.");
		}

		// diagonal
		for (int j = 0; j < keys.size(); j++) {
			mongoDB.insertOrUpdateOne(new MatrixElement(keys.get(j), keys.get(j), 1.0));
		}

		// above diagonal
		for (int k = 1; k < keys.size(); k++) {
			mongoDB.insertOrUpdateOne(new MatrixElement(keys.get(0), keys.get(k), values.get(k)));
		}
		for (int j = 1; j < keys.size(); j++) {
			for (int k = j + 1; k < keys.size(); k++) {
				Double top = mongoDB.findByKeyXAndKeyY(keys.get(0), keys.get(k)).getValue();
				Double bottom = mongoDB.findByKeyXAndKeyY(keys.get(0), keys.get(j)).getValue();
				Double result = top / bottom;
				mongoDB.insertOrUpdateOne(new MatrixElement(keys.get(j), keys.get(k), result));
			}
		}

		// below diagonal (inverse of above diagonal)
		for (int j = keys.size() - 1; j >= 0; j--) {
			for (int k = j - 1; k >= 0; k--) {
				Double top = 1.0;
				Double bottom = mongoDB.findByKeyXAndKeyY(keys.get(k), keys.get(j)).getValue();
				Double result = top / bottom;
				mongoDB.insertOrUpdateOne(new MatrixElement(keys.get(j), keys.get(k), result));
			}
		}

		// calculate sums
		Double[] sums = new Double[keys.size()];
		Double sumsTotal = 0.0;
		for (int j = 0; j < keys.size(); j++) {
			sums[j] = 0.0;
			for (int k = 0; k < keys.size(); k++) {
				sums[j] += mongoDB.findByKeyXAndKeyY(keys.get(j), keys.get(k)).getValue();
			}
			sumsTotal += sums[j];
		}

		// calculate normalized relative weights
		List<Tuple<String, Double>> weights = new ArrayList<>(keys.size());
		for (int j = 0; j < keys.size(); j++) {
			double weight = sums[j] / sumsTotal;
			Vertex vertex = mongoDB.findVertexByKey(Metadata.DEFAULT_CONTEXT, keys.get(j));
			if (vertex != null) {
				vertex.setWeight(weight);
				mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, vertex);
				weights.add(new Tuple<>(keys.get(j), weight));
			}
		}

		return weights;
	}

	@SafeVarargs
	public final List<Tuple<String, Double>> getNextRelativeWeights(
			List<Tuple<String, Double>> matrixInRelativeWeights, Class<? extends Vertex>... typesIn)
			throws Exception {
		Set<String> keys = new HashSet<>();
		for (Class<? extends Vertex> typeIn : typesIn) {
			keys.addAll(getVertexCollectionKeysByType(typeIn));
		}
		List<Tuple<String, Double>> matrixOutRelativeWeights = new ArrayList<>(keys.size());
		for (String keyY : keys) {
			Double weightOut = 0.0;
			for (Tuple<String, Double> weightIn : matrixInRelativeWeights) {
				MatrixElement element = mongoDB.findByKeyXAndKeyY(weightIn.a, keyY);
				Double factor = (element != null ? element.getValue() : 0.0);
				weightOut += (weightIn.b * factor);
			}
			Vertex vertex = mongoDB.findVertexByKey(Metadata.DEFAULT_CONTEXT, keyY);
			if (vertex != null) {
				vertex.setWeight(weightOut);
				mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, vertex);
				matrixOutRelativeWeights.add(new Tuple<>(keyY, weightOut));
			}
		}
		matrixOutRelativeWeights.sort((o1, o2) -> -o1.b.compareTo(o2.b));
		return matrixOutRelativeWeights;
	}

	private Set<String> getVertexCollectionKeysByType(Class<? extends Vertex> type) {
		Set<String> keys = new HashSet<>();
		for (Vertex vertex : mongoDB.findVerticesByType(Metadata.DEFAULT_CONTEXT, type)) {
			keys.add(vertex.getKey());
		}
		return keys;
	}

}
