package eu.h2020.protective.ca.mair.model;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

import eu.h2020.protective.ca.mair.api.constraints.ApiDescription;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
@ToString(callSuper=false)
@ApiModel(value = "ASSoftware")
public class ASSoftware {

	@NotBlank
	@ApiDescription("A globally unique ID independent of the DB")
	@ApiModelProperty(required = true, value = "A globally unique ID independent of the DB")
	@JsonProperty(value = "id")
	private String key;

	@ApiModelProperty(required = false, value = "Software product name")
	private String product;

	@ApiModelProperty(required = false, value = "Software vendor name")
	private String vendor;

	@ApiModelProperty(required = false, value = "Software version")
	private String version;
	
	// TODO Add OS key?
	
	public ASSoftware(String key) {
		this.key = key;
	}
	
}
