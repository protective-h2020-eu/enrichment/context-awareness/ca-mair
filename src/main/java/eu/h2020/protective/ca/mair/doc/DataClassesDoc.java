package eu.h2020.protective.ca.mair.doc;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.zeroturnaround.exec.ProcessExecutor;
import org.zeroturnaround.exec.stream.slf4j.Slf4jStream;

import com.google.common.collect.ImmutableSet;
import com.google.common.reflect.ClassPath;

import eu.h2020.protective.ca.mair.ConfigurationService;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class DataClassesDoc {

	private final String rootPackageName = "eu.h2020.protective.ca.mair.api.model";
	private final StringBuilder content = new StringBuilder();
	private final boolean alwaysDocSuperClass = false;
	private Path outputPath = null;
	private DataClassesDoc doc;

	@Autowired
	private ConfigurationService config;

	public DataClassesDoc() {
	}
	
	@PostConstruct
	private void init() {
		doc = this;
	}

	public void createDocForModel() throws Exception {
		if (doc != null) {
			doc.createDocForPackage(doc.rootPackageName, doc.outputPath);
		}
	}

	private void createDocForPackage(String rootPackageName, Path outputPath,
			String... skipPackageNames) throws Exception {
		List<String> skipList = Arrays.asList(skipPackageNames);
		ClassPath classPath = ClassPath.from(DataClassesDoc.class.getClassLoader());
		ImmutableSet<ClassPath.ClassInfo> infoSet = classPath
				.getTopLevelClassesRecursive(rootPackageName);
		for (ClassPath.ClassInfo classInfo : infoSet) {
			if (!skipList.contains(classInfo.getPackageName())) {
				Class<?> clazz = classInfo.load();
				if (clazz != null && clazz != Object.class) {
					Annotation[] classAnnotations = clazz.getDeclaredAnnotations();
					for (Annotation classAnnotation : classAnnotations) {
						if (classAnnotation instanceof ApiModel) {
							content.append(
									String.format("### Class - **%s**%n", clazz.getSimpleName()));
							content.append(String.format("Property | Type | Description%n"));
							content.append(String.format("-------- | ---- | -----------%n"));
							createDocForClass(clazz, clazz);
						}
					}
				}
			}
		}
		Path markdownPath = outputPath(".md");
		Path htmlPath = outputPath(".html");
		Path wordPath = outputPath(".docx");
		Files.write(markdownPath, content.toString().getBytes(StandardCharsets.UTF_8));
		pandoc(markdownPath, htmlPath, "html5");
		pandoc(markdownPath, wordPath, "docx");
	}

	private void createDocForClass(Class<?> clazz, Class<?> leafClass) {
		if (clazz != null && clazz != Object.class) {
			Annotation[] classAnnotations = clazz.getDeclaredAnnotations();
			for (Annotation classAnnotation : classAnnotations) {
				if (alwaysDocSuperClass || classAnnotation instanceof ApiModel) {
					createDocForClass(clazz.getSuperclass(), leafClass);
					for (Field field : clazz.getDeclaredFields()) {
						Annotation[] fieldAnnotations = field.getDeclaredAnnotations();
						for (Annotation fieldAnnotation : fieldAnnotations) {
							if (fieldAnnotation instanceof ApiModelProperty) {
								ApiModelProperty apiAnnotation = (ApiModelProperty) fieldAnnotation;
								if (clazz == leafClass) {
									content.append(String.format("**%s** | **%s** |   %s  %n",
											field.getName(), field.getType().getSimpleName(),
											apiAnnotation.value()));
								} else {
									content.append(String.format("  %s   |   %s   |   %s  %n",
											field.getName(), field.getType().getSimpleName(),
											apiAnnotation.value()));
								}
								break;
							}
						}
					}
					break;
				} else {
					log.warn("{} super class will not be documented as part of {} class",
							clazz.getSimpleName(), leafClass.getSimpleName());
				}
			}
		}
	}

	private Path outputPath(String extension) throws IOException {
		String fileName = "model-doc" + extension;
		Path folderPath = Paths.get("build/generated-docs/data-classes");
		Files.createDirectories(folderPath);
		Path filePath = Paths.get(folderPath.toString(), fileName);
		return filePath;
	}

	private void pandoc(Path inputPath, Path outputPath, String outputFormat) throws Exception {
		Path pandocPath = Paths.get(config.getPandocPath());
		String inputFormat = "markdown_github";
		Path cssPath = Paths
				.get("src/main/resources/vscode-markdown-css-master", "markdown-github-pandoc.html")
				.toAbsolutePath();
		String command = String.format("%s --from=%s --to=%s --css=%s --output=%s %s", pandocPath,
				inputFormat, outputFormat, cssPath, outputPath, inputPath);
		new ProcessExecutor().commandSplit(command).redirectOutput(Slf4jStream.ofCaller().asInfo())
				.redirectError(Slf4jStream.ofCaller().asError()).timeout(60, TimeUnit.SECONDS)
				.exitValues(0).destroyOnExit().execute();
	}

}
