package eu.h2020.protective.ca.mair;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CaController {

	@GetMapping("/docs/api")
	public String restApiDocs() {
		return "ca-mair-api.html";
	}

	@GetMapping("/docs/model")
	public String jsonModelDocs() {
		return "ca-mair-model.html";
	}

}