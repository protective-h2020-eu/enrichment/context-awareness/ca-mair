package eu.h2020.protective.ca.mair;

import org.springframework.beans.factory.annotation.Autowired;

import eu.h2020.protective.ca.mair.api.model.assets.Application;
import eu.h2020.protective.ca.mair.api.model.assets.Computer;
import eu.h2020.protective.ca.mair.api.model.assets.NetworkNode;
import eu.h2020.protective.ca.mair.api.model.assets.OperatingSystem;
import eu.h2020.protective.ca.mair.api.model.core.Edge;
import eu.h2020.protective.ca.mair.api.model.core.Graph;
import eu.h2020.protective.ca.mair.api.model.core.Vertex;
import eu.h2020.protective.ca.mair.api.model.edges.HostedOn;
import eu.h2020.protective.ca.mair.model.Metadata;

@org.springframework.stereotype.Service
public class MongoDBLoad {

	@Autowired
	private MongoDB mongoDB;
	
	private Graph graph = new Graph();

	private Vertex addVertex(Vertex vertex) {
		vertex = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, vertex);
		graph.addVertex(vertex);
		return vertex;
	}

	private Edge addEdge(Edge edge) {
		edge = mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, edge.getSource(), edge.getTarget(), edge);
		graph.addVertex(edge.getSource());
		graph.addVertex(edge.getTarget());
		graph.addEdge(edge.getSource(), edge.getTarget(), edge);
		return edge;
	}

	public Graph createLargeGraph(int maxLoad) {
		for (int i = 0; i < maxLoad; i++) {
			createComputer(i);
			createNetworkNode(i);
			createApplication(i);
		}
		return graph;
	}

	private void createComputer(int i) {
		Computer computer = new Computer("Computer " + i);
		computer.getIpAddresses().add("192.168.101." + i);
		computer.setBusinessOwner("Eoin " + i);
		computer.setTechnicalOwner("John " + i);
		OperatingSystem os = new OperatingSystem("Windows " + i);
		os.setVersion("10");
		os.setPatchLevel("1");
		os = (OperatingSystem) addVertex(os);
		computer = (Computer) addVertex(computer);
		HostedOn runsOn = new HostedOn(os, computer);
		addEdge(runsOn);
	}

	private void createNetworkNode(int i) {
		NetworkNode networkNode = new NetworkNode("NetworkNode " + i);
		networkNode.getIpAddresses().add("192.168.101." + i);
		OperatingSystem os = new OperatingSystem("Ubuntu " + i);
		os.setVersion("16.10");
		os.setPatchLevel("1");
		os = (OperatingSystem) addVertex(os);
		networkNode = (NetworkNode) addVertex(networkNode);
		HostedOn runsOn = new HostedOn(os, networkNode);
		addEdge(runsOn);
	}

	private void createApplication(int i) {
		Application app = new Application("App " + i);
		app.setVersion("2016");
		app.setPatchLevel("1");
		addVertex(app);
	}

}
