package eu.h2020.protective.ca.mair.model;

import org.bson.types.ObjectId;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString(callSuper=true)
public class MatrixElement {

	private ObjectId id;
	private String keyX;
	private String keyY;
	private Double value;

	public MatrixElement(String keyX, String keyY, Double value) {
		this();
		setKeyX(keyX);
		setKeyY(keyY);
		setValue(value);
	}

}
