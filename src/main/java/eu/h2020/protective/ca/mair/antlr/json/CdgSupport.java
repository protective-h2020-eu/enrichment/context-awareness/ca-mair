package eu.h2020.protective.ca.mair.antlr.json;

import static com.google.common.collect.Iterables.concat;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.misc.Pair;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import org.reflections.Reflections;

import eu.h2020.protective.ca.mair.MongoDB;
import eu.h2020.protective.ca.mair.antlr.errors.CdgErrorListener;
import eu.h2020.protective.ca.mair.api.model.core.Edge;
import eu.h2020.protective.ca.mair.api.model.core.Vertex;
import eu.h2020.protective.ca.mair.api.model2.assets.Information;
import eu.h2020.protective.ca.mair.api.model2.assets.Software;
import eu.h2020.protective.ca.mair.api.model2.edges.DependsOn;
import eu.h2020.protective.ca.mair.api.model2.edges.Uses;
import eu.h2020.protective.ca.mair.api.model2.nodes.Node;
import eu.h2020.protective.ca.mair.api.model2.other.MissionObjective;
import eu.h2020.protective.ca.mair.api.model2.services.BusinessProcess;
import eu.h2020.protective.ca.mair.api.model2.services.ITService;
import eu.h2020.protective.ca.mair.model.Metadata;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.XSlf4j;

@XSlf4j
public class CdgSupport {

	public static final String VERTEX_MISSING_PATTERN = "Vertex with key \"{}\" is not in the database "
			+ "AND not defined correctly in the \"{}\" file.";

	private final boolean guessEdgeType;

	private final MongoDB mongoDB;

	private final Map<String, Vertex> vertices;
	private final Map<String, Map<Pair<String, String>, Edge>> edges;

	private final ParseTreeProperty<Class<? extends Vertex>> classProps;
	private final ParseTreeProperty<Vertex> vertexProps;
	private final ParseTreeProperty<String> cdgProps;

	private final CdgErrorListener errorListener;

	private int cdgIndex = 0; // FIXME Drop or change?

//	private boolean useFranklinWeights = false;

	private Map<String, Double> missionWeights;
	private Map<String, Double> vertexWeights;
	private Map<String, Double> edgeWeights;

	@Getter
	private final String originalFilename;
	@Getter
	private final File file;
	@Getter
	private final CommonTokenStream tokenStream;
	@Getter
	private final Parser parser;
	@Getter
	@Setter
	private ParseTree parseTree;
	@Getter
	@Setter
	private Vertex nameVertex;
	@Getter
	@Setter
	private String context = Metadata.DEFAULT_CONTEXT;

	public void setVertex(ParseTree node, Vertex value) {
		vertexProps.put(node, value);
	}

	public Vertex getVertex(ParseTree node) {
		return vertexProps.get(node);
	}

	public void setClass(ParseTree node, Class<? extends Vertex> value) {
		classProps.put(node, value);
	}

	public Class<? extends Vertex> getClass(ParseTree node) {
		return classProps.get(node);
	}

	public void setCdgName(ParseTree node, String value) {
		cdgProps.put(node, value);
	}

	public String getCdgName(ParseTree node) {
		return cdgProps.get(node);
	}

	public Vertex getPreviousVertex(RuleContext ctx) {
		Vertex value = getVertex(ctx);
		while (value == null) {
			ctx = ctx.getParent();
			if (ctx == null) {
				return null;
			}
			value = getVertex(ctx);
		}
		return value;
	}

	public Class<? extends Vertex> getPreviousClass(RuleContext ctx) {
		Class<? extends Vertex> value = getClass(ctx);
		while (value == null) {
			ctx = ctx.getParent();
			if (ctx == null) {
				return null;
			}
			value = getClass(ctx);
		}
		return value;
	}

	public String getPreviousCdgName(RuleContext ctx) {
		String value = getCdgName(ctx);
		while (value == null) {
			ctx = ctx.getParent();
			if (ctx == null) {
				return null;
			}
			value = getCdgName(ctx);
		}
		return value;
	}

	public CdgSupport(Map<String, Vertex> vertices, Map<String, Map<Pair<String, String>, Edge>> edges, MongoDB mongoDB,
			String originalFilename, File file, boolean guessEdgeType, Map<String, Double> missionWeights,
			Map<String, Double> vertexWeights, Map<String, Double> edgeWeights, CdgErrorListener errorListener,
			CommonTokenStream tokens, Parser parser) throws IOException {
		super();

		this.vertices = vertices;
		this.edges = edges;
		this.mongoDB = mongoDB;
		this.originalFilename = originalFilename;
		this.file = file;
		this.guessEdgeType = guessEdgeType;
		this.missionWeights = missionWeights;
		this.vertexWeights = vertexWeights;
		this.edgeWeights = edgeWeights;
		this.errorListener = errorListener;

		classProps = new ParseTreeProperty<>();
		vertexProps = new ParseTreeProperty<>();
		cdgProps = new ParseTreeProperty<>();
		if (edges != null) {
			if (edges.get(Edge.ALL_EDGES_CDG) == null) {
				edges.put(Edge.ALL_EDGES_CDG, new HashMap<>());
			}
			if (edges.get(Edge.OTHER_EDGES_CDG) == null) {
				edges.put(Edge.OTHER_EDGES_CDG, new HashMap<>());
			}
		}

		parser.setTokenStream(tokens);
		parser.removeErrorListeners();
		parser.addErrorListener(errorListener);
		this.tokenStream = tokens;
		this.parser = parser;
		// this.parseTree = parser.deps();
	}

	public Vertex getVertex(String key) {
		if (mongoDB != null) {
			return mongoDB.findVertexByKey(context, key);
		}
		if (vertices != null) {
			return vertices.get(key);
		}
		return null;
	}

	public Edge getEdge(String cdgName, String key) {
		if (mongoDB != null) {
			return mongoDB.findEdgeByKeyAndCdg(context, key, cdgName);
		}
		if (edges != null && edges.get(cdgName) != null) {
			String[] parts = key.split("-->");
			return edges.get(cdgName).get(new Pair<String, String>(parts[0], parts[1]));
		}
		return null;
	}

	public Iterable<Vertex> getVertices() {
		if (mongoDB != null) {
			return mongoDB.findAllVertices(context);
		}
		if (vertices != null) {
			return vertices.values();
		}
		return new ArrayList<>(0);
	}

	public Iterable<Edge> getEdges(String cdgName) {
		if (mongoDB != null) {
			return mongoDB.findAllEdges(context, cdgName);
		}
		if (edges != null) {
			if (edges.get(cdgName) != null) {
				return edges.get(cdgName).values();
			}
		}
		return new ArrayList<>(0);
	}

	public Iterable<Edge> getEdges() {
		if (mongoDB != null) {
			return mongoDB.findAllEdges(context);
		}
		if (edges != null) {
			Iterable<Edge> allEdges = new ArrayList<>();
			for (Entry<String, Map<Pair<String, String>, Edge>> cdgEdges : edges.entrySet()) {
				if (!cdgEdges.getKey().equals(Edge.ALL_EDGES_CDG)) {
					// XXX Do not return All Edges CDG, only every other CDG.
					// XXX All edges belong to at least two CDGs.
					allEdges = concat(allEdges, cdgEdges.getValue().values());
				}
			}
			return allEdges;
		}
		return new ArrayList<>(0);
	}

	public List<String> getCdgNames() {
		if (mongoDB != null) {
			return mongoDB.getAllCdgNames(context);
		}
		Set<String> cdgNames = new HashSet<>();
		if (edges != null) {
			for (Entry<String, Map<Pair<String, String>, Edge>> entry : edges.entrySet()) {
				String cdgName = entry.getKey();
				if (cdgName != null && entry.getValue().size() > 0) {
					cdgNames.add(cdgName);
				}
			}
		}
		return cdgNames.stream().sorted().collect(Collectors.toList());
	}

	public void initEdgeCollection(String collectionName) {
		if (mongoDB != null) {
			Metadata metadata = mongoDB.initEdgeMetadata(context, collectionName, cdgIndex); // FIXME cdgIndex is wrong?
			mongoDB.initEdgeCollection(metadata);
			cdgIndex++; // FIXME cdgIndex may be wrong!
		}
	}

	public String calculateCdg(Vertex vertex) {
		String cdgName = Edge.OTHER_EDGES_CDG;
		if (vertex instanceof MissionObjective) {
			cdgName = "missionLayer";
		} else if (vertex instanceof BusinessProcess) {
			cdgName = "operationalLayer";
		} else if (vertex instanceof Information) {
			cdgName = "operationalLayer";
		} else if (vertex instanceof ITService) {
			cdgName = "applicationLayer";
		} else if (vertex instanceof Node) {
			cdgName = "infrastructureLayer";
		} else if (vertex instanceof Software) {
			cdgName = "infrastructureLayer";
		}
		return cdgName;
	}

	public Vertex addOrUpdateFromVertex(ParseTree node, Class<? extends Vertex> clazz, String key, Token weightToken,
			Token expressionToken) {
		Vertex vertex = addOrUpdateVertex(clazz, key, weightToken, expressionToken);
		setVertex(node, vertex);
		setNameVertex(vertex);
		return vertex;
	}

	public Vertex addOrUpdateToVertex(Class<? extends Vertex> clazz, Token key) {
		Vertex vertex = addOrUpdateVertex(clazz, key.getText(), null, null);
		return vertex;
	}

	private Vertex addOrUpdateVertex(Class<? extends Vertex> clazz, String key, Token weightToken,
			Token expressionToken) {
		Vertex vertex = null;
		if (mongoDB != null) {
			vertex = mongoDB.findVertexByKey(context, key);
		}
		if (vertices != null) {
			vertex = vertices.get(key);
		}
		if (vertex == null && clazz != null) {
			try {
				vertex = (Vertex) clazz.getDeclaredConstructor().newInstance();
				vertex.setKey(key);
				vertex.setName(key);
			} catch (Exception e) {
				log.error("Cannot create Vertex of class '{}'", clazz, e);
			}
		}
		if (vertex != null && vertex.getClass() == clazz) {
			if (weightToken != null) {
				String grade = weightToken.getText();
				Double weight = getVertexWeightFromGrade(grade, vertex);
				try {
					vertex.setWeight(weight);
				} catch (Exception e) {
					errorListener.error(weightToken, "Cannot set Vertex '{}' to have weight of '{}'", vertex.getKey(),
							weightToken.getText());
				}
			}
			if (expressionToken != null) {
				String expression = expressionToken.getText();
				vertex.setExpression(expression); // TODO check is AND, OR or SINK
				if (vertex.getExpression().equals(Vertex.ERROR)) {
					errorListener.error(expressionToken, "\"{}\" is not one of \"AND\", \"OR\" or \"SINK\".",
							expression);
				}
			}
			vertex = addVertex(vertex);
			return vertex;
		}
		return null;
	}

	private Double getMissionWeightFromGrade(String grade) {
		return missionWeights.get(grade);
	}

	private Double getVertexWeightFromGrade(String grade, Vertex vertex) {
		if (vertex instanceof MissionObjective) {
			return getMissionWeightFromGrade(grade);
		}
		return vertexWeights.get(grade);
	}

	private Double getEdgeWeightFromGrade(String grade) {
		return edgeWeights.get(grade);
	}

	private Vertex addVertex(Vertex vertex) {
		if (mongoDB != null) {
			return mongoDB.insertOrUpdateOne(context, vertex); // vertex updated by repo save
		}
		if (vertices != null) {
			vertices.put(vertex.getKey(), vertex);
			return vertex; // add changed collection
		}
		return null; // add did not change collection
	}

	public Edge addOrUpdateEdge(String toCdg, RuleContext ctx, Vertex from, Vertex to, String grade) {
		return addOrUpdateEdge(toCdg, ctx, from, to, null, grade);
	}

	public Edge addOrUpdateEdge(String toCdg, RuleContext ctx, Vertex from, Vertex to, String edgeType, String grade) {
		String fromCdg = getPreviousCdgName(ctx);
		Class<?> edgeClass = null;
		Edge edge = null;
		if (edgeType == null) {
			edgeClass = getEdgeSubClass(from, to);
		} else {
			edgeClass = getEdgeSubClass(from, to, edgeType);
		}
		if (mongoDB != null) {
			for (Edge it : mongoDB.findEdgesByFromAndTo(context, from.getKey(), to.getKey(), fromCdg)) {
				if (it != null) {
					edge = it; // XXX find first match
					break;
				}
			}
		}
		if (edges != null && edges.containsKey(fromCdg)) {
			Map<Pair<String, String>, Edge> cdgEdges = edges.get(fromCdg);
			Pair<String, String> key = new Pair<>(from.getKey(), to.getKey());
			Edge it = cdgEdges.get(key);
			if (it != null) {
				edge = it; // XXX find first match
			}
		}
		if (edge == null && edgeClass != null) {
			Constructor<?> constructor = getEdgeConstructors(from, to, edgeClass).get(0);
			try {
				edge = (Edge) constructor.newInstance(from, to);
			} catch (Exception e) {
				log.error("Cannot create Edge of class '{}'", edgeClass, e);
			}
		}
		if (edge != null && edge.getClass() == edgeClass) {
			edge.setFromCdg(fromCdg);
			edge.setToCdg(toCdg);
			edge.setSource(from);
			edge.setTarget(to);
			edge.setGrade(grade);
			edge.setWeight(getEdgeWeightFromGrade(grade));
			edge = addEdge(edge);
			return edge;
		}
		return null;
	}

	private Class<?> getEdgeSubClass(Vertex from, Vertex to) {
		return getEdgeConstructor(from, to, null).getDeclaringClass();
	}

	private Class<?> getEdgeSubClass(Vertex from, Vertex to, String edgeType) {
		return getEdgeConstructor(from, to, edgeType).getDeclaringClass();
	}

	private Constructor<?> getEdgeConstructor(Vertex from, Vertex to, String edgeType) {
		if (!guessEdgeType && edgeType == null) {
			return getEdgeConstructors(from, to, DependsOn.class).get(0);
		}
		final String edgesPackage = Uses.class.getPackage().getName();
		Reflections reflections = new Reflections(edgesPackage);
		Set<Class<? extends Edge>> edgeClasses = reflections.getSubTypesOf(Edge.class);
		List<Constructor<?>> matchingConstructors = new ArrayList<>();
		for (Class<?> clazz : edgeClasses) {
			List<Constructor<?>> constructors = getEdgeConstructors(from, to, clazz);
			for (Constructor<?> constructor : constructors) {
				Class<?> paramType0 = constructor.getParameterTypes()[0];
				Class<?> paramType1 = constructor.getParameterTypes()[1];
				if (paramType0 != Vertex.class && paramType1 != Vertex.class) {
					if (edgeType == null || clazz.getName().endsWith(edgeType)) {
						matchingConstructors.add(constructor);
					}
				}
			}
		}
		if (matchingConstructors.size() > 1) {
			log.warn("getEdgeConstructor: START Duplicates");
			for (Constructor<?> constructor : matchingConstructors) {
				String name = constructor.getDeclaringClass().getSimpleName();
				String param0 = constructor.getParameterTypes()[0].getSimpleName();
				String param1 = constructor.getParameterTypes()[1].getSimpleName();
				log.warn("getEdgeConstructor: Duplicate Signature = {}({}, {})", name, param0, param1);
			}
			// log.warn("getEdgeConstructor: END Duplicates");
		}
		if (matchingConstructors.size() != 1) {
			return getEdgeConstructors(from, to, DependsOn.class).get(0);
		}
		return matchingConstructors.get(0);
	}

	private Edge addEdge(Edge edge) {
		if (mongoDB != null) {
			return mongoDB.insertOrUpdateOne(context, edge.getSource(), edge.getTarget(), edge); // edge updated by save
		}
		if (edges != null) {
			String fromCdg = edge.getFromCdg();
			String toCdg = edge.getToCdg();
			addEdge(edge, Edge.ALL_EDGES_CDG, Edge.ALL_EDGES_CDG);
			return addEdge(edge, fromCdg, toCdg);
		}
		return null; // add did not change collection
	}

	private Edge addEdge(Edge edge, String fromCdg, String toCdg) {
		edge.setFromCdg(fromCdg);
		edge.setToCdg(toCdg);
		Pair<String, String> key = new Pair<>(edge.getFrom(), edge.getTo());
		Map<Pair<String, String>, Edge> cdgEdges = edges.get(edge.getFromCdg());
		if (cdgEdges == null) {
			cdgEdges = new HashMap<>();
			edges.put(edge.getFromCdg(), cdgEdges);
		}
		cdgEdges.put(key, edge);
		return edge;
	}

	private List<Constructor<?>> getEdgeConstructors(Vertex vertexFrom, Vertex vertexTo, Class<?> clazz) {
		Constructor<?>[] constructors = clazz.getConstructors();
		List<Constructor<?>> matchingConstructors = new ArrayList<>();
		for (Constructor<?> constructor : constructors) {
			Class<?>[] params = constructor.getParameterTypes();
			if (params.length == 2) {
				Class<?> paramFrom = params[0];
				Class<?> paramTo = params[1];
				if (paramFrom.isAssignableFrom(vertexFrom.getClass())) {
					if (paramTo.isAssignableFrom(vertexTo.getClass())) {
						matchingConstructors.add(constructor);
					}
				}
			}
		}
		return matchingConstructors;
	}

}
