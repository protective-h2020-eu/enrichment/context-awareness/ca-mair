package eu.h2020.protective.ca.mair.antlr.errors;

import java.util.ArrayList;
import java.util.List;

import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.Token;
import org.slf4j.helpers.MessageFormatter;

import lombok.Getter;
import lombok.extern.slf4j.XSlf4j;

@XSlf4j
public class CdgErrorListener extends BaseErrorListener {
	/**
	 * Provides a default instance of {@link CdgErrorListener}.
	 */
	public static final CdgErrorListener INSTANCE = new CdgErrorListener();

	@Getter
	private List<String> errors = new ArrayList<>();
	
	private boolean hasSyntaxError = false;
	
	public boolean hasSyntaxError() {
		return hasSyntaxError;
	}
	
	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * This implementation logs messages and adds them to a {@code List<String>}
	 * containing the values of {@code line}, {@code charPositionInLine}, and
	 * {@code msg} using the following format.
	 * </p>
	 *
	 * <pre>
	 * line <em>line</em>:<em>charPositionInLine</em> <em>msg</em>
	 * </pre>
	 */
	@Override
	public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line,
			int charPositionInLine, String msg, RecognitionException e) {
		hasSyntaxError = true;
		error(line, charPositionInLine, msg);
	}
	
	public void error(Token token, String format, Object... args) {
		error(token.getLine(), token.getCharPositionInLine(), format, args);
	}

	public void error(int line, int charPositionInLine, String format, Object... args) {
		format = "line " + line + ":" + charPositionInLine + " " + format;
		error(format, args);
	}

	public void error(String format, Object... args) {
		String message = MessageFormatter.arrayFormat(format, args).getMessage();
		log.warn(message);
		errors.add(message);
	}

}
