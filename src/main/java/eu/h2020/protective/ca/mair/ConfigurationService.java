package eu.h2020.protective.ca.mair;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Configuration
@ConfigurationProperties(prefix = "ca-mair")
@Data
public class ConfigurationService {

	private String matrixCsvFilename;
	private String pandocPath;
	private String caAsUrl;
	
}
