package eu.h2020.protective.ca.mair;

import static com.google.common.collect.Iterables.concat;
import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.in;
import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.currentDate;
import static com.mongodb.client.model.Updates.set;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.bson.BsonValue;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.IndexOptions;
import com.mongodb.client.model.Indexes;
import com.mongodb.client.model.ReplaceOptions;
import com.mongodb.client.result.UpdateResult;

import eu.h2020.protective.ca.mair.api.model.core.Edge;
import eu.h2020.protective.ca.mair.api.model.core.Graph;
import eu.h2020.protective.ca.mair.api.model.core.Vertex;
import eu.h2020.protective.ca.mair.api.model2.assets.Software;
import eu.h2020.protective.ca.mair.api.model2.nodes.Node;
import eu.h2020.protective.ca.mair.model.MatrixElement;
import eu.h2020.protective.ca.mair.model.Metadata;
import eu.h2020.protective.ca.mair.model.Tuple;
import lombok.extern.slf4j.XSlf4j;

@XSlf4j
@Component
public class MongoDB {

	@Value("${spring.data.mongodb.uri}")
	private String mongoClientURI;

	private MongoClient mongoClient;
	private MongoDatabase database;

	private MongoCollection<Metadata> metadataCollection = null;
	private Map<String, Map<String, Metadata>> vertexMetadataCache = new HashMap<>();
	private Map<String, Map<String, Metadata>> edgeMetadataCache = new HashMap<>();

	private Map<String, MongoCollection<Vertex>> vertexCollectionCache = new HashMap<>();
	private Map<String, MongoCollection<Document>> vertexDocCollectionCache = new HashMap<>();
	private Map<String, MongoCollection<Edge>> edgeCollectionCache = new HashMap<>();

	private MongoCollection<MatrixElement> matrixCollection;

	private static final ReplaceOptions upsert = new ReplaceOptions().upsert(true);

	private static final IndexOptions unique = new IndexOptions().unique(true);
	private static final IndexOptions nonUnique = new IndexOptions().unique(false);

	@PostConstruct
	private void init() {
		CodecRegistry pojoCodecRegistry = fromRegistries(MongoClient.getDefaultCodecRegistry(),
				fromProviders(PojoCodecProvider.builder().automatic(true).build()));
		MongoClientURI clientURI = new MongoClientURI(mongoClientURI);
		mongoClient = new MongoClient(clientURI);
		String databaseName = clientURI.getDatabase();
		if (databaseName != null) {
			database = mongoClient.getDatabase(databaseName).withCodecRegistry(pojoCodecRegistry);

			initExistingMetadataCollection();

			initExistingVertexCollections();
			initExistingEdgeCollections();

			// FIXME index matrices some way
			matrixCollection = database.getCollection("matrix", MatrixElement.class);
		}
	}

	@PreDestroy()
	private void destroy() {
		mongoClient.close();
	}

	private MongoCollection<Vertex> getVertexCollection(String context) {
		for (Metadata metadata : findAllVertexMetadata(context)) {
			if (metadata != null) {
				MongoCollection<Vertex> collection = vertexCollectionCache.get(metadata.getFullCollectionName());
				if (collection != null) {
					return collection;
				}
			}
		}
		return initVertexCollection(initVertexMetadata(context, Metadata.DEFAULT_VERTEX));
	}

	private MongoCollection<Document> getVertexDocCollection(String context) {
		for (Metadata metadata : findAllVertexMetadata(context)) {
			if (metadata != null) {
				MongoCollection<Document> collection = vertexDocCollectionCache.get(metadata.getFullCollectionName());
				if (collection != null) {
					return collection;
				}
			}
		}
		return null;
	}

	private MongoCollection<Edge> getEdgeCollection(String context, String cdgName) {
		Metadata metadata = findEdgeMetadataByCdg(context, cdgName);
		if (metadata != null) {
			MongoCollection<Edge> collection = edgeCollectionCache.get(metadata.getFullCollectionName());
			if (collection != null) {
				return collection;
			}
		}
		metadata = initEdgeMetadata(context, cdgName, 0); // FIXME drop order
		return initEdgeCollection(metadata);
	}

	private String getEdgeCollectionName(String context, String cdgName) {
		Metadata metadata = findEdgeMetadataByCdg(context, cdgName);
		if (metadata != null) {
			return metadata.getFullCollectionName();
		}
		return null;
	}

	public List<String> getAllCdgNames(String context) {
		Set<String> cdgNames = new HashSet<>();
		for (Metadata metadata : findAllEdgeMetadata(context)) {
			String cdgName = metadata.getName();
			String fullCollectionName = metadata.getFullCollectionName();
			MongoCollection<Edge> collection = edgeCollectionCache.get(fullCollectionName);
			if (collection != null && collection.countDocuments() > 0) {
				if (cdgName != null) {
					cdgNames.add(cdgName);
				}
			}
		}
		return cdgNames.stream().sorted().collect(Collectors.toList());
	}

	public List<String> getNormalCdgNames(String context) {
		Set<String> cdgNames = new HashSet<>();
		for (Metadata metadata : findAllEdgeMetadata(context)) {
			String cdgName = metadata.getName();
			if (cdgName == null) {
				continue;
			}
			if (cdgName.equals(Edge.ALL_EDGES_CDG)) {
				continue;
			}
			if (cdgName.equals(Edge.OTHER_EDGES_CDG)) {
				continue;
			}
			if (cdgName.equals(Edge.SINK_CDG)) {
				continue;
			}
			String fullCollectionName = metadata.getFullCollectionName();
			MongoCollection<Edge> collection = edgeCollectionCache.get(fullCollectionName);
			if (collection != null && collection.countDocuments() > 0) {
				cdgNames.add(cdgName);
			}
		}
		return cdgNames.stream().sorted().collect(Collectors.toList());
	}

	private void updateMetadataCache(Metadata metadata, Map<String, Map<String, Metadata>> cache) {
		Map<String, Metadata> names = cache.get(metadata.getContext());
		if (names == null) {
			names = new HashMap<>();
		}
		names.put(metadata.getName(), metadata);
		cache.put(metadata.getContext(), names);
	}

//	private void deleteFromMetadataCache(Metadata metadata, Map<String, Map<String, Metadata>> cache) {
//		Map<String, Metadata> names = cache.get(metadata.getContext());
//		if (names == null) {
//			return;
//		}
//		names.remove(metadata.getName(), metadata);
//		if (names.isEmpty()) {
//			cache.remove(metadata.getContext(), names);	
//		}
//	}

	private void initExistingMetadataCollection() {
		initMetadataCollection();
		for (Metadata metadata : metadataCollection.find(eq("type", Metadata.VERTEX))) {
			updateMetadataCache(metadata, vertexMetadataCache);
		}
		for (Metadata metadata : metadataCollection.find(eq("type", Metadata.EDGE))) {
			updateMetadataCache(metadata, edgeMetadataCache);
		}
	}

	private void initExistingVertexCollections() {
		for (Metadata metadata : findAllVertexMetadata(Metadata.ALL_CONTEXTS)) {
			initVertexCollection(metadata);
		}
	}

	private void initExistingEdgeCollections() {
		Set<String> contexts = new HashSet<>();
		for (Metadata metadata : findAllEdgeMetadata(Metadata.ALL_CONTEXTS)) {
			initEdgeCollection(metadata);
			contexts.add(metadata.getContext());
		}
		for (String context : contexts) {
			Metadata allEdgesMetadata = initEdgeMetadata(context, Edge.ALL_EDGES_CDG, 0); // FIXME drop order
			initEdgeCollection(allEdgesMetadata);
			Metadata otherEdgesMetadata = initEdgeMetadata(context, Edge.OTHER_EDGES_CDG, 0); // FIXME drop order
			initEdgeCollection(otherEdgesMetadata); // XXX Default implicit CDG
		}
	}

	private void initMetadataCollection() {
		metadataCollection = database.getCollection("metadata", Metadata.class);
		metadataCollection.createIndex(Indexes.ascending("context", "name"), unique);
		for (String collectionName : database.listCollectionNames()) {
			if (collectionName.endsWith("{{" + Metadata.VERTEX + "}}")) {
				String name = getNameFromCollectionName(collectionName);
				initVertexMetadata(Metadata.DEFAULT_CONTEXT, name);
			} else if (collectionName.endsWith("{{" + Metadata.EDGE + "}}")) {
				String name = getNameFromCollectionName(collectionName);
				initEdgeMetadata(Metadata.DEFAULT_CONTEXT, name, 0);
			}
		}
	}

	private String getNameFromCollectionName(String collectionName) {
		if (collectionName.contains(Edge.ALL_EDGES_CDG)) {
			return Edge.ALL_EDGES_CDG;
		}
		if (collectionName.contains(Edge.OTHER_EDGES_CDG)) {
			return Edge.OTHER_EDGES_CDG;
		}
//		if (collectionName.contains(Edge.SINK_CDG)) {
//			return Edge.SINK_CDG;
//		}
		return collectionName.replaceAll("\\{\\{.*?\\}\\}", "");
	}

	public Metadata initVertexMetadata(String context, String name) {
		Metadata metadata = new Metadata(context, name); // XXX order signifies Edge, otherwise Vertex
		updateMetadataCache(metadata, vertexMetadataCache);
		return insertOrUpdateOne(metadata);
	}

	public Metadata initEdgeMetadata(String context, String name, int order) {
		Metadata metadata = new Metadata(context, name, order); // XXX order signifies Edge, otherwise Vertex
		updateMetadataCache(metadata, edgeMetadataCache);
		return insertOrUpdateOne(metadata);
	}

	public MongoCollection<Vertex> initVertexCollection(Metadata metadata) {
		MongoCollection<Vertex> collection = database.getCollection(metadata.getFullCollectionName(), Vertex.class);
		collection.createIndex(Indexes.ascending("key"), unique);
		initVertexDocCollection(collection);
		vertexCollectionCache.put(metadata.getFullCollectionName(), collection);
		return collection;
	}

	private MongoCollection<Document> initVertexDocCollection(MongoCollection<Vertex> vertexCollection) {
		MongoCollection<Document> vertexDocCollection = vertexCollection.withDocumentClass(Document.class);
		String collectionName = vertexCollection.getNamespace().getCollectionName();
		vertexDocCollectionCache.put(collectionName, vertexDocCollection);
		return vertexDocCollection;
	}

	public MongoCollection<Edge> initEdgeCollection(Metadata metadata) {
		MongoCollection<Edge> collection = database.getCollection(metadata.getFullCollectionName(), Edge.class);
		collection.createIndex(Indexes.ascending("key"), unique);
		collection.createIndex(Indexes.ascending("to", "from"), nonUnique); // FIXME is this the right direction?
		collection.createIndex(Indexes.ascending("to"), nonUnique);
		collection.createIndex(Indexes.ascending("from"), nonUnique);
		edgeCollectionCache.put(metadata.getFullCollectionName(), collection);
		return collection;
	}

	private void clearAllMatrices() {
		matrixCollection.deleteMany(new Document()); // TODO drop instead of deleteMany?
	}

	private void clearAllGraphs() {
		for (String collectionName : database.listCollectionNames()) {
			if (collectionName.endsWith("{{" + Metadata.VERTEX + "}}")) {
				MongoCollection<Vertex> collection = database.getCollection(collectionName, Vertex.class);
				collection.deleteMany(new Document());
				// collection.drop();
			} else if (collectionName.endsWith("{{" + Metadata.EDGE + "}}")) {
				MongoCollection<Edge> collection = database.getCollection(collectionName, Edge.class);
				collection.deleteMany(new Document());
				// collection.drop();
			}
		}

//		edgeCollectionCache.clear();
//		vertexCollectionCache.clear();
//
//		metadataCollection.drop();
//		edgeMetadataCache.clear();
//		vertexMetadataCache.clear();
	}

//	private void deleteCollection(Metadata metadata, List<Metadata> metadataList) {
//		MongoCollection<Vertex> collection = vertexCollectionCache.get(metadata.getFullCollectionName());
//		if (collection != null) {
//			collection.drop();
//		}
//		vertexCollectionCache.remove(metadata.getFullCollectionName());
//		metadataCollection.deleteOne(eq("id", metadata.getId()));
//		metadataList.add(metadata);
//	}

	public void clearAllMatricesAndGraphs(String context) {
		// FIXME transaction required?
		clearAllMatrices();
		clearAllGraphs();
	}

	// Delete vertex from DB if no longer referenced by a CDG, otherwise decrementCdgCount and update DB.
	public Vertex deleteVertex(String context, Vertex vertex) {
		if (vertex != null) {
			int count = vertex.decrementCdgCount();
			if (count > 0) {
				insertOrUpdateOne(context, vertex);
			} else {
				getVertexCollection(context).deleteOne(eq("key", vertex.getKey()));
			}
		}
		return vertex;
	}

	public Edge deleteEdge(String context, Edge edge) {
		if (edge != null) {
			// Edge belongs to ALL_EDGES_CDG and one other CDG only.
			getEdgeCollection(context, edge.getFromCdg()).deleteOne(eq("key", edge.getKey()));
			getEdgeCollection(context, Edge.ALL_EDGES_CDG).deleteOne(eq("key", edge.getKey()));
			for (String vKey : edge.getVertices()) {
				Vertex vertex = findVertexByKey(context, vKey);
				if (vertex != null) { // FIXME Why delete twice? XXXXXXXXXXXXXXXXXXXXXXXXXXX
					deleteVertex(context, vertex);
					deleteVertex(context, vertex);
				}
			}
		}
		return edge;
	}

	public Metadata insertOrUpdateOne(Metadata metadata) {
		if (metadata != null) {
			UpdateResult result = metadataCollection.replaceOne( // FIXME include type?
					and(eq("context", metadata.getContext()), eq("name", metadata.getName())), metadata, upsert);
			BsonValue upsertedId = result.getUpsertedId();
			if (upsertedId != null) {
				metadata.setId(upsertedId.asObjectId().getValue());
			}
		}
		return metadata;
	}

	public Vertex insertOrUpdateOne(String context, Vertex vertex) {
		if (vertex != null) {
			vertex.setLastModified(new Date()); // FIXME preserve cdgCount?
			UpdateResult result = getVertexCollection(context).replaceOne(eq("key", vertex.getKey()), vertex, upsert);
			BsonValue upsertedId = result.getUpsertedId();
			if (upsertedId != null) {
				vertex.setId(upsertedId.asObjectId().getValue());
			}
		}
		return vertex;
	}

	public Edge insertOrUpdateOne(String context, Vertex sourceVertex, Vertex targetVertex, Edge edge) {
		edge.setFrom(sourceVertex.getKey());
		edge.setTo(targetVertex.getKey());
		edge.setSource(sourceVertex);
		edge.setTarget(targetVertex);
		String fromCdg = edge.getFromCdg();
		String toCdg = edge.getToCdg();
		insertOrUpdateOne(context, edge, Edge.ALL_EDGES_CDG, Edge.ALL_EDGES_CDG);
		return insertOrUpdateOne(context, edge, fromCdg, toCdg);
	}

	private Edge insertOrUpdateOne(String context, Edge edge, String fromCdg, String toCdg) {
		if (edge != null) {
			Edge previous = findEdgeByKeyAndCdg(context, edge.getKey(), fromCdg);
			if (previous == null) {
				edge.setId(null);
			} else {
				edge.setId(previous.getId());
			}
			edge.setFromCdg(fromCdg);
			edge.setToCdg(toCdg);
			edge.setLastModified(new Date());
			Vertex from = edge.getSource();
			from.incrementCdgCount();
			insertOrUpdateOne(context, from);
			Vertex to = edge.getTarget();
			to.incrementCdgCount();
			insertOrUpdateOne(context, to);
			UpdateResult result = getEdgeCollection(context, fromCdg).replaceOne(eq("key", edge.getKey()), edge,
					upsert);
			BsonValue upsertedId = result.getUpsertedId();
			if (upsertedId != null) {
				edge.setId(upsertedId.asObjectId().getValue());
			}
		}
		return edge;
	}

	public MatrixElement insertOrUpdateOne(MatrixElement element) {
		if (element != null) {
			// TODO element.setLastModified(new Date());
			UpdateResult result = matrixCollection
					.replaceOne(and(eq("keyX", element.getKeyX()), eq("keyY", element.getKeyY())), element, upsert);
			BsonValue upsertedId = result.getUpsertedId();
			if (upsertedId != null) {
				element.setId(upsertedId.asObjectId().getValue());
			}
		}
		return element;
	}

	public void insertOrUpdateMany(String context, Vertex... vertices) {
		if (vertices != null) {
			for (Vertex vertex : vertices) {
				insertOrUpdateOne(context, vertex);
			}
		}
	}

	public void insertOrUpdateMany(String context, Edge... edges) {
		if (edges != null) {
			for (Edge edge : edges) {
				insertOrUpdateOne(context, edge.getSource(), edge.getTarget(), edge);
			}
		}
	}

	public void insertOrUpdateManyVertices(String context, List<Vertex> vertices) {
		if (vertices != null) {
			for (Vertex vertex : vertices) {
				insertOrUpdateOne(context, vertex);
			}
		}
	}

	public void insertOrUpdateManyEdges(String context, List<Edge> edges) {
		if (edges != null) {
			for (Edge edge : edges) {
				insertOrUpdateOne(context, edge.getSource(), edge.getTarget(), edge);
			}
		}
	}

	public Iterable<Metadata> findAllVertexMetadata(String context) {
		return findAllMetadata(context, vertexMetadataCache);
	}

	public Iterable<Metadata> findAllEdgeMetadata(String context) {
		return findAllMetadata(context, edgeMetadataCache);
	}

	private Iterable<Metadata> findAllMetadata(String context, Map<String, Map<String, Metadata>> cache) {
		Collection<Metadata> metadata = new ArrayList<>();
		if (context != null && context !=  Metadata.ALL_CONTEXTS) {
			Map<String, Metadata> entry = cache.get(context);
			if (entry != null) {
				metadata.addAll(entry.values());
			}
			return metadata;
		}
		for (Map<String, Metadata> entry : cache.values()) {
			if (entry != null) {
				metadata.addAll(entry.values());
			}
		}
		return metadata;
	}

	public Set<String> findAllContextNames() {
		Set<String> contexts = new HashSet<>();
		contexts.addAll(vertexMetadataCache.keySet());
		contexts.addAll(edgeMetadataCache.keySet());
		return contexts;
	}

	public List<MongoCollection<Vertex>> findAllVertexCollections(String context) {
		List<MongoCollection<Vertex>> collections = new ArrayList<>();
		for (Metadata metadata : findAllVertexMetadata(context)) {
			MongoCollection<Vertex> collection = vertexCollectionCache.get(metadata.getFullCollectionName());
			if (collection != null) {
				collections.add(collection);
			} else {
				log.warn("No collection matches the metadata for collection name '{}'",
						metadata.getFullCollectionName());
			}
		}
		return collections;
	}

	public List<MongoCollection<Edge>> findAllEdgeCollections(String context) {
		List<MongoCollection<Edge>> collections = new ArrayList<>();
		for (Metadata metadata : findAllEdgeMetadata(context)) {
			MongoCollection<Edge> collection = edgeCollectionCache.get(metadata.getFullCollectionName());
			if (collection != null) {
				collections.add(collection);
			} else {
				log.warn("No collection matches the metadata for collection name '{}'",
						metadata.getFullCollectionName());
			}
		}
		return collections;
	}

	public Metadata findEdgeMetadataByCdg(String context, String cdgName) {
		if (context != null) { // Metadata.ALL_CONTEXTS) {
			Map<String, Metadata> metadataByContext = edgeMetadataCache.get(context);
			if (metadataByContext != null) {
				return metadataByContext.get(cdgName);
			}
		}
		return null;
	}

	public Iterable<Vertex> findAllVertices(String context) {
		return getVertexCollection(context).find();
	}

	private Edge fixEdge(String context, Edge edge) {
		if (edge.getFrom() == null) {
			throw new NullPointerException(
					String.format("context = '%s', from = '%s', to = '%s', fromCdg = '%s', toCdg = '%s'", context,
							edge.getFrom(), edge.getTo(), edge.getFromCdg(), edge.getToCdg()));
		}
		if (edge.getTo() == null) {
			throw new NullPointerException(
					String.format("context = '%s', from = '%s', to = '%s', fromCdg = '%s', toCdg = '%s'", context,
							edge.getFrom(), edge.getTo(), edge.getFromCdg(), edge.getToCdg()));
		}
		Vertex source = findVertexByKey(context, edge.getFrom());
		Vertex target = findVertexByKey(context, edge.getTo());
		if (source == null) {
			throw new NullPointerException(
					String.format("context = '%s', from = '%s', to = '%s', fromCdg = '%s', toCdg = '%s'", context,
							edge.getFrom(), edge.getTo(), edge.getFromCdg(), edge.getToCdg()));
		}
		if (target == null) {
			throw new NullPointerException(
					String.format("context = '%s', from = '%s', to = '%s', fromCdg = '%s', toCdg = '%s'", context,
							edge.getFrom(), edge.getTo(), edge.getFromCdg(), edge.getToCdg()));
		}
		edge.setSource(source);
		edge.setTarget(target);
		return edge;
	}

	public Iterable<Edge> findAllEdges(String context, String cdgName) {
		List<Edge> edges = new ArrayList<>();
		for (Edge edge : getEdgeCollection(context, cdgName).find()) {
			edges.add(fixEdge(context, edge));
		}
		return edges;
	}

	public Iterable<Edge> findAllEdges(String context) { // FIXME rewrite for source, target
		List<Edge> edges = new ArrayList<>();
		for (MongoCollection<Edge> edgeCollection : findAllEdgeCollections(context)) {
			String left = edgeCollection.getNamespace().getCollectionName();
			String right = getEdgeCollection(context, Edge.ALL_EDGES_CDG).getNamespace().getCollectionName();
			if (!left.equals(right)) {
				// XXX Do not return All Edges CDG, only every other CDG.
				// XXX All edges belong to at least two CDGs.
				for (Edge edge : edgeCollection.find()) {
					edges.add(fixEdge(context, edge));
				}
			}
		}
		return edges;
	}

	public Vertex findVertexByKey(String context, String key) {
		return getVertexCollection(context).find(eq("key", key)).first();
	}

	public Edge findEdgeByKeyAndCdg(String context, String key, String cdgName) {
		Edge edge = getEdgeCollection(context, cdgName).find(eq("key", key)).first();
		if (edge != null) {
			return fixEdge(context, edge);
		}
		return null;
	}

	public Edge findEdgeByKey(String context, String key) {
		for (String cdgName : getAllCdgNames(context)) {
			// TODO what if same key in multiple collections. Should NOT be allowed!
			Edge edge = findEdgeByKeyAndCdg(context, key, cdgName);
			if (edge != null) {
				return edge;
			}
		}
		return null;
	}

	public Iterable<Edge> findEdgesByTo(String context, String to, String cdgName) {
		List<Edge> edges = new ArrayList<>();
		for (Edge edge : getEdgeCollection(context, cdgName).find(eq("to", to))) {
			edges.add(fixEdge(context, edge));
		}
		return edges;
	}

	public Iterable<Edge> findEdgesByTo(String context, String to) {
		Iterable<Edge> edges = Collections.emptyList();
		for (String cdgName : getAllCdgNames(context)) {
			edges = concat(edges, findEdgesByTo(context, to, cdgName));
		}
		return edges;
	}

	public Iterable<Edge> findEdgesByToAndName(String context, String to, String name, String cdgName) {
		List<Edge> edges = new ArrayList<>();
		for (Edge edge : getEdgeCollection(context, cdgName).find(and(eq("to", to), eq("name", name)))) {
			edges.add(fixEdge(context, edge));
		}
		return edges;
	}

	public Iterable<Edge> findEdgesByToAndName(String context, String to, String name) {
		Iterable<Edge> edges = Collections.emptyList();
		for (String cdgName : getAllCdgNames(context)) {
			edges = concat(edges, findEdgesByToAndName(context, to, name, cdgName));
		}
		return edges;
	}

	public Iterable<Edge> findEdgesByFromAndTo(String context, String from, String to, String cdgName) {
		List<Edge> edges = new ArrayList<>();
		for (Edge edge : getEdgeCollection(context, cdgName).find(and(eq("from", from), eq("to", to)))) {
			edges.add(fixEdge(context, edge));
		}
		return edges;
	}

	public Iterable<Edge> findEdgesByFromAndTo(String context, String from, String to) {
		Iterable<Edge> edges = Collections.emptyList();
		for (String cdgName : getAllCdgNames(context)) {
			edges = concat(edges, findEdgesByFromAndTo(context, from, to, cdgName));
		}
		return edges;
	}

	public MatrixElement findByKeyXAndKeyY(String keyX, String keyY) {
		return matrixCollection.find(and(eq("keyX", keyX), eq("keyY", keyY))).first();
	}

	public Iterable<Vertex> findVerticesByType(String context, Class<? extends Vertex> clazz) {
		return getVertexCollection(context).find(eq("type", clazz.getName()));
	}

	public Iterable<Vertex> findVerticesByIpAddress(String context, String ipAddress) {
		return getVertexCollection(context).find(in("ipAddresses", ipAddress));
	}

	public Iterable<Vertex> findVerticesByDns(String context, String dns) {
		return getVertexCollection(context).find(eq("dns", dns));
	}

	public Double updateVertexCollection(String context, ObjectId id, Double oc) throws Exception {
		getVertexCollection(context).updateOne(eq("_id", id), set("weight", oc));
		return oc;
	}

	public Double updateEdgeCollection(String context, ObjectId id, Double oc, String cdgName) throws Exception {
		getEdgeCollection(context, cdgName).updateOne(eq("_id", id),
				combine(set("weight", oc), currentDate("lastModified")));
		return oc;
	}

	public Tuple<Set<Vertex>, Set<Edge>> getVerticesAndEdgesSupportingVertex(String context,
			Class<? extends Vertex> verticesType, String vertexKey) {
		Tuple<Set<Vertex>, Set<Edge>> result = new Tuple<>(new HashSet<>(), new HashSet<>());
		for (String cdgName : getAllCdgNames(context)) { // TODO Optimize!
			Tuple<Set<Vertex>, Set<Edge>> partialResult = getVerticesAndEdgesSupportingVertex(context, verticesType,
					vertexKey, cdgName);
			result.a.addAll(partialResult.a);
			result.b.addAll(partialResult.b);
		}
		return result;
	}

	private Tuple<Set<Vertex>, Set<Edge>> getVerticesAndEdgesSupportingVertex(String context,
			Class<? extends Vertex> verticesType, String vertexKey, String cdgName) {
		final String collectionName = getEdgeCollectionName(context, cdgName);
		final String KEY = "key";
		final String UP = "to";
		final String DOWN = "from";
		AggregateIterable<Document> docs = getVertexDocCollection(context)
				.aggregate(Arrays.asList(Aggregates.match(eq("key", vertexKey)),
						Aggregates.graphLookup(collectionName, "$key", DOWN, UP, "listOfEdges"),
						Aggregates.unwind("$listOfEdges")));
		Set<Vertex> vertices = new HashSet<>();
		Set<Edge> edges = new HashSet<>();
		for (Document doc : docs) {
			Document edgeDoc = (Document) doc.get("listOfEdges");
			String eKey = edgeDoc.getString(KEY);
			String vKey = edgeDoc.getString(DOWN);
			Edge edge = findEdgeByKey(context, eKey);
			Vertex vertex = findVertexByKey(context, vKey);
			if (edge != null && vertex != null && vertex.getType().equals(verticesType.getName())) {
				vertices.add(vertex);
				edges.add(edge);
			}
		}
		return new Tuple<>(vertices, edges);
	}

	public Set<Vertex> getSoftwareDependingOnNode(String context, Node node) {
		return getVerticesDependingOnVertex(context, Software.class, node.getKey(), Edge.ALL_EDGES_CDG);
	}

	public Set<String> getVerticesDependingOnNodeByIpAddress(String context, Class<? extends Vertex> verticesType,
			String ipAddress) {
		return getVerticesDependingOnNodeByIpAddress(context, verticesType, ipAddress, Edge.ALL_EDGES_CDG);
	}

	public Set<String> getVerticesDependingOnNodeByDns(String context, Class<? extends Vertex> verticesType,
			String dns) {
		return getVerticesDependingOnNodeByDns(context, verticesType, dns, Edge.ALL_EDGES_CDG);
	}

	public Set<String> getVerticesDependingOnVertex(String context, Class<? extends Vertex> verticesType,
			String vertexKey) {
		return getVertexNamesDependingOnVertex(context, verticesType, vertexKey, Edge.ALL_EDGES_CDG);
	}

	private Set<String> getVerticesDependingOnNodeByIpAddress(String context, Class<? extends Vertex> verticesType,
			String ipAddress, String cdgName) {
		final String collectionName = getEdgeCollectionName(context, cdgName);
		final String UP = "to";
		final String DOWN = "from";
		AggregateIterable<Document> docs = getVertexDocCollection(context)
				.aggregate(Arrays.asList(Aggregates.match(in("ipAddresses", ipAddress)),
						Aggregates.graphLookup(collectionName, "$key", UP, DOWN, "listOfEdges"),
						Aggregates.unwind("$listOfEdges")));
		Set<String> vertexNames = new HashSet<>();
		for (Document doc : docs) {
			Document edge = (Document) doc.get("listOfEdges");
			String vKey = edge.getString(UP);
			Vertex vertex = findVertexByKey(context, vKey);
			if (vertex != null && vertex.getType().equals(verticesType.getName())) {
				vertexNames.add(vKey);
			}
		}
		return vertexNames;
	}

	private Set<String> getVerticesDependingOnNodeByDns(String context, Class<? extends Vertex> verticesType,
			String dns, String cdgName) {
		final String collectionName = getEdgeCollectionName(context, cdgName);
		final String UP = "to";
		final String DOWN = "from";
		AggregateIterable<Document> docs = getVertexDocCollection(context)
				.aggregate(Arrays.asList(Aggregates.match(eq("dns", dns)),
						Aggregates.graphLookup(collectionName, "$key", UP, DOWN, "listOfEdges"),
						Aggregates.unwind("$listOfEdges")));
		Set<String> vertexNames = new HashSet<>();
		for (Document doc : docs) {
			Document edge = (Document) doc.get("listOfEdges");
			String vKey = edge.getString(UP);
			Vertex vertex = findVertexByKey(context, vKey);
			if (vertex != null && vertex.getType().equals(verticesType.getName())) {
				vertexNames.add(vKey);
			}
		}
		return vertexNames;
	}

	private Set<String> getVertexNamesDependingOnVertex(String context, Class<? extends Vertex> verticesType,
			String vertexKey, String cdgName) {
		final String collectionName = getEdgeCollectionName(context, cdgName);
		// Note: direction is different to getVerticesDependingOnVertex() 
		final String UP = "to";
		final String DOWN = "from";
		AggregateIterable<Document> docs = getVertexDocCollection(context)
				.aggregate(Arrays.asList(Aggregates.match(eq("key", vertexKey)),
						Aggregates.graphLookup(collectionName, "$key", UP, DOWN, "listOfEdges"),
						Aggregates.unwind("$listOfEdges")));
		Set<String> vertexNames = new HashSet<>();
		for (Document doc : docs) {
			Document edge = (Document) doc.get("listOfEdges");
			String vKey = edge.getString(UP);
			Vertex vertex = findVertexByKey(context, vKey);
			if (vertex != null && vertex.getType().equals(verticesType.getName())) {
				vertexNames.add(vKey);
			}
		}
		return vertexNames;
	}

	private Set<Vertex> getVerticesDependingOnVertex(String context, Class<? extends Vertex> verticesType,
			String vertexKey, String cdgName) {

		final String fromCollectionName = getEdgeCollectionName(context, cdgName);
		final String startWith = "$key";
		// Note: direction is different to getVertexNamesDependingOnVertex() 
		final String connectFromField = "from";
		final String connectToField = "to";
		final String as = "listOfEdges";

		final MongoCollection<Document> docCollection = getVertexDocCollection(context);

		AggregateIterable<Document> docs = docCollection.aggregate(Arrays.asList(Aggregates.match(eq("key", vertexKey)),
				Aggregates.graphLookup(fromCollectionName, startWith, connectFromField, connectToField, as),
				Aggregates.unwind("$" + as)));

		Set<Vertex> vertices = new HashSet<>();
		for (Document doc : docs) {
			Document edge = (Document) doc.get(as);
			String vKey = edge.getString(connectFromField);
			Vertex vertex = findVertexByKey(context, vKey);
			if (verticesType.isInstance(vertex)) {
				vertices.add(vertex);
			}
		}
		return vertices;
	}

	public Set<Vertex> getVerticesByTypeAndCdg(String context, Class<?> verticesType, String cdgName) {
		Set<Vertex> vertices = new HashSet<>(); // TODO Optimize
		if (Vertex.class.isAssignableFrom(verticesType)) {
			for (Edge edge : findAllEdges(context, cdgName)) {
				for (String vKey : edge.getVertices()) {
					Vertex vertex = findVertexByKey(context, vKey);
					if (vertex != null && verticesType.isAssignableFrom(vertex.getClass())) {
						vertices.add(vertex);
					}
				}
			}
		}
		return vertices;
	}

	public Set<Vertex> getVerticesByCdg(String context, String cdgName) {
		Set<Vertex> vertices = new HashSet<>();
		for (Edge edge : findAllEdges(context, cdgName)) {
			Vertex from = edge.getSource();
			Vertex to = edge.getTarget();
			vertices.add(from);
			vertices.add(to); // TODO do not include "to" Vertex as it is not part of the CDG? Impacts other methods!
		}
		return vertices;
	}

	public Graph getGraphByCdg(String context, String cdgName) {
		Graph graph = new Graph();
		graph.setContext(context);
		for (Edge edge : findAllEdges(context, cdgName)) {
			Vertex from = edge.getSource();
			Vertex to = edge.getTarget();
			graph.addVertex(from);
			graph.addVertex(to);
			graph.addEdge(from, to, edge);
		}
		return graph;
	}

	public void addGraphToCDG(Graph graph, String cdgName) {
		String context = graph.getContext();
		for (Edge edge : graph.getEdges()) {
			if (edge.getFromCdg().equals(cdgName)) {
				insertOrUpdateOne(context, edge.getSource(), edge.getTarget(), edge);
			}
		}
	}

	public void deleteGraphFromCDG(Graph graph, String cdgName) {
		String context = graph.getContext();
		for (Edge edge : graph.getEdges()) {
			if (edge.getFromCdg().equals(cdgName)) {
				deleteEdge(context, edge);
			}
		}
	}

	public void deleteCDG(String context, String cdgName) {
		for (Edge edge : findAllEdges(context, cdgName)) {
			deleteEdge(context, edge); // Edge belongs to ALL_EDGES_CDG and one other CDG only.
		}
	}

}
