package eu.h2020.protective.ca.mair.antlr.json;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.misc.Pair;

import eu.h2020.protective.ca.mair.MongoDB;
import eu.h2020.protective.ca.mair.antlr.errors.CdgErrorListener;
import eu.h2020.protective.ca.mair.antlr.errors.VisitorErrorInterface;
import eu.h2020.protective.ca.mair.api.model.core.Edge;
import eu.h2020.protective.ca.mair.api.model.core.Vertex;
import eu.h2020.protective.ca.mair.api.model2.assets.Information;
import eu.h2020.protective.ca.mair.api.model2.assets.Software;
import eu.h2020.protective.ca.mair.api.model2.nodes.Node;
import eu.h2020.protective.ca.mair.api.model2.other.MissionObjective;
import eu.h2020.protective.ca.mair.api.model2.services.BusinessProcess;
import eu.h2020.protective.ca.mair.api.model2.services.ITService;
import lombok.Getter;

public class CdgPass1Visitor extends cdgsBaseVisitor<Object> implements VisitorErrorInterface {

	@Getter
	private final CdgErrorListener errorListener;

	@Getter
	private final CdgSupport cdgSupport;

	@Getter
	private final CdgPass2Visitor cdgPass2Visitor;

	public CdgPass1Visitor(Map<String, Vertex> vertices, Map<String, Map<Pair<String, String>, Edge>> edges,
			String originalFilename, File file, boolean guessEdgeType, Map<String, Double> missionWeights,
			Map<String, Double> vertexWeights, Map<String, Double> edgeWeights) throws IOException {
		this(vertices, edges, null, originalFilename, file, true, missionWeights, vertexWeights, edgeWeights);
	}

	public CdgPass1Visitor(MongoDB mongoDB, String originalFilename, File file, boolean guessEdgeType,
			Map<String, Double> missionWeights, Map<String, Double> vertexWeights, Map<String, Double> edgeWeights)
			throws IOException {
		this(null, null, mongoDB, originalFilename, file, true, missionWeights, vertexWeights, edgeWeights);
	}

	private CdgPass1Visitor(Map<String, Vertex> vertices, Map<String, Map<Pair<String, String>, Edge>> edges,
			MongoDB mongoDB, String originalFilename, File file, boolean guessEdgeType,
			Map<String, Double> missionWeights, Map<String, Double> vertexWeights, Map<String, Double> edgeWeights)
			throws IOException {
		CharStream input = CharStreams.fromFileName(file.getAbsolutePath());
		cdgsLexer lexer = new cdgsLexer(input); // specific to this class
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		cdgsParser parser = new cdgsParser(tokens); // specific to this class

		errorListener = new CdgErrorListener();

		cdgSupport = new CdgSupport(vertices, edges, mongoDB, originalFilename, file, true, missionWeights,
				vertexWeights, edgeWeights, errorListener, tokens, parser);

		cdgSupport.setParseTree(parser.mair_context()); // specific to this class

		visit(cdgSupport.getParseTree());

		cdgPass2Visitor = new CdgPass2Visitor(this);
	}

	@Override
	public List<String> getErrors() {
		List<String> allErrors = new ArrayList<>();
		allErrors.addAll(this.getErrorListener().getErrors());
		allErrors.addAll(cdgPass2Visitor.getErrorListener().getErrors());
		return allErrors;
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitMair_context(cdgsParser.Mair_contextContext ctx) {
		if (getErrorListener().hasSyntaxError()) {
			return null;
		}
		if (ctx.ID() != null) {
			Token contextToken = ctx.ID().getSymbol();
			if (contextToken != null) {
				String context = contextToken.getText();
				cdgSupport.setContext(context);
			}
		}
		return visitChildren(ctx);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitCdg_deps(cdgsParser.Cdg_depsContext ctx) {
		String cdgName = ctx.ID().getSymbol().getText();
		cdgSupport.initEdgeCollection(cdgName); // prepare for second pass
		cdgSupport.setCdgName(ctx, cdgName);
		return visitChildren(ctx);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitM_dep(cdgsParser.M_depContext ctx) {
		cdgSupport.setClass(ctx, MissionObjective.class);
		return visitChildren(ctx);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitBp_dep(cdgsParser.Bp_depContext ctx) {
		cdgSupport.setClass(ctx, BusinessProcess.class);
		return visitChildren(ctx);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitIa_dep(cdgsParser.Ia_depContext ctx) {
		cdgSupport.setClass(ctx, Information.class);
		return visitChildren(ctx);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitIts_dep(cdgsParser.Its_depContext ctx) {
		cdgSupport.setClass(ctx, ITService.class);
		return visitChildren(ctx);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitSw_dep(cdgsParser.Sw_depContext ctx) {
		cdgSupport.setClass(ctx, Software.class);
		return visitChildren(ctx);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitNet_dep(cdgsParser.Net_depContext ctx) {
		cdgSupport.setClass(ctx, Node.class);
		return visitChildren(ctx);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitMdeps(cdgsParser.MdepsContext ctx) {
		cdgSupport.setClass(ctx, MissionObjective.class);
		return visitChildren(ctx);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitBpdeps(cdgsParser.BpdepsContext ctx) {
		cdgSupport.setClass(ctx, BusinessProcess.class);
		return visitChildren(ctx);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitIadeps(cdgsParser.IadepsContext ctx) {
		cdgSupport.setClass(ctx, Information.class);
		return visitChildren(ctx);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitItsdeps(cdgsParser.ItsdepsContext ctx) {
		cdgSupport.setClass(ctx, ITService.class);
		return visitChildren(ctx);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitSwdeps(cdgsParser.SwdepsContext ctx) {
		cdgSupport.setClass(ctx, Software.class);
		return visitChildren(ctx);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitNetdeps(cdgsParser.NetdepsContext ctx) {
		cdgSupport.setClass(ctx, Node.class);
		return visitChildren(ctx);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitName_and_grade(cdgsParser.Name_and_gradeContext ctx) {
		String cdgName = cdgSupport.getPreviousCdgName(ctx);

		Vertex fromVertex = cdgSupport.getNameVertex();

		Class<? extends Vertex> toVertexClass = cdgSupport.getPreviousClass(ctx);
		Token toKeyToken = ctx.ID(0).getSymbol();
		Vertex toVertex = cdgSupport.addOrUpdateToVertex(toVertexClass, toKeyToken);

		Token gradeToken = ctx.ID(1).getSymbol();
		String grade = null;
		if (gradeToken != null) {
			grade = gradeToken.getText();
			if (!Edge.isValidGrade(grade)) {
				getErrorListener().error(gradeToken, "\"{}\" is not one of {}.", grade, Edge.getAllowedGrades());
			}
		}

		cdgSupport.addOrUpdateEdge(cdgName, ctx, fromVertex, toVertex, grade);

		return visitChildren(ctx);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitName(cdgsParser.NameContext ctx) {
		Class<? extends Vertex> vertexClass = cdgSupport.getPreviousClass(ctx);
		Token key = ctx.ID().getSymbol();
		cdgSupport.addOrUpdateFromVertex(ctx, vertexClass, key.getText(), null, null);
		return visitChildren(ctx);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitWeight(cdgsParser.WeightContext ctx) {
		Vertex vertex = cdgSupport.getPreviousVertex(ctx);
		Token weightToken = ctx.ID().getSymbol();
		cdgSupport.addOrUpdateFromVertex(ctx, vertex.getClass(), vertex.getKey(), weightToken, null);
		return visitChildren(ctx);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation returns the result of calling {@link #visitChildren} on {@code ctx}.
	 * </p>
	 */
	@Override
	public Object visitExpression(cdgsParser.ExpressionContext ctx) {
		Vertex vertex = cdgSupport.getPreviousVertex(ctx);
		Token expressionToken = null;
		if (ctx.ID() != null) {
			expressionToken = ctx.ID().getSymbol();
		} else if (ctx.STRING() != null) {
			expressionToken = ctx.STRING().getSymbol();
		}
		cdgSupport.addOrUpdateFromVertex(ctx, vertex.getClass(), vertex.getKey(), null, expressionToken);
		return visitChildren(ctx);
	}

}
