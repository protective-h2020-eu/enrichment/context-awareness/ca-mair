package eu.h2020.protective.ca.mair.scheduled.tasks;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import eu.h2020.protective.ca.mair.model.Metadata;
import lombok.extern.slf4j.XSlf4j;
import eu.h2020.protective.ca.mair.CaAsClient;
import eu.h2020.protective.ca.mair.CaRestController;
import eu.h2020.protective.ca.mair.SyncGraph;
import eu.h2020.protective.ca.mair.api.model.core.Graph;

@Component
@XSlf4j
public class ScheduledTasks {

	@Autowired
	private CaRestController rest;
	
	@Autowired
	private SyncGraph syncGraph;
	
	@Autowired
	private CaAsClient asClient;

	// delays in milliseconds
	@Scheduled(fixedDelayString = "${delay.between.runs.of.ranking.algorithm}", initialDelayString = "${delay.between.runs.of.ranking.algorithm}")
	public void runAssetRank() {
		rest.startAssetRank();
	}
	
	@Scheduled(fixedDelayString = "${delay.between.checks.of.asm}", initialDelayString = "${initial.delay.before.check.of.asm}")
	public void checkAsmForUpdates() {
		
		Graph graph = syncGraph.getGraph(Metadata.DEFAULT_CONTEXT);
		int numberOfVertices = graph.getVertices().size();
		log.info("Number of vertices in graph is: " + numberOfVertices);

				
		if (Graph.lastLocalCpeUpdateTime == null)
		{
			String serverTimeString = this.asClient.getLastCpeUpdateTime();
			Graph.lastLocalCpeUpdateTime = serverTimeString;
			syncGraph.addOrUpdateGraph(graph, true);
			log.info("Updating CVSS when Graph.lastLocalCpeUpdateTime is null ...");
			log.info("Graph update time: " + Graph.lastLocalCpeUpdateTime);
			return;
		}
		if (Graph.lastLocalCpeUpdateTime.equals("No data about last CPE update")) {
			String serverTimeString = this.asClient.getLastCpeUpdateTime();
			if (serverTimeString.equals("No data about last CPE update")) {
				log.info("Not Updating CVSS because serverTimeString = No data about last CPE update...");
				return;
			} else if (!serverTimeString.equals("No data about last CPE update"))
			{
				log.info("Updating CVSS when serverTimeString contains date...");
				Graph.lastLocalCpeUpdateTime = serverTimeString;
				syncGraph.addOrUpdateGraph(graph, true);
				return;
			}
		} else {
			String serverTimeString = this.asClient.getLastCpeUpdateTime();
			LocalDateTime serverDateTime = LocalDateTime.parse(serverTimeString);
			LocalDateTime clientDateTime = LocalDateTime.parse(Graph.lastLocalCpeUpdateTime);
			
			if (serverDateTime.compareTo(clientDateTime) > 0) {
				Graph.lastLocalCpeUpdateTime = serverTimeString;
				syncGraph.addOrUpdateGraph(graph, true);
				log.info("Updating CVSS when serverTimeString is more recent than Graph.lastLocalCpeUpdateTime...");
				return;
			} else
			{
				log.info("Not Updating CVSS because serverTimeString is NOT more recent than Graph.lastLocalCpeUpdateTime...");
				return;
			}
			
		}
		
		
	}

}