package eu.h2020.protective.ca.mair.model;

import org.bson.types.ObjectId;

import eu.h2020.protective.ca.mair.api.model.core.Edge;
import eu.h2020.protective.ca.mair.api.model.core.Vertex;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString(
		callSuper = true)
public class Metadata {

	public static final String ALL_CONTEXTS = null; 
	public static final String DEFAULT_CONTEXT = "DefaultContext"; 
	public static final String DEFAULT_VERTEX = "Vertex"; 
	public static final String VERTEX = "V";
	public static final String EDGE = "E";

	private ObjectId id;
	private String type;
	private String context;
	private String name;
	private Integer order;

	public String getFullCollectionName() {
		return String.format("{{%s}}%s{{%s}}", context, name, type);
	}

	public Metadata(String context, String name) {
		this(context, name, null, Vertex.class);
	}

	public Metadata(String context, String name, Integer order) {
		this(context, name, order, Edge.class);
	}

	private Metadata(String context, String name, Integer order, Class<?> cls) {
		this();
		if (context == null) {
			throw new IllegalArgumentException("context is null");
		}
		if (name == null) {
			throw new IllegalArgumentException("name is null");
		}
		if (cls == null) {
			throw new IllegalArgumentException("cls is null");
		}
		if (Vertex.class.isAssignableFrom(cls)) {
			setType(VERTEX);
		} else if (Edge.class.isAssignableFrom(cls)) {
			if (order == null) {
				throw new IllegalArgumentException("order is null");
			}
			setType(EDGE);
		} else {
			throw new IllegalArgumentException("cls is not an Edge or Vertex");
		}
		setContext(context);
		setName(name);
		setOrder(order);
	}

}
