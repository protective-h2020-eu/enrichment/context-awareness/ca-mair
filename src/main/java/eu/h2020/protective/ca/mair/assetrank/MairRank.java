package eu.h2020.protective.ca.mair.assetrank;

import java.text.DecimalFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.jgrapht.alg.interfaces.VertexScoringAlgorithm;

import eu.h2020.protective.ca.mair.MongoDB;
import eu.h2020.protective.ca.mair.api.model.core.Edge;
import eu.h2020.protective.ca.mair.api.model.core.GEdge;
import eu.h2020.protective.ca.mair.api.model.core.GVertex;
import eu.h2020.protective.ca.mair.api.model.core.Graph;
import eu.h2020.protective.ca.mair.api.model.core.Vertex;
import eu.h2020.protective.ca.mair.api.model2.other.MissionObjective;
import lombok.extern.slf4j.XSlf4j;

@XSlf4j
public class MairRank implements VertexScoringAlgorithm<GVertex, Double> {

	private final String context;
	private final MongoDB mongoDB;
	private Map<GVertex, Double> scores = new HashMap<>(); // key = vertex, value = score
	private Map<GVertex, Boolean> weightsTransferred = new HashMap<>(); // key = vertex, value = transferred
	private Map<String, Boolean> layersProcessed = new HashMap<>(); // key = name, value = processed
	private String missionLayer = null;
	private final int maxIterations = 100;
	private final Vertex defaultSinkVertex = new Vertex(Vertex.SINK); // TODO Change the key to something with {{}}?
	private final double defaultMissionWeight = 1.0d;

	public MairRank(String context, MongoDB mongoDB) throws Exception {
		this.context = context;
		this.mongoDB = mongoDB;

		mongoDB.insertOrUpdateOne(context, defaultSinkVertex);

		// get unattached vertices
		{
//			boolean unattachedVertex = false;

			Graph g = mongoDB.getGraphByCdg(context, Edge.ALL_EDGES_CDG);

			for (GVertex v : g.vertexSet()) {
				if (!(v.getVertex() instanceof MissionObjective)) {
					if (g.inDegreeOf(v) == 0) {
						log.warn("{} with key '{}' is not linked to a parent Vertex",
								v.getVertex().getClass().getSimpleName(), v.getVertex().getKey());
//						unattachedVertex = true;
						setWeightToDefaultIfNecessary(v);
					}
				}
			}

//			if (unattachedVertex) {
//				// return;
//			}
		}

		// get mission layer
		for (String cdgName : mongoDB.getNormalCdgNames(context)) {
			Graph g = mongoDB.getGraphByCdg(context, cdgName);

			for (GVertex v : g.vertexSet()) {
				if (v.getVertex() instanceof MissionObjective) {
					missionLayer = cdgName;
					setWeightToDefaultIfNecessary(v);
				}
			}

			for (GEdge e : g.edgeSet()) {
				GVertex v = e.getEdge().getGTarget();
				String finalCdgName = e.getEdge().getToCdg();
				if (g.outDegreeOf(v) == 0) {
					Edge e2 = new Edge(v.getVertex(), defaultSinkVertex);
					e2.setFromCdg(finalCdgName);
					e2.setToCdg(Edge.SINK_CDG);
					mongoDB.insertOrUpdateOne(context, v.getVertex(), defaultSinkVertex, e2);
				}
			}
		}

		if (missionLayer == null) {
			log.error("No MissionObjective in any layer");
			return;
		}

		// populate layers processed, initially false for each layer
		for (String cdgName : mongoDB.getNormalCdgNames(context)) {
			layersProcessed.put(cdgName, false);
		}

		// run ranking algorithm for mission layer
		rankLayer(missionLayer);

		if (allLayersHaveBeenProcessed()) {
			log.warn("All layers have been processed in {} iteration", 1);
			logScores(this);
			return;
		}

		// run ranking algorithm for other layers
		int i = 2;
		while (i <= maxIterations) {
			for (String layer : layersProcessed.keySet()) {
				rankLayer(layer);
			}
			if (allLayersHaveBeenProcessed()) {
				log.info("All layers have been processed in {} iterations", i);
				logScores(this);
				return;
			}
			i++;
		}
		log.warn("Maximum iterations ({}) has been reached without all layers being processed!", --i);
		logScores(this);
	}

	private void setWeightToDefaultIfNecessary(GVertex gv) {
		Vertex v = gv.getVertex();
		if (v.getWeight() <= 0.0d) {
			log.warn("{} with key '{}' has weight '{}' not greater than '0.0', assigning it '{}'",
					v.getClass().getSimpleName(), v.getKey(), v.getWeight(), defaultMissionWeight);
			v.setWeight(defaultMissionWeight);
			mongoDB.insertOrUpdateOne(context, v);
		}
		weightsTransferred.put(gv, true);
	}

	private void rankLayer(String layer) throws Exception {
		if (layersProcessed.get(layer)) {
			return; // layer already processed
		}

		Graph g = mongoDB.getGraphByCdg(context, layer);

		if (!allTopVerticesHaveTransferredWeight(g)) {
			return; // not ready for processing, do later
		}

		boolean allEdgesAreInterLayerDependencies = true;

		for (Edge edge : g.getEdges()) {
			if (edge.isInterLayerDependency()) {
				Vertex vertex = edge.getSource();
				vertex.setExpression(Vertex.SINK);
				vertex.setScore(vertex.getWeight());
				this.scores.put(vertex.getGVertex(), vertex.getScore());
			} else {
				allEdgesAreInterLayerDependencies = false;
			}
		}
		logScores(this, "Before AssetRank");

		if (!allEdgesAreInterLayerDependencies) {
			AssetRank assetRank = new AssetRank(g);
			scores.putAll(assetRank.getScores());
		}
		logScores(this, "After  AssetRank");

		for (Edge edge : g.getEdges()) {
			if (edge.isInterLayerDependency()) {
				Vertex source = edge.getSource();
				Vertex target = edge.getTarget();
				target.setWeight(source.getScore());
				weightsTransferred.put(target.getGVertex(), true);
			}
		}
		logScores(this, "Before   mongoDB");

		mongoDB.addGraphToCDG(g, layer);

		layersProcessed.put(layer, true);
	}

	private boolean allLayersHaveBeenProcessed() {
		for (boolean processed : layersProcessed.values()) {
			if (processed == false) {
				return false;
			}
		}
		return true;
	}

	private boolean allTopVerticesHaveTransferredWeight(Graph g) {
		for (GVertex v : g.vertexSet()) {
			if (g.inDegreeOf(v) == 0) { // top vertex
				if (!weightsTransferred.getOrDefault(v, false)) { // some weights not transferred
					return false;
				}
			}
		}
		return true;
	}

	private void logScores(VertexScoringAlgorithm<GVertex, Double> algorithm) {
		logScores(algorithm, "");
	}

	private void logScores(VertexScoringAlgorithm<GVertex, Double> algorithm, String msg) {
		Map<GVertex, Double> sortedScores = algorithm.getScores().entrySet().stream()
				.sorted(Map.Entry.comparingByValue(Comparator.reverseOrder())).collect(Collectors.toMap(
						Map.Entry::getKey, Map.Entry::getValue, (oldValue, newValue) -> oldValue, LinkedHashMap::new));
		log.info(msg);
		for (Entry<GVertex, Double> entry : sortedScores.entrySet()) {
			double score = entry.getValue().doubleValue() * 100;
			DecimalFormat df = new DecimalFormat("###.###");
			log.info("{} = {}", df.format(score), entry.getKey().getKey());
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<GVertex, Double> getScores() {
		return Collections.unmodifiableMap(scores);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Double getVertexScore(GVertex v) {
		Double score = scores.get(v);
		if (score == null) {
			throw new IllegalArgumentException("Cannot return score of unknown vertex");
		}
		return score;
	}

}
