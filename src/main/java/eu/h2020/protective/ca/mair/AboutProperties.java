package eu.h2020.protective.ca.mair;

import java.util.Optional;

import org.springframework.boot.info.BuildProperties;

import eu.h2020.protective.ca.mair.api.constraints.ApiDescription;
import lombok.Data;

@Data
public class AboutProperties {

	@ApiDescription("The app's name.")
	private final String name;
	@ApiDescription("The app's maven Group ID.")
	private final String group;
	@ApiDescription("The app's maven Artifact ID.")
	private final String artifact;
	@ApiDescription("The app's maven Version.")
	private final String version;
	@ApiDescription("The app's build time.")
	private final String time;
	@ApiDescription("Description of this application.")
	private final String description;

	public AboutProperties(Optional<BuildProperties> buildProperties) {
		if (buildProperties.isPresent()) {
			BuildProperties build = buildProperties.get();
			this.name = build.getName();
			this.group = build.getGroup();
			this.artifact = build.getArtifact();
			this.version = build.getVersion();
			this.time = build.get("formattedTime");
			this.description = build.get("description");
		} else {
			this.name = "";
			this.group = "";
			this.artifact = "";
			this.version = "";
			this.time = "";
			this.description = "Build Properties are not defined as not running gradle build";
		}
	}

}
