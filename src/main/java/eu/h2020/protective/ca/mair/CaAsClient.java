package eu.h2020.protective.ca.mair;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import eu.h2020.protective.ca.mair.api.model.core.Graph;
import eu.h2020.protective.ca.mair.api.model.core.Vertex;
import eu.h2020.protective.ca.mair.api.model2.assets.OperatingSystem;
import eu.h2020.protective.ca.mair.api.model2.assets.Software;
import eu.h2020.protective.ca.mair.api.model2.nodes.Node;
import eu.h2020.protective.ca.mair.model.ASNode;
import eu.h2020.protective.ca.mair.model.ASSoftware;
import lombok.extern.slf4j.XSlf4j;

@Service
@XSlf4j
public class CaAsClient {

	@Value("${ca.as.rest.api.url}")
	private String caAsUrl;

	@Autowired
	private Environment env;

	@Autowired
	private ConfigurationService config;

	@Autowired
	private MongoDB mongoDB;

	private final RestTemplate restTemplate;

//	public CaAsClient() { // FIXME improve auth.
////		this.restTemplate = new RestTemplate(
////				new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory()));
//		this.restTemplate = new RestTemplate();
////		List<ClientHttpRequestInterceptor> interceptors = this.restTemplate.getInterceptors();
////		if (CollectionUtils.isEmpty(interceptors)) {
////			interceptors = new ArrayList<>();
////		}
////		interceptors.add(new LoggingRequestInterceptor());
////		this.restTemplate.setInterceptors(interceptors);
//
//		
//	}

	@Autowired
	public CaAsClient(RestTemplateBuilder restTemplateBuilder) {
		this.restTemplate = restTemplateBuilder.build();
	}

	// This code runs in the background and updates nodes in the DB with the maximum
	// CVSS of all software running on the
	// node.
	// The maximum CVSS is obtained by querying a remote CA AS over it's REST API.
	// This code is executed in a separate thread asynchronously.
	// It returns void therefore the caller does not wait on it or receive
	// Exceptions from it.
//	@Async
//	public void getAssetCvssAsync(Graph graph) {
//		try {
//			for (String profile : env.getActiveProfiles()) {
//				if (profile.equals("non-async")) {
//					return;
//				}
//			}
//			getAssetCvss(graph);
//		} catch (Exception e) {
//			// do nothing
//		}
//	}
	// Tests failed without this try catch wrapper
	public void getAssetCvssTryCatch(Graph graph) {
		try {
			for (String profile : env.getActiveProfiles()) {
				if (profile.equals("non-async")) {
					return;
				}
			}
			getAssetCvss(graph);
		} catch (Exception e) {
			// do nothing
		}
	}

	public void getAssetCvss(Graph graph) throws Exception {
		try {
			for (Vertex vertex : graph.getVertices()) {
				if (vertex instanceof Node) {
					Node node = (Node) vertex;
					Set<Vertex> softwareSet = mongoDB.getSoftwareDependingOnNode(graph.getContext(), node);
					if (!softwareSet.isEmpty()) {
						ASNode asNode = new ASNode(node.getKey());
						for (Vertex v : softwareSet) {
							Software sw = (Software) v;
							if (sw.getKey() == null || sw.getKey().trim().isEmpty()) {
								continue;
							}
							if (sw.getProduct() == null || sw.getProduct().trim().isEmpty()) {
								continue;
							}
							if (sw.getVendor() == null || sw.getVendor().trim().isEmpty()) {
								continue;
							}
							if (sw.getVersion() == null || sw.getVersion().trim().isEmpty()) {
								continue;
							}
							ASSoftware asSoftware = new ASSoftware(sw.getKey(), sw.getProduct(), sw.getVendor(),
									sw.getVersion());
							if (v instanceof OperatingSystem) {
								// XXX Multiple OS (nested containers / VMs)?
								asNode.getOperatingSystems().add(asSoftware);
							} else {
								asNode.getSoftware().add(asSoftware);
							}
						}
						Double maxCvss = getAssetCvss(asNode);
						if (maxCvss != null && !maxCvss.equals(node.getMaxCvss())) {
							node.setMaxCvss(maxCvss);
							mongoDB.insertOrUpdateOne(graph.getContext(), node);
						}
					}
				}
			}
		} catch (Exception e) {
			log.warn("Something went wrong calling CA-AS", e);
			throw e;
		}
	}

	public Double getAssetCvss(ASNode node) {
//		log.info("ASNode sent to AS = {}", node);
		String requestUrl = getRequestUrl("get-asset-max-cvss");
		if (requestUrl == null) {
			return null;
		}
//		if (!node.getSoftware().isEmpty() && !node.getOperatingSystems().isEmpty() && !node.getKey().isEmpty()) {
			HttpEntity<ASNode> requestBody = new HttpEntity<>(node);

			ResponseEntity<String> response = restTemplate.postForEntity(requestUrl, requestBody, String.class);
//			String resultString = response.getBody();
			log.info("The status code of the response is: " + response.getStatusCodeValue());

			if (response.getStatusCodeValue() == 200) {
				String resultString = response.getBody();
				Double result = Double.parseDouble(resultString);
				log.info("maxCvss returned from successful call to /get-asset-max-cvss-is: " + result);
				return result;
			} else if (response.getStatusCodeValue() == 1001) {
				log.warn("1. database blocked / unavailable (usually due to an initialization / update)\n"
						+ "OR\n"
						+ "2. (multiple) parallel calls that were not cached yet and are heavy on processing\n"
						+ ": returning 0.0");
				return 0.0d;
			} else if (response.getStatusCodeValue() == 1002) {
				log.warn(
						"access denied (usually caused when trying to add CVEs outside of uploaded CVE library): returning 0.0");
				return 0.0d;
			} else if (response.getStatusCodeValue() == 1003) {
				log.warn(
						"database / system error (caused by e.g. trying to add new CVE data without proper DB initialization): returning 0.0");
				return 0.0d;
			} else if (response.getStatusCodeValue() == 1004) {
				log.warn("bad argument (e.g. empty software table etc.): returning 0.0");
				return 0.0d;
			} else {
				log.warn("Unknown Error. Status Code: " + response.getStatusCodeValue() + ". returning 0.0");
				return 0.0d;
			}

//			if (resultString.trim().equals("no_cvss") || resultString.trim().equals("data")) {
//				log.info("String returned from CAAS: " + resultString.trim());
//				return 0.0d;
//			} else {
//				Double result = Double.parseDouble(resultString);
//				return result;
//			}
//		} else {
//			return 0.0d;
//		}

//		Double result = Double.parseDouble(response.getBody());
	}

	public String getLastCpeUpdateTime() {
//		log.info("ASNode sent to AS = {}", node);
		String requestUrl = getRequestUrl("get-last-cpe-update-time");
		if (requestUrl == null) {
			return null;
		}
		ResponseEntity<String> response = restTemplate.getForEntity(requestUrl, String.class);

		log.info("Response from get-last-cpe-update-time is: " + response.getBody().trim());
		String lastTime = response.getBody();
		return lastTime.trim();
	}

	public Double getSoftwareCvss(ASSoftware sw) {
//		log.info("ASSoftware sent to AS = {}", sw);
		String requestUrl = getRequestUrl("get-software-max-cvss");
		if (requestUrl == null) {
			return null;
		}

		HttpEntity<ASSoftware> requestBody = new HttpEntity<>(sw);
		ResponseEntity<String> response = restTemplate.postForEntity(requestUrl, requestBody, String.class);
		Double result = Double.parseDouble(response.getBody());
		return result;
	}

	private String getRequestUrl(String operation) {
		if (config.getCaAsUrl() == null || config.getCaAsUrl().trim().isEmpty()) {
			log.warn("CA-AS URL is not configured!");
//			return null;
			config.setCaAsUrl(this.caAsUrl);
		}
		String requestUrl = config.getCaAsUrl() + "/api/" + operation;
		log.info("requestUrl = '{}'", requestUrl);
		return requestUrl;
	}

}
