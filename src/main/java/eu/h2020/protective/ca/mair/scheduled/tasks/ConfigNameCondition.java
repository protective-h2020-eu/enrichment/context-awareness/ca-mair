package eu.h2020.protective.ca.mair.scheduled.tasks;

import org.springframework.boot.autoconfigure.condition.AnyNestedCondition;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;;

public class ConfigNameCondition extends AnyNestedCondition {
	
	public ConfigNameCondition() {
        super(ConfigurationPhase.PARSE_CONFIGURATION);
    }
	
	@ConditionalOnProperty(name = "run.ranking.algorithm.on.a.schedule", havingValue = "true")
    static class AssetRankCondition {

    }

    @ConditionalOnProperty(name = "run.cvss.retrieval.on.a.schedule", havingValue = "true")
    static class CvssCondition {

    }
	

}
