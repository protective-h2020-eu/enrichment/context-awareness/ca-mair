package eu.h2020.protective.ca.mair.antlr.json;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import eu.h2020.protective.ca.mair.MongoDB;
import eu.h2020.protective.ca.mair.antlr.original.DepsPass1Visitor;
import eu.h2020.protective.ca.mair.antlr.original.DepsTests;
import eu.h2020.protective.ca.mair.api.model.core.Edge;
import eu.h2020.protective.ca.mair.api.model.core.Graph;
import eu.h2020.protective.ca.mair.export.json.ExportCdgs;
import eu.h2020.protective.ca.mair.model.Metadata;
import eu.h2020.protective.ca.mair.test.support.JsonBase;
import eu.h2020.protective.ca.mair.test.support.TestTags;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { MongoDB.class })
@TestPropertySource(locations = "classpath:application-test.properties")
@Tag(TestTags.SLOW)
@Tag(TestTags.MONGO)
public class CdgDBTests extends JsonBase {

	@Autowired
	private MongoDB mongoDB;

	private Map<String, Double> missionWeights = new HashMap<>();
	private Map<String, Double> vertexWeights = new HashMap<>();
	private Map<String, Double> edgeWeights = new HashMap<>();

	@BeforeEach
	public void setUp() {
		mongoDB.clearAllMatricesAndGraphs(Metadata.DEFAULT_CONTEXT);

		missionWeights.put("none", 0.0d);
		missionWeights.put("low", 1.0d);
		missionWeights.put("medium", 2.0d);
		missionWeights.put("high", 3.0d);
		missionWeights.put("critical", 4.0d);

		vertexWeights.put("none", 0.0d);
		vertexWeights.put("low", 1.0d);
		vertexWeights.put("medium", 2.0d);
		vertexWeights.put("high", 3.0d);
		vertexWeights.put("critical", 4.0d);

		edgeWeights.put("none", 0.0d);
		edgeWeights.put("low", 1.0d);
		edgeWeights.put("medium", 2.0d);
		edgeWeights.put("high", 3.0d);
		edgeWeights.put("critical", 4.0d);
	}

	@Test
	public void readCdg() throws Exception {
		org.reflections.Reflections.log = null;
		File file = new File("src/test/resources/antlr/cdg_1.cdgs");
		CdgTests.readCdg(mongoDB, file, null, true, true, missionWeights, vertexWeights, edgeWeights);
	}

	@Test // Only reports invalid ID.
	public void readCdgMissingVertexAndInvalidIDAndInvalidGrade() throws Exception {
		org.reflections.Reflections.log = null;
		File file = new File("src/test/resources/antlr/cdg_2.cdgs");
		String error1 = "line 74:11 no viable alternative at input '{\"id\":g,'";
		String error2 = "line 75:5 mismatched input '\"expression\"' expecting '{'";
		String error3 = "line 76:5 mismatched input '\"weight\"' expecting '{'";
		String error4 = "line 77:5 missing '}' at ','";
		String error5 = "line 79:5 mismatched input '\"id\"' expecting '\"cdg\"'";

		CdgTests.readCdg(mongoDB, file, null, false, true, missionWeights, vertexWeights, edgeWeights, error1, error2, error3,
				error4, error5);
	}

	@Test
	public void readCdgMissingVertexAndInvalidGrade() throws Exception {
		org.reflections.Reflections.log = null;
		File file = new File("src/test/resources/antlr/cdg_3.cdgs");
//		String error1 = "line 68:14 \"high123\" is not one of "
//				+ "[\"none\", \"low\", \"medium\", \"high\", \"critical\"]."; // TODO change test to check grade.
		String error2 = "line 24:21 Vertex with key \"m123\" is not in the database AND "
				+ "not defined correctly in the \"src/test/resources/antlr/cdg_3.cdgs\" file.";
		String error3 = "line 25:19 Vertex with key \"c1\" is not in the database AND "
				+ "not defined correctly in the \"src/test/resources/antlr/cdg_3.cdgs\" file.";
		String error4 = "line 31:19 Vertex with key \"g123\" is not in the database AND "
				+ "not defined correctly in the \"src/test/resources/antlr/cdg_3.cdgs\" file.";
		CdgTests.readCdg(mongoDB, file, null, false, true, missionWeights, vertexWeights, edgeWeights, error2, error3,
				error4);
	}

	@Test
	public void readCdgTwice() throws Exception {
		org.reflections.Reflections.log = null;
		File file = new File("src/test/resources/antlr/cdg_1.cdgs");
		CdgTests.readCdg(mongoDB, file, null, true, true, missionWeights, vertexWeights, edgeWeights);
		CdgTests.readCdg(mongoDB, file, null, true, true, missionWeights, vertexWeights, edgeWeights);
	}

	@Test
	public void importPsncDepsAndExportCdgs() throws Exception {
		org.reflections.Reflections.log = null;
		File file = new File("src/test/resources/antlr/psnc_vc.deps");
		DepsTests.readCdg(mongoDB, file, false, true, missionWeights, vertexWeights, edgeWeights);
		String expected = ExportCdgs.asJson(Metadata.DEFAULT_CONTEXT, mongoDB);
		setUp();
		Path path = Paths.get("src/test/resources/antlr/psnc_vc_1.tmp.cdgs");
		File tmpFile = toCdgsFile(expected, path);
		CdgTests.readCdg(mongoDB, tmpFile, null, false, true, missionWeights, vertexWeights, edgeWeights);
		String actual = ExportCdgs.asJson(Metadata.DEFAULT_CONTEXT, mongoDB);
//		Path path2 = Paths.get("src/test/resources/antlr/psnc_vc_2.tmp.cdgs");
//		File tmpFile2 = toCdgsFile(actual, path2);
		JSONAssert.assertEquals(expected, actual, false);
	}

	@Test
	public void importPsncDepsV7AndExportCdgs() throws Exception {
		org.reflections.Reflections.log = null;
		File file = new File("src/test/resources/antlr/psnc_vc_v7.deps");
		DepsTests.readCdg(mongoDB, file, false, true, missionWeights, vertexWeights, edgeWeights);
		String expected = ExportCdgs.asJson(Metadata.DEFAULT_CONTEXT, mongoDB);
		setUp();
		Path path1 = Paths.get("src/test/resources/antlr/psnc_vc_v7.1.tmp.cdgs");
		File tmpFile1 = toCdgsFile(expected, path1);
		CdgTests.readCdg(mongoDB, tmpFile1, null, false, true, missionWeights, vertexWeights, edgeWeights);
		String actual = ExportCdgs.asJson(Metadata.DEFAULT_CONTEXT, mongoDB);
//		Path path2 = Paths.get("src/test/resources/antlr/psnc_vc_v7.2.tmp.cdgs");
//		File tmpFile2 = toCdgsFile(actual, path2);
		JSONAssert.assertEquals(expected, actual, false);
	}

	@Test
	public void importPsncDepsV7layersAndExportCdgs() throws Exception {
		org.reflections.Reflections.log = null;
		File file = new File("src/test/resources/antlr/psnc_vc_v7.layers.deps");
		DepsPass1Visitor passDeps = DepsTests.readCdg(mongoDB, file, false, true, missionWeights, vertexWeights,
				edgeWeights);
		assertThat(passDeps.getCdgSupport().getVertices()).hasSize(176); // 1 less than V7
		assertThat(passDeps.getCdgSupport().getEdges()).hasSize(517); // 1 less than V7
		String expected = ExportCdgs.asJson(Metadata.DEFAULT_CONTEXT, mongoDB);
		setUp();
		Path path1 = Paths.get("src/test/resources/antlr/psnc_vc_v7.1.tmp.layers.cdgs");
		File tmpFile1 = toCdgsFile(expected, path1);
		CdgPass1Visitor passCdg = CdgTests.readCdg(mongoDB, tmpFile1, null, false, true, missionWeights, vertexWeights,
				edgeWeights);
		assertThat(passCdg.getCdgSupport().getVertices()).hasSize(176); // 1 less than V7
		assertThat(passCdg.getCdgSupport().getEdges()).hasSize(517); // 1 less than V7
		String actual = ExportCdgs.asJson(Metadata.DEFAULT_CONTEXT, mongoDB);
//		Path path2 = Paths.get("src/test/resources/antlr/psnc_vc_v7.2.tmp.layers.cdgs");
//		File tmpFile2 = toCdgsFile(actual, path2);
		JSONAssert.assertEquals(expected, actual, false);
	}

	@Test
	public void importPsncCdgsAndExportVertices() throws Exception {
		org.reflections.Reflections.log = null;
		File file = new File("src/test/resources/antlr/psnc_vc.cdgs");
		CdgTests.readCdg(mongoDB, file, null, false, true, missionWeights, vertexWeights, edgeWeights);
		Graph graph = mongoDB.getGraphByCdg(Metadata.DEFAULT_CONTEXT, Edge.ALL_EDGES_CDG);
		graph.setEdges(new ArrayList<Edge>(0));
		toJsonFile(graph, "psnc_vc.tmp.cdgv");
	}

	@Test
	public void getCdgNames() throws Exception {
		readCdg();
		List<String> expected = CdgTests.getCdgNames();
		List<String> cdgNames = mongoDB.getAllCdgNames(Metadata.DEFAULT_CONTEXT);
		assertThat(cdgNames.size()).isGreaterThanOrEqualTo(expected.size());
		assertThat(cdgNames).containsExactly(expected.toArray(new String[expected.size()]));
	}

	@Test
	public void exportAndImportCdg() throws Exception {
		org.reflections.Reflections.log = null;
		File file = new File("src/test/resources/antlr/cdg_1.cdgs");
		CdgTests.readCdg(mongoDB, file, null, true, true, missionWeights, vertexWeights, edgeWeights);
		String expected = toJsonString(file.getPath());
		String actual = ExportCdgs.asJson(Metadata.DEFAULT_CONTEXT, mongoDB);
		JSONAssert.assertEquals(expected, actual, false);
		setUp();
		File tmpFile = toCdgsTempFile(actual);
		CdgTests.readCdg(mongoDB, tmpFile, null, true, true, missionWeights, vertexWeights, edgeWeights);
	}

	private File toCdgsFile(String json, Path path) throws IOException {
		Files.write(path, json.getBytes(StandardCharsets.UTF_8));
		return path.toFile();
	}

	private File toCdgsTempFile(String json) throws IOException {
		Path path = Files.createTempFile("tmp-json-file", ".cdgs");
		toCdgsFile(json, path);
		return path.toFile();
	}

}
