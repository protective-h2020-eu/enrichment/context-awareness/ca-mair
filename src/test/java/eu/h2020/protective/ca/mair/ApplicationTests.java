package eu.h2020.protective.ca.mair;

import static java.util.stream.Collectors.toSet;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import eu.h2020.protective.ca.mair.api.model.assets.OperatingSystem;
import eu.h2020.protective.ca.mair.api.model.core.Edge;
import eu.h2020.protective.ca.mair.api.model.core.Graph;
import eu.h2020.protective.ca.mair.api.model.core.Vertex;
import eu.h2020.protective.ca.mair.api.model.orig.BusinessService;
import eu.h2020.protective.ca.mair.api.model.orig.Department;
import eu.h2020.protective.ca.mair.api.model.other.Organisation;
import eu.h2020.protective.ca.mair.api.model.other.Site;
import eu.h2020.protective.ca.mair.api.model.other.Vulnerability;
import eu.h2020.protective.ca.mair.api.model2.assets.Asset;
import eu.h2020.protective.ca.mair.api.model2.assets.Information;
import eu.h2020.protective.ca.mair.api.model2.assets.Software;
import eu.h2020.protective.ca.mair.api.model2.edges.DependsOn;
import eu.h2020.protective.ca.mair.api.model2.groups.Network;
import eu.h2020.protective.ca.mair.api.model2.nodes.Computer;
import eu.h2020.protective.ca.mair.api.model2.nodes.Node;
import eu.h2020.protective.ca.mair.api.model2.services.ITService;
import eu.h2020.protective.ca.mair.model.Metadata;
import eu.h2020.protective.ca.mair.test.support.JsonBase;
import eu.h2020.protective.ca.mair.test.support.TestTags;
import lombok.extern.slf4j.XSlf4j;

@SpringBootTest
@ExtendWith( SpringExtension.class )
@TestPropertySource(locations = "classpath:application-test.properties")
@Tag(TestTags.SLOW)
@Tag(TestTags.MONGO)
@XSlf4j
public class ApplicationTests extends JsonBase {

	@Autowired
	private MongoDB mongoDB;

	@Autowired
	private MongoDBQueries mongoDBQueries;

	@Autowired
	private MongoDBInit mongoDBInit;

	private Edge insertOrUpdateOne(String key, Vertex from, Vertex to) {
		Edge e = new Edge(key, from, to);
		return mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, e.getSource(), e.getTarget(), e);
	}

	@BeforeEach
	public void setUp() {
		mongoDB.clearAllMatricesAndGraphs(Metadata.DEFAULT_CONTEXT);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void saveAndReadRawReposWorks() {
		Vertex vAIT = new Vertex("AIT", "Organisation");
		mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, vAIT);
		Vertex vSRI = new Vertex("SRI", "Department");
		mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, vSRI);
		Vertex vAthlone = new Vertex("Athlone", "Site");
		mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, vAthlone);
		Vertex v239 = new Vertex("192.168.101.239", "Computer");
		mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, v239);
		Vertex v123 = new Vertex("192.168.101.123", "Computer");
		mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, v123);
		Vertex vWord = new Vertex("Microsoft Word", "Software");
		mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, vWord);
		Vertex vUbuntu = new Vertex("Ubuntu", "OperatingSystem");
		mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, vUbuntu);
		Vertex vWindows = new Vertex("Microsoft Windows", "OperatingSystem");
		mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, vWindows);
		Vertex v101 = new Vertex("192.168.101.*", "Network");
		mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, v101);
		Vertex vCVE = new Vertex("CVE-123", "Vulnerability");
		mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, vCVE);
		Vertex vGitLab = new Vertex("SRI GitLab", "InformationAsset");
		mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, vGitLab);
		Vertex vWebSite = new Vertex("SRI Web Site", "BusinessService");
		mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, vWebSite);
		Vertex vHelpdesk = new Vertex("AIT Helpdesk", "ITService");
		mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, vHelpdesk);

		insertOrUpdateOne("PartOf1", vSRI, vAIT);
		insertOrUpdateOne("RunsOn1", vWord, vWindows);
		insertOrUpdateOne("ConnectedTo1", v123, v101);
		insertOrUpdateOne("ConnectedTo2", v239, v101);
		insertOrUpdateOne("RunsOn2", vWindows, v123);
		insertOrUpdateOne("RunsOn3", vUbuntu, v239);
		insertOrUpdateOne("RunsOn4", vGitLab, vUbuntu);
		insertOrUpdateOne("RunsOn5", vWebSite, vUbuntu);
		insertOrUpdateOne("RunsOn6", vHelpdesk, vUbuntu);
		insertOrUpdateOne("Impacts1", vCVE, vWebSite);

		log.info("Test Vertices found with findAll():");
		log.info("-----------------------------------");
		for (Vertex vertex : mongoDB.findAllVertices(Metadata.DEFAULT_CONTEXT)) {
			log.info("\n{}", vertex);
		}
		log.info("\n");

		log.info("Test Edges found with findAll():");
		log.info("--------------------------------");
		for (Edge edge : mongoDB.findAllEdges(Metadata.DEFAULT_CONTEXT)) {
			log.info("\n{}", edge);
		}
		log.info("\n");
	}

	@Test
	public void saveAndReadDerivedReposWorks() {
		saveAndReadDerivedRepos();
	}

	@Test
	public void getNodeWithIpAddress() {
		saveAndReadDerivedRepos();
		String ipAddress = "192.168.101.123";
		Node node = mongoDBQueries.getNodeWithIpAddress(Metadata.DEFAULT_CONTEXT, ipAddress);
		assertThat(node.getIpAddresses()).containsExactly(ipAddress);
	}

	@Test
	public void getNodeWithDns() {
		saveAndReadDerivedRepos();
		String dns = "dns.239.com";
		Node node = mongoDBQueries.getNodeWithDns(Metadata.DEFAULT_CONTEXT, dns);
		assertThat(node.getDns()).isEqualTo(dns);
	}

	@Test
	public void getNodeImportance() {
		saveAndReadDerivedRepos();
		String ipAddress = "192.168.101.123";
		Node node = mongoDBQueries.getNodeWithIpAddress(Metadata.DEFAULT_CONTEXT, ipAddress);
		node.setGrade("none");
		assertThat(node.getImportance()).isEqualTo(0);
		node.setGrade("low");
		assertThat(node.getImportance()).isEqualTo(1);
		node.setGrade("medium");
		assertThat(node.getImportance()).isEqualTo(2);
		node.setGrade("high");
		assertThat(node.getImportance()).isEqualTo(3);
		node.setGrade("critical");
		assertThat(node.getImportance()).isEqualTo(4);
		node.setGrade("");
		assertThat(node.getImportance()).isEqualTo(0);
		node.setGrade(null);
		assertThat(node.getImportance()).isEqualTo(0);
		node.setGrade(" ");
		assertThat(node.getImportance()).isNull();
	}

	@Test
	public void getVerticesByTypeAndCdg() {
		saveAndReadDerivedRepos();
		Set<Vertex> vertices = mongoDB.getVerticesByTypeAndCdg(Metadata.DEFAULT_CONTEXT, Node.class, Edge.OTHER_EDGES_CDG);
		for (Vertex vertex : vertices) {
			assertThat(vertex).isInstanceOf(Node.class);
		}
		assertThat(vertices).hasSize(2);
	}

	@Test
	public void getVerticesByCdg() {
		saveAndReadDerivedRepos();
		Set<Vertex> vertices = mongoDB.getVerticesByCdg(Metadata.DEFAULT_CONTEXT, Edge.OTHER_EDGES_CDG);
		for (Vertex vertex : vertices) {
			assertThat(vertex).isInstanceOf(Vertex.class);
		}
		assertThat(vertices).hasSize(12);
	}

	@Test
	public void addGraphToCdg() {
		Graph graph = new Graph();
		graph.setContext(Metadata.DEFAULT_CONTEXT);
		
		Software s1 = new Software("s1");
		graph.addVertex(s1);
		
		Computer c1 = new Computer("c1");
		graph.addVertex(c1);
		
		Edge edge = new DependsOn(s1, c1);
		edge.setKey("dependsOn");
		edge.setFromCdg("cdg1");		
		graph.addEdge(s1, c1, edge);
		
		mongoDB.addGraphToCDG(graph, "cdg1");
		
		assertThat(mongoDB.findAllVertices(Metadata.DEFAULT_CONTEXT)).hasSize(2);
		assertThat(mongoDB.findAllEdges(Metadata.DEFAULT_CONTEXT, "cdg1")).hasSize(1);
	}

	@Test
	public void removeGraphFromCdg() {
		Graph graph = new Graph();
		graph.setContext(Metadata.DEFAULT_CONTEXT);
		
		Software s1 = new Software("s1");
		graph.addVertex(s1);
		
		Computer c1 = new Computer("c1");
		graph.addVertex(c1);
		
		Edge edge1 = new DependsOn(s1, c1);
		edge1.setKey("dependsOn1");
		edge1.setFromCdg("cdg1");		
		graph.addEdge(s1, c1, edge1);
		
		mongoDB.addGraphToCDG(graph, "cdg1");

		Software s2 = new Software("s2");
		graph.addVertex(s2);
		
		Computer c2 = new Computer("c2");
		graph.addVertex(c2);
		
		Edge edge2 = new DependsOn(s2, c2);
		edge2.setKey("dependsOn2");
		edge2.setFromCdg("cdg2");		
		graph.addEdge(s2, c2, edge2);
		
		mongoDB.addGraphToCDG(graph, "cdg2");

		assertThat(mongoDB.findAllVertices(Metadata.DEFAULT_CONTEXT)).hasSize(4);
		assertThat(mongoDB.findAllEdges(Metadata.DEFAULT_CONTEXT, "cdg1")).hasSize(1);
		assertThat(mongoDB.findAllEdges(Metadata.DEFAULT_CONTEXT, "cdg2")).hasSize(1);
		
		mongoDB.deleteGraphFromCDG(graph, "cdg1");

		assertThat(mongoDB.findAllVertices(Metadata.DEFAULT_CONTEXT)).hasSize(2);
		assertThat(mongoDB.findAllEdges(Metadata.DEFAULT_CONTEXT, "cdg1")).hasSize(0);
		assertThat(mongoDB.findAllEdges(Metadata.DEFAULT_CONTEXT, "cdg2")).hasSize(1);
	}

	@Test
	public void removeGraphFromCdgWhereVerticesAreShared() {
		Graph graph = new Graph();
		graph.setContext(Metadata.DEFAULT_CONTEXT);
		
		Software s1 = new Software("s1");
		graph.addVertex(s1);
		
		Computer c1 = new Computer("c1");
		graph.addVertex(c1);
		
		Edge edge1 = new DependsOn(s1, c1);
		edge1.setKey("dependsOn1");
		edge1.setFromCdg("cdg1");		
		graph.addEdge(s1, c1, edge1);
		
		mongoDB.addGraphToCDG(graph, "cdg1");

		Edge edge2 = new DependsOn(s1, c1);
		edge2.setKey("dependsOn2");
		edge2.setFromCdg("cdg2");		
		graph.addEdge(s1, c1, edge2);
		
		mongoDB.addGraphToCDG(graph, "cdg2");

		assertThat(mongoDB.findAllVertices(Metadata.DEFAULT_CONTEXT)).hasSize(2);
		assertThat(mongoDB.findAllEdges(Metadata.DEFAULT_CONTEXT, "cdg1")).hasSize(1);
		assertThat(mongoDB.findAllEdges(Metadata.DEFAULT_CONTEXT, "cdg2")).hasSize(1);
		
		mongoDB.deleteGraphFromCDG(graph, "cdg1");

//		assertThat(mongoDB.findAllVertices(Metadata.DEFAULT_CONTEXT)).hasSize(2);
		assertThat(mongoDB.findAllEdges(Metadata.DEFAULT_CONTEXT, "cdg1")).hasSize(0);
		assertThat(mongoDB.findAllEdges(Metadata.DEFAULT_CONTEXT, "cdg2")).hasSize(1);
	}

	@Test
	public void removeCdg() {
		Graph graph = new Graph();
		graph.setContext(Metadata.DEFAULT_CONTEXT);
		
		Software s1 = new Software("s1");
		graph.addVertex(s1);
		
		Computer c1 = new Computer("c1");
		graph.addVertex(c1);
		
		Edge edge1 = new DependsOn(s1, c1);
		edge1.setKey("dependsOn1");
		edge1.setFromCdg("cdg1");
		log.info("1 s1 = {}", s1);
		log.info("1 c1 = {}", c1);
		graph.addEdge(s1, c1, edge1);
		
		mongoDB.addGraphToCDG(graph, "cdg1");

		Edge edge2 = new DependsOn(s1, c1);
		edge2.setKey("dependsOn2");
		edge2.setFromCdg("cdg2");		
		log.info("2 s1 = {}", s1);
		log.info("2 c1 = {}", c1);
		graph.addEdge(s1, c1, edge2);
		
		mongoDB.addGraphToCDG(graph, "cdg2");

		assertThat(mongoDB.findAllVertices(Metadata.DEFAULT_CONTEXT)).hasSize(2);
		assertThat(mongoDB.findAllEdges(Metadata.DEFAULT_CONTEXT, "cdg1")).hasSize(1);
		assertThat(mongoDB.findAllEdges(Metadata.DEFAULT_CONTEXT, "cdg2")).hasSize(1);
		
		mongoDB.deleteCDG(Metadata.DEFAULT_CONTEXT, "cdg1");

		assertThat(mongoDB.findAllVertices(Metadata.DEFAULT_CONTEXT)).hasSize(2);
		assertThat(mongoDB.findAllEdges(Metadata.DEFAULT_CONTEXT, "cdg1")).hasSize(0);
		assertThat(mongoDB.findAllEdges(Metadata.DEFAULT_CONTEXT, "cdg2")).hasSize(1);
	}

	private void saveAndReadDerivedRepos() {
		Vertex vAIT = new Organisation("AIT");
		mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, vAIT);
		Vertex vSRI = new Department("SRI"); // FIXME vertex x2
		mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, vSRI);
		Vertex vAthlone = new Site("Athlone");
		mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, vAthlone);
		Node v239 = new Computer("192.168.101.239");
		List<String> ipAddresses239 = new ArrayList<>(1);
		ipAddresses239.add("192.168.101.239");
		v239.setIpAddresses(ipAddresses239);
		v239.setDns("dns.239.com");
		mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, v239);
		Node v123 = new Computer("192.168.101.123");
		List<String> ipAddresses123 = new ArrayList<>(1);
		ipAddresses123.add("192.168.101.123");
		v123.setIpAddresses(ipAddresses123);
		v123.setDns("dns.123.com");
		mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, v123);
		Vertex vWord = new Software("Microsoft Word");
		mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, vWord);
		Vertex vUbuntu = new OperatingSystem("Ubuntu");
		mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, vUbuntu);
		Vertex vWindows = new OperatingSystem("Microsoft Windows");
		mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, vWindows);
		Vertex v101 = new Network("192.168.101.*");
		mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, v101);
		Vertex vCVE = new Vulnerability("CVE-123");
		mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, vCVE);
		Vertex vGitLab = new Information("SRI GitLab");
		mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, vGitLab);
		Vertex vWebSite = new BusinessService("SRI Web Site"); // FIXME vertex x2
		mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, vWebSite);
		Vertex vHelpdesk = new ITService("AIT Helpdesk");
		mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, vHelpdesk);

		insertOrUpdateOne("PartOf1", vSRI, vAIT);
		insertOrUpdateOne("RunsOn1", vWord, vWindows);
		insertOrUpdateOne("ConnectedTo1", v123, v101);
		insertOrUpdateOne("ConnectedTo2", v239, v101);
		insertOrUpdateOne("RunsOn2", vWindows, v123);
		insertOrUpdateOne("RunsOn3", vUbuntu, v239);
		insertOrUpdateOne("RunsOn4", vGitLab, vUbuntu);
		insertOrUpdateOne("RunsOn5", vWebSite, vUbuntu);
		insertOrUpdateOne("RunsOn6", vHelpdesk, vUbuntu);
		insertOrUpdateOne("Impacts1", vCVE, vWebSite);

		log.info("Test Vertices found with findAll():");
		log.info("-----------------------------------");
		for (Vertex vertex : mongoDB.findAllVertices(Metadata.DEFAULT_CONTEXT)) {
			log.info("\n{}", vertex);
		}
		log.info("\n");

		log.info("Test Edges found with findAll():");
		log.info("--------------------------------");
		for (Edge edge : mongoDB.findAllEdges(Metadata.DEFAULT_CONTEXT)) {
			log.info("\n{}", edge);
		}
		log.info("\n");
	}

	private Vertex addVertex(Vertex vertex) {
		mongoDB.insertOrUpdateOne(Metadata.DEFAULT_CONTEXT, vertex);
		return vertex;
	}

	@Test
	public void impactDependentGraphReposWorks() throws Exception {
		mongoDBInit.initImpactDependentGraph();

		log.info("IDG Vertices found with findAll():");
		log.info("-----------------------------------");
		for (Vertex vertex : mongoDB.findAllVertices(Metadata.DEFAULT_CONTEXT)) {
			log.info("\n{}", vertex);
		}
		log.info("\n");

		log.info("IDG Edges found with findAll():");
		log.info("--------------------------------");
		for (Edge edge : mongoDB.findAllEdges(Metadata.DEFAULT_CONTEXT)) {
			log.info("\n{}", edge);
		}
		log.info("\n");
	}

	@Test
	public void getMissionsImpactedByAssetWorks() throws Exception {
		mongoDBInit.initImpactDependentGraph();

		Set<String> names = null;

		names = mongoDBQueries.getMissionsImpactedByAsset(Metadata.DEFAULT_CONTEXT, "A1");
		assertThat(names).containsExactlyInAnyOrder("Mission1");

		names = mongoDBQueries.getMissionsImpactedByAsset(Metadata.DEFAULT_CONTEXT, "A2");
		assertThat(names).containsExactlyInAnyOrder("Mission1", "Mission2");

		names = mongoDBQueries.getMissionsImpactedByAsset(Metadata.DEFAULT_CONTEXT, "A3");
		assertThat(names).containsExactlyInAnyOrder("Mission2");

		names = mongoDBQueries.getMissionsImpactedByAsset(Metadata.DEFAULT_CONTEXT, "A4");
		assertThat(names).containsExactlyInAnyOrder("Mission1", "Mission2");

		names = mongoDBQueries.getMissionsImpactedByAsset(Metadata.DEFAULT_CONTEXT, "A5");
		assertThat(names).containsExactlyInAnyOrder("Mission2");
	}

	@Test
	public void getSupportingAssetsForMissionWorks() throws Exception {
		mongoDBInit.initImpactDependentGraph();

		Set<Vertex> assets = null;
		Set<String> assetKeys = null;

		assets = mongoDBQueries.getSupportingAssetsForMission(Metadata.DEFAULT_CONTEXT, "Mission1");
		assetKeys = assets.stream().map(e -> e.getKey()).collect(toSet());

		assertThat(assetKeys).containsExactlyInAnyOrder("A1", "A2", "A4");

		assets = mongoDBQueries.getSupportingAssetsForMission(Metadata.DEFAULT_CONTEXT, "Mission2");
		assetKeys = assets.stream().map(e -> e.getKey()).collect(toSet());

		assertThat(assetKeys).containsExactlyInAnyOrder("A2", "A3", "A4", "A5");
	}

	@Test
	public void getServicesDependingOnServiceWorks() throws Exception {
		mongoDBInit.initImpactDependentGraph();

		Set<String> names = null;

		names = mongoDBQueries.getServicesDependingOnService(Metadata.DEFAULT_CONTEXT, "S1");
		assertThat(names).containsExactlyInAnyOrder();

		names = mongoDBQueries.getServicesDependingOnService(Metadata.DEFAULT_CONTEXT, "S2");
		assertThat(names).containsExactlyInAnyOrder();

		names = mongoDBQueries.getServicesDependingOnService(Metadata.DEFAULT_CONTEXT, "S3");
		assertThat(names).containsExactlyInAnyOrder();

		names = mongoDBQueries.getServicesDependingOnService(Metadata.DEFAULT_CONTEXT, "S4");
		assertThat(names).containsExactlyInAnyOrder();

		names = mongoDBQueries.getServicesDependingOnService(Metadata.DEFAULT_CONTEXT, "S5");
		assertThat(names).containsExactlyInAnyOrder("S3");

		names = mongoDBQueries.getServicesDependingOnService(Metadata.DEFAULT_CONTEXT, "S6");
		assertThat(names).containsExactlyInAnyOrder();

		names = mongoDBQueries.getServicesDependingOnService(Metadata.DEFAULT_CONTEXT, "S7");
		assertThat(names).containsExactlyInAnyOrder();
	}

	@Test
	public void cyberAttackModelAndToHgJsonWorks() throws Exception {
		Vertex asset1 = addVertex(new Asset("Asset1"));

		mongoDBInit.initCyberAttackModel(asset1);

		log.info("CAM Vertices found with findAll():");
		log.info("-----------------------------------");
		for (Vertex vertex : mongoDB.findAllVertices(Metadata.DEFAULT_CONTEXT)) {
			log.info("\n{}", vertex);
		}
		log.info("\n");

		log.info("CAM Edges found with findAll():");
		log.info("--------------------------------");
		for (Edge edge : mongoDB.findAllEdges(Metadata.DEFAULT_CONTEXT)) {
			log.info("\n{}", edge);
		}
		log.info("\n");
	}

	@Test
	public void combinedModelWorks() throws Exception {
		mongoDBInit.initImpactDependentGraphWithAttack();

		System.out.println("Operational Capacity calculation in progress...");

		Double mission1oc = mongoDBQueries.getMissionOC(Metadata.DEFAULT_CONTEXT, "Mission1");
		Double mission2oc = mongoDBQueries.getMissionOC(Metadata.DEFAULT_CONTEXT, "Mission2");
		Double overallOC = mongoDBQueries.getOverallOC(Metadata.DEFAULT_CONTEXT);

		log.info("Combined Vertices found with findAll():");
		log.info("-----------------------------------");
		for (Vertex vertex : mongoDB.findAllVertices(Metadata.DEFAULT_CONTEXT)) {
			log.info("\n{}\n", vertex);
		}

		log.info("Combined Edges found with findAll():");
		log.info("--------------------------------");
		for (Edge edge : mongoDB.findAllEdges(Metadata.DEFAULT_CONTEXT)) {
			log.info("\n{}\n", edge);
		}

		log.info("Operational Capacity:");
		log.info("---------------------");
		log.info("Mission 1 Operational Capacity = {}\n", mission1oc);
		log.info("Mission 2 Operational Capacity = {}\n", mission2oc);
		log.info("*Overall* Operational Capacity = {}\n", overallOC);
	}

}
