package eu.h2020.protective.ca.mair.model;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import eu.h2020.protective.ca.mair.test.support.TestTags;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

@Tag(TestTags.FAST)
@Tag(TestTags.PARALLEL)
public class MatrixElementTest {

	@Test
	public void equalsContract() {
		class TestMatrixElement extends MatrixElement {
			@Override
			public boolean canEqual(Object obj) {
				return false;
			}
		}

		EqualsVerifier.forClass(MatrixElement.class).withRedefinedSuperclass()
				.withRedefinedSubclass(TestMatrixElement.class).suppress(Warning.NONFINAL_FIELDS)
				.verify();
	}

	@Test
	public void toStringContract() {
		new MatrixElement().toString();
	}
	
}
