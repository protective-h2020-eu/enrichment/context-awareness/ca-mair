package eu.h2020.protective.ca.mair.scheduled.tasks;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import eu.h2020.protective.ca.mair.test.support.TestTags;

@SpringBootTest
@ExtendWith({ SpringExtension.class })
@TestPropertySource(locations = "classpath:application-test.properties")
@Tag(TestTags.SLOW)
public class ScheduledTasksTests {

//    @SpyBean
//    private ScheduledTasks task;
//
//    @Test
//    public void jobRuns() {
//        Awaitility.await().atMost(Duration.FIVE_SECONDS).untilAsserted(() -> verify(task, times(1)).work());
//    }
    
}
