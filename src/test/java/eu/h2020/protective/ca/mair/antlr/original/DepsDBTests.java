package eu.h2020.protective.ca.mair.antlr.original;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import eu.h2020.protective.ca.mair.MongoDB;
import eu.h2020.protective.ca.mair.api.model.core.Graph;
import eu.h2020.protective.ca.mair.model.Metadata;
import eu.h2020.protective.ca.mair.test.support.TestTags;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { MongoDB.class })
@TestPropertySource(locations = "classpath:application-test.properties")
@Tag(TestTags.SLOW)
@Tag(TestTags.MONGO)
public class DepsDBTests {

	@Autowired
	private MongoDB mongoDB;

	private Map<String, Double> missionWeights = new HashMap<>();
	private Map<String, Double> vertexWeights = new HashMap<>();
	private Map<String, Double> edgeWeights = new HashMap<>();

	@BeforeEach
	public void setUp() {
		mongoDB.clearAllMatricesAndGraphs(Metadata.DEFAULT_CONTEXT);

		missionWeights.put("none", 0.0d);
		missionWeights.put("low", 1.0d);
		missionWeights.put("medium", 2.0d);
		missionWeights.put("high", 3.0d);
		missionWeights.put("critical", 4.0d);

		vertexWeights.put("none", 0.0d);
		vertexWeights.put("low", 1.0d);
		vertexWeights.put("medium", 2.0d);
		vertexWeights.put("high", 3.0d);
		vertexWeights.put("critical", 4.0d);

		edgeWeights.put("none", 0.0d);
		edgeWeights.put("low", 1.0d);
		edgeWeights.put("medium", 2.0d);
		edgeWeights.put("high", 3.0d);
		edgeWeights.put("critical", 4.0d);
	}

	@Test
	public void readCdg() throws Exception {
		org.reflections.Reflections.log = null;
		File file = new File("src/test/resources/antlr/cdg_1.deps");
		DepsTests.readCdg(mongoDB, file, false, true, missionWeights, vertexWeights, edgeWeights);
	}

	@Test
	public void readPsncCdg() throws Exception {
		org.reflections.Reflections.log = null;
		File file = new File("src/test/resources/antlr/psnc_vc.deps");
		DepsTests.readCdg(mongoDB, file, false, true, missionWeights, vertexWeights, edgeWeights);
	}

	@Test
	public void readPsncV7Cdg() throws Exception {
		org.reflections.Reflections.log = null;
		File file = new File("src/test/resources/antlr/psnc_vc_v7.deps");
		DepsTests.readCdg(mongoDB, file, false, true, missionWeights, vertexWeights, edgeWeights);
	}

	@Test
	public void readPsncV7LayersCdg() throws Exception {
		org.reflections.Reflections.log = null;
		File file = new File("src/test/resources/antlr/psnc_vc_v7.layers.deps");
		DepsTests.readCdg(mongoDB, file, false, true, missionWeights, vertexWeights, edgeWeights);
	}

	// @Test
	public void getCdgNames() throws Exception {
		readCdg();
		List<String> expected = DepsTests.getCdgNames();
		List<String> cdgNames = mongoDB.getAllCdgNames(Metadata.DEFAULT_CONTEXT);
		assertThat(cdgNames.size()).isGreaterThanOrEqualTo(expected.size());
		assertThat(cdgNames).containsExactly(expected.toArray(new String[expected.size()]));
	}

	// @Test
	public void getGraphByCdg() throws Exception {
		readCdg();
		Graph graph = mongoDB.getGraphByCdg(Metadata.DEFAULT_CONTEXT, "cdg1");
		assertThat(graph.getVertices()).hasSize(17);
		assertThat(graph.getEdges()).hasSize(17); // TODO Expand checks.
	}

}
