package eu.h2020.protective.ca.mair.model;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import eu.h2020.protective.ca.mair.test.support.TestTags;
import nl.jqno.equalsverifier.EqualsVerifier;

@Tag(TestTags.FAST)
@Tag(TestTags.PARALLEL)
public class TupleTest {

	@Test
	public void equalsContract() {
		class TestTuple extends Tuple<Object, Object> {
			public TestTuple(Object a, Object b) {
				super(a, b);
			}

			@Override
			public boolean canEqual(Object obj) {
				return false;
			}
		}

		EqualsVerifier.forClass(Tuple.class).withRedefinedSuperclass()
				.withRedefinedSubclass(TestTuple.class).verify();
	}

	@Test
	public void toStringContract() {
		new Tuple<Integer, Integer>(1,2).toString();
	}
	
}
