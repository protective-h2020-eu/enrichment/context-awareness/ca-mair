package eu.h2020.protective.ca.mair;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import eu.h2020.protective.ca.mair.api.model.core.Edge;
import eu.h2020.protective.ca.mair.api.model.core.Graph;
import eu.h2020.protective.ca.mair.api.model.core.Vertex;
import eu.h2020.protective.ca.mair.api.model2.edges.DependsOn;
import eu.h2020.protective.ca.mair.api.model2.nodes.Node;
import eu.h2020.protective.ca.mair.model.ASSoftware;
import eu.h2020.protective.ca.mair.model.Metadata;
import eu.h2020.protective.ca.mair.test.support.JsonBase;
import eu.h2020.protective.ca.mair.test.support.TestTags;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:application-test.properties")
@Tag(TestTags.SLOW)
@Tag(TestTags.MONGO)
public class MairApiTests extends JsonBase {

	@LocalServerPort
	private int port;

	@Autowired
	private MongoDB mongoDB;

	@Autowired
	private SyncGraph syncGraph;

//	@Autowired
//	private ConfigurationService config;
//
//	@Autowired
//	private MockRestServiceServer server;

	@BeforeEach
	public void setUpDB() throws Exception {
		mongoDB.clearAllMatricesAndGraphs(Metadata.DEFAULT_CONTEXT);
	}

	// @Test
	public void getNodeWithIpAddress() {
		createModel2SimpleGraph();
		// String ipAddress = "192.168.101.11";
		// Node node = mongoDBQueries.getNodeWithIpAddress(ipAddress);
		// assertThat(node.getIpAddresses()).containsExactly(ipAddress);
	}

	// @Test
	public void getNodeWithDns() {
		createModel2SimpleGraph();
		// String dns = "dns.11.com";
		// Node node = mongoDBQueries.getNodeWithDns(dns);
		// assertThat(node.getDns()).isEqualTo(dns);
	}

	// @Test
	public void getAssetCvss() {
//		Double expectedCvss = 1.23d;

		List<ASSoftware> swList = new ArrayList<>();
		ASSoftware sw1 = new ASSoftware("Word-1");
		sw1.setProduct("Word");
		sw1.setVendor("Microsoft");
		sw1.setVersion("1.0");
		swList.add(sw1);
		ASSoftware sw2 = new ASSoftware("Excel-1");
		sw2.setProduct("Excel");
		sw2.setVendor("Microsoft");
		sw2.setVersion("1.0");
		swList.add(sw2);
		ASSoftware sw3 = new ASSoftware("Windows-1");
		sw3.setProduct("Windows");
		sw3.setVendor("Microsoft");
		sw3.setVersion("10.0");
		swList.add(sw3);

		given().port(port).contentType(MediaType.APPLICATION_JSON_VALUE).body(swList).when()
				.post("/api/get-asset-max-cvss").then().statusCode(200);

//		Double actualCvss = this.client.getAssetCvss(swList);
//		assertThat(actualCvss).isEqualTo(expectedCvss);
	}

	// @Test
	public void getSoftwareCvss() {
//		Double expectedCvss = 1.23d;

		ASSoftware sw = new ASSoftware("Word-1");
		sw.setProduct("Word");
		sw.setVendor("Microsoft");
		sw.setVersion("1.0");

		given().port(port).contentType(MediaType.APPLICATION_JSON_VALUE).body(sw).when()
				.post("/api/get-software-max-cvss").then().statusCode(200);

//		Double actualCvss = this.client.getSoftwareCvss(sw);
//		assertThat(actualCvss).isEqualTo(expectedCvss);
	}

	@Test
	public void castDependsOnToEdge() {
		Graph graphIn = createModel2SimpleGraph();
		assertThat(graphIn).isNotNull();
		DependsOn dependsOn = (DependsOn) graphIn.getEdges().get(0);
		assertThat(dependsOn).isNotNull();
		Edge edge = (Edge) dependsOn;
		assertThat(edge).isNotNull();
	}

	@Test
	public void getGraphByCDG() throws Exception {
		Graph graphIn = createModel2SimpleGraph();
		syncGraph.addOrUpdateGraph(graphIn);
		/* Graph graphOut = */ syncGraph.getGraph(graphIn.getContext());
		// String graphJsonOut = graphOut.toJson(); // TODO add body json check
		String cdgName = URLEncoder.encode(Edge.OTHER_EDGES_CDG, StandardCharsets.UTF_8.name());
		given().port(port).when().get("/api/get-graph-by-cdg/{cdgName}", cdgName).then().statusCode(200);// .body(arguments,
																											// equalTo(graphJsonOut));
	}

	@Test
	public void importVerticesAndExportVerticesWithAddedIpEtc() throws Exception {
		Path path = Paths.get("src/test/resources/antlr/psnc_vc.cdgv");
		String json = new String(Files.readAllBytes(path));
		given().port(port).contentType(MediaType.APPLICATION_JSON_VALUE).body(json).when().post("/api/graph-add").then()
				.statusCode(200);
		Graph graphIn = syncGraph.getGraph(Metadata.DEFAULT_CONTEXT);
		int i = 5;
		for (Vertex vertex : graphIn.getVertices()) {
			if (vertex instanceof Node) {
				String hex = String.format("%02X", i & 0xFF);
				Node node = (Node) vertex;
				node.getIpAddresses().add("192.168.1." + i);
				node.getIpV4Addresses().add("192.168.1." + i);
				node.setMacAddress("fa:16:3e:b4:4b:" + hex);
				node.setDns("dns." + i + ".com");
				node.setDeviceType("Physical");
				node.setArchitecture("AMD64");
				node.setAssetTag("PSNC-" + i);
				mongoDB.insertOrUpdateOne(graphIn.getContext(), node);
				i++;
			}
		}
		Graph graphOut = syncGraph.getGraph(graphIn.getContext());
		graphOut.setEdges(new ArrayList<Edge>(0));
		toJsonFile(graphOut, "psnc_vc.with.ip.tmp.cdgv");
	}

	private Graph createModel2SimpleGraph() {
		Graph graph = new Graph();
		graph.setContext(Metadata.DEFAULT_CONTEXT);
		{
			eu.h2020.protective.ca.mair.api.model2.nodes.Computer computer = new eu.h2020.protective.ca.mair.api.model2.nodes.Computer(
					"Computer 1");
			computer.setName("Brians PC");
			computer.getIpAddresses().add("192.168.101.11");
			computer.setBusinessOwner("Brian");
			computer.setTechnicalOwner("Eoin");
			computer.setLocation("Athlone");
			computer.setDescription("Dell Inspiron 1700 Laptop");
			computer.setMacAddress("fa:16:3e:b4:4b:fb");
			computer.setDefaultGateway("192.168.101.1");
			computer.setIpGateway("192.168.101.1");
			computer.setDns("dns.11.com");
			computer.setIpV4Subnet("192.168.101.0");
			computer.setIpV4SubnetMask("255.255.255.0");
			computer.getIpV4Addresses().add("192.168.101.11");
			computer.setNetworkType("Wifi");
			computer.setModel("Dell Inspiron");
			computer.setDeviceType("Physical");
			computer.setArchitecture("AMD64");

			eu.h2020.protective.ca.mair.api.model2.assets.Software os = new eu.h2020.protective.ca.mair.api.model2.assets.Software(
					"Computer 1 - Windows");
			os.setName("Windows 10");
			os.setVersion("10");
			os.setPatchLevel("1");
			os.setBusinessOwner("Brian");
			os.setTechnicalOwner("Eoin");
			os.setLocation("Athlone");
			os.setDescription("Windows OS");
			os.setTargetArchitecture("AMD64");

			eu.h2020.protective.ca.mair.api.model2.assets.Software app = new eu.h2020.protective.ca.mair.api.model2.assets.Software(
					"Word");
			app.setName("Brians Word");
			app.setVersion("2016");
			app.setPatchLevel("1");
			app.setBusinessOwner("Brian");
			app.setTechnicalOwner("Eoin");
			app.setLocation("Athlone");
			app.setDescription("Microsoft Word 2016");
			app.setTargetArchitecture("AMD64");

			DependsOn runsOnPC = new DependsOn(os, computer);
			DependsOn runsOnOS = new DependsOn(app, os);

			graph.addVertex(app);
			graph.addVertex(os);
			graph.addVertex(computer);
			graph.addEdge(os, computer, runsOnPC);
			graph.addEdge(app, os, runsOnOS);
		}
		{
			eu.h2020.protective.ca.mair.api.model2.nodes.NetworkNode networkNode = new eu.h2020.protective.ca.mair.api.model2.nodes.NetworkNode(
					"NetworkNode 1");
			networkNode.setName("Brians Router");
			networkNode.getIpAddresses().add("192.168.101.2");
			networkNode.setBusinessOwner("Brian");
			networkNode.setTechnicalOwner("Eoin");
			networkNode.setLocation("Athlone");
			networkNode.setDescription("Belkin router");
			networkNode.setMacAddress("fa:16:3e:b4:4b:5a");
			networkNode.setDefaultGateway("192.168.101.1");
			networkNode.setIpGateway("192.168.101.1");
			networkNode.setDns("dns.2.com");
			networkNode.setIpV4Subnet("192.168.101.0");
			networkNode.setIpV4SubnetMask("255.255.255.0");
			networkNode.getIpV4Addresses().add("192.168.101.2");
			networkNode.setNetworkType("Wifi");
			networkNode.setModel("Belkin Router");
			networkNode.setDeviceType("Physical");
			networkNode.setArchitecture("ARM32");

			eu.h2020.protective.ca.mair.api.model2.assets.Software os = new eu.h2020.protective.ca.mair.api.model2.assets.Software(
					"NetworkNode 1 - Ubuntu");
			os.setName("Ubuntu");
			os.setVersion("16.10");
			os.setPatchLevel("1");
			os.setBusinessOwner("Brian");
			os.setTechnicalOwner("Eoin");
			os.setLocation("Athlone");
			os.setDescription("Ubuntu Linux OS");
			os.setTargetArchitecture("AMD64");

			DependsOn runsOn = new DependsOn(os, networkNode);

			graph.addVertex(os);
			graph.addVertex(networkNode);
			graph.addEdge(os, networkNode, runsOn);
		}
		mongoDB.insertOrUpdateManyVertices(graph.getContext(), graph.getVertices());
		mongoDB.insertOrUpdateManyEdges(graph.getContext(), graph.getEdges());
		return graph;
	}

}
