package eu.h2020.protective.ca.mair.assetrank;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.text.DecimalFormat;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.antlr.v4.runtime.misc.Pair;
import org.jgrapht.alg.interfaces.VertexScoringAlgorithm;
import org.jgrapht.alg.scoring.PageRank;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import eu.h2020.protective.ca.mair.antlr.json.CdgTests;
import eu.h2020.protective.ca.mair.api.model.core.Edge;
import eu.h2020.protective.ca.mair.api.model.core.GVertex;
import eu.h2020.protective.ca.mair.api.model.core.Graph;
import eu.h2020.protective.ca.mair.api.model.core.Vertex;
import eu.h2020.protective.ca.mair.test.support.TestTags;
import lombok.extern.slf4j.XSlf4j;

@XSlf4j
@Tag(TestTags.FAST)
@Tag(TestTags.PARALLEL)
public class AssetRankTests {

	public static final int MAX_ITERATIONS_DEFAULT = 100;
	public static final double TOLERANCE_DEFAULT = 0.000_001;
	public static final double DAMPING_FACTOR_DEFAULT = 0.85d;

	private Map<String, Vertex> vertices;
	private Map<String, Map<Pair<String, String>, Edge>> edges;

	private Map<String, Double> missionWeights = new HashMap<>();
	private Map<String, Double> vertexWeights = new HashMap<>();
	private Map<String, Double> edgeWeights = new HashMap<>();

	private void createCdgs() throws Exception {
		org.reflections.Reflections.log = null;
		File file = new File("src/test/resources/antlr/cdg_1.cdgs");
		CdgTests.readCdg(vertices, edges, file, null, true, true, missionWeights, vertexWeights, edgeWeights);
	}

	private Graph createGraph() throws Exception {
		createCdgs();
		Graph graph = new Graph();
		for (Map<Pair<String, String>, Edge> cdg : this.edges.values()) {
			for (Edge edge : cdg.values()) {
				Vertex from = vertices.get(edge.getFrom());
				Vertex to = vertices.get(edge.getTo());
				graph.addVertex(from);
				graph.addVertex(to);
				graph.addEdge(from, to, edge);
			}
		}
		return graph;
	}

	private void logScores(VertexScoringAlgorithm<GVertex, Double> algorithm) {
		Map<GVertex, Double> sortedScores = algorithm.getScores().entrySet().stream()
				.sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
						(oldValue, newValue) -> oldValue, LinkedHashMap::new));
		for (Entry<GVertex, Double> entry : sortedScores.entrySet()) {
			double score = entry.getValue().doubleValue() * 100;
			DecimalFormat df = new DecimalFormat("###.###");
			log.info("{} = {}", df.format(score), entry.getKey().getKey());
		}
	}

	private Graph createBasicOrGraph(boolean reverse) throws Exception {
		Graph graph = new Graph();
		Vertex p1 = new Vertex("p1");
		Vertex p2 = new Vertex("p2");
		Vertex p3 = new Vertex("p3");
		Vertex v1 = new Vertex("v1");
		Vertex v2 = new Vertex("v2");
		Vertex v3 = new Vertex("v3");
		Vertex v4 = new Vertex("v4");
		Edge p1p2 = new Edge(p1, p2);
		Edge p1p3 = new Edge(p1, p3);
		Edge p2v1 = new Edge(p2, v1);
		Edge p2v2 = new Edge(p2, v2);
		Edge p3v3 = new Edge(p3, v3);
		Edge p3v4 = new Edge(p3, v4);
		p1.setWeight(1.0d);
		p1.setExpression(Vertex.OR);
		p2.setExpression(Vertex.OR);
		p3.setExpression(Vertex.OR);
		v1.setExpression(Vertex.SINK);
		v2.setExpression(Vertex.SINK);
		v3.setExpression(Vertex.SINK);
		v4.setExpression(Vertex.SINK);
		addEdge(graph, p1, p2, p1p2, reverse);
		addEdge(graph, p1, p3, p1p3, reverse);
		addEdge(graph, p2, v1, p2v1, reverse);
		addEdge(graph, p2, v2, p2v2, reverse);
		addEdge(graph, p3, v3, p3v3, reverse);
		addEdge(graph, p3, v4, p3v4, reverse);
		return graph;
	}

	private Graph createBasicAndOrGraph(boolean reverse) throws Exception {
		Graph graph = new Graph();
		Vertex p1 = new Vertex("p1");
		Vertex p2 = new Vertex("p2");
		Vertex p3 = new Vertex("p3");
		Vertex v1 = new Vertex("v1");
		Vertex v2 = new Vertex("v2");
		Vertex v3 = new Vertex("v3");
		Vertex v4 = new Vertex("v4");
		Edge p1p2 = new Edge(p1, p2);
		Edge p1p3 = new Edge(p1, p3);
		Edge p2v1 = new Edge(p2, v1);
		Edge p2v2 = new Edge(p2, v2);
		Edge p3v3 = new Edge(p3, v3);
		Edge p3v4 = new Edge(p3, v4);
		p1.setWeight(1.0d);
		p1.setExpression(Vertex.AND);
		p2.setExpression(Vertex.AND);
		p3.setExpression(Vertex.OR);
		v1.setExpression(Vertex.SINK);
		v2.setExpression(Vertex.SINK);
		v3.setExpression(Vertex.SINK);
		v4.setExpression(Vertex.SINK);
		addEdge(graph, p1, p2, p1p2, reverse);
		addEdge(graph, p1, p3, p1p3, reverse);
		addEdge(graph, p2, v1, p2v1, reverse);
		addEdge(graph, p2, v2, p2v2, reverse);
		addEdge(graph, p3, v3, p3v3, reverse);
		addEdge(graph, p3, v4, p3v4, reverse);
		return graph;
	}

	private void addEdge(Graph g, Vertex sourceVertex, Vertex targetVertex, Edge e,
			boolean reverse) {
		g.addVertex(sourceVertex);
		g.addVertex(targetVertex);
		if (reverse) {
			e = e.changeDirection();
			g.addEdge(targetVertex, sourceVertex, e);
		} else {
			g.addEdge(sourceVertex, targetVertex, e);
		}
	}

	@BeforeEach
	public void setUp() {
		vertices = new HashMap<>();
		edges = new HashMap<>();

		missionWeights.put("none", 0.0d);
		missionWeights.put("low", 1.0d);
		missionWeights.put("medium", 2.0d);
		missionWeights.put("high", 3.0d);
		missionWeights.put("critical", 4.0d);

		vertexWeights.put("none", 0.0d);
		vertexWeights.put("low", 1.0d);
		vertexWeights.put("medium", 2.0d);
		vertexWeights.put("high", 3.0d);
		vertexWeights.put("critical", 4.0d);

		edgeWeights.put("none", 0.0d);
		edgeWeights.put("low", 1.0d);
		edgeWeights.put("medium", 2.0d);
		edgeWeights.put("high", 3.0d);
		edgeWeights.put("critical", 4.0d);
	}


	@Test
	public void runPageRank() throws Exception {
		logScores(new PageRank<>(createGraph()));
	}

	@Test
	public void runBasicOrPageRank() throws Exception {
		logScores(new PageRank<>(createBasicOrGraph(true), DAMPING_FACTOR_DEFAULT,
				MAX_ITERATIONS_DEFAULT, Double.MIN_VALUE));
	}

	@Test
	public void runBasicOrAssetRankFor2Iterations() throws Exception {
		logScores(new AssetRank(createBasicOrGraph(false), DAMPING_FACTOR_DEFAULT, 2,
				TOLERANCE_DEFAULT));
	}

	@Test // 3 matches the rankings in the paper, but does not converge at 4.
	public void runBasicOrAssetRankFor3Iterations() throws Exception {
		logScores(new AssetRank(createBasicOrGraph(false), DAMPING_FACTOR_DEFAULT, 3,
				TOLERANCE_DEFAULT));
	}

	@Test // 4 should be similar to 3 in order to converge at 4. This is not the case.
	public void runBasicOrAssetRankFor4Iterations() throws Exception {
		logScores(new AssetRank(createBasicOrGraph(false), DAMPING_FACTOR_DEFAULT, 4,
				TOLERANCE_DEFAULT));
	}

	@Test // This actually converges at 39
	public void runBasicOrAssetRank() throws Exception {
		logScores(new AssetRank(createBasicOrGraph(false), DAMPING_FACTOR_DEFAULT,
				MAX_ITERATIONS_DEFAULT, TOLERANCE_DEFAULT));
	}

	@Test
	public void runBasicAndOrAssetRank4() throws Exception {
		logScores(new AssetRank(createBasicAndOrGraph(false), DAMPING_FACTOR_DEFAULT,
				MAX_ITERATIONS_DEFAULT, TOLERANCE_DEFAULT));
	}

	@Test
	public void runBasicAndOrAssetRank3() throws Exception {
		logScores(new AssetRank(createBasicAndOrGraph(false), DAMPING_FACTOR_DEFAULT,
				MAX_ITERATIONS_DEFAULT));
	}

	@Test
	public void runBasicAndOrAssetRank2() throws Exception {
		logScores(new AssetRank(createBasicAndOrGraph(false), DAMPING_FACTOR_DEFAULT));
	}

	@Test
	public void runBasicAndOrAssetRank1() throws Exception {
		logScores(new AssetRank(createBasicAndOrGraph(false)));
	}

	@Test
	public void getVertexScore() throws Exception {
		Graph graph = createBasicAndOrGraph(false);
		AssetRank assetRank = new AssetRank(graph);
		GVertex vertex = graph.vertexSet().stream().findFirst().get();
		double score = vertex.getVertex().getScore();
		assertThat(assetRank.getVertexScore(vertex)).isEqualTo(score);
	}

	@Test
	public void getVertexScoreException() throws Exception {
		try {
			Graph graph = createBasicAndOrGraph(false);
			GVertex vertex = graph.vertexSet().stream().findFirst().get();
			AssetRank assetRank = new AssetRank(graph); // This modifies vertex!
			assetRank.getVertexScore(vertex);
		} catch (IllegalArgumentException e) {
			assertThat(e.getMessage()).isEqualTo("Cannot return score of unknown vertex");
		}
	}

	@Test
	public void invalidMaxIterations() throws Exception {
		try {
			new AssetRank(createBasicAndOrGraph(false), DAMPING_FACTOR_DEFAULT, 0,
					TOLERANCE_DEFAULT);
		} catch (IllegalArgumentException e) {
			assertThat(e.getMessage()).isEqualTo("Maximum iterations must be positive");
		}
	}

	@Test
	public void invalidDampingFactor() throws Exception {
		try {
			new AssetRank(createBasicAndOrGraph(false), -Double.MIN_VALUE, MAX_ITERATIONS_DEFAULT,
					TOLERANCE_DEFAULT);
		} catch (IllegalArgumentException e) {
			assertThat(e.getMessage()).isEqualTo("Damping factor not valid");
		}
		try {
			new AssetRank(createBasicAndOrGraph(false), 1.0d + Double.MIN_VALUE,
					MAX_ITERATIONS_DEFAULT, TOLERANCE_DEFAULT);
		} catch (IllegalArgumentException e) {
			assertThat(e.getMessage()).isEqualTo("Damping factor not valid");
		}
	}

	@Test
	public void invalidTolerance() throws Exception {
		try {
			new AssetRank(createBasicAndOrGraph(false), DAMPING_FACTOR_DEFAULT,
					MAX_ITERATIONS_DEFAULT, 0.0d);
		} catch (IllegalArgumentException e) {
			assertThat(e.getMessage()).isEqualTo("Tolerance not valid, must be positive");
		}
	}

	@Test
	public void runBasicOrAssetRanking() throws Exception {
		createBasicOrGraph(true);
		new AssetRanking("src/test/resources/assetRank/basicOr.txt", 7, 100);
	}

	@Test
	public void runBasicAndOrAssetRanking() throws Exception {
		createBasicAndOrGraph(true);
		new AssetRanking("src/test/resources/assetRank/basicAndOr.txt", 7, 100);
	}

}
