package eu.h2020.protective.ca.mair;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.File;
import java.io.InputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.assertj.core.data.Offset;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.google.gson.Gson;

import eu.h2020.protective.ca.mair.antlr.cluster.insight.ClusterTests;
import eu.h2020.protective.ca.mair.antlr.json.CdgTests;
import eu.h2020.protective.ca.mair.antlr.original.DepsTests;
import eu.h2020.protective.ca.mair.api.model.assets.Application;
import eu.h2020.protective.ca.mair.api.model.assets.Asset;
import eu.h2020.protective.ca.mair.api.model.assets.Computer;
import eu.h2020.protective.ca.mair.api.model.assets.NetworkNode;
import eu.h2020.protective.ca.mair.api.model.assets.OperatingSystem;
import eu.h2020.protective.ca.mair.api.model.core.Edge;
import eu.h2020.protective.ca.mair.api.model.core.Graph;
import eu.h2020.protective.ca.mair.api.model.core.GraphElement;
import eu.h2020.protective.ca.mair.api.model.core.Vertex;
import eu.h2020.protective.ca.mair.api.model.edges.ConnectedTo;
import eu.h2020.protective.ca.mair.api.model.edges.HostedOn;
import eu.h2020.protective.ca.mair.api.model.edges.RealisedBy;
import eu.h2020.protective.ca.mair.api.model.services.ITService;
import eu.h2020.protective.ca.mair.api.model.services.Service;
import eu.h2020.protective.ca.mair.api.model2.edges.DependsOn;
import eu.h2020.protective.ca.mair.api.model2.edges.RealisedAs;
import eu.h2020.protective.ca.mair.api.model2.edges.RunsOn;
import eu.h2020.protective.ca.mair.api.model2.edges.Uses;
import eu.h2020.protective.ca.mair.assetrank.AssetRank;
import eu.h2020.protective.ca.mair.model.Metadata;
import eu.h2020.protective.ca.mair.test.support.MockMvcBase;
import eu.h2020.protective.ca.mair.test.support.TestTags;
import lombok.extern.slf4j.XSlf4j;

// @ActiveProfiles(profiles = "non-async")
@Tag(TestTags.MONGO)
@XSlf4j
public class RestApiTests extends MockMvcBase {

	private MediaType contentType = MediaType.APPLICATION_JSON_UTF8;

	private HttpMessageConverter<Object> mappingJackson2HttpMessageConverter;

	private Map<String, Double> missionWeights = new HashMap<>();
	private Map<String, Double> vertexWeights = new HashMap<>();
	private Map<String, Double> edgeWeights = new HashMap<>();

//	@Autowired
//	private ConfigurationService config;

	@Autowired
	private MongoDBInit mongoDBInit;

	@Autowired
	private MatrixInit matrixInit;

	@Autowired
	private SyncGraph syncGraph;

	@Autowired
	private MongoDB mongoDB;

	@Autowired
	private MongoDBQueries mongoDBQueries;

	@SuppressWarnings("unchecked")
	@Autowired
	void setConverters(HttpMessageConverter<?>[] converters) {
		this.mappingJackson2HttpMessageConverter = (HttpMessageConverter<Object>) Arrays.asList(converters).stream()
				.filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter).findAny().orElse(null);
		assertThat(this.mappingJackson2HttpMessageConverter).as("the JSON message converter must not be null")
				.isNotNull();
		// // TODO new Below
		// Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
		// builder.indentOutput(true).dateFormat(new
		// SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ"));
		// this.mappingJackson2HttpMessageConverter = new
		// MappingJackson2HttpMessageConverter(builder.build());
	}

	@BeforeEach
	public void setUpDB() throws Exception {
		mongoDB.clearAllMatricesAndGraphs(Metadata.DEFAULT_CONTEXT);

		missionWeights.put("none", 0.0d);
		missionWeights.put("low", 1.0d);
		missionWeights.put("medium", 2.0d);
		missionWeights.put("high", 3.0d);
		missionWeights.put("critical", 4.0d);

		vertexWeights.put("none", 0.0d);
		vertexWeights.put("low", 1.0d);
		vertexWeights.put("medium", 2.0d);
		vertexWeights.put("high", 3.0d);
		vertexWeights.put("critical", 4.0d);

		edgeWeights.put("none", 0.0d);
		edgeWeights.put("low", 1.0d);
		edgeWeights.put("medium", 2.0d);
		edgeWeights.put("high", 3.0d);
		edgeWeights.put("critical", 4.0d);
	}

	@Test
	public void uploadGraph() throws Exception {
		matrixInit.initMatrices();

		String filename1 = "temp.upload.json";
		Graph graph1 = syncGraph.getGraph(Metadata.DEFAULT_CONTEXT);
		assertThat(graph1.getVertices()).isNotEmpty();
		assertThat(graph1.getEdges()).isNotEmpty();

		matrixInit.clearMatrices();

		Graph graph2 = syncGraph.getGraph(graph1.getContext());
		assertThat(graph2.getVertices()).isEmpty();
		assertThat(graph2.getEdges()).isEmpty();

		Path path = toJsonFile(graph1, filename1);
		InputStream inputStream = Files.newInputStream(path);

		MockMultipartFile jsonFile = new MockMultipartFile("json", filename1, MediaType.APPLICATION_JSON_UTF8_VALUE,
				inputStream);
		mockMvc.perform(MockMvcRequestBuilders.multipart("/api/graph-upload").file(jsonFile))
				.andExpect(status().isOk());

		Graph graph3 = syncGraph.getGraph(graph1.getContext());
		assertThat(graph3.getVertices()).isNotEmpty();
		assertThat(graph3.getEdges()).isNotEmpty();

		assertThat(graph1.getVertices()).hasSameSizeAs(graph3.getVertices());
		assertThat(graph1.getEdges()).hasSameSizeAs(graph3.getEdges());

		toJsonFile(graph1, "RestApiTests.uploadGraph.graph1.json");
		toJsonFile(graph3, "RestApiTests.uploadGraph.graph3.json");

		assertEqualToIgnoringGivenFields(graph1, graph3, "id", "created", "lastModified", "group", "cdgCount");
	}

	@Test
	public void about() throws Exception {
		this.mockMvc.perform(get("/api/about")).andExpect(status().isOk())
				.andDo(MockMvcRestDocumentation.document("about"));
	}

	@Test
	public void graphInEqualsGraphOut() throws Exception {
		Graph graphIn = createSimpleGraph();
		syncGraph.addOrUpdateGraph(graphIn);
		Graph graphOut = syncGraph.getGraph(graphIn.getContext());
		assertEqualToIgnoringGivenFields(graphIn, graphOut, "id", "created", "lastModified", "group", "cdgCount");
	}

	@Test
	public void getGraph() throws Exception {
		Graph graphIn = createSimpleGraph();
		syncGraph.addOrUpdateGraph(graphIn);
		Graph graphOut = syncGraph.getGraph(graphIn.getContext());
		String graphJsonOut = graphOut.toJson();
		MvcResult result = this.mockMvc.perform(get("/api/graph-get")).andExpect(status().isOk())
				.andDo(MockMvcRestDocumentation.document("graph-get")).andReturn();
		String graphJsonFromApi = result.getResponse().getContentAsString();
		JSONAssert.assertEquals(graphJsonOut, graphJsonFromApi, false);
	}

	@Test
	public void getGraphByCDG() throws Exception { // FIXME Use specific CDG.
		Graph graphIn = createSimpleGraph();
		syncGraph.addOrUpdateGraph(graphIn);
		Graph graphOut = syncGraph.getGraph(graphIn.getContext());
		String graphJsonOut = graphOut.toJson();
		String cdgName = URLEncoder.encode(Edge.OTHER_EDGES_CDG, StandardCharsets.UTF_8.name());
		MvcResult result = this.mockMvc.perform(get("/api/get-graph-by-cdg/" + cdgName)).andExpect(status().isOk())
				.andDo(MockMvcRestDocumentation.document("get-graph-by-cdg")).andReturn();
		String graphJsonFromApi = result.getResponse().getContentAsString();
		JSONAssert.assertEquals(graphJsonOut, graphJsonFromApi, false);
	}

	@Test
	public void getSubGraph() throws Exception { // FIXME Empty sub graph!
		Graph graphIn = createSimpleGraph();
		syncGraph.addOrUpdateGraph(graphIn);
		Graph graphOut = mongoDBQueries.getSubGraph(graphIn.getContext(), "Computer 1 - Windows");
		String graphJsonOut = graphOut.toJson();
		this.mockMvc.perform(get("/api/graph-get/Computer 1 - Windows")).andExpect(status().isOk())
				.andExpect(content().json(graphJsonOut)).andDo(MockMvcRestDocumentation.document("graph-get-by-key"));
	}

	@Test
	public void updateGraph() throws Exception {
		Graph graph = createSimpleGraph();
		String graphJson = graph.toJson();
		this.mockMvc
				.perform(post("/api/graph-update").accept(MediaType.APPLICATION_JSON).contentType(contentType)
						.content(graphJson))
				.andExpect(status().isOk()).andDo(MockMvcRestDocumentation.document("graph-update"));
	}

	@Test
	public void addGraphComplete() throws Exception {
		Graph graph = createModel2CompleteGraph();
		String graphJson = graph.toJson();
		this.mockMvc
				.perform(post("/api/graph-add").accept(MediaType.APPLICATION_JSON).contentType(contentType)
						.content(graphJson))
				.andExpect(status().isOk()).andDo(MockMvcRestDocumentation.document("graph-add-complete"));
	}

	@Test
	public void addGraph() throws Exception {
		Graph graph = createSimpleGraph();
		String graphJson = graph.toJson();
		this.mockMvc
				.perform(post("/api/graph-add").accept(MediaType.APPLICATION_JSON).contentType(contentType)
						.content(graphJson))
				.andExpect(status().isOk()).andDo(MockMvcRestDocumentation.document("graph-add"));
	}

	@Test
	public void addGraphToCdg() throws Exception { // FIXME Use specific CDG.
		Graph graph = createSimpleGraph();
		String graphJson = graph.toJson();
		String encodedCdgName = URLEncoder.encode(Edge.OTHER_EDGES_CDG, StandardCharsets.UTF_8.name());
		this.mockMvc
				.perform(post("/api/graph-add/" + encodedCdgName).accept(MediaType.APPLICATION_JSON)
						.contentType(contentType).content(graphJson))
				.andExpect(status().isOk()).andDo(MockMvcRestDocumentation.document("graph-add-to-cdg"));
	}

	@Test
	public void addGraphTwice() throws Exception {
		Graph graph = createSimpleGraph();
		String graphJson = graph.toJson();
		this.mockMvc.perform(
				post("/api/graph-add").accept(MediaType.APPLICATION_JSON).contentType(contentType).content(graphJson))
				.andExpect(status().isOk());
		this.mockMvc.perform(
				post("/api/graph-add").accept(MediaType.APPLICATION_JSON).contentType(contentType).content(graphJson))
				.andExpect(status().isOk());
	}

	// @Test // FIXME Check userToken with graph-add
	public void addGraphFromJsonWithUserToken() throws Exception {
		Graph graph = createSimpleGraph();
		String graphJson = graph.toJson();
		this.mockMvc
				.perform(post("/api/graph-add").with(userToken()).accept(MediaType.APPLICATION_JSON)
						.contentType(contentType).content(graphJson))
				.andExpect(status().isOk()).andDo(MockMvcRestDocumentation.document("graph-add-with-token"));
	}

	@Test
	public void removeCdg() throws Exception {
		Graph graphIn = createModel2SimpleGraph();
		String graphJsonIn = graphIn.toJson();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getVertices()).isNotEmpty();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getEdges()).isNotEmpty();
		String encodedCdgName = URLEncoder.encode("infrastructureLayer", StandardCharsets.UTF_8.name());
		this.mockMvc
				.perform(post("/api/remove-cdg/" + encodedCdgName).accept(MediaType.APPLICATION_JSON)
						.contentType(contentType).content(graphJsonIn))
				.andExpect(status().isOk()).andDo(MockMvcRestDocumentation.document("remove-cdg-1"));
		assertThat(syncGraph.getGraph(graphIn.getContext()).getVertices()).isNotEmpty();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getEdges()).isNotEmpty();
		encodedCdgName = URLEncoder.encode("applicationLayer", StandardCharsets.UTF_8.name());
		this.mockMvc
				.perform(post("/api/remove-cdg/" + encodedCdgName).accept(MediaType.APPLICATION_JSON)
						.contentType(contentType).content(graphJsonIn))
				.andExpect(status().isOk()).andDo(MockMvcRestDocumentation.document("remove-cdg-2"));
		assertThat(syncGraph.getGraph(graphIn.getContext()).getEdges()).isEmpty();
		// FIXME only from vertices are deleted? is this correct behaviour?
		// Why some left?
		// assertThat(syncGraph.getGraph().getVertices()).isEmpty();
	}

	@Test
	public void removeGraphFromCdg() throws Exception {
		Graph graphIn = createModel2SimpleGraph();
		// syncGraph.addOrUpdateGraph(graphIn);
		String graphJsonIn = graphIn.toJson();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getVertices()).isNotEmpty();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getEdges()).isNotEmpty();
		String encodedCdgName = URLEncoder.encode("infrastructureLayer", StandardCharsets.UTF_8.name());
		this.mockMvc
				.perform(post("/api/graph-remove/" + encodedCdgName).accept(MediaType.APPLICATION_JSON)
						.contentType(contentType).content(graphJsonIn))
				.andExpect(status().isOk()).andDo(MockMvcRestDocumentation.document("graph-remove"));
		assertThat(syncGraph.getGraph(graphIn.getContext()).getVertices()).isNotEmpty();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getEdges()).isNotEmpty();
		encodedCdgName = URLEncoder.encode("applicationLayer", StandardCharsets.UTF_8.name());
		this.mockMvc
				.perform(post("/api/graph-remove/" + encodedCdgName).accept(MediaType.APPLICATION_JSON)
						.contentType(contentType).content(graphJsonIn))
				.andExpect(status().isOk()).andDo(MockMvcRestDocumentation.document("graph-remove"));
		assertThat(syncGraph.getGraph(graphIn.getContext()).getEdges()).isEmpty();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getVertices()).isEmpty();
	}

	@Test
	public void getVerticesByTypeAndCdg() throws Exception {
		Graph graphIn = createModel2SimpleGraph();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getVertices()).isNotEmpty();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getEdges()).isNotEmpty();
		String encodedVertexType = URLEncoder.encode(ITService.class.getName(), StandardCharsets.UTF_8.name());
		String encodedCdgName = URLEncoder.encode("infrastructureLayer", StandardCharsets.UTF_8.name());
		this.mockMvc
				.perform(get("/api/get-vertices-by-type-and-cdg/" + encodedVertexType + "/" + encodedCdgName)
						.accept(MediaType.APPLICATION_JSON).contentType(contentType))
				.andExpect(status().isOk()).andDo(MockMvcRestDocumentation.document("get-vertices-by-type-and-cdg"));
		// FIXME Check returned content!
	}

	@Test
	public void getVerticesByCdg() throws Exception {
		Graph graphIn = createModel2SimpleGraph();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getVertices()).isNotEmpty();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getEdges()).isNotEmpty();
		String encodedCdgName = URLEncoder.encode("infrastructureLayer", StandardCharsets.UTF_8.name());
		this.mockMvc
				.perform(get("/api/get-vertices-by-cdg/" + encodedCdgName).accept(MediaType.APPLICATION_JSON)
						.contentType(contentType))
				.andExpect(status().isOk()).andDo(MockMvcRestDocumentation.document("get-vertices-by-cdg"));
		// FIXME Check returned content!
	}

	@Test
	public void getCdgNames() throws Exception {
		Graph graphIn = createModel2SimpleGraph();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getVertices()).isNotEmpty();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getEdges()).isNotEmpty();
		this.mockMvc.perform(get("/api/get-cdg-names").accept(MediaType.APPLICATION_JSON).contentType(contentType))
				.andExpect(status().isOk()).andDo(MockMvcRestDocumentation.document("get-cdg-names"));
		// FIXME Check returned content!
	}

	@Test
	public void getNodeWithIp() throws Exception {
		Graph graphIn = createModel2SimpleGraph();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getVertices()).isNotEmpty();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getEdges()).isNotEmpty();
		String encodedIpAddress = URLEncoder.encode("192.168.101.2", StandardCharsets.UTF_8.name());
		this.mockMvc
				.perform(get("/api/get-node-with-ip/" + encodedIpAddress).accept(MediaType.APPLICATION_JSON)
						.contentType(contentType))
				.andExpect(status().isOk()).andExpect(jsonPath("id").value("NetworkNode 1"))
				.andDo(MockMvcRestDocumentation.document("get-node-with-ip"));
	}

	@Test
	public void getNodeWithDns() throws Exception {
		Graph graphIn = createModel2SimpleGraph();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getVertices()).isNotEmpty();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getEdges()).isNotEmpty();
		String encodedDns = URLEncoder.encode("dns.11.com", StandardCharsets.UTF_8.name());
		this.mockMvc
				.perform(get("/api/get-node-with-dns/" + encodedDns).accept(MediaType.APPLICATION_JSON)
						.contentType(contentType))
				.andExpect(status().isOk()).andExpect(jsonPath("id").value("Computer 1"))
				.andDo(MockMvcRestDocumentation.document("get-node-with-dns"));
	}

	@Test
	public void getNodeCriticalityForIp() throws Exception {
		Graph graphIn = createModel2SimpleGraph();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getVertices()).isNotEmpty();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getEdges()).isNotEmpty();
		String encodedIpAddress = URLEncoder.encode("192.168.101.2", StandardCharsets.UTF_8.name());
		MvcResult result = this.mockMvc
				.perform(get("/api/get-node-criticality-for-ip/" + encodedIpAddress).accept(MediaType.APPLICATION_JSON)
						.contentType(contentType))
				.andExpect(status().isOk()).andDo(MockMvcRestDocumentation.document("get-node-criticality-for-ip"))
				.andReturn();
		assertThat(result.getResponse().getContentAsString()).isEqualTo("1.0");
	}

	@Test
	public void getNodeCriticalityForDns() throws Exception {
		Graph graphIn = createModel2SimpleGraph();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getVertices()).isNotEmpty();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getEdges()).isNotEmpty();
		String encodedDns = URLEncoder.encode("dns.11.com", StandardCharsets.UTF_8.name());
		MvcResult result = this.mockMvc
				.perform(get("/api/get-node-criticality-for-dns/" + encodedDns).accept(MediaType.APPLICATION_JSON)
						.contentType(contentType))
				.andExpect(status().isOk()).andDo(MockMvcRestDocumentation.document("get-node-criticality-for-dns"))
				.andReturn();
		assertThat(result.getResponse().getContentAsString()).isEqualTo("3.0");
	}

	@Test
	public void getNodeCriticalityForWrongIp() throws Exception {
		Graph graphIn = createModel2SimpleGraph();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getVertices()).isNotEmpty();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getEdges()).isNotEmpty();
		String encodedIpAddress = URLEncoder.encode("192.178.101.2", StandardCharsets.UTF_8.name());
		MvcResult result = this.mockMvc
				.perform(get("/api/get-node-criticality-for-ip/" + encodedIpAddress).accept(MediaType.APPLICATION_JSON)
						.contentType(contentType))
				.andExpect(status().isOk()).andDo(MockMvcRestDocumentation.document("get-node-criticality-for-ip"))
				.andReturn();
		assertThat(result.getResponse().getContentAsString()).isEmpty();
	}

	@Test
	public void getNodeCriticalityForWrongDns() throws Exception {
		Graph graphIn = createModel2SimpleGraph();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getVertices()).isNotEmpty();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getEdges()).isNotEmpty();
		String encodedDns = URLEncoder.encode("dns178.11.com", StandardCharsets.UTF_8.name());
		MvcResult result = this.mockMvc
				.perform(get("/api/get-node-criticality-for-dns/" + encodedDns).accept(MediaType.APPLICATION_JSON)
						.contentType(contentType))
				.andExpect(status().isOk()).andDo(MockMvcRestDocumentation.document("get-node-criticality-for-dns"))
				.andReturn();
		assertThat(result.getResponse().getContentAsString()).isEmpty();
	}

	@Test
	public void getNodeCriticalitiesForIpList() throws Exception {
		Graph graphIn = createModel2SimpleGraph();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getVertices()).isNotEmpty();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getEdges()).isNotEmpty();
		String encodedIpAddressLo = URLEncoder.encode("192.168.101.2", StandardCharsets.UTF_8.name());
		String encodedIpAddressHi = URLEncoder.encode("192.168.101.11", StandardCharsets.UTF_8.name()); // max
		List<String> list = Arrays.asList(encodedIpAddressHi, encodedIpAddressLo);
		String jsonToApi = new Gson().toJson(list);
		MvcResult result = this.mockMvc
				.perform(post("/api/get-node-criticalities-for-ip-list").accept(MediaType.APPLICATION_JSON)
						.contentType(contentType).content(jsonToApi))
				.andExpect(status().isOk())
				.andDo(MockMvcRestDocumentation.document("get-node-criticalities-for-ip-list")).andReturn();
		String jsonFromApi = result.getResponse().getContentAsString();
		assertThat(jsonFromApi).contains("\"192.168.101.2\":1.0");
		assertThat(jsonFromApi).contains("\"192.168.101.11\":3.0");
	}
	
	@Test
	public void getNodeCriticalitiesForSubnet() throws Exception {
		Graph graphIn = createModel2SimpleGraphNew();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getVertices()).isNotEmpty();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getEdges()).isNotEmpty();		
		String ip = URLEncoder.encode("192.168.101.10", StandardCharsets.UTF_8.name());
		String mask = URLEncoder.encode("31", StandardCharsets.UTF_8.name());
		Map<String, String> cidr = new HashMap<>(2);
		cidr.put("ip", ip);
		cidr.put("mask", mask);
		String jsonToApi = new Gson().toJson(cidr);
		log.info("jsonToApi: " + jsonToApi);
		MvcResult result = this.mockMvc
				.perform(post("/api/get-node-criticalities-for-subnet").accept(MediaType.APPLICATION_JSON)
						.contentType(contentType).content(jsonToApi))
				.andExpect(status().isOk())
				.andDo(MockMvcRestDocumentation.document("get-node-criticalities-for-subnet")).andReturn();
		String jsonFromApi = result.getResponse().getContentAsString();
		assertThat(jsonFromApi).contains("\"192.168.101.10\":1.0");
		assertThat(jsonFromApi).contains("\"192.168.101.11\":3.0");
	}
	@Test
	public void getNodeWithMaxCriticalityFromSubnet() throws Exception {
		Graph graphIn = createModel2SimpleGraphNew();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getVertices()).isNotEmpty();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getEdges()).isNotEmpty();		
		String ip = URLEncoder.encode("192.168.101.10", StandardCharsets.UTF_8.name());
		String mask = URLEncoder.encode("31", StandardCharsets.UTF_8.name());
		Map<String, String> cidr = new HashMap<>(2);
		cidr.put("ip", ip);
		cidr.put("mask", mask);
		String jsonToApi = new Gson().toJson(cidr);
		log.info("jsonToApi: " + jsonToApi);
		MvcResult result = this.mockMvc
				.perform(post("/api/get-node-with-max-criticality-from-subnet").accept(MediaType.APPLICATION_JSON)
						.contentType(contentType).content(jsonToApi))
				.andExpect(status().isOk())
				.andDo(MockMvcRestDocumentation.document("get-node-with-max-criticality-from-subnet")).andReturn();
		String jsonFromApi = result.getResponse().getContentAsString();
		log.info("max crit for subnet: " + jsonFromApi);
//		assertThat(jsonFromApi).contains("\"192.168.101.10\":1.0");
		assertThat(jsonFromApi).contains("\"192.168.101.11\":3.0");
	}
	@Test
	public void getMaxCvssScoresForNodesFromSubnet() throws Exception {
		Graph graphIn = createModel2SimpleGraphNew();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getVertices()).isNotEmpty();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getEdges()).isNotEmpty();		
		String ip = URLEncoder.encode("192.168.101.10", StandardCharsets.UTF_8.name());
		String mask = URLEncoder.encode("31", StandardCharsets.UTF_8.name());
		Map<String, String> cidr = new HashMap<>(2);
		cidr.put("ip", ip);
		cidr.put("mask", mask);
		String jsonToApi = new Gson().toJson(cidr);
		log.info("jsonToApi: " + jsonToApi);
		MvcResult result = this.mockMvc
				.perform(post("/api/get-max-cvss-scores-for-nodes-from-subnet").accept(MediaType.APPLICATION_JSON)
						.contentType(contentType).content(jsonToApi))
				.andExpect(status().isOk())
				.andDo(MockMvcRestDocumentation.document("get-max-cvss-scores-for-nodes-from-subnet")).andReturn();
		String jsonFromApi = result.getResponse().getContentAsString();
		assertThat(jsonFromApi).contains("\"192.168.101.10\":2.0");
		assertThat(jsonFromApi).contains("\"192.168.101.11\":6.0");
	}
	@Test
	public void getNodeWithMaxCvssScoreFromSubnet() throws Exception {
		Graph graphIn = createModel2SimpleGraphNew();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getVertices()).isNotEmpty();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getEdges()).isNotEmpty();		
		String ip = URLEncoder.encode("192.168.101.10", StandardCharsets.UTF_8.name());
		String mask = URLEncoder.encode("31", StandardCharsets.UTF_8.name());
		Map<String, String> cidr = new HashMap<>(2);
		cidr.put("ip", ip);
		cidr.put("mask", mask);
		String jsonToApi = new Gson().toJson(cidr);
		log.info("jsonToApi: " + jsonToApi);
		MvcResult result = this.mockMvc
				.perform(post("/api/get-node-with-max-cvss-score-from-subnet").accept(MediaType.APPLICATION_JSON)
						.contentType(contentType).content(jsonToApi))
				.andExpect(status().isOk())
				.andDo(MockMvcRestDocumentation.document("get-node-with-max-cvss-score-from-subnet")).andReturn();
		String jsonFromApi = result.getResponse().getContentAsString();
		log.info("max cvss for subnet: " + jsonFromApi);
		assertThat(jsonFromApi).contains("\"192.168.101.11\":6.0");
	}
	
	@Test
	public void getMaxCvssAndCriticalityForNodesFromSubnet() throws Exception {
		Graph graphIn = createModel2SimpleGraphNew();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getVertices()).isNotEmpty();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getEdges()).isNotEmpty();		
		String ip = URLEncoder.encode("192.168.101.10", StandardCharsets.UTF_8.name());
		String mask = URLEncoder.encode("31", StandardCharsets.UTF_8.name());
		Map<String, String> cidr = new HashMap<>(2);
		cidr.put("ip", ip);
		cidr.put("mask", mask);
		String jsonToApi = new Gson().toJson(cidr);
		log.info("jsonToApi: " + jsonToApi);
		MvcResult result = this.mockMvc
				.perform(post("/api/get-max-cvss-and-criticality-for-nodes-from-subnet").accept(MediaType.APPLICATION_JSON)
						.contentType(contentType).content(jsonToApi))
				.andExpect(status().isOk())
				.andDo(MockMvcRestDocumentation.document("get-max-cvss-and-criticality-for-nodes-from-subnet")).andReturn();
		String jsonFromApi = result.getResponse().getContentAsString();
		assertThat(jsonFromApi).contains("\"192.168.101.10\":{\"criticality\":1.0,\"maxCvss\":2.0}");
		assertThat(jsonFromApi).contains("\"192.168.101.11\":{\"criticality\":3.0,\"maxCvss\":6.0}");
	}

	@Test
	public void getNodeCriticalitiesForDnsList() throws Exception {
		Graph graphIn = createModel2SimpleGraph();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getVertices()).isNotEmpty();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getEdges()).isNotEmpty();
		String encodedDnsLo = URLEncoder.encode("dns.2.com", StandardCharsets.UTF_8.name());
		String encodedDnsHi = URLEncoder.encode("dns.11.com", StandardCharsets.UTF_8.name()); // max
		List<String> list = Arrays.asList(encodedDnsLo, encodedDnsHi);
		String jsonToApi = new Gson().toJson(list);
		MvcResult result = this.mockMvc
				.perform(post("/api/get-node-criticalities-for-dns-list").accept(MediaType.APPLICATION_JSON)
						.contentType(contentType).content(jsonToApi))
				.andExpect(status().isOk())
				.andDo(MockMvcRestDocumentation.document("get-node-criticalities-for-dns-list")).andReturn();
		String jsonFromApi = result.getResponse().getContentAsString();
		assertThat(jsonFromApi).contains("\"dns.2.com\":1.0");
		assertThat(jsonFromApi).contains("\"dns.11.com\":3.0");
	}

	@Test
	public void getMaxNodeCriticalityForIpList() throws Exception {
		Graph graphIn = createModel2SimpleGraph();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getVertices()).isNotEmpty();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getEdges()).isNotEmpty();
		String encodedIpAddressLo = URLEncoder.encode("192.168.101.2", StandardCharsets.UTF_8.name());
		String encodedIpAddressHi = URLEncoder.encode("192.168.101.11", StandardCharsets.UTF_8.name()); // max
		List<String> list = Arrays.asList(encodedIpAddressHi, encodedIpAddressLo);
		String jsonToApi = new Gson().toJson(list);
		MvcResult result = this.mockMvc
				.perform(post("/api/get-node-with-max-criticality-from-ip-list").accept(MediaType.APPLICATION_JSON)
						.contentType(contentType).content(jsonToApi))
				.andExpect(status().isOk())
				.andDo(MockMvcRestDocumentation.document("get-node-with-max-criticality-from-ip-list")).andReturn();
		String jsonFromApi = result.getResponse().getContentAsString();
		assertThat(jsonFromApi).isEqualTo("{\"192.168.101.11\":3.0}");
	}

	@Test
	public void getMaxNodeCriticalityForDnsList() throws Exception {
		Graph graphIn = createModel2SimpleGraph();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getVertices()).isNotEmpty();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getEdges()).isNotEmpty();
		String encodedDnsLo = URLEncoder.encode("dns.2.com", StandardCharsets.UTF_8.name());
		String encodedDnsHi = URLEncoder.encode("dns.11.com", StandardCharsets.UTF_8.name()); // max
		List<String> list = Arrays.asList(encodedDnsLo, encodedDnsHi);
		String jsonToApi = new Gson().toJson(list);
		MvcResult result = this.mockMvc
				.perform(post("/api/get-node-with-max-criticality-from-dns-list").accept(MediaType.APPLICATION_JSON)
						.contentType(contentType).content(jsonToApi))
				.andExpect(status().isOk())
				.andDo(MockMvcRestDocumentation.document("get-node-with-max-criticality-from-dns-list")).andReturn();
		String jsonFromApi = result.getResponse().getContentAsString();
		assertThat(jsonFromApi).isEqualTo("{\"dns.11.com\":3.0}");
	}
	
	@Test
	public void getMaxCvssForIp() throws Exception {
		Graph graphIn = createModel2SimpleGraph();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getVertices()).isNotEmpty();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getEdges()).isNotEmpty();
		String encodedIpAddress = URLEncoder.encode("192.168.101.2", StandardCharsets.UTF_8.name());
		MvcResult result = this.mockMvc
				.perform(get("/api/get-max-cvss-for-node-with-ip/" + encodedIpAddress).accept(MediaType.APPLICATION_JSON)
						.contentType(contentType))
				.andExpect(status().isOk()).andDo(MockMvcRestDocumentation.document("get-max-cvss-for-node-with-ip"))
				.andReturn();
		assertThat(result.getResponse().getContentAsString()).isEqualTo("10.0");
	}
	
	@Test
	public void getMaxCvssForDns() throws Exception {
		Graph graphIn = createModel2SimpleGraph();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getVertices()).isNotEmpty();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getEdges()).isNotEmpty();
		String encodedDns = URLEncoder.encode("dns.11.com", StandardCharsets.UTF_8.name());
		MvcResult result = this.mockMvc
				.perform(get("/api/get-max-cvss-for-node-with-dns/" + encodedDns).accept(MediaType.APPLICATION_JSON)
						.contentType(contentType))
				.andExpect(status().isOk()).andDo(MockMvcRestDocumentation.document("get-max-cvss-for-node-with-dns"))
				.andReturn();
		assertThat(result.getResponse().getContentAsString()).isEqualTo("6.0");
	}
	
	@Test
	public void getMaxCvssScoresForIpList() throws Exception {
		Graph graphIn = createModel2SimpleGraph();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getVertices()).isNotEmpty();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getEdges()).isNotEmpty();
		String encodedIpAddressLo = URLEncoder.encode("192.168.101.2", StandardCharsets.UTF_8.name());
		String encodedIpAddressHi = URLEncoder.encode("192.168.101.11", StandardCharsets.UTF_8.name()); // max
		List<String> list = Arrays.asList(encodedIpAddressHi, encodedIpAddressLo);
		String jsonToApi = new Gson().toJson(list);
		MvcResult result = this.mockMvc
				.perform(post("/api/get-max-cvss-scores-for-ip-list").accept(MediaType.APPLICATION_JSON)
						.contentType(contentType).content(jsonToApi))
				.andExpect(status().isOk())
				.andDo(MockMvcRestDocumentation.document("get-max-cvss-scores-for-ip-list")).andReturn();
		String jsonFromApi = result.getResponse().getContentAsString();
		assertThat(jsonFromApi).contains("\"192.168.101.2\":10.0");
		assertThat(jsonFromApi).contains("\"192.168.101.11\":6.0");
	}

	@Test
	public void getMaxCvssScoresForDnsList() throws Exception {
		Graph graphIn = createModel2SimpleGraph();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getVertices()).isNotEmpty();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getEdges()).isNotEmpty();
		String encodedDnsLo = URLEncoder.encode("dns.2.com", StandardCharsets.UTF_8.name());
		String encodedDnsHi = URLEncoder.encode("dns.11.com", StandardCharsets.UTF_8.name()); // max
		List<String> list = Arrays.asList(encodedDnsLo, encodedDnsHi);
		String jsonToApi = new Gson().toJson(list);
		MvcResult result = this.mockMvc
				.perform(post("/api/get-max-cvss-scores-for-dns-list").accept(MediaType.APPLICATION_JSON)
						.contentType(contentType).content(jsonToApi))
				.andExpect(status().isOk())
				.andDo(MockMvcRestDocumentation.document("get-max-cvss-scores-for-dns-list")).andReturn();
		String jsonFromApi = result.getResponse().getContentAsString();
		assertThat(jsonFromApi).contains("\"dns.2.com\":10.0");
		assertThat(jsonFromApi).contains("\"dns.11.com\":6.0");
	}
	
	@Test
	public void getMaxCvssAndCriticalityForDns() throws Exception {
		Graph graphIn = createModel2SimpleGraph();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getVertices()).isNotEmpty();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getEdges()).isNotEmpty();
		String encodedDns = URLEncoder.encode("dns.11.com", StandardCharsets.UTF_8.name());
		MvcResult result = this.mockMvc
				.perform(get("/api/get-max-cvss-and-criticality-for-dns/" + encodedDns).accept(MediaType.APPLICATION_JSON)
						.contentType(contentType))
				.andExpect(status().isOk()).andDo(MockMvcRestDocumentation.document("get-max-cvss-and-criticality-for-dns"))
				.andReturn();
		log.info("getMaxCvssAndCriticalityForDns JSON is: " + (result.getResponse().getContentAsString()));
//		assertThat(result.getResponse().getContentAsString()).isEqualTo("[3.0,6.0]");
		String jsonFromApi = result.getResponse().getContentAsString();
		log.info("Returned getMaxCvssAndCriticalityForDns JSON is: " + jsonFromApi);
		assertThat(jsonFromApi).contains("\"criticality\":3.0");
		assertThat(jsonFromApi).contains("\"maxCvss\":6.0");
	}
	
	@Test
	public void getMaxCvssAndCriticalityForDnsList() throws Exception {
		Graph graphIn = createModel2SimpleGraph();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getVertices()).isNotEmpty();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getEdges()).isNotEmpty();
		String encodedDnsLo = URLEncoder.encode("dns.2.com", StandardCharsets.UTF_8.name());
		String encodedDnsHi = URLEncoder.encode("dns.11.com", StandardCharsets.UTF_8.name()); // max
		List<String> list = Arrays.asList(encodedDnsLo, encodedDnsHi);
		String jsonToApi = new Gson().toJson(list);
		MvcResult result = this.mockMvc
				.perform(post("/api/get-max-cvss-and-criticality-for-dns-list").accept(MediaType.APPLICATION_JSON)
						.contentType(contentType).content(jsonToApi))
				.andExpect(status().isOk())
				.andDo(MockMvcRestDocumentation.document("get-max-cvss-and-criticality-for-dns-list")).andReturn();
		String jsonFromApi = result.getResponse().getContentAsString();
		log.info("Returned getMaxCvssAndCriticalityForDnsList JSON is: " + jsonFromApi);
		assertThat(jsonFromApi).contains("\"dns.2.com\":{\"criticality\":1.0,\"maxCvss\":10.0}");
		assertThat(jsonFromApi).contains("\"dns.11.com\":{\"criticality\":3.0,\"maxCvss\":6.0}");
	}
	
	@Test
	public void getMaxCvssAndCriticalityForIp() throws Exception {
		Graph graphIn = createModel2SimpleGraph();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getVertices()).isNotEmpty();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getEdges()).isNotEmpty();
		String encodedIpAddress = URLEncoder.encode("192.168.101.2", StandardCharsets.UTF_8.name());
		MvcResult result = this.mockMvc
				.perform(get("/api/get-max-cvss-and-criticality-for-ip/" + encodedIpAddress).accept(MediaType.APPLICATION_JSON)
						.contentType(contentType))
				.andExpect(status().isOk()).andDo(MockMvcRestDocumentation.document("get-max-cvss-and-criticality-for-ip"))
				.andReturn();
		String jsonFromApi = result.getResponse().getContentAsString();
		log.info("Returned getMaxCvssAndCriticalityForIp JSON is: " + jsonFromApi);
		assertThat(jsonFromApi).contains("\"criticality\":1.0");
		assertThat(jsonFromApi).contains("\"maxCvss\":10.0");
	}
	
	@Test
	public void getMaxCvssAndCriticalityForIpList() throws Exception {
		Graph graphIn = createModel2SimpleGraph();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getVertices()).isNotEmpty();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getEdges()).isNotEmpty();
		String encodedIpAddressLo = URLEncoder.encode("192.168.101.2", StandardCharsets.UTF_8.name());
		String encodedIpAddressHi = URLEncoder.encode("192.168.101.11", StandardCharsets.UTF_8.name()); // max
		List<String> list = Arrays.asList(encodedIpAddressHi, encodedIpAddressLo);
		String jsonToApi = new Gson().toJson(list);
		MvcResult result = this.mockMvc
				.perform(post("/api/get-max-cvss-and-criticality-for-ip-list").accept(MediaType.APPLICATION_JSON)
						.contentType(contentType).content(jsonToApi))
				.andExpect(status().isOk())
				.andDo(MockMvcRestDocumentation.document("get-max-cvss-and-criticality-for-ip-list")).andReturn();
		String jsonFromApi = result.getResponse().getContentAsString();
		assertThat(jsonFromApi).contains("\"192.168.101.2\":{\"criticality\":1.0,\"maxCvss\":10.0}");
		assertThat(jsonFromApi).contains("\"192.168.101.11\":{\"criticality\":3.0,\"maxCvss\":6.0}");
	}

	@Test
	public void clearGraph() throws Exception {
		Graph graphIn = createSimpleGraph();
		syncGraph.addOrUpdateGraph(graphIn);
		assertThat(syncGraph.getGraph(graphIn.getContext()).getVertices()).isNotEmpty();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getEdges()).isNotEmpty();
		this.mockMvc.perform(post("/api/graph-clear")).andExpect(status().isOk())
				.andDo(MockMvcRestDocumentation.document("graph-clear"));
		assertThat(syncGraph.getGraph(graphIn.getContext()).getVertices()).isEmpty();
		assertThat(syncGraph.getGraph(graphIn.getContext()).getEdges()).isEmpty();
	}

	@Test
	public void getMissionsImpactedByAsset() throws Exception {
		mongoDBInit.initImpactDependentGraph();
		this.mockMvc.perform(get("/api/get-missions-impacted-by-asset/A3")).andExpect(status().isOk())
				.andExpect(content().string(Matchers.not(Matchers.containsString("Mission1"))))
				.andExpect(content().string(Matchers.containsString("Mission2")))
				.andDo(MockMvcRestDocumentation.document("get-missions-impacted-by-asset"));
	}

	@Test
	public void getMostImportantMissionForVertexKey() throws Exception {
		mongoDBInit.initImpactDependentGraph();
		this.mockMvc.perform(get("/api/get-most-important-mission-for-vertex-key/OPC Server"))
				.andExpect(status().isOk())
				.andDo(MockMvcRestDocumentation.document("get-most-important-mission-for-vertex-key"));
		// FIXME Check returned content!
	}

	@Test
	public void getSupportingAssetsForMission() throws Exception {
		mongoDBInit.initImpactDependentGraph();
		this.mockMvc.perform(get("/api/get-supporting-assets-for-mission/Mission1")).andExpect(status().isOk())
				.andExpect(content().string(Matchers.containsString("A1")))
				.andExpect(content().string(Matchers.containsString("A2")))
				.andExpect(content().string(Matchers.not(Matchers.containsString("A3"))))
				.andExpect(content().string(Matchers.containsString("A4")))
				.andExpect(content().string(Matchers.not(Matchers.containsString("A5"))))
				.andDo(MockMvcRestDocumentation.document("get-supporting-assets-for-mission"));
	}

	@Test
	public void getMostCriticalAssets() throws Exception {
		matrixInit.initMatrices();
		this.mockMvc.perform(get("/api/get-most-critical-assets")).andExpect(status().isOk())
				.andDo(MockMvcRestDocumentation.document("get-most-critical-assets"));
		// FIXME Check returned content!
	}

	@Test
	public void getServicesDependingOnService() throws Exception {
		mongoDBInit.initImpactDependentGraph();
		this.mockMvc.perform(get("/api/get-services-depending-on-service/S5")).andExpect(status().isOk())
				.andExpect(content().string(Matchers.not(Matchers.containsString("S1"))))
				.andExpect(content().string(Matchers.not(Matchers.containsString("S2"))))
				.andExpect(content().string(Matchers.containsString("S3")))
				.andExpect(content().string(Matchers.not(Matchers.containsString("S4"))))
				.andExpect(content().string(Matchers.not(Matchers.containsString("S5"))))
				.andExpect(content().string(Matchers.not(Matchers.containsString("S6"))))
				.andExpect(content().string(Matchers.not(Matchers.containsString("S7"))))
				.andDo(MockMvcRestDocumentation.document("get-services-depending-on-service"));
	}

	@Test
	public void uploadMatrices() throws Exception {
		String filename = "RiskMap_Matrix.csv";
		Resource resource = new ClassPathResource(filename);
		InputStream inputStream = resource.getInputStream();

		Graph graphIn = syncGraph.getGraph(Metadata.DEFAULT_CONTEXT);
		assertThat(graphIn.getVertices()).isEmpty();
		assertThat(graphIn.getEdges()).isEmpty();

		MockMultipartFile csvFile = new MockMultipartFile("file", filename, "application/csv", inputStream);
		mockMvc.perform(MockMvcRequestBuilders.multipart("/api/weighting-matrix-upload").file(csvFile))
				.andExpect(status().isOk()).andDo(MockMvcRestDocumentation.document("weighting-matrix-upload"));

		Graph graphOut = syncGraph.getGraph(graphIn.getContext());
		assertThat(graphOut.getVertices()).isNotEmpty();
		assertThat(graphOut.getEdges()).isNotEmpty();
	}

	// @Test // does not currently work with restdocs
	public void getAssetAndServiceTypes() throws Exception {
		mockMvc.perform(get("/api/get-asset-and-service-types")).andExpect(status().isOk())
				.andExpect(content().string(Matchers.containsString(Asset.class.getName())))
				.andExpect(content().string(Matchers.containsString(Computer.class.getName())))
				.andExpect(content().string(Matchers.containsString(Service.class.getName())))
				.andExpect(content().string(Matchers.containsString(ITService.class.getName())))
				.andDo(MockMvcRestDocumentation.document("get-asset-and-service-types"));
	}

	// @Test // TODO does not currently work with restdocs
	public void getRelationshipTypes() throws Exception {
		mockMvc.perform(get("/api/get-relationship-types")).andExpect(status().isOk())
				.andExpect(content().string(Matchers.containsString(RealisedBy.class.getName())))
				.andExpect(content().string(Matchers.containsString(ConnectedTo.class.getName())))
				.andDo(MockMvcRestDocumentation.document("get-relationship-types"));
	}

	@Test
	public void getPsncCdgAsGraph() throws Exception {
		org.reflections.Reflections.log = null;
		File file = new File("src/test/resources/antlr/psnc_vc.deps");
		DepsTests.readCdg(mongoDB, file, false, true, missionWeights, vertexWeights, edgeWeights);
		Graph graphOut = syncGraph.getGraph(Metadata.DEFAULT_CONTEXT);
		toJsonFile(graphOut, "psnc_vc.json");
		String graphJsonOut = graphOut.toJson();
		MvcResult result = this.mockMvc.perform(get("/api/graph-get")).andExpect(status().isOk())
				.andDo(MockMvcRestDocumentation.document("graph-get")).andReturn();
		String graphJsonFromApi = result.getResponse().getContentAsString();
		JSONAssert.assertEquals(graphJsonOut, graphJsonFromApi, false);
	}

	@Test
	public void getGraphByCdg() throws Exception {
		org.reflections.Reflections.log = null;
		File file = new File("src/test/resources/antlr/cdg_1.deps");
		DepsTests.readCdg(mongoDB, file, false, true, missionWeights, vertexWeights, edgeWeights);
		Graph graphOut = syncGraph.getGraph(Metadata.DEFAULT_CONTEXT);
		toJsonFile(graphOut, "cdg_1.deps.json");
		this.mockMvc.perform(get("/api/get-graph-by-cdg/cdg1")).andExpect(status().isOk())
				.andDo(MockMvcRestDocumentation.document("graph-get-by-cdg")).andReturn();
		// FIXME Check returned content!
	}

	@Test
	public void getGraphByCdgCheckFields() throws Exception {
		org.reflections.Reflections.log = null;
		File file = new File("src/test/resources/antlr/cdg_1.deps");
		DepsTests.readCdg(mongoDB, file, false, true, missionWeights, vertexWeights, edgeWeights);
		Graph graphOut = syncGraph.getGraph(Metadata.DEFAULT_CONTEXT);
		toJsonFile(graphOut, "cdg_1.deps.json");
		this.mockMvc.perform(get("/api/get-graph-by-cdg/cdg1")).andExpect(status().isOk())
				.andExpect(jsonPath("$.links[0].sourceType").exists())
				.andExpect(jsonPath("$.links[0].targetType").exists()).andReturn();
	}

	@Test
	public void uploadCdgCdgsFile() throws Exception {
		uploadCdgFile("antlr/cdg_1.cdgs", "upload-cdg", CdgTests.NUMBER_OF_VERTICES, CdgTests.NUMBER_OF_EDGES);
	}

	@Test
	public void uploadCdgCdgsSyntaxErrorFile() throws Exception {
		String error1 = "line 74:11 no viable alternative at input '{\\\"id\\\":g,'";
		String error2 = "line 75:5 mismatched input '\\\"expression\\\"' expecting '{'";
		String error3 = "line 76:5 mismatched input '\\\"weight\\\"' expecting '{'";
		String error4 = "line 77:5 missing '}' at ','";
		String error5 = "line 79:5 mismatched input '\\\"id\\\"' expecting '\\\"cdg\\\"'";
		uploadCdgFile("antlr/cdg_2.cdgs", "upload-cdg-syntax-error", error1, error2, error3, error4, error5);
	}

	@Test
	public void uploadCdgCdgsSemanticErrorFile() throws Exception {
//		String error1 = "line 68:14 \\\"high123\\\" is not one of " // TODO change test to check grade.
//				+ "[\\\"none\\\", \\\"low\\\", \\\"medium\\\", \\\"high\\\", \\\"critical\\\"].";
		String error2 = "line 24:21 Vertex with key \\\"m123\\\" is not in the database AND "
				+ "not defined correctly in the \\\"antlr/cdg_3.cdgs\\\" file.";
		String error3 = "line 25:19 Vertex with key \\\"c1\\\" is not in the database AND "
				+ "not defined correctly in the \\\"antlr/cdg_3.cdgs\\\" file.";
		String error4 = "line 31:19 Vertex with key \\\"g123\\\" is not in the database AND "
				+ "not defined correctly in the \\\"antlr/cdg_3.cdgs\\\" file.";
		uploadCdgFile("antlr/cdg_3.cdgs", "upload-cdg-semantic-error", error2, error3, error4);
	}

	@Test
	public void uploadCdgDepsFile() throws Exception {
		uploadCdgFile("antlr/cdg_1.deps", "upload-cdg-original", DepsTests.NUMBER_OF_VERTICES,
				DepsTests.NUMBER_OF_EDGES);
	}

	@Test
	public void uploadCdgJsonFile() throws Exception {
		uploadCdgFile("cluster-insight/cluster.json", "upload-cdg-cluster", ClusterTests.NUMBER_OF_VERTICES,
				ClusterTests.NUMBER_OF_EDGES);
	}

	@Test
	public void uploadCdgvPsncFile() throws Exception {
//		String oldUrl = config.getCaAsUrl();
//		config.setCaAsUrl("http://localhost:1234");
		uploadCdgFile("antlr/psnc_vc.cdgs", "upload-cdgs-psnc_vc", null, null);
		uploadCdgFile("antlr/psnc_vc.cdgv", "upload-cdgv-psnc_vc", null, null);
//		config.setCaAsUrl(oldUrl);
	}

	@Test
	public void uploadCdgvPsncWithIpFile() throws Exception {
//		String oldUrl = config.getCaAsUrl();
//		config.setCaAsUrl("http://localhost:1234");
		uploadCdgFile("antlr/psnc_vc.cdgs", "upload-cdgs-psnc_ip_vc", null, null);
		uploadCdgFile("antlr/psnc_vc.with.ip.cdgv", "upload-cdgv-psnc_ip_vc", null, null);
//		config.setCaAsUrl(oldUrl);
	}

	@Test
	public void uploadCdgvPsncWithIpAndContextFile() throws Exception {
//		String oldUrl = config.getCaAsUrl();
//		config.setCaAsUrl("http://localhost:1234");
		uploadCdgFile("antlr/psnc_vc.cdgs", "upload-cdgs-psnc_ip_vc", null, null);
		uploadCdgFile("PSNC", "antlr/psnc_vc.with.ip.context.cdgv", "upload-cdgv-psnc_ip_context_vc", null, null);
//		config.setCaAsUrl(oldUrl);
	}

	@Test
	public void startAssetRank() throws Exception {
		createModel2SimpleGraph();
		this.mockMvc.perform(get("/api/start-asset-rank")).andExpect(status().isOk())
				.andDo(MockMvcRestDocumentation.document("start-asset-rank")).andReturn();
		Graph graphOut = syncGraph.getGraph(Metadata.DEFAULT_CONTEXT);
		assertModel2SimpleGraphAssetRankScoresAreOk(graphOut);
	}

	@Test
	public void startAssetRankDetailedNoMockMvc() throws Exception {
		/* Graph graphTmp = */ createModel2SimpleGraph();
		Graph graphIn = syncGraph.getGraph(Metadata.DEFAULT_CONTEXT); // TODO Debug ordering in AssetRank vertices
		AssetRank assetRank = new AssetRank(graphIn);
		syncGraph.addOrUpdateGraph(assetRank.getGraph());
		Graph graphOut = syncGraph.getGraph(Metadata.DEFAULT_CONTEXT);
		assertModel2SimpleGraphAssetRankScoresAreOk(graphOut);
		List<Vertex> vertices = graphOut.getVertices();
		for (Vertex v : vertices) {
			assertThat(assetRank.getVertexScore(v.getGVertex())).isEqualTo(v.getScore());
		}
	}

	private void assertModel2SimpleGraphAssetRankScoresAreOk(Graph graphOut) throws Exception {
		List<Vertex> vertices = graphOut.getVertices();
		for (Vertex v : vertices) {
			log.info("Vertex key = '{}' score '{}' weight = '{}' exp = '{}'", v.getKey(), v.getScore(), v.getWeight(),
					v.getExpression());
		}
		Offset<Double> offset = Offset.offset(AssetRank.TOLERANCE_DEFAULT);
		for (Vertex v : vertices) {
			switch (v.getKey()) {
			case "Computer 1":
				assertThat(v.getScore()).isEqualTo(0.3436975823118405, offset);
				break;
			case "Computer 1 - Windows":
				assertThat(v.getScore()).isEqualTo(0.14403924622872552, offset);
				break;
			case "NetworkNode 1":
				assertThat(v.getScore()).isEqualTo(0.15630241768815958, offset);
				break;
			case "Word":
				assertThat(v.getScore()).isEqualTo(0.15630241768815958, offset);
				break;
			case "IT Service 1":
				assertThat(v.getScore()).isEqualTo(0.09982916804155748, offset);
				break;
			case "NetworkNode 1 - Ubuntu":
				assertThat(v.getScore()).isEqualTo(0.09982916804155748, offset);
				break;
			default:
				throw new Exception("Unexpected vertex key = " + v.getKey());
			}
		}
	}

	private void uploadCdgFile(String filename, String doc, Integer verticesSize, Integer edgesSize) throws Exception {
		uploadCdgFile(Metadata.DEFAULT_CONTEXT, filename, doc, verticesSize, edgesSize);
	}
	
	private void uploadCdgFile(String context, String filename, String doc, Integer verticesSize, Integer edgesSize) throws Exception {
		Resource resource = new ClassPathResource(filename);
		InputStream inputStream = resource.getInputStream();

		MockMultipartFile cdgFile = new MockMultipartFile("file", filename, MediaType.TEXT_PLAIN_VALUE, inputStream);
		mockMvc.perform(MockMvcRequestBuilders.multipart("/api/upload-cdg-file").file(cdgFile))
				.andExpect(status().isOk()).andDo(MockMvcRestDocumentation.document(doc));

		Graph graphOut = syncGraph.getGraph(context);

		if (verticesSize != null) {
			assertThat(graphOut.vertexSet()).hasSize(verticesSize);
			assertThat(graphOut.getVertices()).hasSize(verticesSize);
		}
		if (edgesSize != null) {
			assertThat(graphOut.edgeSet()).hasSize(edgesSize);
			assertThat(graphOut.getEdges()).hasSize(edgesSize);
		}
	}

	private void uploadCdgFile(String filename, String doc, String... errors) throws Exception {
		Resource resource = new ClassPathResource(filename);
		InputStream inputStream = resource.getInputStream();

		MockMultipartFile cdgFile = new MockMultipartFile("file", filename, MediaType.TEXT_PLAIN_VALUE, inputStream);
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart("/api/upload-cdg-file").file(cdgFile))
				.andExpect(status().isBadRequest()).andDo(MockMvcRestDocumentation.document(doc)).andReturn();
		String content = result.getResponse().getContentAsString();
		assertThat(content).contains(errors);
	}

	private void assertEqualToIgnoringGivenFields(Graph graphA, Graph graphB, String... propertiesOrFieldsToIgnore) {
		assertThat(graphA.getVertices()).hasSameSizeAs(graphB.getVertices());
		assertThat(graphA.getEdges()).hasSameSizeAs(graphB.getEdges());
		int size = graphA.getVertices().size() + graphA.getEdges().size();
		log.info("Number of elements in Graph is {}", size);
		int count = 0;
		for (GraphElement elementA : graphA.getVertices()) {
			for (GraphElement elementB : graphB.getVertices()) {
				if (elementA.getKey().equals(elementB.getKey())) {
					assertThat(elementA).isEqualToIgnoringGivenFields(elementB, propertiesOrFieldsToIgnore);
					count++;
				}
			}
		}
		for (GraphElement elementA : graphA.getEdges()) {
			for (GraphElement elementB : graphB.getEdges()) {
				if (elementA.getKey().equals(elementB.getKey())) {
					assertThat(elementA).isEqualToIgnoringGivenFields(elementB, propertiesOrFieldsToIgnore);
					count++;
				}
			}
		}
		assertThat(size).isEqualTo(count);
	}

	private Graph createSimpleGraph() {
		Graph graph = new Graph();
		graph.setContext(Metadata.DEFAULT_CONTEXT);
		{
			Computer computer = new Computer("Computer 1");
			computer.setName("Brians PC");
			computer.getIpAddresses().add("192.168.101.11");
			computer.setBusinessOwner("Brian");
			computer.setTechnicalOwner("Eoin");
			computer.setLocation("Athlone");
			computer.setDescription("Dell Inspiron 1700 Laptop");
			computer.setDeviceId("Dell Inspiron 1/1/2010");
			computer.setMacAddress("fa:16:3e:b4:4b:fb");
			computer.setDefaultGateway("192.168.101.1");
			computer.setIpGateway("192.168.101.1");
			computer.setDns("dns.11.com");
			computer.setIpV4Subnet("192.168.101.0");
			computer.setIpV4SubnetMask("255.255.255.0");
			computer.getIpV4Addresses().add("192.168.101.11");
			computer.setNetworkType("Wifi");
			computer.setModel("Dell Inspiron");
			computer.setDeviceType("Physical");
			computer.setArchitecture("AMD64");

			OperatingSystem os = new OperatingSystem("Computer 1 - Windows");
			os.setName("Windows 10");
			os.setVersion("10");
			os.setPatchLevel("1");
			os.setBusinessOwner("Brian");
			os.setTechnicalOwner("Eoin");
			os.setLocation("Athlone");
			os.setDescription("Windows OS");
			os.setTargetArchitecture("AMD64");
			os.setFullName("Windows 10 Enterprise");

			Application app = new Application("Word");
			app.setName("Brians Word");
			app.setVersion("2016");
			app.setPatchLevel("1");
			app.setBusinessOwner("Brian");
			app.setTechnicalOwner("Eoin");
			app.setLocation("Athlone");
			app.setDescription("Microsoft Word 2016");
			app.setTargetArchitecture("AMD64");

			HostedOn runsOnPC = new HostedOn(os, computer);
			HostedOn runsOnOS = new HostedOn(app, os);

			graph.addVertex(app);
			graph.addVertex(os);
			graph.addVertex(computer);
			graph.addEdge(os, computer, runsOnPC);
			graph.addEdge(app, os, runsOnOS);
		}
		{
			NetworkNode networkNode = new NetworkNode("NetworkNode 1");
			networkNode.setName("Brians Router");
			networkNode.getIpAddresses().add("192.168.101.2");
			networkNode.setBusinessOwner("Brian");
			networkNode.setTechnicalOwner("Eoin");
			networkNode.setLocation("Athlone");
			networkNode.setDescription("Belkin router");
			networkNode.setDeviceId("Belkin router 1/1/2010");
			networkNode.setMacAddress("fa:16:3e:b4:4b:5a");
			networkNode.setDefaultGateway("192.168.101.1");
			networkNode.setIpGateway("192.168.101.1");
			networkNode.setDns("dns.2.com");
			networkNode.setIpV4Subnet("192.168.101.0");
			networkNode.setIpV4SubnetMask("255.255.255.0");
			networkNode.getIpV4Addresses().add("192.168.101.2");
			networkNode.setNetworkType("Wifi");
			networkNode.setModel("Belkin Router");
			networkNode.setDeviceType("Physical");
			networkNode.setArchitecture("ARM32");

			OperatingSystem os = new OperatingSystem("NetworkNode 1 - Ubuntu");
			os.setName("Ubuntu");
			os.setVersion("16.10");
			os.setPatchLevel("1");
			os.setBusinessOwner("Brian");
			os.setTechnicalOwner("Eoin");
			os.setLocation("Athlone");
			os.setDescription("Ubuntu Linux OS");
			os.setTargetArchitecture("AMD64");
			os.setFullName("Ubuntu Linux 16.10");

			HostedOn runsOn = new HostedOn(os, networkNode);

			graph.addVertex(os);
			graph.addVertex(networkNode);
			graph.addEdge(os, networkNode, runsOn);
		}
		return graph;
	}

	private Graph createModel2SimpleGraph() {
		Graph graph = new Graph();
		graph.setContext(Metadata.DEFAULT_CONTEXT);
		{
			eu.h2020.protective.ca.mair.api.model2.nodes.Computer computer = new eu.h2020.protective.ca.mair.api.model2.nodes.Computer(
					"Computer 1");
			computer.setName("Brians PC");
			computer.getIpAddresses().add("192.168.101.11");
			computer.setBusinessOwner("Brian");
			computer.setTechnicalOwner("Eoin");
			computer.setLocation("Athlone");
			computer.setDescription("Dell Inspiron 1700 Laptop");
			computer.setMacAddress("fa:16:3e:b4:4b:fb");
			computer.setDefaultGateway("192.168.101.1");
			computer.setIpGateway("192.168.101.1");
			computer.setDns("dns.11.com");
			computer.setIpV4Subnet("192.168.101.0");
			computer.setIpV4SubnetMask("255.255.255.0");
			computer.getIpV4Addresses().add("192.168.101.11");
			computer.setNetworkType("Wifi");
			computer.setModel("Dell Inspiron");
			computer.setDeviceType("Physical");
			computer.setArchitecture("AMD64");
			computer.setWeight(3.0d);
			computer.setScore(0.2506);
			computer.setMaxCvss(6.0d);

			eu.h2020.protective.ca.mair.api.model2.assets.Software os = new eu.h2020.protective.ca.mair.api.model2.assets.Software(
					"Computer 1 - Windows");
			os.setName("Windows 10");
			os.setVersion("10");
			os.setPatchLevel("1");
			os.setBusinessOwner("Brian");
			os.setTechnicalOwner("Eoin");
			os.setLocation("Athlone");
			os.setDescription("Windows OS");
			os.setTargetArchitecture("AMD64");

			eu.h2020.protective.ca.mair.api.model2.assets.Software app = new eu.h2020.protective.ca.mair.api.model2.assets.Software(
					"Word");
			app.setName("Brians Word");
			app.setVersion("2016");
			app.setPatchLevel("1");
			app.setBusinessOwner("Brian");
			app.setTechnicalOwner("Eoin");
			app.setLocation("Athlone");
			app.setDescription("Microsoft Word 2016");
			app.setTargetArchitecture("AMD64");

			eu.h2020.protective.ca.mair.api.model2.services.ITService its = new eu.h2020.protective.ca.mair.api.model2.services.ITService(
					"IT Service 1");
			its.setName("Brians IT Service");

			Uses runsOnOS = new Uses(app, os);
			runsOnOS.setFromCdg("infrastructureLayer");
			runsOnOS.setToCdg("infrastructureLayer");

			RealisedAs realisedAs = new RealisedAs(its, app);
			realisedAs.setFromCdg("applicationLayer");
			realisedAs.setToCdg("infrastructureLayer");

			RunsOn runsOn = new RunsOn(app, computer);
			runsOn.setFromCdg("infrastructureLayer");
			runsOn.setToCdg("infrastructureLayer");
			runsOn.setGrade("high");

			graph.addVertex(its);
			graph.addVertex(app);
			graph.addVertex(os);
			graph.addVertex(computer);
			graph.addEdge(its, app, realisedAs);
			graph.addEdge(app, os, runsOnOS);
			graph.addEdge(os, computer, runsOn);
		}
		{
			eu.h2020.protective.ca.mair.api.model2.nodes.NetworkNode networkNode = new eu.h2020.protective.ca.mair.api.model2.nodes.NetworkNode(
					"NetworkNode 1");
			networkNode.setName("Brians Router");
			networkNode.getIpAddresses().add("192.168.101.2");
			networkNode.setBusinessOwner("Brian");
			networkNode.setTechnicalOwner("Eoin");
			networkNode.setLocation("Athlone");
			networkNode.setDescription("Belkin router");
			networkNode.setMacAddress("fa:16:3e:b4:4b:5a");
			networkNode.setDefaultGateway("192.168.101.1");
			networkNode.setIpGateway("192.168.101.1");
			networkNode.setDns("dns.2.com");
			networkNode.setIpV4Subnet("192.168.101.0");
			networkNode.setIpV4SubnetMask("255.255.255.0");
			networkNode.getIpV4Addresses().add("192.168.101.2");
			networkNode.setNetworkType("Wifi");
			networkNode.setModel("Belkin Router");
			networkNode.setDeviceType("Physical");
			networkNode.setArchitecture("ARM32");
			networkNode.setWeight(1.0d);
			networkNode.setScore(0.1245);
			networkNode.setMaxCvss(10.0d);

			eu.h2020.protective.ca.mair.api.model2.assets.Software os = new eu.h2020.protective.ca.mair.api.model2.assets.Software(
					"NetworkNode 1 - Ubuntu");
			os.setName("Ubuntu");
			os.setVersion("16.10");
			os.setPatchLevel("1");
			os.setBusinessOwner("Brian");
			os.setTechnicalOwner("Eoin");
			os.setLocation("Athlone");
			os.setDescription("Ubuntu Linux OS");
			os.setTargetArchitecture("AMD64");

			DependsOn runsOn = new DependsOn(os, networkNode);
			runsOn.setFromCdg("infrastructureLayer");
			runsOn.setToCdg("infrastructureLayer");
			runsOn.setGrade("low");

			graph.addVertex(os);
			graph.addVertex(networkNode);
			graph.addEdge(os, networkNode, runsOn);
		}
		mongoDB.insertOrUpdateManyVertices(graph.getContext(), graph.getVertices());
		mongoDB.insertOrUpdateManyEdges(graph.getContext(), graph.getEdges());
		return graph;
	}
	private Graph createModel2SimpleGraphNew() {
		Graph graph = new Graph();
		graph.setContext(Metadata.DEFAULT_CONTEXT);
		{
			eu.h2020.protective.ca.mair.api.model2.nodes.Computer computer = new eu.h2020.protective.ca.mair.api.model2.nodes.Computer(
					"Computer 1");
			computer.setName("Brians PC");
			computer.getIpAddresses().add("192.168.101.11");
			computer.setBusinessOwner("Brian");
			computer.setTechnicalOwner("Eoin");
			computer.setLocation("Athlone");
			computer.setDescription("Dell Inspiron 1700 Laptop");
			computer.setMacAddress("fa:16:3e:b4:4b:fb");
			computer.setDefaultGateway("192.168.101.1");
			computer.setIpGateway("192.168.101.1");
			computer.setDns("dns.11.com");
			computer.setIpV4Subnet("192.168.101.0");
			computer.setIpV4SubnetMask("255.255.255.0");
			computer.getIpV4Addresses().add("192.168.101.11");
			computer.setNetworkType("Wifi");
			computer.setModel("Dell Inspiron");
			computer.setDeviceType("Physical");
			computer.setArchitecture("AMD64");
			computer.setWeight(3.0d);
			computer.setScore(0.2506);
			computer.setMaxCvss(6.0d);

			eu.h2020.protective.ca.mair.api.model2.assets.Software os = new eu.h2020.protective.ca.mair.api.model2.assets.Software(
					"Computer 1 - Windows");
			os.setName("Windows 10");
			os.setVersion("10");
			os.setPatchLevel("1");
			os.setBusinessOwner("Brian");
			os.setTechnicalOwner("Eoin");
			os.setLocation("Athlone");
			os.setDescription("Windows OS");
			os.setTargetArchitecture("AMD64");

			eu.h2020.protective.ca.mair.api.model2.assets.Software app = new eu.h2020.protective.ca.mair.api.model2.assets.Software(
					"Word");
			app.setName("Brians Word");
			app.setVersion("2016");
			app.setPatchLevel("1");
			app.setBusinessOwner("Brian");
			app.setTechnicalOwner("Eoin");
			app.setLocation("Athlone");
			app.setDescription("Microsoft Word 2016");
			app.setTargetArchitecture("AMD64");

			eu.h2020.protective.ca.mair.api.model2.services.ITService its = new eu.h2020.protective.ca.mair.api.model2.services.ITService(
					"IT Service 1");
			its.setName("Brians IT Service");

			Uses runsOnOS = new Uses(app, os);
			runsOnOS.setFromCdg("infrastructureLayer");
			runsOnOS.setToCdg("infrastructureLayer");

			RealisedAs realisedAs = new RealisedAs(its, app);
			realisedAs.setFromCdg("applicationLayer");
			realisedAs.setToCdg("infrastructureLayer");

			RunsOn runsOn = new RunsOn(app, computer);
			runsOn.setFromCdg("infrastructureLayer");
			runsOn.setToCdg("infrastructureLayer");
			runsOn.setGrade("high");

			graph.addVertex(its);
			graph.addVertex(app);
			graph.addVertex(os);
			graph.addVertex(computer);
			graph.addEdge(its, app, realisedAs);
			graph.addEdge(app, os, runsOnOS);
			graph.addEdge(os, computer, runsOn);
		}
		{
			eu.h2020.protective.ca.mair.api.model2.nodes.Computer computer = new eu.h2020.protective.ca.mair.api.model2.nodes.Computer(
					"Computer 2");
			computer.setName("Sauls PC");
			computer.getIpAddresses().add("192.168.101.10");
			computer.setBusinessOwner("Brian");
			computer.setTechnicalOwner("Saul");
			computer.setLocation("Athlone");
			computer.setDescription("Dell XPS 8500 Desktop");
			computer.setMacAddress("fa:16:3e:b4:4b:ft");
			computer.setDefaultGateway("192.168.101.1");
			computer.setIpGateway("192.168.101.1");
			computer.setDns("dns.10.com");
			computer.setIpV4Subnet("192.168.101.0");
			computer.setIpV4SubnetMask("255.255.255.0");
			computer.getIpV4Addresses().add("192.168.101.10");
			computer.setNetworkType("Wired");
			computer.setModel("Dell XPS");
			computer.setDeviceType("Physical");
			computer.setArchitecture("AMD64");
			computer.setWeight(2.0d);
			computer.setScore(0.1034);
			computer.setMaxCvss(2.0d);

			eu.h2020.protective.ca.mair.api.model2.assets.Software os = new eu.h2020.protective.ca.mair.api.model2.assets.Software(
					"Computer 2 - Windows");
			os.setName("Windows 10");
			os.setVersion("10");
			os.setPatchLevel("1");
			os.setBusinessOwner("Brian");
			os.setTechnicalOwner("Saul");
			os.setLocation("Athlone");
			os.setDescription("Windows OS");
			os.setTargetArchitecture("AMD64");

			eu.h2020.protective.ca.mair.api.model2.assets.Software app = new eu.h2020.protective.ca.mair.api.model2.assets.Software(
					"Word");
			app.setName("Sauls Word");
			app.setVersion("2016");
			app.setPatchLevel("1");
			app.setBusinessOwner("Brian");
			app.setTechnicalOwner("Saul");
			app.setLocation("Athlone");
			app.setDescription("Microsoft Word 2016");
			app.setTargetArchitecture("AMD64");

			eu.h2020.protective.ca.mair.api.model2.services.ITService its = new eu.h2020.protective.ca.mair.api.model2.services.ITService(
					"IT Service 1");
			its.setName("Sauls IT Service");

			Uses runsOnOS = new Uses(app, os);
			runsOnOS.setFromCdg("infrastructureLayer");
			runsOnOS.setToCdg("infrastructureLayer");

			RealisedAs realisedAs = new RealisedAs(its, app);
			realisedAs.setFromCdg("applicationLayer");
			realisedAs.setToCdg("infrastructureLayer");

			RunsOn runsOn = new RunsOn(app, computer);
			runsOn.setFromCdg("infrastructureLayer");
			runsOn.setToCdg("infrastructureLayer");
			runsOn.setGrade("high");

			graph.addVertex(its);
			graph.addVertex(app);
			graph.addVertex(os);
			graph.addVertex(computer);
			graph.addEdge(its, app, realisedAs);
			graph.addEdge(app, os, runsOnOS);
			graph.addEdge(os, computer, runsOn);
		}
		{
			eu.h2020.protective.ca.mair.api.model2.nodes.NetworkNode networkNode = new eu.h2020.protective.ca.mair.api.model2.nodes.NetworkNode(
					"NetworkNode 1");
			networkNode.setName("Brians Router");
			networkNode.getIpAddresses().add("192.168.101.2");
			networkNode.setBusinessOwner("Brian");
			networkNode.setTechnicalOwner("Eoin");
			networkNode.setLocation("Athlone");
			networkNode.setDescription("Belkin router");
			networkNode.setMacAddress("fa:16:3e:b4:4b:5a");
			networkNode.setDefaultGateway("192.168.101.1");
			networkNode.setIpGateway("192.168.101.1");
			networkNode.setDns("dns.2.com");
			networkNode.setIpV4Subnet("192.168.101.0");
			networkNode.setIpV4SubnetMask("255.255.255.0");
			networkNode.getIpV4Addresses().add("192.168.101.2");
			networkNode.setNetworkType("Wifi");
			networkNode.setModel("Belkin Router");
			networkNode.setDeviceType("Physical");
			networkNode.setArchitecture("ARM32");
			networkNode.setWeight(1.0d);
			networkNode.setScore(0.1245);
			networkNode.setMaxCvss(10.0d);

			eu.h2020.protective.ca.mair.api.model2.assets.Software os = new eu.h2020.protective.ca.mair.api.model2.assets.Software(
					"NetworkNode 1 - Ubuntu");
			os.setName("Ubuntu");
			os.setVersion("16.10");
			os.setPatchLevel("1");
			os.setBusinessOwner("Brian");
			os.setTechnicalOwner("Eoin");
			os.setLocation("Athlone");
			os.setDescription("Ubuntu Linux OS");
			os.setTargetArchitecture("AMD64");

			DependsOn runsOn = new DependsOn(os, networkNode);
			runsOn.setFromCdg("infrastructureLayer");
			runsOn.setToCdg("infrastructureLayer");
			runsOn.setGrade("low");

			graph.addVertex(os);
			graph.addVertex(networkNode);
			graph.addEdge(os, networkNode, runsOn);
		}
		mongoDB.insertOrUpdateManyVertices(graph.getContext(), graph.getVertices());
		mongoDB.insertOrUpdateManyEdges(graph.getContext(), graph.getEdges());
		return graph;
	}

	private Graph createModel2CompleteGraph() throws Exception {
		Graph graph = new Graph();
		graph.setContext(Metadata.DEFAULT_CONTEXT);
		{
			eu.h2020.protective.ca.mair.api.model2.assets.Asset asset = new eu.h2020.protective.ca.mair.api.model2.assets.Asset(
					"Asset 1");
			asset.setName("Brians Asset");
			asset.setBusinessOwner("Brian");
			asset.setTechnicalOwner("Eoin");
			asset.setLocation("Athlone");
			asset.setDescription("Dell Inspiron 1700 Laptop");
			asset.setWeight(3.0d);

			eu.h2020.protective.ca.mair.api.model2.assets.Information information = new eu.h2020.protective.ca.mair.api.model2.assets.Information(
					"Computer 1");
			information.setName("Brians PC");
			information.setBusinessOwner("Brian");
			information.setTechnicalOwner("Eoin");
			information.setLocation("Athlone");
			information.setDescription("Dell Inspiron 1700 Laptop");
			information.setWeight(3.0d);

			eu.h2020.protective.ca.mair.api.model2.assets.OperatingSystem os = new eu.h2020.protective.ca.mair.api.model2.assets.OperatingSystem(
					"NetworkNode 1 - Ubuntu");
			os.setName("Ubuntu");
			os.setVersion("16.10");
			os.setPatchLevel("1");
			os.setBusinessOwner("Brian");
			os.setTechnicalOwner("Eoin");
			os.setLocation("Athlone");
			os.setDescription("Ubuntu Linux OS");
			os.setTargetArchitecture("AMD64");

			eu.h2020.protective.ca.mair.api.model2.assets.Software sw = new eu.h2020.protective.ca.mair.api.model2.assets.Software(
					"Word");
			sw.setName("Brians Word");
			sw.setVersion("2016");
			sw.setPatchLevel("1");
			sw.setBusinessOwner("Brian");
			sw.setTechnicalOwner("Eoin");
			sw.setLocation("Athlone");
			sw.setDescription("Microsoft Word 2016");
			sw.setTargetArchitecture("AMD64");

			eu.h2020.protective.ca.mair.api.model2.groups.Cluster cluster = new eu.h2020.protective.ca.mair.api.model2.groups.Cluster(
					"Cluster 1");
			cluster.setName("Brians Cluster");
			cluster.setBusinessOwner("Brian");
			cluster.setTechnicalOwner("Eoin");
			cluster.setLocation("Athlone");
			cluster.setDescription("Demo Cluster");

			// Generic type not supported by PojoCodec
//			eu.h2020.protective.ca.mair.api.model2.groups.Group<Node> group = new eu.h2020.protective.ca.mair.api.model2.groups.Group<>(
//					"Group 1");
//			group.setName("Brians Group");
//			group.setBusinessOwner("Brian");
//			group.setTechnicalOwner("Eoin");
//			group.setLocation("Athlone");
//			group.setDescription("Demo Group");

			eu.h2020.protective.ca.mair.api.model2.groups.Network network = new eu.h2020.protective.ca.mair.api.model2.groups.Network(
					"Network 1");
			network.setName("Brians Netywork");
			network.setBusinessOwner("Brian");
			network.setTechnicalOwner("Eoin");
			network.setLocation("Athlone");
			network.setDescription("Demo Network");

			eu.h2020.protective.ca.mair.api.model2.groups.Package aPackage = new eu.h2020.protective.ca.mair.api.model2.groups.Package(
					"Package 1");
			aPackage.setName("Brians Cluster");
			aPackage.setBusinessOwner("Brian");
			aPackage.setTechnicalOwner("Eoin");
			aPackage.setLocation("Athlone");
			aPackage.setDescription("Demo Cluster");

			eu.h2020.protective.ca.mair.api.model2.groups.Pool pool = new eu.h2020.protective.ca.mair.api.model2.groups.Pool(
					"Pool 1");
			pool.setName("Brians Cluster");
			pool.setBusinessOwner("Brian");
			pool.setTechnicalOwner("Eoin");
			pool.setLocation("Athlone");
			pool.setDescription("Demo Cluster");

			eu.h2020.protective.ca.mair.api.model2.nodes.Computer computer = new eu.h2020.protective.ca.mair.api.model2.nodes.Computer(
					"Computer 1");
			computer.setName("Brians PC");
			computer.getIpAddresses().add("192.168.101.11");
			computer.setBusinessOwner("Brian");
			computer.setTechnicalOwner("Eoin");
			computer.setLocation("Athlone");
			computer.setDescription("Dell Inspiron 1700 Laptop");
			computer.setMacAddress("fa:16:3e:b4:4b:fb");
			computer.setDefaultGateway("192.168.101.1");
			computer.setIpGateway("192.168.101.1");
			computer.setDns("dns.11.com");
			computer.setIpV4Subnet("192.168.101.0");
			computer.setIpV4SubnetMask("255.255.255.0");
			computer.getIpV4Addresses().add("192.168.101.11");
			computer.setNetworkType("Wifi");
			computer.setModel("Dell Inspiron");
			computer.setDeviceType("Physical");
			computer.setArchitecture("AMD64");
			computer.setWeight(3.0d);
			computer.setMaxCvss(6.0d);

			eu.h2020.protective.ca.mair.api.model2.nodes.Container container = new eu.h2020.protective.ca.mair.api.model2.nodes.Container(
					"Container 1");
			container.setName("Brians Container");
			container.getIpAddresses().add("192.168.102.11");
			container.setBusinessOwner("Brian");
			container.setTechnicalOwner("Eoin");
			container.setLocation("Athlone");
			// container.setDescription("Dell Inspiron 1700 Laptop");
			container.setMacAddress("fa:16:3e:b4:4b:fc");
			container.setDefaultGateway("192.168.102.1");
			container.setIpGateway("192.168.102.1");
			container.setDns("dns.1111.com");
			container.setIpV4Subnet("192.168.102.0");
			container.setIpV4SubnetMask("255.255.255.0");
			container.getIpV4Addresses().add("192.168.102.11");
			// container.setNetworkType("Wifi");
			// container.setModel("Dell Inspiron");
			container.setDeviceType("Virtual");
			container.setArchitecture("AMD64");
			container.setWeight(3.0d);

			eu.h2020.protective.ca.mair.api.model2.nodes.NetworkNode networkNode = new eu.h2020.protective.ca.mair.api.model2.nodes.NetworkNode(
					"NetworkNode 1");
			networkNode.setName("Brians Router");
			networkNode.getIpAddresses().add("192.168.101.2");
			networkNode.setBusinessOwner("Brian");
			networkNode.setTechnicalOwner("Eoin");
			networkNode.setLocation("Athlone");
			networkNode.setDescription("Belkin router");
			networkNode.setMacAddress("fa:16:3e:b4:4b:5a");
			networkNode.setDefaultGateway("192.168.101.1");
			networkNode.setIpGateway("192.168.101.1");
			networkNode.setDns("dns.2.com");
			networkNode.setIpV4Subnet("192.168.101.0");
			networkNode.setIpV4SubnetMask("255.255.255.0");
			networkNode.getIpV4Addresses().add("192.168.101.2");
			networkNode.setNetworkType("Wifi");
			networkNode.setModel("Belkin Router");
			networkNode.setDeviceType("Physical");
			networkNode.setArchitecture("ARM32");
			networkNode.setWeight(1.0d);

			eu.h2020.protective.ca.mair.api.model2.nodes.Node node = new eu.h2020.protective.ca.mair.api.model2.nodes.Node(
					"Node 1");
			node.setName("Brians Node");
			node.getIpAddresses().add("192.168.103.11");
			node.setBusinessOwner("Brian");
			node.setTechnicalOwner("Eoin");
			node.setLocation("Athlone");
			// node.setDescription("Dell Inspiron 1700 Laptop");
			node.setMacAddress("fa:16:3e:b4:4b:fd");
			node.setDefaultGateway("192.168.103.1");
			node.setIpGateway("192.168.103.1");
			node.setDns("dns.111111.com");
			node.setIpV4Subnet("192.168.103.0");
			node.setIpV4SubnetMask("255.255.255.0");
			node.getIpV4Addresses().add("192.168.103.11");
			// node.setNetworkType("Wifi");
			// node.setModel("Dell Inspiron");
			node.setDeviceType("Virtual");
			node.setArchitecture("AMD64");
			node.setWeight(3.0d);
			node.setMaxCvss(10.0d);

			eu.h2020.protective.ca.mair.api.model2.nodes.VirtualMachine vm = new eu.h2020.protective.ca.mair.api.model2.nodes.VirtualMachine(
					"Virtual Machine 1");
			vm.setName("Brians Virtual Machine");
			vm.getIpAddresses().add("192.168.104.11");
			vm.setBusinessOwner("Brian");
			vm.setTechnicalOwner("Eoin");
			vm.setLocation("Athlone");
			// vm.setDescription("Dell Inspiron 1700 Laptop");
			vm.setMacAddress("fa:16:3e:b4:4b:fe");
			vm.setDefaultGateway("192.168.104.1");
			vm.setIpGateway("192.168.104.1");
			vm.setDns("dns.11111111.com");
			vm.setIpV4Subnet("192.168.104.0");
			vm.setIpV4SubnetMask("255.255.255.0");
			vm.getIpV4Addresses().add("192.168.104.11");
			// vm.setNetworkType("Wifi");
			// vm.setModel("Dell Inspiron");
			vm.setDeviceType("Virtual");
			vm.setArchitecture("AMD64");
			vm.setWeight(3.0d);

			eu.h2020.protective.ca.mair.api.model2.other.BusinessUnit bu = new eu.h2020.protective.ca.mair.api.model2.other.BusinessUnit(
					"Business Unit 1");
			bu.setName("Brians Business Unit");

			eu.h2020.protective.ca.mair.api.model2.other.MissionObjective m = new eu.h2020.protective.ca.mair.api.model2.other.MissionObjective(
					"Mission Objective 1");
			m.setName("Brians Mission Objective");

			eu.h2020.protective.ca.mair.api.model2.other.SecurityObjective sec = new eu.h2020.protective.ca.mair.api.model2.other.SecurityObjective(
					"Security Objective 1");
			sec.setName("Brians Security Objective");

			eu.h2020.protective.ca.mair.api.model2.other.User u = new eu.h2020.protective.ca.mair.api.model2.other.User(
					"User 1");
			u.setName("Brians User");

			eu.h2020.protective.ca.mair.api.model2.services.BusinessProcess bp = new eu.h2020.protective.ca.mair.api.model2.services.BusinessProcess(
					"Business Process 1");
			bp.setName("Brians Business Process");

			eu.h2020.protective.ca.mair.api.model2.services.ConnectivityService con = new eu.h2020.protective.ca.mair.api.model2.services.ConnectivityService(
					"Connectivity Service 1");
			con.setName("Brians Connectivity Service");

			eu.h2020.protective.ca.mair.api.model2.services.ITService its = new eu.h2020.protective.ca.mair.api.model2.services.ITService(
					"IT Service 1");
			its.setName("Brians IT Service");

			eu.h2020.protective.ca.mair.api.model2.services.Service ser = new eu.h2020.protective.ca.mair.api.model2.services.Service(
					"Service 1");
			ser.setName("Brians Service");

//			Uses runsOnOS = new Uses(app, os);
//			runsOnOS.setFromCdg("infrastructureLayer");
//			runsOnOS.setToCdg("infrastructureLayer");
//
//			RealisedAs realisedAs = new RealisedAs(its, app);
//			realisedAs.setFromCdg("applicationLayer");
//			realisedAs.setToCdg("infrastructureLayer");
//
//			RunsOn runsOn = new RunsOn(app, computer);
//			runsOn.setFromCdg("infrastructureLayer");
//			runsOn.setToCdg("infrastructureLayer");
//			runsOn.setGrade("high");

			graph.addVertex(asset);
			graph.addVertex(information);
			graph.addVertex(os);
			graph.addVertex(sw);
			graph.addVertex(cluster);
//			graph.addVertex(group);
			graph.addVertex(network);
			graph.addVertex(aPackage);
			graph.addVertex(pool);
			graph.addVertex(computer);
			graph.addVertex(container);
			graph.addVertex(networkNode);
			graph.addVertex(node);
			graph.addVertex(vm);
			graph.addVertex(bu);
			graph.addVertex(m);
			graph.addVertex(sec);
			graph.addVertex(u);
			graph.addVertex(bp);
			graph.addVertex(con);
			graph.addVertex(its);
			graph.addVertex(ser);

//			graph.addEdge(its, app, realisedAs);
//			graph.addEdge(app, os, runsOnOS);
//			graph.addEdge(os, computer, runsOn);
		}
//		{
//
//			graph.addVertex(os);
//			graph.addVertex(networkNode);
//			graph.addEdge(os, networkNode, runsOn);
//		}
		mongoDB.insertOrUpdateManyVertices(graph.getContext(), graph.getVertices());
		mongoDB.insertOrUpdateManyEdges(graph.getContext(), graph.getEdges());
		return graph;
	}

}
