package eu.h2020.protective.ca.mair;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;
import static org.springframework.http.HttpMethod.POST;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.client.MockRestServiceServer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.h2020.protective.ca.mair.api.model.core.Graph;
import eu.h2020.protective.ca.mair.api.model2.assets.OperatingSystem;
import eu.h2020.protective.ca.mair.api.model2.assets.Software;
import eu.h2020.protective.ca.mair.api.model2.edges.DependsOn;
import eu.h2020.protective.ca.mair.api.model2.edges.RunsOn;
import eu.h2020.protective.ca.mair.api.model2.nodes.Node;
import eu.h2020.protective.ca.mair.matrices.Matrices;
import eu.h2020.protective.ca.mair.model.ASNode;
import eu.h2020.protective.ca.mair.model.ASSoftware;
import eu.h2020.protective.ca.mair.model.Metadata;
import eu.h2020.protective.ca.mair.test.support.TestTags;

@RestClientTest({ CaRestController.class, BuildProperties.class, SyncGraph.class, MongoDB.class, MongoDBQueries.class,
		Matrices.class, MatrixInit.class, CaAsClient.class, ConfigurationService.class })
@ExtendWith({ SpringExtension.class })
@TestPropertySource(locations = "classpath:application-test.properties")
@ActiveProfiles(profiles = "non-async")
@Tag(TestTags.FAST)
public class CaAsApiTests {

	@Autowired
	private ConfigurationService config;

	@Autowired
	private MongoDB mongoDB;

	@Autowired
	private SyncGraph syncGraph;

	@Autowired
	private CaAsClient asClient;

	@Autowired
	private MockRestServiceServer server;

	private ObjectMapper mapper = new ObjectMapper();

	@BeforeEach
	public void setUp() throws Exception {
		config.setCaAsUrl("http://localhost:1234"); // for these tests only.
		mongoDB.clearAllMatricesAndGraphs(Metadata.DEFAULT_CONTEXT);
	}

	@Test
	public void getAssetCvssForGraph() throws Exception {
		String node1Key = "Node-1";

		Graph g = new Graph();
		g.setContext(Metadata.DEFAULT_CONTEXT);

		Node n1 = new Node(node1Key);
		g.addVertex(n1);

		OperatingSystem os1 = new OperatingSystem("Windows-1");
		os1.setProduct("Windows");
		os1.setVendor("Microsoft");
		os1.setVersion("10.0");
		g.addVertex(os1);

		Software sw1 = new Software("Word-1");
		sw1.setProduct("Word");
		sw1.setVendor("Microsoft");
		sw1.setVersion("1.0");
		g.addVertex(sw1);

		Software sw2 = new Software("Excel-1");
		sw2.setProduct("Excel");
		sw2.setVendor("Microsoft");
		sw2.setVersion("1.0");
		g.addVertex(sw2);

		RunsOn os1n1 = new RunsOn(os1, n1);
		g.addEdge(os1, n1, os1n1);

		DependsOn sw1os1 = new DependsOn(sw1, os1);
		g.addEdge(sw1, os1, sw1os1);

		DependsOn sw2os1 = new DependsOn(sw2, os1);
		g.addEdge(sw2, os1, sw2os1);

		ASNode asn1 = createASNode(node1Key);
		String jsonToApi = mapper.writeValueAsString(asn1); // using jackson annotations

		Double expectedCvss = 1.23d;
		String jsonFromApi = mapper.writeValueAsString(expectedCvss);

		this.server.expect(requestTo(config.getCaAsUrl() + "/api/get-asset-max-cvss")).andExpect(method(POST))
				.andExpect(content().json(jsonToApi)).andRespond(withSuccess(jsonFromApi, MediaType.APPLICATION_JSON));

		syncGraph.addOrUpdateGraph(g);

		this.asClient.getAssetCvss(g);

		Node n = (Node) mongoDB.findVertexByKey(g.getContext(), node1Key);
		assertThat(n.getMaxCvss()).isEqualTo(expectedCvss);
	}

	@Test
	public void getAssetCvss() throws JsonProcessingException {
		String node1Key = "Node-1";
		Double expectedCvss = 1.23d;
		String jsonFromApi = mapper.writeValueAsString(expectedCvss);
		this.server.expect(requestTo(config.getCaAsUrl() + "/api/get-asset-max-cvss"))
				.andRespond(withSuccess(jsonFromApi, MediaType.APPLICATION_JSON));

		ASNode n1 = createASNode(node1Key);

		String jsonToApi = mapper.writeValueAsString(n1);
		System.out.println(jsonToApi);
		System.out.println(jsonFromApi);

		Double actualCvss = this.asClient.getAssetCvss(n1);
		assertThat(actualCvss).isEqualTo(expectedCvss);
	}

	private ASNode createASNode(String node1Key) {
		ASNode n1 = new ASNode(node1Key);
		ASSoftware sw1 = new ASSoftware("Word-1");
		sw1.setProduct("Word");
		sw1.setVendor("Microsoft");
		sw1.setVersion("1.0");
		n1.getSoftware().add(sw1);
		ASSoftware sw2 = new ASSoftware("Excel-1");
		sw2.setProduct("Excel");
		sw2.setVendor("Microsoft");
		sw2.setVersion("1.0");
		n1.getSoftware().add(sw2);
		ASSoftware sw3 = new ASSoftware("Windows-1");
		sw3.setProduct("Windows");
		sw3.setVendor("Microsoft");
		sw3.setVersion("10.0");
		n1.getOperatingSystems().add(sw3);
		return n1;
	}

	@Test
	public void getSoftwareCvss() throws JsonProcessingException {
		Double expectedCvss = 1.23d;
		this.server.expect(requestTo(config.getCaAsUrl() + "/api/get-software-max-cvss"))
				.andRespond(withSuccess(expectedCvss.toString(), MediaType.APPLICATION_JSON));
		ASSoftware sw = new ASSoftware("Word-1");
		sw.setProduct("Word");
		sw.setVendor("Microsoft");
		sw.setVersion("1.0");
		String jsonToApi = mapper.writeValueAsString(sw);
		System.out.println(jsonToApi);
		Double actualCvss = this.asClient.getSoftwareCvss(sw);
		assertThat(actualCvss).isEqualTo(expectedCvss);
	}

}
