package eu.h2020.protective.ca.mair.antlr.json;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.antlr.v4.gui.TreeViewer;
import org.antlr.v4.runtime.misc.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import eu.h2020.protective.ca.mair.MongoDB;
import eu.h2020.protective.ca.mair.api.model.core.Edge;
import eu.h2020.protective.ca.mair.api.model.core.GraphElement;
import eu.h2020.protective.ca.mair.api.model.core.Vertex;
import eu.h2020.protective.ca.mair.api.model2.assets.Information;
import eu.h2020.protective.ca.mair.api.model2.assets.Software;
import eu.h2020.protective.ca.mair.api.model2.edges.AccessedVia;
import eu.h2020.protective.ca.mair.api.model2.edges.Contains;
import eu.h2020.protective.ca.mair.api.model2.edges.DependsOn;
import eu.h2020.protective.ca.mair.api.model2.edges.RealisedAs;
import eu.h2020.protective.ca.mair.api.model2.edges.RunsOn;
import eu.h2020.protective.ca.mair.api.model2.edges.SupportedBy;
import eu.h2020.protective.ca.mair.api.model2.edges.Uses;
import eu.h2020.protective.ca.mair.api.model2.nodes.Node;
import eu.h2020.protective.ca.mair.api.model2.other.MissionObjective;
import eu.h2020.protective.ca.mair.api.model2.services.BusinessProcess;
import eu.h2020.protective.ca.mair.api.model2.services.ITService;
import eu.h2020.protective.ca.mair.test.support.TestTags;
import lombok.extern.slf4j.XSlf4j;

@XSlf4j
@Tag(TestTags.FAST)
@Tag(TestTags.PARALLEL)
public class CdgTests {

	private static boolean viewTree = false;

	public static final int NUMBER_OF_CDGS = 8;
	public static final int NUMBER_OF_VERTICES = 21;
	public static final int NUMBER_OF_EDGES = 21;

	private Map<String, Vertex> vertices;
	private Map<String, Map<Pair<String, String>, Edge>> edges;

	private Map<String, Double> missionWeights = new HashMap<>();
	private Map<String, Double> vertexWeights = new HashMap<>();
	private Map<String, Double> edgeWeights = new HashMap<>();

	public static List<String> getCdgNames() {
		List<String> cdgNames = new ArrayList<>();
		cdgNames.add("applicationLayer");
		cdgNames.add("infrastructureLayer");
		cdgNames.add("missionLayer");
		cdgNames.add("networkLayer1");
		cdgNames.add("networkLayer2");
		cdgNames.add("networkLayer3");
		cdgNames.add("operationalLayer");
		cdgNames.add(Edge.ALL_EDGES_CDG);
		// cdgNames.add(Edge.OTHER_EDGES_CDG); // empty
		return cdgNames;
	}

	private static void logGraphElements(Iterable<? extends GraphElement> iterable) {
		List<GraphElement> list = new ArrayList<>();
		iterable.forEach(list::add);
		list.sort(Comparator.comparing(GraphElement::getSimpleType).thenComparing(GraphElement::getKey));
		for (GraphElement element : list) {
			if (element instanceof Vertex) {
				log.info("{} = {}", element.getSimpleType(), element.getKey());
			}
			if (element instanceof Edge) {
				log.info("{} = {}, '{}'", element.getSimpleType(), element.getKey(), ((Edge) element).getGrade());
			}
		}
	}

	private static void viewTree(CdgPass2Visitor passN) {
		if (viewTree) {
			String[] ruleNamesArray = passN.getCdgSupport().getParser().getRuleNames();
			List<String> ruleNamesList = new ArrayList<>();
			Collections.addAll(ruleNamesList, ruleNamesArray);

			TreeViewer viewr = new TreeViewer(ruleNamesList, passN.getCdgSupport().getParseTree());
			viewr.open();
		}
	}

	private static CdgPass1Visitor readCdg(CdgPass1Visitor pass1, String context, boolean validate, String... errors)
			throws IOException {

		CdgPass2Visitor pass2 = pass1.getCdgPass2Visitor();

		viewTree(pass2);

		log.info("");
		log.info("Vertices follow:");
		log.info("");
		logGraphElements(pass2.getCdgSupport().getVertices());
		log.info("");
		log.info("Edges follow:");
		log.info("");
		for (String cdgName : pass2.getCdgSupport().getCdgNames()) {
			log.info("\t{}:", cdgName);
			logGraphElements(pass2.getCdgSupport().getEdges(cdgName));
			log.info("");
		}
		log.info("Tokens follow:");
		log.info("");
		log.info("{}", pass2.getCdgSupport().getTokenStream().getTokens());
		log.info("");
		log.info("Tree follows:");
		log.info("");
		log.info("{}", pass2.getCdgSupport().getParseTree().toStringTree(pass2.getCdgSupport().getParser()));
		log.info("");

		if (context != null) {
			assertThat(pass2.getCdgSupport().getContext()).isEqualTo(context);
		}
		if (validate) {
			assertCdg_1(pass2);
		}
		if (errors.length > 0) {
			assertThat(pass2.getErrors()).containsExactly(fixSeparators(errors));
		}

		return pass1;
	}

	// Change separators to match server.
	private static String[] fixSeparators(String... errors) {
		for (int i = 0; i < errors.length; i++) {
			errors[i] = errors[i].replace("/", File.separator);
		}
		return errors;
	}

	private static void assertCdg_1(CdgPass2Visitor passN) {

		// Vertices follow:

		assertThat(passN.getCdgSupport().getVertex("c1")).isExactlyInstanceOf(BusinessProcess.class)
				.hasFieldOrPropertyWithValue("weight", 1.0d).hasFieldOrPropertyWithValue("expression", Vertex.SINK);
		assertThat(passN.getCdgSupport().getVertex("g")).isExactlyInstanceOf(BusinessProcess.class)
				.hasFieldOrPropertyWithValue("weight", 2.0d).hasFieldOrPropertyWithValue("expression", Vertex.AND);
		assertThat(passN.getCdgSupport().getVertex("help_desk")).isExactlyInstanceOf(BusinessProcess.class)
				.hasFieldOrPropertyWithValue("weight", 4.0d).hasFieldOrPropertyWithValue("expression", Vertex.AND);
		assertThat(passN.getCdgSupport().getVertex("log_requests")).isExactlyInstanceOf(BusinessProcess.class)
				.hasFieldOrPropertyWithValue("weight", 1.0d).hasFieldOrPropertyWithValue("expression", Vertex.OR);
		assertThat(passN.getCdgSupport().getVertex("service_rollout")).isExactlyInstanceOf(BusinessProcess.class)
				.hasFieldOrPropertyWithValue("weight", 3.0d).hasFieldOrPropertyWithValue("expression", Vertex.OR);

		assertThat(passN.getCdgSupport().getVertex("dns")).isExactlyInstanceOf(ITService.class);
		assertThat(passN.getCdgSupport().getVertex("https")).isExactlyInstanceOf(ITService.class);
		assertThat(passN.getCdgSupport().getVertex("my_sql")).isExactlyInstanceOf(ITService.class);
		assertThat(passN.getCdgSupport().getVertex("ssh")).isExactlyInstanceOf(ITService.class);

		assertThat(passN.getCdgSupport().getVertex("customer_info")).isExactlyInstanceOf(Information.class);
		assertThat(passN.getCdgSupport().getVertex("traffic_level")).isExactlyInstanceOf(Information.class);

		assertThat(passN.getCdgSupport().getVertex("m .1")).isExactlyInstanceOf(MissionObjective.class);

		assertThat(passN.getCdgSupport().getVertex("host1")).isExactlyInstanceOf(Node.class);
		assertThat(passN.getCdgSupport().getVertex("server1")).isExactlyInstanceOf(Node.class);

		assertThat(passN.getCdgSupport().getVertex("libc")).isExactlyInstanceOf(Software.class);
		assertThat(passN.getCdgSupport().getVertex("mysqlsw")).isExactlyInstanceOf(Software.class);
		assertThat(passN.getCdgSupport().getVertex("swlib2")).isExactlyInstanceOf(Software.class);

		assertThat(passN.getCdgSupport().getVertex("node1")).isExactlyInstanceOf(Node.class);

		assertThat(passN.getCdgSupport().getVertex("node2")).isExactlyInstanceOf(Node.class);

		assertThat(passN.getCdgSupport().getVertex("node3")).isExactlyInstanceOf(Node.class);

		assertThat(passN.getCdgSupport().getVertex("node4")).isExactlyInstanceOf(Node.class);

		// CDG Names follow:

		assertThat(passN.getCdgSupport().getCdgNames()).containsAll(getCdgNames());

		log.info("CDG names follow:");
		for (String cdg : passN.getCdgSupport().getCdgNames()) {
			log.info("\t{}", cdg);
		}
		log.info("");

		// Edges follow:

		String cdgName = "missionLayer";

		assertThat(passN.getCdgSupport().getEdge(cdgName, "m .1-->c1")).isExactlyInstanceOf(SupportedBy.class)
				.hasFieldOrPropertyWithValue("grade", "high");
		assertThat(passN.getCdgSupport().getEdge(cdgName, "m .1-->customer_info"))
				.isExactlyInstanceOf(SupportedBy.class).hasFieldOrPropertyWithValue("grade", "critical");
		assertThat(passN.getCdgSupport().getEdge(cdgName, "m .1-->g")).isExactlyInstanceOf(SupportedBy.class)
				.hasFieldOrPropertyWithValue("grade", "critical");
		assertThat(passN.getCdgSupport().getEdge(cdgName, "m .1-->help_desk")).isExactlyInstanceOf(SupportedBy.class)
				.hasFieldOrPropertyWithValue("grade", "low");
		assertThat(passN.getCdgSupport().getEdge(cdgName, "m .1-->service_rollout"))
				.isExactlyInstanceOf(SupportedBy.class).hasFieldOrPropertyWithValue("grade", "low");
		assertThat(passN.getCdgSupport().getEdge(cdgName, "m .1-->traffic_level"))
				.isExactlyInstanceOf(SupportedBy.class).hasFieldOrPropertyWithValue("grade", "low");

		cdgName = "operationalLayer";

		assertThat(passN.getCdgSupport().getEdge(cdgName, "help_desk-->log_requests"))
				.isExactlyInstanceOf(Contains.class).hasFieldOrPropertyWithValue("grade", "high");
		assertThat(passN.getCdgSupport().getEdge(cdgName, "help_desk-->https")).isExactlyInstanceOf(Uses.class)
				.hasFieldOrPropertyWithValue("grade", "critical");
		assertThat(passN.getCdgSupport().getEdge(cdgName, "help_desk-->ssh")).isExactlyInstanceOf(Uses.class)
				.hasFieldOrPropertyWithValue("grade", "low");
		assertThat(passN.getCdgSupport().getEdge(cdgName, "customer_info-->my_sql"))
				.isExactlyInstanceOf(AccessedVia.class).hasFieldOrPropertyWithValue("grade", "critical");

		cdgName = "applicationLayer";

		assertThat(passN.getCdgSupport().getEdge(cdgName, "my_sql-->dns")).isExactlyInstanceOf(Uses.class)
				.hasFieldOrPropertyWithValue("grade", "critical");
		assertThat(passN.getCdgSupport().getEdge(cdgName, "my_sql-->https")).isExactlyInstanceOf(Uses.class)
				.hasFieldOrPropertyWithValue("grade", "high");
		assertThat(passN.getCdgSupport().getEdge(cdgName, "my_sql-->mysqlsw")).isExactlyInstanceOf(RealisedAs.class)
				.hasFieldOrPropertyWithValue("grade", "critical");

		cdgName = "infrastructureLayer";

		assertThat(passN.getCdgSupport().getEdge(cdgName, "mysqlsw-->server1")).isExactlyInstanceOf(RunsOn.class)
				.hasFieldOrPropertyWithValue("grade", "high");
		assertThat(passN.getCdgSupport().getEdge(cdgName, "mysqlsw-->libc")).isExactlyInstanceOf(Uses.class)
				.hasFieldOrPropertyWithValue("grade", "critical");
		assertThat(passN.getCdgSupport().getEdge(cdgName, "mysqlsw-->swlib2")).isExactlyInstanceOf(Uses.class)
				.hasFieldOrPropertyWithValue("grade", "critical");
		assertThat(passN.getCdgSupport().getEdge(cdgName, "server1-->host1")).isExactlyInstanceOf(DependsOn.class)
				.hasFieldOrPropertyWithValue("grade", "high");
		assertThat(passN.getCdgSupport().getEdge(cdgName, "host1-->node1")).isExactlyInstanceOf(DependsOn.class)
				.hasFieldOrPropertyWithValue("grade", "high");

		cdgName = "networkLayer1";

		assertThat(passN.getCdgSupport().getEdge(cdgName, "node1-->node2")).isExactlyInstanceOf(DependsOn.class)
				.hasFieldOrPropertyWithValue("grade", "high");

		cdgName = "networkLayer2";

		assertThat(passN.getCdgSupport().getEdge(cdgName, "node2-->node3")).isExactlyInstanceOf(DependsOn.class)
				.hasFieldOrPropertyWithValue("grade", "high");

		cdgName = "networkLayer3";

		assertThat(passN.getCdgSupport().getEdge(cdgName, "node3-->node4")).isExactlyInstanceOf(DependsOn.class)
				.hasFieldOrPropertyWithValue("grade", "high");

		// Totals follow:

		assertThat(passN.getCdgSupport().getVertices()).hasSize(NUMBER_OF_VERTICES);
		assertThat(passN.getCdgSupport().getEdges()).hasSize(NUMBER_OF_EDGES);
		assertThat(passN.getCdgSupport().getCdgNames()).hasSize(NUMBER_OF_CDGS);
	}

	public static CdgPass1Visitor readCdg(Map<String, Vertex> vertices,
			Map<String, Map<Pair<String, String>, Edge>> edges, File file, String context, boolean validate,
			boolean guessEdgeType, Map<String, Double> missionWeights, Map<String, Double> vertexWeights,
			Map<String, Double> edgeWeights, String... errors) throws Exception {
		return readCdg(new CdgPass1Visitor(vertices, edges, file.getPath(), file, guessEdgeType, missionWeights,
				vertexWeights, edgeWeights), context, validate, errors);
	}

	public static CdgPass1Visitor readCdg(MongoDB mongoDB, File file, String context, boolean validate,
			boolean guessEdgeType, Map<String, Double> missionWeights, Map<String, Double> vertexWeights,
			Map<String, Double> edgeWeights, String... errors) throws Exception {
		return readCdg(new CdgPass1Visitor(mongoDB, file.getPath(), file, guessEdgeType, missionWeights, vertexWeights,
				edgeWeights), context, validate, errors);
	}

	@BeforeEach
	public void setUp() {
		vertices = new HashMap<>();
		edges = new HashMap<>();

		missionWeights.put("none", 0.0d);
		missionWeights.put("low", 1.0d);
		missionWeights.put("medium", 2.0d);
		missionWeights.put("high", 3.0d);
		missionWeights.put("critical", 4.0d);

		vertexWeights.put("none", 0.0d);
		vertexWeights.put("low", 1.0d);
		vertexWeights.put("medium", 2.0d);
		vertexWeights.put("high", 3.0d);
		vertexWeights.put("critical", 4.0d);

		edgeWeights.put("none", 0.0d);
		edgeWeights.put("low", 1.0d);
		edgeWeights.put("medium", 2.0d);
		edgeWeights.put("high", 3.0d);
		edgeWeights.put("critical", 4.0d);
	}

	@Test
	public void readCdg() throws Exception {
		org.reflections.Reflections.log = null;
		File file = new File("src/test/resources/antlr/cdg_1.cdgs");
		readCdg(vertices, edges, file, null, true, true, missionWeights, vertexWeights, edgeWeights);
	}

	@Test
	public void readCdgWithContext() throws Exception {
		org.reflections.Reflections.log = null;
		File file = new File("src/test/resources/antlr/cdg_4.cdgs");
		readCdg(vertices, edges, file, "PSNC", true, true, missionWeights, vertexWeights, edgeWeights);
	}

	@Test // Only reports invalid ID.
	public void readCdgMissingVertexAndInvalidIDAndInvalidGrade() throws Exception {
		org.reflections.Reflections.log = null;
		File file = new File("src/test/resources/antlr/cdg_2.cdgs");
		String error1 = "line 74:11 no viable alternative at input '{\"id\":g,'";
		String error2 = "line 75:5 mismatched input '\"expression\"' expecting '{'";
		String error3 = "line 76:5 mismatched input '\"weight\"' expecting '{'";
		String error4 = "line 77:5 missing '}' at ','";
		String error5 = "line 79:5 mismatched input '\"id\"' expecting '\"cdg\"'";
		readCdg(vertices, edges, file, null, false, true, missionWeights, vertexWeights, edgeWeights, error1, error2,
				error3, error4, error5);
	}

	@Test
	public void readCdgMissingVertexAndInvalidGrade() throws Exception {
		org.reflections.Reflections.log = null;
		File file = new File("src/test/resources/antlr/cdg_3.cdgs");
//		String error1 = "line 68:14 \"high123\" is not one of "
//				+ "[\"none\", \"low\", \"medium\", \"high\", \"critical\"]."; // TODO change test to check grade.
		String error2 = "line 24:21 Vertex with key \"m123\" is not in the database AND "
				+ "not defined correctly in the \"src/test/resources/antlr/cdg_3.cdgs\" file.";
		String error3 = "line 25:19 Vertex with key \"c1\" is not in the database AND "
				+ "not defined correctly in the \"src/test/resources/antlr/cdg_3.cdgs\" file.";
		String error4 = "line 31:19 Vertex with key \"g123\" is not in the database AND "
				+ "not defined correctly in the \"src/test/resources/antlr/cdg_3.cdgs\" file.";
		readCdg(vertices, edges, file, null, false, true, missionWeights, vertexWeights, edgeWeights, error2, error3,
				error4);
	}

	@Test
	public void readCdgTwice() throws Exception {
		org.reflections.Reflections.log = null;
		File file = new File("src/test/resources/antlr/cdg_1.cdgs");
		readCdg(vertices, edges, file, null, true, true, missionWeights, vertexWeights, edgeWeights);
		readCdg(vertices, edges, file, null, true, true, missionWeights, vertexWeights, edgeWeights);
	}

}
