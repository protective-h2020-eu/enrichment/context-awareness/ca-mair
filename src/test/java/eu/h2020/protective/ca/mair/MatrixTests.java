package eu.h2020.protective.ca.mair;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import eu.h2020.protective.ca.mair.api.model.assets.Asset;
import eu.h2020.protective.ca.mair.api.model.assets.Information;
import eu.h2020.protective.ca.mair.api.model.other.Mission;
import eu.h2020.protective.ca.mair.api.model.other.SecurityObjective;
import eu.h2020.protective.ca.mair.model.MatrixElement;
import eu.h2020.protective.ca.mair.model.Metadata;
import eu.h2020.protective.ca.mair.model.Tuple;
import eu.h2020.protective.ca.mair.test.support.MockMvcBase;
import eu.h2020.protective.ca.mair.test.support.TestTags;

@Tag(TestTags.MONGO)
public class MatrixTests extends MockMvcBase {

	@Autowired
	private MatrixInit matrixInit;

	@Autowired
	private MongoDB mongoDB;

	@Autowired
	private MongoDBQueries mongoDBQueries;

	@BeforeEach
	public void setUpDB() throws Exception {
		matrixInit.clearMatrices();
		matrixInit.initMatrices();
	}

	@Test
	public void initMatrices() throws Exception {
		MatrixElement element = null;

		element = mongoDB.findByKeyXAndKeyY("Stay safe", "Acquire Natural Gas");
		assertThat(element.getValue()).isEqualTo(4.0);

		element = mongoDB.findByKeyXAndKeyY("Stay profitable", "Bill for Product");
		assertThat(element.getValue()).isEqualTo(2.0);

		element = mongoDB.findByKeyXAndKeyY("Util Compressors Control Command", "Printer 1");
		assertThat(element.getValue()).isEqualTo(2.0);

		element = mongoDB.findByKeyXAndKeyY("Util Compressors Control Command", "Work Station 1");
		assertThat(element.getValue()).isEqualTo(4.0);

		element = mongoDB.findByKeyXAndKeyY("Stay safe", "Stay safe");
		assertThat(element).isNull();
	}

	@Test
	public void getBusinessObjectiveRelativeWeights() throws Exception {
		Map<String, Double> values = new HashMap<>();
		values.put("Stay safe", 1.0);
		values.put("Stay profitable", 1.25);
		values.put("Stay in compliance", 1.75);
		values.put("Supply customers well", 2.0);
		List<Tuple<String, Double>> weights = matrixInit.getBusinessObjectiveRelativeWeights(values);
		assertThat(weights.get(0).b).isEqualTo(0.348, within(0.0005));
		assertThat(weights.get(1).b).isEqualTo(0.279, within(0.0005));
		assertThat(weights.get(2).b).isEqualTo(0.199, within(0.0005));
		assertThat(weights.get(3).b).isEqualTo(0.174, within(0.0005));
	}

	@Test
	public void getTaskRelativeWeights() throws Exception {
		Map<String, Double> values = new HashMap<>();
		values.put("Stay safe", 1.0);
		values.put("Stay profitable", 1.25);
		values.put("Stay in compliance", 1.75);
		values.put("Supply customers well", 2.0);
		List<Tuple<String, Double>> matrix1weights = matrixInit.getBusinessObjectiveRelativeWeights(values);
		List<Tuple<String, Double>> matrix2weights = matrixInit.getNextRelativeWeights(matrix1weights,
				SecurityObjective.class);
		int size = 14;
		int last = size - 1;
		assertThat(matrix2weights.size()).isEqualTo(size);
		assertThat(matrix2weights.get(0).a).isEqualTo("Acquire Natural Gas");
		assertThat(matrix2weights.get(0).b).isEqualTo(4.56, within(0.005));
		assertThat(matrix2weights.get(last).a).isEqualTo("Acceptance Test Crude");
		assertThat(matrix2weights.get(last).b).isEqualTo(0.56, within(0.005));
	}

	@Test
	public void getInformationAssetRelativeWeights() throws Exception {
		Map<String, Double> values = new HashMap<>();
		values.put("Stay safe", 1.0);
		values.put("Stay profitable", 1.25);
		values.put("Stay in compliance", 1.75);
		values.put("Supply customers well", 2.0);
		List<Tuple<String, Double>> matrix1weights = matrixInit.getBusinessObjectiveRelativeWeights(values);
		List<Tuple<String, Double>> matrix2weights = matrixInit.getNextRelativeWeights(matrix1weights,
				SecurityObjective.class);
		List<Tuple<String, Double>> matrix3weights = matrixInit.getNextRelativeWeights(matrix2weights,
				Information.class);
		int minSize = 30;
		int last = minSize - 1;
		assertThat(matrix3weights.size()).isGreaterThanOrEqualTo(minSize);
		assertThat(matrix3weights.get(0).a).isEqualTo("Util Pump Safety Command");
		assertThat(matrix3weights.get(0).b).isEqualTo(32.08, within(0.005));
		assertThat(matrix3weights.get(last).a).isEqualTo("HPU Pump Control Sensor Output");
		assertThat(matrix3weights.get(last).b).isEqualTo(14.49, within(0.005));
	}

	@Test
	public void getNetworkNodeRelativeWeights() throws Exception {
		List<Tuple<String, Double>> matrix4weights = mongoDBQueries.getNetworkNodeRelativeWeights();
		int minSize = 30;
		int last = minSize - 2;
		assertThat(matrix4weights.size()).isGreaterThanOrEqualTo(minSize);
		assertThat(matrix4weights.get(0).a).isEqualTo("OPC Server");
		assertThat(matrix4weights.get(0).b).isEqualTo(1682, within(0.5));
		assertThat(matrix4weights.get(last).a).isEqualTo("Plant LAN Router");
		assertThat(matrix4weights.get(last).b).isEqualTo(49, within(0.5));
	}

	@Test
	public void getMostImportantMissionForVertex() throws Exception {
		mongoDBQueries.getNetworkNodeRelativeWeights(); // initialize all weights
		Mission mission = mongoDBQueries.getMostImportantMissionForVertexKey(Metadata.DEFAULT_CONTEXT, "OPC Server");
		assertThat(mission.getKey()).isEqualTo("Stay safe");
	}

	// @Test // FIXME need data source with ip
	public void getMostImportantMissionForNodeIpAddress() throws Exception {
		mongoDBQueries.getNetworkNodeRelativeWeights(); // initialize all weights
		Mission mission = mongoDBQueries.getMostImportantMissionForNodeIpAddress(Metadata.DEFAULT_CONTEXT,
				"OPC Server");
		assertThat(mission.getKey()).isEqualTo("Stay safe");
	}

	// @Test // FIXME need data source with dns
	public void getMostImportantMissionForNodeDns() throws Exception {
		mongoDBQueries.getNetworkNodeRelativeWeights(); // initialize all weights
		Mission mission = mongoDBQueries.getMostImportantMissionForNodeDns(Metadata.DEFAULT_CONTEXT, "OPC Server");
		assertThat(mission.getKey()).isEqualTo("Stay safe");
	}

	@Test
	public void getMostCriticalAssets() throws Exception {
		List<Asset> assets = mongoDBQueries.getMostCriticalAssets(Metadata.DEFAULT_CONTEXT, 10);
		assertThat(assets).hasSize(10);
		assertThat(assets.get(0).getKey()).isEqualTo("OPC Server");
		assertThat(assets.get(9).getKey()).isEqualTo("Comm Processor");
	}

}
