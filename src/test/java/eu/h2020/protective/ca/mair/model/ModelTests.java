package eu.h2020.protective.ca.mair.model;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.meanbean.test.BeanTestException;

import com.google.common.reflect.ClassPath;

import eu.h2020.protective.ca.mair.test.support.TestTags;
import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtConstructor;
import javassist.CtMethod;
import javassist.CtNewConstructor;
import javassist.CtNewMethod;
import javassist.NotFoundException;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

// ClassLoader code adapted from http://stackoverflow.com/a/21430849/2464657
@Tag(TestTags.FAST)
@Tag(TestTags.PARALLEL)
public class ModelTests {

	private final String MODEL_PACKAGE = this.getClass().getPackage().getName();

	// private BeanTester beanTester; // dropped by Eoin

	@BeforeEach
	public void before() {
		// beanTester = new BeanTester(); // dropped by Eoin
	}

	@Test
	public void testAbstractModels() throws IllegalArgumentException, BeanTestException, InstantiationException,
			IllegalAccessException, IOException, AssertionError, NotFoundException, CannotCompileException {
		// Loop through classes in the model package
		final ClassLoader loader = Thread.currentThread().getContextClassLoader();
		for (final ClassPath.ClassInfo info : ClassPath.from(loader).getTopLevelClassesRecursive(MODEL_PACKAGE)) {
			final Class<?> clazz = info.load();

			// Only test abstract classes
			if (Modifier.isAbstract(clazz.getModifiers())) {
				// Test #equals and #hashCode
				EqualsVerifier.forClass(clazz).suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS).verify();
			}
		}
	}

	@Test
	public void testConcreteModels() throws IOException, InstantiationException, IllegalAccessException,
			NotFoundException, CannotCompileException {
		// Loop through classes in the model package
		final ClassLoader loader = Thread.currentThread().getContextClassLoader();
		for (final ClassPath.ClassInfo info : ClassPath.from(loader).getTopLevelClassesRecursive(MODEL_PACKAGE)) {
			final Class<?> clazz = info.load();

			// Skip abstract classes, interfaces and this class.
			int modifiers = clazz.getModifiers();
			if (Modifier.isAbstract(modifiers) || Modifier.isInterface(modifiers) || clazz.equals(this.getClass())
					|| clazz.getSimpleName().endsWith("Test")) {
				continue;
			}

			// Test getters, setters and #toString
			// beanTester.testBean(clazz); // dropped by Eoin

			// Test #equals and #hashCode
			EqualsVerifier.forClass(clazz).withRedefinedSuperclass().suppress(Warning.INHERITED_DIRECTLY_FROM_OBJECT,
					Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS).verify();
			// .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS).verify();

			// Verify not equals with subclass (for code coverage with Lombok) // dropped by
			// Eoin
			// Assert.assertFalse(clazz.newInstance().equals(createSubClassInstance(clazz.getName())));
		}
	}

	// Adapted from
	// http://stackoverflow.com/questions/17259421/java-creating-a-subclass-dynamically
	static Object createSubClassInstance(String superClassName)
			throws NotFoundException, CannotCompileException, InstantiationException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		ClassPool pool = ClassPool.getDefault();

		// Create the class.
		CtClass subClass = pool.makeClass(superClassName + "Extended");
		final CtClass superClass = pool.get(superClassName);
		subClass.setSuperclass(superClass);
		subClass.setModifiers(Modifier.PUBLIC);

		// Add a constructor which will call super( ... );
		CtClass[] params = new CtClass[] {};
		final CtConstructor ctor = CtNewConstructor.make(params, null, CtNewConstructor.PASS_PARAMS, null, null,
				subClass);
		subClass.addConstructor(ctor);

		// Add a canEquals method
		final CtMethod ctmethod = CtNewMethod.make(
				"public boolean canEqual(Object o) { return o instanceof " + superClassName + "Extended; }", subClass);
		subClass.addMethod(ctmethod);

		@SuppressWarnings("unchecked")
		Constructor<?> constructor = subClass.toClass().getDeclaredConstructor();
		return constructor.newInstance();
	}

}