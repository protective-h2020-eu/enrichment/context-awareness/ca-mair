package eu.h2020.protective.ca.mair.test.support;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import eu.h2020.protective.ca.mair.api.model.core.Graph;

public class JsonBase {

	protected Path toJsonFile(Graph graph, String path) throws Exception {
		return graph.toJsonFile(testPath(path));
	}

	protected String testPath(String path) throws IOException {
		Path outputFolderPath = Paths.get("build/tmp/output-for-tests");
		Files.createDirectories(outputFolderPath);
		Path outputFilePath = Paths.get(outputFolderPath.toString(), path);
		return outputFilePath.toString();
	}
	
	protected String toJsonString(String path) throws IOException {
		Path inputFilePath = Paths.get(path);
		return new String(Files.readAllBytes(inputFilePath));
	}

}
