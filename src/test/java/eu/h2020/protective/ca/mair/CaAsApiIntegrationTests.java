package eu.h2020.protective.ca.mair;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.is;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.h2020.protective.ca.mair.api.model.core.Graph;
import eu.h2020.protective.ca.mair.api.model2.assets.OperatingSystem;
import eu.h2020.protective.ca.mair.api.model2.assets.Software;
import eu.h2020.protective.ca.mair.api.model2.edges.DependsOn;
import eu.h2020.protective.ca.mair.api.model2.edges.RunsOn;
import eu.h2020.protective.ca.mair.api.model2.nodes.Node;
import eu.h2020.protective.ca.mair.model.ASNode;
import eu.h2020.protective.ca.mair.model.ASSoftware;
import eu.h2020.protective.ca.mair.model.Metadata;
import eu.h2020.protective.ca.mair.test.support.TestTags;
import lombok.extern.slf4j.XSlf4j;

@SpringBootTest
//@RestClientTest({ CaRestController.class, BuildProperties.class, SyncGraph.class, MongoDB.class, MongoDBQueries.class,
//		Matrices.class, MatrixInit.class, CaAsClient.class, ConfigurationService.class })
@ExtendWith({ SpringExtension.class })
@TestPropertySource(locations = "classpath:application-test.properties")
@ActiveProfiles(profiles = "non-async")
@Tag(TestTags.SLOW)
@XSlf4j
public class CaAsApiIntegrationTests {

	@Autowired
	private ConfigurationService config;

	@Autowired
	private MongoDB mongoDB;

	@Autowired
	private SyncGraph syncGraph;

	@Autowired
	private CaAsClient asClient;

//	@Autowired
//	private MockRestServiceServer server;

	private ObjectMapper mapper = new ObjectMapper();

	@BeforeEach
	public void setUp() throws Exception {
//		config.setCaAsUrl("http://localhost:5000"); // for these tests only.
		config.setCaAsUrl("http://192.168.200.65:5000");
		
		mongoDB.clearAllMatricesAndGraphs(Metadata.DEFAULT_CONTEXT);
	}

	@Test
	public void getIntegratedAssetCvssForGraph() throws Exception {
		config.setCaAsUrl("http://localhost:5000");
		String node1Key = "node-1";

		Graph g = new Graph();
		g.setContext(Metadata.DEFAULT_CONTEXT);

		Node n1 = new Node(node1Key);
		g.addVertex(n1);

		OperatingSystem os1 = new OperatingSystem("Windows-1");
		os1.setProduct("Windows");
		os1.setVendor("Microsoft");
		os1.setVersion("10.0");
		g.addVertex(os1);

		Software sw1 = new Software("Mozilla Thunderbird");
		sw1.setProduct("Mozilla Thunderbird 38.6.0 (x86 pl)");
		sw1.setVendor("Mozilla");
		sw1.setVersion("38.6.0");
		g.addVertex(sw1);

		Software sw2 = new Software("TeamViewer 11");
		sw2.setProduct("TeamViewer 11");
		sw2.setVendor("TeamViewer");
		sw2.setVersion("11.065452");
		g.addVertex(sw2);

		RunsOn os1n1 = new RunsOn(os1, n1);
		g.addEdge(os1, n1, os1n1);

		DependsOn sw1os1 = new DependsOn(sw1, os1);
		g.addEdge(sw1, os1, sw1os1);

		DependsOn sw2os1 = new DependsOn(sw2, os1);
		g.addEdge(sw2, os1, sw2os1);

		ASNode asn1 = createASNode(node1Key);
		String jsonToApi = mapper.writeValueAsString(asn1); // using jackson annotations
		log.info("jsonToApi: " + jsonToApi);

		Double expectedCvss = 10.0d;

		syncGraph.addOrUpdateGraph(g);

		this.asClient.getAssetCvss(g);

		Node n = (Node) mongoDB.findVertexByKey(g.getContext(), node1Key);
		assertThat(n.getMaxCvss()).isEqualTo(expectedCvss);
	}

	@Test
	public void getIntegratedAssetCvssForGraphDirect() throws Exception {
		config.setCaAsUrl("http://localhost:5000");
		String node1Key = "node-1";

		Graph g = new Graph();
		g.setContext(Metadata.DEFAULT_CONTEXT);

		Node n1 = new Node(node1Key);
		g.addVertex(n1);

		OperatingSystem os1 = new OperatingSystem("Windows-1");
		os1.setProduct("Windows");
		os1.setVendor("Microsoft");
		os1.setVersion("10.0");
		g.addVertex(os1);

		Software sw1 = new Software("Mozilla Thunderbird");
		sw1.setProduct("Mozilla Thunderbird 38.6.0 (x86 pl)");
		sw1.setVendor("Mozilla");
		sw1.setVersion("38.6.0");
		g.addVertex(sw1);

		Software sw2 = new Software("TeamViewer 11");
		sw2.setProduct("TeamViewer 11");
		sw2.setVendor("TeamViewer");
		sw2.setVersion("11.065452");
		g.addVertex(sw2);

		RunsOn os1n1 = new RunsOn(os1, n1);
		g.addEdge(os1, n1, os1n1);

		DependsOn sw1os1 = new DependsOn(sw1, os1);
		g.addEdge(sw1, os1, sw1os1);

		DependsOn sw2os1 = new DependsOn(sw2, os1);
		g.addEdge(sw2, os1, sw2os1);

		ASNode asn1 = createASNode(node1Key);
		String jsonToApi = mapper.writeValueAsString(asn1); // using jackson annotations

		String expectedCvss = "10.000000";

		given().baseUri(config.getCaAsUrl()).contentType(MediaType.APPLICATION_JSON_VALUE).body(jsonToApi).when()
				.post("/api/get-asset-max-cvss").then().statusCode(200).body(is(expectedCvss));

		log.info("jsonToApi: " + jsonToApi);
	}

	@Test
	public void getAssetCvss() throws JsonProcessingException {
		config.setCaAsUrl("http://localhost:5000");
		String node1Key = "node-1";
		Double expectedCvss = 10.0d;
		ASNode n1 = createASNode(node1Key);

		String jsonToApi = mapper.writeValueAsString(n1);
		log.info("jsonToApi: " + jsonToApi);

		Double actualCvss = this.asClient.getAssetCvss(n1);
		assertThat(actualCvss).isEqualTo(expectedCvss);
	}

	@Test
	public void getAssetCvssDirect() throws JsonProcessingException {
		config.setCaAsUrl("http://localhost:5000");
		String node1Key = "node-1";
		String expectedCvss = "10.000000";

		ASNode n1 = createASNode(node1Key);
		String jsonToApi = mapper.writeValueAsString(n1);

		log.info("jsonToApi: " + jsonToApi);

		given().baseUri(config.getCaAsUrl()).contentType(MediaType.APPLICATION_JSON_VALUE).body(jsonToApi).when()
				.post("/api/get-asset-max-cvss").then().statusCode(200).body(is(expectedCvss));
	}

	private ASNode createASNode(String node1Key) {
		ASNode n1 = new ASNode(node1Key);
		ASSoftware sw1 = new ASSoftware("Mozilla Thunderbird");
		sw1.setProduct("Mozilla Thunderbird 38.6.0 (x86 pl)");
		sw1.setVendor("Mozilla");
		sw1.setVersion("38.6.0");
		n1.getSoftware().add(sw1);
		ASSoftware sw2 = new ASSoftware("TeamViewer 11");
		sw2.setProduct("TeamViewer 11");
		sw2.setVendor("TeamViewer");
		sw2.setVersion("11.065452");
		n1.getSoftware().add(sw2);
		ASSoftware sw3 = new ASSoftware("Windows-1");
		sw3.setProduct("Windows");
		sw3.setVendor("Microsoft");
		sw3.setVersion("10.0");
		n1.getOperatingSystems().add(sw3);
		return n1;
	}

	@Test
	public void getSoftwareCvss() throws JsonProcessingException {
		config.setCaAsUrl("http://localhost:5000");
		Double expectedCvss = 10.0d;
		ASSoftware sw = new ASSoftware("Mozilla Thunderbird");
		sw.setProduct("Mozilla Thunderbird 38.6.0 (x86 pl)");
		sw.setVendor("Mozilla");
		sw.setVersion("38.6.0");
		String jsonToApi = mapper.writeValueAsString(sw);
		log.info("jsonToApi: " + jsonToApi);
		Double actualCvss = this.asClient.getSoftwareCvss(sw);
		assertThat(actualCvss).isEqualTo(expectedCvss);
	}
	@Test
	public void getUpdateTime() {
		String expectedUpdateString = "No data about last CPE update";
		String actualUpdateString = this.asClient.getLastCpeUpdateTime();
		log.info("actualUpdateString is: " + actualUpdateString.trim());
		assertThat(actualUpdateString.trim()).isEqualTo(expectedUpdateString);
	}

	@Test
	public void getSoftwareCvssDirect() throws JsonProcessingException {
		config.setCaAsUrl("http://localhost:5000");
		String expectedCvss = "10.000000";
		ASSoftware sw = new ASSoftware("Mozilla Thunderbird");
		sw.setProduct("Mozilla Thunderbird 38.6.0 (x86 pl)");
		sw.setVendor("Mozilla");
		sw.setVersion("38.6.0");
		String jsonToApi = mapper.writeValueAsString(sw);
		log.info("jsonToApi: " + jsonToApi);
		given().baseUri(config.getCaAsUrl()).contentType(MediaType.APPLICATION_JSON_VALUE).body(jsonToApi).when()
				.post("/api/get-software-max-cvss").then().statusCode(200).body(is(expectedCvss));
	}

}
