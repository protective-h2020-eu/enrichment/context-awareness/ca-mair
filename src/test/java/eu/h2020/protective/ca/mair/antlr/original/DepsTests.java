package eu.h2020.protective.ca.mair.antlr.original;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.antlr.v4.gui.TreeViewer;
import org.antlr.v4.runtime.misc.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import eu.h2020.protective.ca.mair.MongoDB;
import eu.h2020.protective.ca.mair.api.model.core.Edge;
import eu.h2020.protective.ca.mair.api.model.core.GraphElement;
import eu.h2020.protective.ca.mair.api.model.core.Vertex;
import eu.h2020.protective.ca.mair.api.model2.assets.Information;
import eu.h2020.protective.ca.mair.api.model2.assets.Software;
import eu.h2020.protective.ca.mair.api.model2.edges.AccessedVia;
import eu.h2020.protective.ca.mair.api.model2.edges.ConnectedTo;
import eu.h2020.protective.ca.mair.api.model2.edges.Contains;
import eu.h2020.protective.ca.mair.api.model2.edges.DependsOn;
import eu.h2020.protective.ca.mair.api.model2.edges.RealisedAs;
import eu.h2020.protective.ca.mair.api.model2.edges.SupportedBy;
import eu.h2020.protective.ca.mair.api.model2.edges.Uses;
import eu.h2020.protective.ca.mair.api.model2.nodes.Node;
import eu.h2020.protective.ca.mair.api.model2.other.MissionObjective;
import eu.h2020.protective.ca.mair.api.model2.services.BusinessProcess;
import eu.h2020.protective.ca.mair.api.model2.services.ITService;
import eu.h2020.protective.ca.mair.test.support.TestTags;
import lombok.extern.slf4j.XSlf4j;

@XSlf4j
@Tag(TestTags.FAST)
@Tag(TestTags.PARALLEL)
public class DepsTests {

	private static boolean viewTree = false;

	public static final int NUMBER_OF_VERTICES = 17;
	public static final int NUMBER_OF_EDGES = NUMBER_OF_VERTICES * 3;

	private Map<String, Double> missionWeights = new HashMap<>();
	private Map<String, Double> vertexWeights = new HashMap<>();
	private Map<String, Double> edgeWeights = new HashMap<>();

	public static List<String> getCdgNames() {
		List<String> cdgNames = new ArrayList<>();
		cdgNames.add("cdg1");
		cdgNames.add("cdg2");
		cdgNames.add("cdg3");
		cdgNames.add(Edge.ALL_EDGES_CDG);
		// cdgNames.add(Edge.OTHER_EDGES_CDG); // empty
		return cdgNames;
	}

	private static void logGraphElements(Iterable<? extends GraphElement> iterable) {
		List<GraphElement> list = new ArrayList<>();
		iterable.forEach(list::add);
		list.sort(Comparator.comparing(GraphElement::getSimpleType).thenComparing(GraphElement::getKey));
		for (GraphElement element : list) {
			if (element instanceof Vertex) {
				log.info("{} = {}", element.getSimpleType(), element.getKey());
			}
			if (element instanceof Edge) {
				log.info("{} = {}, '{}'", element.getSimpleType(), element.getKey(), ((Edge) element).getGrade());
			}
		}
	}

	private static void viewTree(DepsPass1Visitor pass1) {
		if (viewTree) {
			String[] ruleNamesArray = pass1.getCdgSupport().getParser().getRuleNames();
			List<String> ruleNamesList = new ArrayList<>();
			Collections.addAll(ruleNamesList, ruleNamesArray);

			TreeViewer viewr = new TreeViewer(ruleNamesList, pass1.getCdgSupport().getParseTree());
			viewr.open();
		}
	}

	private static DepsPass1Visitor readCdg(DepsPass1Visitor pass1, boolean validate) throws IOException {

		viewTree(pass1);

		log.info("");
		log.info("Vertices follow:");
		log.info("");
		logGraphElements(pass1.getCdgSupport().getVertices());
		log.info("");
		log.info("Edges follow:");
		log.info("");
		for (String cdgName : pass1.getCdgSupport().getCdgNames()) {
			log.info("\t{}:", cdgName);
			logGraphElements(pass1.getCdgSupport().getEdges(cdgName));
			log.info("");
		}
		log.info("Tokens follow:");
		log.info("");
		log.info("{}", pass1.getCdgSupport().getTokenStream().getTokens());
		log.info("");
		log.info("Tree follows:");
		log.info("");
		log.info("{}", pass1.getCdgSupport().getParseTree().toStringTree(pass1.getCdgSupport().getParser()));
		log.info("");

		if (validate) {
			assertCdg_1(pass1);
		}

		return pass1;
	}

	private static void assertCdg_1(DepsPass1Visitor pass1) {

		// Vertices follow:

		assertThat(pass1.getCdgSupport().getVertex("c1")).isExactlyInstanceOf(BusinessProcess.class)
				.hasFieldOrPropertyWithValue("grade", "high");
		assertThat(pass1.getCdgSupport().getVertex("g")).isExactlyInstanceOf(BusinessProcess.class)
				.hasFieldOrPropertyWithValue("grade", "critical");
		assertThat(pass1.getCdgSupport().getVertex("help_desk")).isExactlyInstanceOf(BusinessProcess.class)
				.hasFieldOrPropertyWithValue("grade", "low");
		assertThat(pass1.getCdgSupport().getVertex("log_requests")).isExactlyInstanceOf(BusinessProcess.class)
				.hasFieldOrPropertyWithValue("grade", "high");
		assertThat(pass1.getCdgSupport().getVertex("service_rollout")).isExactlyInstanceOf(BusinessProcess.class)
				.hasFieldOrPropertyWithValue("grade", "low");

		assertThat(pass1.getCdgSupport().getVertex("dns")).isExactlyInstanceOf(ITService.class)
				.hasFieldOrPropertyWithValue("grade", "critical");
		assertThat(pass1.getCdgSupport().getVertex("https")).isExactlyInstanceOf(ITService.class)
				.hasFieldOrPropertyWithValue("grade", "high");
		assertThat(pass1.getCdgSupport().getVertex("my_sql")).isExactlyInstanceOf(ITService.class)
				.hasFieldOrPropertyWithValue("grade", "critical");
		assertThat(pass1.getCdgSupport().getVertex("ssh")).isExactlyInstanceOf(ITService.class)
				.hasFieldOrPropertyWithValue("grade", "low");

		assertThat(pass1.getCdgSupport().getVertex("customer_info")).isExactlyInstanceOf(Information.class)
				.hasFieldOrPropertyWithValue("grade", "critical");
		assertThat(pass1.getCdgSupport().getVertex("traffic_level")).isExactlyInstanceOf(Information.class)
				.hasFieldOrPropertyWithValue("grade", "low");

		assertThat(pass1.getCdgSupport().getVertex("m1")).isExactlyInstanceOf(MissionObjective.class)
				.hasFieldOrPropertyWithValue("grade", "none");

		assertThat(pass1.getCdgSupport().getVertex("host1")).isExactlyInstanceOf(Node.class)
				.hasFieldOrPropertyWithValue("grade", "low");
		assertThat(pass1.getCdgSupport().getVertex("server1")).isExactlyInstanceOf(Node.class)
				.hasFieldOrPropertyWithValue("grade", "none");

		assertThat(pass1.getCdgSupport().getVertex("libc")).isExactlyInstanceOf(Software.class)
				.hasFieldOrPropertyWithValue("grade", "critical");
		assertThat(pass1.getCdgSupport().getVertex("mysqlsw")).isExactlyInstanceOf(Software.class)
				.hasFieldOrPropertyWithValue("grade", "critical");
		assertThat(pass1.getCdgSupport().getVertex("swlib2")).isExactlyInstanceOf(Software.class)
				.hasFieldOrPropertyWithValue("grade", "critical");

		// CDG Names follow:

		assertThat(pass1.getCdgSupport().getCdgNames()).containsAll(getCdgNames());

		log.info("CDG names follow:");
		for (String cdgName : pass1.getCdgSupport().getCdgNames()) {
			log.info("\t{}", cdgName);
		}
		log.info("");

		// Edges follow:

		for (String cdgName : getCdgNames()) {
			if (cdgName.startsWith("{{") && cdgName.endsWith("}}")) {
				continue;
			}
			assertThat(pass1.getCdgSupport().getEdge(cdgName, "customer_info-->my_sql"))
					.isExactlyInstanceOf(AccessedVia.class);

			if (cdgName.equals("cdg2")) {
				assertThat(pass1.getCdgSupport().getEdge(cdgName, "server1-->host1"))
						.isExactlyInstanceOf(ConnectedTo.class);
			}

			assertThat(pass1.getCdgSupport().getEdge(cdgName, "my_sql-->dns")).isExactlyInstanceOf(Uses.class);
			assertThat(pass1.getCdgSupport().getEdge(cdgName, "my_sql-->https")).isExactlyInstanceOf(Uses.class);

			if (cdgName.equals("cdg1")) { // TODO Should be RunsOn or ConnectedTo?
				assertThat(pass1.getCdgSupport().getEdge(cdgName, "server1-->host1"))
						.isExactlyInstanceOf(DependsOn.class);
			}

			assertThat(pass1.getCdgSupport().getEdge(cdgName, "help_desk-->log_requests"))
					.isExactlyInstanceOf(Contains.class);
			assertThat(pass1.getCdgSupport().getEdge(cdgName, "mysqlsw-->libc")).isExactlyInstanceOf(Uses.class);
			assertThat(pass1.getCdgSupport().getEdge(cdgName, "mysqlsw-->swlib2")).isExactlyInstanceOf(Uses.class);

			if (cdgName.equals("cdg3")) { // TODO Do not use DependsOn
				assertThat(pass1.getCdgSupport().getEdge(cdgName, "server1-->host1"))
						.isExactlyInstanceOf(DependsOn.class);
			}

			assertThat(pass1.getCdgSupport().getEdge(cdgName, "my_sql-->mysqlsw"))
					.isExactlyInstanceOf(RealisedAs.class);

			assertThat(pass1.getCdgSupport().getEdge(cdgName, "mysqlsw-->server1"))
					.isExactlyInstanceOf(DependsOn.class); // TODO Do not use DependsOn

			assertThat(pass1.getCdgSupport().getEdge(cdgName, "help_desk-->https")).isExactlyInstanceOf(Uses.class);
			assertThat(pass1.getCdgSupport().getEdge(cdgName, "help_desk-->ssh")).isExactlyInstanceOf(Uses.class);
			assertThat(pass1.getCdgSupport().getEdge(cdgName, "m1-->c1")).isExactlyInstanceOf(SupportedBy.class);
			assertThat(pass1.getCdgSupport().getEdge(cdgName, "m1-->customer_info"))
					.isExactlyInstanceOf(SupportedBy.class);
			assertThat(pass1.getCdgSupport().getEdge(cdgName, "m1-->g")).isExactlyInstanceOf(SupportedBy.class);
			assertThat(pass1.getCdgSupport().getEdge(cdgName, "m1-->help_desk")).isExactlyInstanceOf(SupportedBy.class);
			assertThat(pass1.getCdgSupport().getEdge(cdgName, "m1-->service_rollout"))
					.isExactlyInstanceOf(SupportedBy.class);
			assertThat(pass1.getCdgSupport().getEdge(cdgName, "m1-->traffic_level"))
					.isExactlyInstanceOf(SupportedBy.class);
		}

		// Totals follow:

		assertThat(pass1.getCdgSupport().getVertices()).hasSize(NUMBER_OF_VERTICES);
		assertThat(pass1.getCdgSupport().getEdges()).hasSize(NUMBER_OF_EDGES);
	}

	public static DepsPass1Visitor readCdg(Map<String, Vertex> vertices,
			Map<String, Map<Pair<String, String>, Edge>> edges, File file, boolean validate, boolean guessEdgeType,
			Map<String, Double> missionWeights, Map<String, Double> vertexWeights, Map<String, Double> edgeWeights)
			throws Exception {
		return readCdg(new DepsPass1Visitor(vertices, edges, file.getPath(), file, guessEdgeType, missionWeights,
				vertexWeights, edgeWeights), validate);
	}

	public static DepsPass1Visitor readCdg(MongoDB mongoDB, File file, boolean validate, boolean guessEdgeType,
			Map<String, Double> missionWeights, Map<String, Double> vertexWeights, Map<String, Double> edgeWeights)
			throws Exception {
		return readCdg(new DepsPass1Visitor(mongoDB, file.getPath(), file, guessEdgeType, missionWeights, vertexWeights,
				edgeWeights), validate);
	}

	@BeforeEach
	public void setUp() {
		missionWeights.put("none", 0.0d);
		missionWeights.put("low", 1.0d);
		missionWeights.put("medium", 2.0d);
		missionWeights.put("high", 3.0d);
		missionWeights.put("critical", 4.0d);

		vertexWeights.put("none", 0.0d);
		vertexWeights.put("low", 1.0d);
		vertexWeights.put("medium", 2.0d);
		vertexWeights.put("high", 3.0d);
		vertexWeights.put("critical", 4.0d);

		edgeWeights.put("none", 0.0d);
		edgeWeights.put("low", 1.0d);
		edgeWeights.put("medium", 2.0d);
		edgeWeights.put("high", 3.0d);
		edgeWeights.put("critical", 4.0d);
	}

	@Test
	public void readCdg() throws Exception {
		org.reflections.Reflections.log = null;
		Map<String, Vertex> vertices = new HashMap<>();
		Map<String, Map<Pair<String, String>, Edge>> edges = new HashMap<>();
		File file = new File("src/test/resources/antlr/cdg_1.deps");
		DepsPass1Visitor pass1 = readCdg(vertices, edges, file, false, true, missionWeights, vertexWeights,
				edgeWeights);
		assertThat(pass1.getCdgSupport().getVertices()).hasSize(NUMBER_OF_VERTICES);
		assertThat(pass1.getCdgSupport().getEdges()).hasSize(NUMBER_OF_EDGES);
	}
	
	@Test
	public void readCdgWithContext() throws Exception {
		org.reflections.Reflections.log = null;
		Map<String, Vertex> vertices = new HashMap<>();
		Map<String, Map<Pair<String, String>, Edge>> edges = new HashMap<>();
		File file = new File("src/test/resources/antlr/cdg_2.deps");
		DepsPass1Visitor pass1 = readCdg(vertices, edges, file, false, true, missionWeights, vertexWeights,
				edgeWeights);
		assertThat(pass1.getCdgSupport().getContext()).isEqualTo("PSNC");
		assertThat(pass1.getCdgSupport().getVertices()).hasSize(NUMBER_OF_VERTICES);
		assertThat(pass1.getCdgSupport().getEdges()).hasSize(NUMBER_OF_EDGES);
	}

	@Test
	public void readPsncCdg() throws Exception {
		org.reflections.Reflections.log = null;
		Map<String, Vertex> vertices = new HashMap<>();
		Map<String, Map<Pair<String, String>, Edge>> edges = new HashMap<>();
		File file = new File("src/test/resources/antlr/psnc_vc.deps");
		DepsPass1Visitor pass1 = readCdg(vertices, edges, file, false, true, missionWeights, vertexWeights,
				edgeWeights);
		assertThat(pass1.getCdgSupport().getVertices()).hasSize(139);
		assertThat(pass1.getCdgSupport().getEdges()).hasSize(279);
	}

	@Test
	public void readPsncV7Cdg() throws Exception {
		org.reflections.Reflections.log = null;
		Map<String, Vertex> vertices = new HashMap<>();
		Map<String, Map<Pair<String, String>, Edge>> edges = new HashMap<>();
		File file = new File("src/test/resources/antlr/psnc_vc_v7.deps");
		DepsPass1Visitor pass1 = readCdg(vertices, edges, file, false, true, missionWeights, vertexWeights,
				edgeWeights);
		assertThat(pass1.getCdgSupport().getVertices()).hasSize(177);
		assertThat(pass1.getCdgSupport().getEdges()).hasSize(518);
	}

	@Test
	public void readPsncV7LayersCdg() throws Exception {
		org.reflections.Reflections.log = null;
		Map<String, Vertex> vertices = new HashMap<>();
		Map<String, Map<Pair<String, String>, Edge>> edges = new HashMap<>();
		File file = new File("src/test/resources/antlr/psnc_vc_v7.layers.deps");
		DepsPass1Visitor pass1 = readCdg(vertices, edges, file, false, true, missionWeights, vertexWeights,
				edgeWeights);
		assertThat(pass1.getCdgSupport().getVertices()).hasSize(176); // 1 less than V7
		assertThat(pass1.getCdgSupport().getEdges()).hasSize(517); // 1 less than V7
	}

}
