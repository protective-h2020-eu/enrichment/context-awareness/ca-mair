package eu.h2020.protective.ca.mair;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import eu.h2020.protective.ca.mair.doc.DataClassesDoc;
import eu.h2020.protective.ca.mair.test.support.TestTags;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:application-test.properties")
@Tag(TestTags.SLOW)
public class DocTests {

	@Test
	public void createModelDoc() throws Exception {
		DataClassesDoc doc = new DataClassesDoc();
		doc.createDocForModel();
	}
	
}
