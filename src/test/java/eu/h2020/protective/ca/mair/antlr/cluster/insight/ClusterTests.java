package eu.h2020.protective.ca.mair.antlr.cluster.insight;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.antlr.v4.gui.TreeViewer;
import org.antlr.v4.runtime.misc.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import eu.h2020.protective.ca.mair.MongoDB;
import eu.h2020.protective.ca.mair.api.model.core.Edge;
import eu.h2020.protective.ca.mair.api.model.core.GraphElement;
import eu.h2020.protective.ca.mair.api.model.core.Vertex;
import eu.h2020.protective.ca.mair.test.support.TestTags;
import lombok.extern.slf4j.XSlf4j;

@XSlf4j
@Tag(TestTags.FAST)
@Tag(TestTags.PARALLEL)
public class ClusterTests {

	private static boolean viewTree = false;

	public static final int NUMBER_OF_VERTICES = 38;
	public static final int NUMBER_OF_EDGES = 45;

	private Map<String, Double> missionWeights = new HashMap<>();
	private Map<String, Double> vertexWeights = new HashMap<>();
	private Map<String, Double> edgeWeights = new HashMap<>();

	private static void logGraphElements(Iterable<? extends GraphElement> iterable) {
		List<GraphElement> list = new ArrayList<>();
		iterable.forEach(list::add);
		list.sort(Comparator.comparing(GraphElement::getSimpleType).thenComparing(GraphElement::getKey));
		for (GraphElement element : list) {
			if (element instanceof Vertex) {
				log.info("{} = {}", element.getSimpleType(), element.getKey());
			}
			if (element instanceof Edge) {
				log.info("{} = {}, '{}'", element.getSimpleType(), element.getKey(), ((Edge) element).getGrade());
			}
		}
	}

	private static void viewTree(ClusterPass1Visitor pass1) {
		if (viewTree) {
			String[] ruleNamesArray = pass1.getCdgSupport().getParser().getRuleNames();
			List<String> ruleNamesList = new ArrayList<>();
			Collections.addAll(ruleNamesList, ruleNamesArray);

			TreeViewer viewr = new TreeViewer(ruleNamesList, pass1.getCdgSupport().getParseTree());
			viewr.open();
		}
	}

	private static void readCdg(ClusterPass1Visitor pass1) throws IOException {

		viewTree(pass1);

		log.info("");
		log.info("Vertices follow:");
		log.info("");
		logGraphElements(pass1.getCdgSupport().getVertices());
		log.info("");
		log.info("Edges follow:");
		log.info("");
		for (String cdgName : pass1.getCdgSupport().getCdgNames()) {
			log.info("\t{}:", cdgName);
			logGraphElements(pass1.getCdgSupport().getEdges(cdgName));
			log.info("");
		}
//		log.info("Tokens follow:");
//		log.info("");
//		log.info("{}", pass1.getTokenStream().getTokens());
//		log.info("");
		log.info("Tree follows:");
		log.info("");
		log.info("{}", pass1.getCdgSupport().getParseTree().toStringTree(pass1.getCdgSupport().getParser()));
		log.info("");
	}

	public static void readCdg(Map<String, Vertex> vertices, Map<String, Map<Pair<String, String>, Edge>> edges,
			File file, boolean guessEdgeType, Map<String, Double> missionWeights, Map<String, Double> vertexWeights,
			Map<String, Double> edgeWeights) throws Exception {
		readCdg(new ClusterPass1Visitor(vertices, edges, file.getPath(), file, guessEdgeType, missionWeights,
				vertexWeights, edgeWeights));
	}

	public static void readCdg(MongoDB mongoDB, File file, boolean guessEdgeType, Map<String, Double> missionWeights,
			Map<String, Double> vertexWeights, Map<String, Double> edgeWeights) throws Exception {
		readCdg(new ClusterPass1Visitor(mongoDB, file.getPath(), file, guessEdgeType, missionWeights, vertexWeights,
				edgeWeights));
	}

	@BeforeEach
	public void setUp() {
		missionWeights.put("none", 0.0d);
		missionWeights.put("low", 1.0d);
		missionWeights.put("medium", 2.0d);
		missionWeights.put("high", 3.0d);
		missionWeights.put("critical", 4.0d);

		vertexWeights.put("none", 0.0d);
		vertexWeights.put("low", 1.0d);
		vertexWeights.put("medium", 2.0d);
		vertexWeights.put("high", 3.0d);
		vertexWeights.put("critical", 4.0d);

		edgeWeights.put("none", 0.0d);
		edgeWeights.put("low", 1.0d);
		edgeWeights.put("medium", 2.0d);
		edgeWeights.put("high", 3.0d);
		edgeWeights.put("critical", 4.0d);
	}

	@Test
	public void readCdg() throws Exception {
		org.reflections.Reflections.log = null;
		Map<String, Vertex> vertices = new HashMap<>();
		Map<String, Map<Pair<String, String>, Edge>> edges = new HashMap<>();
		File file = new File("src/test/resources/cluster-insight/cluster.json");
		readCdg(vertices, edges, file, true, missionWeights, vertexWeights, edgeWeights);
	}

}
