package eu.h2020.protective.ca.mair;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Duration;
import java.time.Instant;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import eu.h2020.protective.ca.mair.api.model.core.Graph;
import eu.h2020.protective.ca.mair.model.Metadata;
import eu.h2020.protective.ca.mair.test.support.MockMvcBase;
import eu.h2020.protective.ca.mair.test.support.TestTags;
import lombok.extern.slf4j.XSlf4j;

@Tag(TestTags.MONGO)
@XSlf4j
public class LoadApiTests extends MockMvcBase {

	@Autowired
	private MongoDB mongoDB;

	@Autowired
	private MongoDBQueries mongoDBQueries;

	@Autowired
	private MongoDBInit mongoDBInit;

	@Autowired
	private MongoDBLoad mongoDBLoad;

	@BeforeEach
	public void setUpDB() throws Exception {
		mongoDB.clearAllMatricesAndGraphs(Metadata.DEFAULT_CONTEXT);
	}

	@Test
	public void loadGraph() throws Exception {
		Graph graph = mongoDBLoad.createLargeGraph(1_000);
		int graphSize = graph.getVertices().size() + graph.getEdges().size();
		assertThat(graphSize).isEqualTo(7 * 1_000);
	}

	@Test
	public void loadIdgGraph() throws Exception {
		int load = 4;// 1_000; // TODO use some way to alter this dynamically, e.g. env var.

		String suffix = "-" + load / 2;
		Set<String> keys = null;

		Instant startLoad = Instant.now();

		mongoDBInit.initImpactDependentGraph(0, load);

		Instant startQueries = Instant.now();

		keys = mongoDBQueries.getMissionsImpactedByAsset(Metadata.DEFAULT_CONTEXT, "A1" + suffix);
		assertThat(keys).containsExactlyInAnyOrder("Mission1" + suffix);

		keys = mongoDBQueries.getMissionsImpactedByAsset(Metadata.DEFAULT_CONTEXT, "A2" + suffix);
		assertThat(keys).containsExactlyInAnyOrder("Mission1" + suffix, "Mission2" + suffix);

		keys = mongoDBQueries.getMissionsImpactedByAsset(Metadata.DEFAULT_CONTEXT, "A3" + suffix);
		assertThat(keys).containsExactlyInAnyOrder("Mission2" + suffix);

		keys = mongoDBQueries.getMissionsImpactedByAsset(Metadata.DEFAULT_CONTEXT, "A4" + suffix);
		assertThat(keys).containsExactlyInAnyOrder("Mission1" + suffix, "Mission2" + suffix);

		keys = mongoDBQueries.getMissionsImpactedByAsset(Metadata.DEFAULT_CONTEXT, "A5" + suffix);
		assertThat(keys).containsExactlyInAnyOrder("Mission2" + suffix);

		Instant endQueries = Instant.now();

		Duration loadTime = Duration.between(startLoad, startQueries);
		Duration queriesTime = Duration.between(startQueries, endQueries);

		log.info("Load Duration is {} seconds.", loadTime.getSeconds());
		log.info("Queries Duration is {} seconds.", queriesTime.getSeconds());

		assertThat(queriesTime.getSeconds()).isEqualTo(0);
	}

}
