package eu.h2020.protective.ca.mair.antlr.cluster.insight;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import eu.h2020.protective.ca.mair.MongoDB;
import eu.h2020.protective.ca.mair.api.model.core.Edge;
import eu.h2020.protective.ca.mair.api.model.core.Graph;
import eu.h2020.protective.ca.mair.model.Metadata;
import eu.h2020.protective.ca.mair.test.support.TestTags;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { MongoDB.class })
@TestPropertySource(locations = "classpath:application-test.properties")
@Tag(TestTags.SLOW)
@Tag(TestTags.MONGO)
public class ClusterDBTests {

	@Autowired
	private MongoDB mongoDB;

	private Map<String, Double> missionWeights = new HashMap<>();
	private Map<String, Double> vertexWeights = new HashMap<>();
	private Map<String, Double> edgeWeights = new HashMap<>();

	@BeforeEach
	public void setUp() {
		mongoDB.clearAllMatricesAndGraphs(Metadata.DEFAULT_CONTEXT);

		missionWeights.put("none", 0.0d);
		missionWeights.put("low", 1.0d);
		missionWeights.put("medium", 2.0d);
		missionWeights.put("high", 3.0d);
		missionWeights.put("critical", 4.0d);

		vertexWeights.put("none", 0.0d);
		vertexWeights.put("low", 1.0d);
		vertexWeights.put("medium", 2.0d);
		vertexWeights.put("high", 3.0d);
		vertexWeights.put("critical", 4.0d);

		edgeWeights.put("none", 0.0d);
		edgeWeights.put("low", 1.0d);
		edgeWeights.put("medium", 2.0d);
		edgeWeights.put("high", 3.0d);
		edgeWeights.put("critical", 4.0d);
	}

	@Test
	public void readCdg() throws Exception {
		org.reflections.Reflections.log = null;
		File file = new File("src/test/resources/cluster-insight/cluster.json");
		ClusterTests.readCdg(mongoDB, file, true, missionWeights, vertexWeights, edgeWeights);
	}

	@Test
	public void getGraphByAllEdgesCdg() throws Exception {
		readCdg();
		Graph graph = mongoDB.getGraphByCdg(Metadata.DEFAULT_CONTEXT, Edge.ALL_EDGES_CDG);
		assertThat(graph.getVertices()).hasSize(38);
		assertThat(graph.getEdges()).hasSize(45);
	}

	// @Test
	// public void getGraphByCdg() throws Exception {
	// readCdg();
	// Graph graph = mongoDB.getGraphByCdg(Edge.ALL_EDGES_CDG);
	// assertThat(graph.getVertices()).hasSize(38);
	// assertThat(graph.getEdges()).hasSize(45);
	// }

}
