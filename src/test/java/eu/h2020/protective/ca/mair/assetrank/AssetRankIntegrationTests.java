package eu.h2020.protective.ca.mair.assetrank;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.jgrapht.alg.interfaces.VertexScoringAlgorithm;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import eu.h2020.protective.ca.mair.CaAsClient;
import eu.h2020.protective.ca.mair.ConfigurationService;
import eu.h2020.protective.ca.mair.MongoDB;
import eu.h2020.protective.ca.mair.SyncGraph;
import eu.h2020.protective.ca.mair.antlr.json.CdgPass1Visitor;
import eu.h2020.protective.ca.mair.antlr.json.CdgTests;
import eu.h2020.protective.ca.mair.api.model.core.Edge;
import eu.h2020.protective.ca.mair.api.model.core.GVertex;
import eu.h2020.protective.ca.mair.api.model.core.Graph;
import eu.h2020.protective.ca.mair.api.model.core.Vertex;
import eu.h2020.protective.ca.mair.api.model2.other.MissionObjective;
import eu.h2020.protective.ca.mair.model.Metadata;
import eu.h2020.protective.ca.mair.test.support.TestTags;
import lombok.extern.slf4j.XSlf4j;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { MongoDB.class, SyncGraph.class, CaAsClient.class, ConfigurationService.class })
@TestPropertySource(locations = "classpath:application-test.properties")
@Tag(TestTags.SLOW)
@Tag(TestTags.MONGO)
@XSlf4j
@AutoConfigureWebClient
public class AssetRankIntegrationTests {

	@Autowired
	private MongoDB mongoDB;

	@Autowired
	private SyncGraph syncGraph;

	private Map<String, Double> missionWeights = new HashMap<>();
	private Map<String, Double> vertexWeights = new HashMap<>();
	private Map<String, Double> edgeWeights = new HashMap<>();

	@BeforeEach
	public void setUp() {
		mongoDB.clearAllMatricesAndGraphs(Metadata.DEFAULT_CONTEXT);

// Original:		
//
//		missionWeights.put("none", 0.0d);
//		missionWeights.put("low", 1.0d);
//		missionWeights.put("medium", 2.0d);
//		missionWeights.put("high", 3.0d);
//		missionWeights.put("critical", 4.0d);
//
//		vertexWeights.put("none", 0.0d);
//		vertexWeights.put("low", 1.0d);
//		vertexWeights.put("medium", 2.0d);
//		vertexWeights.put("high", 3.0d);
//		vertexWeights.put("critical", 4.0d);
//
//		edgeWeights.put("none", 0.0d);
//		edgeWeights.put("low", 1.0d);
//		edgeWeights.put("medium", 2.0d);
//		edgeWeights.put("high", 3.0d);
//		edgeWeights.put("critical", 4.0d);

// Eoin/Brian:
//
		missionWeights.put("none", 0.0d);
		missionWeights.put("low", 1.0d);
		missionWeights.put("medium", 3.0d);
		missionWeights.put("high", 7.0d);
		missionWeights.put("critical", 10.0d);

		vertexWeights.put("none", 1.0d);
		vertexWeights.put("low", 1.0d);
		vertexWeights.put("medium", 1.0d);
		vertexWeights.put("high", 1.0d);
		vertexWeights.put("critical", 1.0d);

		edgeWeights.put("none", 0.0d);
		edgeWeights.put("low", 1.0d);
		edgeWeights.put("medium", 2.0d);
		edgeWeights.put("high", 3.0d);
		edgeWeights.put("critical", 4.0d);

// Franklin:		
//
//		missionWeights.put("none", 0.3d);
//		missionWeights.put("low", 0.3d);
//		missionWeights.put("medium", 0.3d);
//		missionWeights.put("high", 0.7d);
//		missionWeights.put("critical", 0.7d);
//
//		vertexWeights.put("none", 1.0d);
//		vertexWeights.put("low", 1.0d);
//		vertexWeights.put("medium", 1.0d);
//		vertexWeights.put("high", 1.0d);
//		vertexWeights.put("critical", 1.0d);
//
//		edgeWeights.put("none", 0.1d);
//		edgeWeights.put("low", 0.2d);
//		edgeWeights.put("medium", 0.5d);
//		edgeWeights.put("high", 0.8d);
//		edgeWeights.put("critical", 1.0d);

	}

	private void logScores(VertexScoringAlgorithm<GVertex, Double> algorithm) {
		Map<GVertex, Double> sortedScores = algorithm.getScores().entrySet().stream()
				.sorted(Map.Entry.comparingByValue(Comparator.reverseOrder())).collect(Collectors.toMap(
						Map.Entry::getKey, Map.Entry::getValue, (oldValue, newValue) -> oldValue, LinkedHashMap::new));
		double totalScore = 0.0d;
		for (Entry<GVertex, Double> entry : sortedScores.entrySet()) {
			double score = entry.getValue().doubleValue() * 100;
			System.out.printf("%10.3f = %s \n", score, entry.getKey().getKey());
			totalScore += score;
		}
		System.out.printf("%10.3f = Total Score \n", totalScore);
	}

	private Graph createBasicOrGraphIntraLayerDependencies(boolean reverse) throws Exception {
		Graph graph = new Graph();
		graph.setContext(Metadata.DEFAULT_CONTEXT);
		Vertex p1 = new MissionObjective("p1");
		Vertex p2 = new Vertex("p2");
		Vertex p3 = new Vertex("p3");
		Vertex v1 = new Vertex("v1");
		Vertex v2 = new Vertex("v2");
		Vertex v3 = new Vertex("v3");
		Vertex v4 = new Vertex("v4");
		Edge p1p2 = new Edge(p1, p2);
		p1p2.setFromCdg("missionLayer");
		p1p2.setToCdg("applicationLayer");
		Edge p1p3 = new Edge(p1, p3);
		p1p3.setFromCdg("missionLayer");
		p1p3.setToCdg("applicationLayer");
		Edge p2v1 = new Edge(p2, v1);
		p2v1.setFromCdg("applicationLayer");
		p2v1.setToCdg("applicationLayer");
		Edge p2v2 = new Edge(p2, v2);
		p2v2.setFromCdg("applicationLayer");
		p2v2.setToCdg("applicationLayer");
		Edge p3v3 = new Edge(p3, v3);
		p3v3.setFromCdg("applicationLayer");
		p3v3.setToCdg("applicationLayer");
		Edge p3v4 = new Edge(p3, v4);
		p3v4.setFromCdg("applicationLayer");
		p3v4.setToCdg("applicationLayer");

		addEdge(graph, p1, p2, p1p2, reverse);
		addEdge(graph, p1, p3, p1p3, reverse);
		addEdge(graph, p2, v1, p2v1, reverse);
		addEdge(graph, p2, v2, p2v2, reverse);
		addEdge(graph, p3, v3, p3v3, reverse);
		addEdge(graph, p3, v4, p3v4, reverse);
		return graph;
	}

	private Graph createBasicOrGraphNoIntraLayerDependencies(boolean reverse) throws Exception {
		Graph graph = new Graph();
		graph.setContext(Metadata.DEFAULT_CONTEXT);
		Vertex p1 = new MissionObjective("p1");
		Vertex p2 = new Vertex("p2");
		Vertex p3 = new Vertex("p3");
		Vertex v1 = new Vertex("v1");
		Vertex v2 = new Vertex("v2");
		Vertex v3 = new Vertex("v3");
		Vertex v4 = new Vertex("v4");
		Edge p1p2 = new Edge(p1, p2);
		p1p2.setFromCdg("missionLayer");
		p1p2.setToCdg("applicationLayer");
		Edge p1p3 = new Edge(p1, p3);
		p1p3.setFromCdg("missionLayer");
		p1p3.setToCdg("applicationLayer");
		Edge p2v1 = new Edge(p2, v1);
		p2v1.setFromCdg("applicationLayer");
		p2v1.setToCdg("infrastructureLayer");
		Edge p2v2 = new Edge(p2, v2);
		p2v2.setFromCdg("applicationLayer");
		p2v2.setToCdg("infrastructureLayer");
		Edge p3v3 = new Edge(p3, v3);
		p3v3.setFromCdg("applicationLayer");
		p3v3.setToCdg("infrastructureLayer");
		Edge p3v4 = new Edge(p3, v4);
		p3v4.setFromCdg("applicationLayer");
		p3v4.setToCdg("infrastructureLayer");

		addEdge(graph, p1, p2, p1p2, reverse);
		addEdge(graph, p1, p3, p1p3, reverse);
		addEdge(graph, p2, v1, p2v1, reverse);
		addEdge(graph, p2, v2, p2v2, reverse);
		addEdge(graph, p3, v3, p3v3, reverse);
		addEdge(graph, p3, v4, p3v4, reverse);

		return graph;
	}

	private void addEdge(Graph g, Vertex sourceVertex, Vertex targetVertex, Edge e, boolean reverse) {
		g.addVertex(sourceVertex);
		g.addVertex(targetVertex);
		if (reverse) {
			e = e.changeDirection();
			g.addEdge(targetVertex, sourceVertex, e);
		} else {
			g.addEdge(sourceVertex, targetVertex, e);
		}
	}

	@Test
	public void runPsncAllEdgesOrVerticesAssetRank() throws Exception {
		org.reflections.Reflections.log = null;
		File file1 = new File("src/test/resources/antlr/psnc_vc.cdgs");
//		File file2 = new File("src/test/resources/antlr/psnc_vc.with.ip.cdgv");

		CdgPass1Visitor pass1Cdgs = CdgTests.readCdg(mongoDB, file1, null, false, true, missionWeights, vertexWeights,
				edgeWeights);
//		CdgPass1Visitor pass1Cdgv = CdgTests.readCdg(mongoDB, file2, null, false, true, missionWeights, vertexWeights,
//				edgeWeights);

		assertThat(pass1Cdgs.getCdgSupport().getVertices()).hasSize(176);
		assertThat(pass1Cdgs.getCdgSupport().getEdges()).hasSize(517);

//		assertThat(pass1Cdgv.getCdgSupport().getVertices()).hasSize(176);
//		assertThat(pass1Cdgv.getCdgSupport().getEdges()).hasSize(517);

		Graph g = syncGraph.getGraph(Metadata.DEFAULT_CONTEXT);
		int sinkCount = 0;
		int orCount = 0;
		int missionCount = 0;
		for (GVertex v : g.vertexSet()) {
			if (g.outDegreeOf(v) == 0) {
				v.getVertex().setExpression(Vertex.SINK);
				sinkCount++; // FIXME Why not being executed?
			} else {
				v.getVertex().setExpression(Vertex.OR);
				orCount++;
			}
			if (v.getVertex() instanceof MissionObjective) {
				log.info("mission = {}, weight = {}", v.getVertex().getKey(), v.getVertex().getWeight());
				missionCount++;
			}
		}
		syncGraph.addOrUpdateGraph(g);

		log.info("sinkCount    = {}", sinkCount);
		log.info("orCount      = {}", orCount);
		log.info("missionCount = {}", missionCount);
		log.info("edgesCount   = {}", g.getEdges().size());
		log.info("edgeSetCount = {}", g.edgeSet().size());

		assertThat(sinkCount).isEqualTo(0); // TODO change
		assertThat(orCount).isEqualTo(176); // TODO change
		assertThat(missionCount).isEqualTo(2);
		assertThat(g.getVertices()).hasSize(176);
		assertThat(g.getEdges()).hasSize(517);

		logScores(new AssetRank(g));
	}

	@Test
	public void runPsncAllEdgesOrVerticesAssetRankRoundTrip() throws Exception {
		org.reflections.Reflections.log = null;
		File file1 = new File("src/test/resources/antlr/psnc_vc.cdgs");
		File file2 = new File("src/test/resources/antlr/psnc_vc.with.ip.cdgv");
		CdgTests.readCdg(mongoDB, file1, null, false, true, missionWeights, vertexWeights, edgeWeights);
		CdgTests.readCdg(mongoDB, file2, null, false, true, missionWeights, vertexWeights, edgeWeights);

		Graph g = syncGraph.getGraph(Metadata.DEFAULT_CONTEXT);
		int sinkCount = 0;
		int orCount = 0;
		int missionCount = 0;
		for (GVertex v : g.vertexSet()) {
			if (g.outDegreeOf(v) == 0) {
				v.getVertex().setExpression(Vertex.SINK);
				sinkCount++;
			} else {
				v.getVertex().setExpression(Vertex.OR);
				orCount++;
			}
			if (v.getVertex() instanceof MissionObjective) {
				v.getVertex().setWeight(1.0d);
				missionCount++;
			}
		}
		syncGraph.addOrUpdateGraph(g);
		g = syncGraph.getGraph(Metadata.DEFAULT_CONTEXT);
		log.info("sinkCount    = {}", sinkCount);
		log.info("orCount      = {}", orCount);
		log.info("missionCount = {}", missionCount);
		log.info("edgesCount   = {}", g.getEdges().size());
		log.info("edgeSetCount = {}", g.edgeSet().size());
		logScores(new AssetRank(g));
	}

	@Test
	public void runBasicOrMairRankIntra() throws Exception {
		Graph graph = createBasicOrGraphIntraLayerDependencies(false);
		syncGraph.addOrUpdateGraph(graph);
		logScores(new MairRank(Metadata.DEFAULT_CONTEXT, mongoDB));
	}

	@Test
	public void runBasicOrMairRankInter() throws Exception {
		Graph graph = createBasicOrGraphNoIntraLayerDependencies(false);
		syncGraph.addOrUpdateGraph(graph);
		logScores(new MairRank(Metadata.DEFAULT_CONTEXT, mongoDB));
	}

	@Test
	public void runPsncMultiLayerMairRankMongoDB() throws Exception {
		org.reflections.Reflections.log = null;
		File file1 = new File("src/test/resources/antlr/psnc_vc.cdgs");
		File file2 = new File("src/test/resources/antlr/psnc_vc.with.ip.cdgv");
		CdgTests.readCdg(mongoDB, file1, null, false, true, missionWeights, vertexWeights, edgeWeights);
		CdgTests.readCdg(mongoDB, file2, null, false, true, missionWeights, vertexWeights, edgeWeights);
		logScores(new MairRank(Metadata.DEFAULT_CONTEXT, mongoDB));
	}

	@Test
	public void runPsncMultiLayerMairRankSyncGraph() throws Exception {
		org.reflections.Reflections.log = null;
		File file1 = new File("src/test/resources/antlr/psnc_vc.cdgs");
		File file2 = new File("src/test/resources/antlr/psnc_vc.with.ip.cdgv");
		CdgTests.readCdg(mongoDB, file1, null, false, true, missionWeights, vertexWeights, edgeWeights);
		CdgTests.readCdg(mongoDB, file2, null, false, true, missionWeights, vertexWeights, edgeWeights);

		Graph g = syncGraph.getGraph(Metadata.DEFAULT_CONTEXT);
		for (Vertex v : g.getVertices()) {
			if (v instanceof MissionObjective) {
				v.setWeight(1.0d);
			}
			v.setExpression(Vertex.OR);
		}
		syncGraph.addOrUpdateGraph(g);

		logScores(new MairRank(Metadata.DEFAULT_CONTEXT, mongoDB));
	}

	@Test
	public void invalidGraph() throws Exception {
		try {
			org.reflections.Reflections.log = null;
			File file1 = new File("src/test/resources/antlr/psnc_vc.cdgs");
			CdgTests.readCdg(mongoDB, file1, null, false, true, missionWeights, vertexWeights, edgeWeights);
			Graph g = syncGraph.getGraph(Metadata.DEFAULT_CONTEXT);
			for (GVertex v : g.vertexSet()) {
				if (g.outDegreeOf(v) == 0) {
					v.getVertex().setExpression(Vertex.SINK);
				} else {
					v.getVertex().setExpression(Vertex.OR);
				}
				if (v.getVertex() instanceof MissionObjective) {
					v.getVertex().setWeight(1.0d);
				}
			}
			new AssetRank(g);
		} catch (IllegalArgumentException e) {
			assertThat(e.getMessage()).isEqualTo("Graph not valid");
		}
	}

}
