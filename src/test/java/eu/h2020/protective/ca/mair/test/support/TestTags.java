package eu.h2020.protective.ca.mair.test.support;

public class TestTags {
	
	public static final String FAST = "fast";
	public static final String SLOW = "slow";
	public static final String MONGO = "mongo";
	public static final String PARALLEL = "parallel";
	
}
